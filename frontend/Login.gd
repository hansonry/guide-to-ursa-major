extends Control


# Declare member variables here. Examples:
# var a = 2
# var b = "text"
var connection
var state
var usernameEdit : LineEdit
var passwordEdit : LineEdit
var statusLabel  : Label
var loginButton  : Button

# Called when the node enters the scene tree for the first time.
func _ready():
	state = 'wating_for_input'
	usernameEdit = get_node(@"VBoxContainer/MarginContainer/Grid/edit_username")
	passwordEdit = get_node(@"VBoxContainer/MarginContainer/Grid/edit_password")
	statusLabel  = get_node(@"VBoxContainer/label_status")
	loginButton  = get_node(@"VBoxContainer/button_login")
	connection   = get_node(@"/root/Connection")
	connection.connect("client_connection_state_change", self, "_client_connection_state_change")
	connection.connect("host_connection_state_change",   self, "_host_connection_state_change")
	connection.connect("message_received",               self, "_message_received")
	_set_status_to_disconnected()
	connection.message_hold = false


func _client_connection_state_change(_status):
	pass

func _host_connection_state_change(status):
	if status == "connected" && state == 'wating_to_login':
		_set_status_to_logging_in()
		# TODO: Hash and salt password. Plaintext is bad
		var msg = {"type":         ClientMessage.MessageType.AuthRequest,
				   "requestType":  ClientMessage.AuthRequestType.LogIn,
				   "userName":     usernameEdit.text,
				   "userPassword": passwordEdit.text}
		Connection.send_message(msg)
		state = 'waiting_for_login_response'
		
func _message_received(msg):
	match msg.type:
		ClientMessage.MessageType.AuthResponse:
			_authResponse_received(msg);
		_:
			assert(false, "Unhanlded Message: %d" % msg.type)


func _authResponse_received(msg):
	match msg.requestType:
		ClientMessage.AuthRequestType.LogIn:
			if state == 'waiting_for_login_response':
				if msg.success:
					connection.message_hold = true
					assert(get_tree().change_scene("res://CommandView.tscn") == OK)
				else:
					connection.disconnect_from_host()
					_set_status_to_invalid_username_or_password()
					state = 'wating_for_input'

func _set_status_to_connecting():
	statusLabel.visible = true
	statusLabel.text = "Connecting..."
	loginButton.disabled = true;
	
func _set_status_to_disconnected():
	statusLabel.visible = false
	statusLabel.text = ""
	loginButton.disabled = false

func _set_status_to_logging_in():
	statusLabel.visible = true
	statusLabel.text = "Loggin In..."
	loginButton.disabled = true;

func _set_status_to_failed_to_connect():
	statusLabel.visible = true
	statusLabel.text = "Host not found!"
	loginButton.disabled = false

func _set_status_to_unknwon_connection_error():
	statusLabel.visible = true
	statusLabel.text = "Unkown Error!"
	loginButton.disabled = false


func _set_status_to_invalid_username_or_password():
	statusLabel.visible = true
	statusLabel.text = "Invalid Username or Password"
	loginButton.disabled = false


# Called every frame. 'delta' is the elapsed time since the previous frame.
#func _process(delta):
#	pass

func login_failed():
	pass

func login_success():
	pass

func _try_to_login():
	var result = connection.connect_to_host("localhost", "12233")
	
	if result:
		_set_status_to_connecting()
		state = 'wating_to_login'
	else:
		_set_status_to_unknwon_connection_error()

func _on_button_login_pressed():
	_try_to_login()


func _on_edit_password_text_entered(_new_text):
	_try_to_login()
