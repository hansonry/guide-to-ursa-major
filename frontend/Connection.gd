extends Node

# Declare member variables here. Examples:
# var a = 2
# var b = "text"
const MESSAGE_HEADER_BYTES : int = 2

var _peer : StreamPeerTCP = StreamPeerTCP.new()
var _send_buffer : StreamPeerBuffer = StreamPeerBuffer.new()
var _receive_buffer : StreamPeerBuffer = StreamPeerBuffer.new()
var _bytesNeeded : int
var _readingHeaderFlag : bool
var _prevStatus
var keep_connected : bool = true
var message_hold : bool = false
var _connection_status : String = "disconnected"
var stop_client_on_close : bool = true


# Declare Signals
signal client_connection_state_change(status)
signal host_connection_state_change(status)
signal message_received(msg)

# Called when the node enters the scene tree for the first time.
func _ready():
	_readingHeaderFlag = true
	_bytesNeeded = MESSAGE_HEADER_BYTES
	_prevStatus = StreamPeerTCP.STATUS_NONE
	message_hold = false
	keep_connected = true
	_connection_status = "unknown"

func put_login_request(username, password):
	var msg = {"type":         ClientMessage.MessageType.AuthRequest,
			   "requestType":  ClientMessage.AuthRequestType.LogIn,
			   "userName":     username,
			   "userPassword": password}
	send_message(msg)

func get_status() -> String:
	return _connection_status
	
func is_connected_to_host():
	return _peer.is_connected_to_host()

func _attempt_to_connect_to_client_host():
	var address = "localhost"
	var port = Config.ClientHostPort
	print("Attempting to connect to Client Host at address %s:%s" % [address, port])
	var result = _peer.connect_to_host(address, port)
	if result != OK:
		print("Error Initalizing Connection To Client Host")
		return false
	return true

func send_message(msg : Dictionary):
	if _peer.get_status() != StreamPeerTCP.STATUS_CONNECTED: 
		return false
	_send_buffer.clear()
	ClientMessage.write(_send_buffer, msg)
	_peer.put_16(_send_buffer.get_size())
# warning-ignore:return_value_discarded
	_peer.put_data(_send_buffer.get_data_array())
	_send_buffer.clear()
	return true
	
func connect_to_host(address : String, port : String):
	var hostMsg = {"type": ClientMessage.MessageType.HostInfo,
				   "set": true,
				   "host": address,
				   "service": port}
	send_message(hostMsg)
	var connectionMsg = {"type": ClientMessage.MessageType.ConnectionState,
						 "set": true,
						 "stayConnected": true,
						 "state": ClientMessage.ConnectionState.Connected}
	send_message(connectionMsg)
	return true
	
func disconnect_from_host():
	print("Disconnecting From Host")
	var msg = {"type": ClientMessage.MessageType.HostInfo,
			   "set": true,
			   "stayConnected": false,
			   "state": ClientMessage.ConnectionState.Disconnected}
	send_message(msg)

func _handle_internal_message(msg: Dictionary) -> bool:
	var handled : bool = true
	match msg.type:
		ClientMessage.MessageType.HostInfo:
			pass
		ClientMessage.MessageType.ConnectionState:
			var newStatus = _connection_status
			match msg.state:
				ClientMessage.ConnectionState.Disconnected:
					newStatus = "disconnected"
				ClientMessage.ConnectionState.Connecting:
					newStatus = "connecting"
				ClientMessage.ConnectionState.Connected:
					newStatus = "connected"
				ClientMessage.ConnectionState.Disconnecting:
					newStatus = "disconnecting"
			if newStatus != _connection_status:
				_connection_status = newStatus
				emit_signal("host_connection_state_change", _connection_status)
		_:
			handled = false
	return handled

# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(_delta):
	var status = _peer.get_status()
	if status == StreamPeerTCP.STATUS_NONE and keep_connected:
		_attempt_to_connect_to_client_host()
		status = _peer.get_status()
		
	if status != _prevStatus:
		if status == StreamPeerTCP.STATUS_CONNECTED:
			_peer.set_no_delay(true)
			print("Connected To Client Host")
		emit_signal("client_connection_state_change", status)
		_prevStatus = status
		
	if (status == StreamPeerTCP.STATUS_CONNECTED and
		_bytesNeeded <= _peer.get_available_bytes() and
		not message_hold):
		if _readingHeaderFlag:
			_readingHeaderFlag = false
			_bytesNeeded = _peer.get_16()
			#print("Bytes Needed: ", bytesNeeded)
		else:
			var result = _peer.get_partial_data(_bytesNeeded)
			_receive_buffer.clear()
			_receive_buffer.set_data_array(result[1])
			var msg :Dictionary = ClientMessage.read(_receive_buffer)
			_receive_buffer.set_data_array(PoolByteArray())
			
			print("Message Received:")
			ClientMessage.dump(msg)
			if not _handle_internal_message(msg):
				emit_signal("message_received", msg)
			_readingHeaderFlag = true
			_bytesNeeded = MESSAGE_HEADER_BYTES

func _notification(what):
	if what == MainLoop.NOTIFICATION_WM_QUIT_REQUEST and stop_client_on_close:
		var cmd = {
			"type": ClientMessage.MessageType.Exit}
		send_message(cmd)
		#get_tree().quit() # default behavior
