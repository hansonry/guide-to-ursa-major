extends VBoxContainer


# Declare member variables here. Examples:
# var a = 2
# var b = "text"

signal position_control_commanded(x, y, z)
signal Orientation_control_commanded(yaw, pitch, roll)


# Called when the node enters the scene tree for the first time.
func _ready():
	pass # Replace with function body.


# Called every frame. 'delta' is the elapsed time since the previous frame.
#func _process(delta):
#	pass

func _on_BtnPosCtrl_pressed():
	var vec3ctrl = $V3PosCtrl
	var x = float(vec3ctrl.input1.text)
	var y = float(vec3ctrl.input2.text)
	var z = float(vec3ctrl.input3.text)
	emit_signal("position_control_commanded", x, y, z)


func _on_BtnOriCtrl_pressed():
	var vec3ctrl = $V3OriCtrl
	var yaw   = deg2rad(float(vec3ctrl.input1.text))
	var pitch = deg2rad(float(vec3ctrl.input2.text))
	var roll  = deg2rad(float(vec3ctrl.input3.text))
	emit_signal("Orientation_control_commanded", yaw, pitch, roll)
