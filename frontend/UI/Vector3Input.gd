tool
extends Control


# Declare member variables here. Examples:
# var a = 2
# var b = "text"
export(String) var component_name1 = 'x' setget _set_component_name1
export(String) var component_name2 = 'y' setget _set_component_name2
export(String) var component_name3 = 'z' setget _set_component_name3

var input1 : LineEdit
var input2 : LineEdit
var input3 : LineEdit

# Called when the node enters the scene tree for the first time.
func _ready():
	$Label1.text = component_name1 + ":"
	$Label2.text = component_name2 + ":"
	$Label3.text = component_name3 + ":"
	input1 = $LineEdit1
	input2 = $LineEdit2
	input3 = $LineEdit3

func _set_component_name1(value):
	component_name1 = value
	$Label1.text = component_name1 + ":"

func _set_component_name2(value):
	component_name2 = value
	$Label2.text = component_name2 + ":"

func _set_component_name3(value):
	component_name3 = value
	$Label3.text = component_name3 + ":"


# Called every frame. 'delta' is the elapsed time since the previous frame.
#func _process(delta):
#	pass
