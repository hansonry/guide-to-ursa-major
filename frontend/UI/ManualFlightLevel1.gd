extends VBoxContainer


# Declare member variables here. Examples:
# var a = 2
# var b = "text"

signal velocity_control_commanded(x, y, z)
signal velocity_direction_control_commanded(front, right, up)
signal velocity_release_commanded()
signal angular_velocity_control_commanded(yaw, pitch, roll)
signal angular_velocity_release_commanded()


# Called when the node enters the scene tree for the first time.
func _ready():
	pass # Replace with function body.


# Called every frame. 'delta' is the elapsed time since the previous frame.
#func _process(delta):
#	pass


func _on_BtnVelCtrl_pressed():
	var vec3ctrl = $V3VelCtrl
	var x = float(vec3ctrl.input1.text)
	var y = float(vec3ctrl.input2.text)
	var z = float(vec3ctrl.input3.text)
	emit_signal("velocity_control_commanded", x, y, z)
	


func _on_BtnVelDirCtrl_pressed():
	var vec3ctrl = $V3VelDirCtrl
	var front = float(vec3ctrl.input1.text)
	var right = float(vec3ctrl.input2.text)
	var up    = float(vec3ctrl.input3.text)
	emit_signal("velocity_direction_control_commanded", front, right, up)


func _on_BtnVelRelease_pressed():
	emit_signal("velocity_release_commanded")


func _on_BtnAngVelRelease_pressed():
	emit_signal("angular_velocity_release_commanded")


func _on_BtnAngVelCtrl_pressed():
	var vec3ctrl = $V3AngVelCtrl
	var yaw   = deg2rad(float(vec3ctrl.input1.text))
	var pitch = deg2rad(float(vec3ctrl.input2.text))
	var roll  = deg2rad(float(vec3ctrl.input3.text))
	emit_signal("angular_velocity_control_commanded", yaw, pitch, roll)
