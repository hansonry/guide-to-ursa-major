extends Spatial

class_name OrbitalCamera

# Exported Variables
export(NodePath) var target_path : NodePath setget _set_target_path
export(NodePath) var lookat_path : NodePath setget _set_lookat_path
export(int, FLAGS, "Invert Zoom") var flags : int

const FLAG_INVERT_ZOOM = 1

# Declare member variables here. Examples:
# var a = 2
# var b = "text"
var _tracking_lookat = false
var distance
var pitch
var yaw
var _node_target: Spatial
var _node_lookat: Spatial
var camera : Camera

const MAX_PITCH = deg2rad(90)
const MIN_PITCH = deg2rad(-90)
const MOUSE_MOTION_SCALE = deg2rad(1)

# Called when the node enters the scene tree for the first time.
func _ready():
	distance = 5
	pitch    = 0
	yaw      = 0
	_node_target = get_node_or_null(target_path)
	_node_lookat = get_node_or_null(lookat_path)
	camera       = get_node(@"Camera")

func _unhandled_input(event):
	if (event is InputEventMouseMotion and 
		Input.is_mouse_button_pressed(BUTTON_RIGHT)):
		yaw   -= event.relative.x * MOUSE_MOTION_SCALE
		pitch -= event.relative.y * MOUSE_MOTION_SCALE
		pitch = clamp(pitch, MIN_PITCH, MAX_PITCH)
	elif (event is InputEventMouseButton and 
		  (event.button_index == BUTTON_WHEEL_UP or
		   event.button_index == BUTTON_WHEEL_DOWN)):
			var direction = 1;
			if (flags & FLAG_INVERT_ZOOM) == FLAG_INVERT_ZOOM:
				direction *= -1
			if event.button_index == BUTTON_WHEEL_UP:
				direction *= -1
			distance += direction * 0.5
			if distance < 1:
				distance = 1
		
func _set_target_path(value):
	target_path = value
	_node_target = get_node_or_null(value)

func _set_lookat_path(value):
	lookat_path = value
	_node_lookat = get_node_or_null(value)

# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	if _node_target == null:
		global_transform.origin = Vector3.ZERO
	else:
		global_transform.origin = _node_target.global_transform.origin
	camera.set_identity();
	camera.rotate_x(pitch);
	camera.rotate_y(yaw);
	camera.translate(Vector3(0, 0, distance))
