extends Node


const ClientHostPort = 5486

const Shift = {
	"Position"           : 1,
	"Velocity"           : 2,
	"Acceleration"       : 4,
	"Angle"              : 16,
	"AngularVelocity"    : 18,
	"AngularAcceleration": 20
}
