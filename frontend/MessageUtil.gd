extends Node


func _convert_to_unsigned_int(value: int, sizeInBits: int) -> int:
	if value >= 0:
		return value
	var base: int = 1 << (sizeInBits)
	return base + value

func read_uint(stream: StreamPeer, sizeInBytes : int) -> int:
	var value = 0;
	match sizeInBytes:
		1: 
			value = _convert_to_unsigned_int(stream.get_8(), 8)
		2:
			value = _convert_to_unsigned_int(stream.get_16(), 16)
		4: 
			value = _convert_to_unsigned_int(stream.get_32(), 32)
		_:
			assert(false, "Unsupported number of bytes %d" % sizeInBytes)
	return value

func write_uint(stream: StreamPeer, sizeInBytes : int, value: int):
	match sizeInBytes:
		1: 
			stream.put_8(_convert_to_unsigned_int(value, 8))
		2:
			stream.put_16(_convert_to_unsigned_int(value, 16))
		4: 
			stream.put_32(_convert_to_unsigned_int(value, 32))
		_:
			assert(false, "Unsupported number of bytes %d" % sizeInBytes)


func read_int(stream: StreamPeer, sizeInBytes : int) -> int:
	var value = 0;
	match sizeInBytes:
		1: 
			value = stream.get_8()
		2:
			value = stream.get_16()
		4: 
			value = stream.get_32()
		_:
			assert(false, "Unsupported number of bytes %d" % sizeInBytes)
	return value


func write_int(stream: StreamPeer, sizeInBytes : int, value: int):
	match sizeInBytes:
		1: 
			stream.put_8(value)
		2:
			stream.put_16(value)
		4: 
			stream.put_32(value)
		_:
			assert(false, "Unsupported number of bytes %d" % sizeInBytes)

func read_bool(stream: StreamPeer) -> bool:
	return stream.get_8() != 0

func write_bool(stream: StreamPeer, value: bool):
	stream.put_8(1 if value else 0)

func write_string(stream : StreamPeer, byteSize : int, string: String):
	var array = string.to_ascii();
	var size = array.size();
	write_uint(stream, byteSize, size + 1)
# warning-ignore:return_value_discarded
	stream.put_data(array)
	stream.put_8(0)

func read_string(stream : StreamPeer, byteSize : int):
	var size = read_uint(stream, byteSize)
	if size == 0:
		return null
	var results = stream.get_data(size - 1)
	var array : PoolByteArray = results[1]
	var string : String = array.get_string_from_ascii()
	# warning-ignore:return_value_discarded
	stream.get_8()
	return string

func write_vector3(stream :StreamPeer, vect3: Dictionary):
	write_uint(stream, 4, vect3.x)
	write_uint(stream, 4, vect3.y)
	write_uint(stream, 4, vect3.z)

func read_vector3(stream: StreamPeer)->Dictionary:
	return {
		"x": read_uint(stream, 4),
		"y": read_uint(stream, 4),
		"z": read_uint(stream, 4)}
		
func write_quat(stream :StreamPeer, vect3: Dictionary):
	write_uint(stream, 4, vect3.w)
	write_uint(stream, 4, vect3.x)
	write_uint(stream, 4, vect3.y)
	write_uint(stream, 4, vect3.z)

func read_quat(stream: StreamPeer)->Dictionary:
	return {
		"w": read_uint(stream, 4),
		"x": read_uint(stream, 4),
		"y": read_uint(stream, 4),
		"z": read_uint(stream, 4)}
