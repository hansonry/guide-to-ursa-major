extends Control


# Declare member variables here. Examples:
# var a = 2
# var b = "text"
var _connection

# Called when the node enters the scene tree for the first time.
func _ready():
	_connection = get_node(@"/root/Connection")
	_connection.connect("host_connection_state_change", self, "_host_connection_state_change")
	_connection.message_hold = false

func _host_connection_state_change(status):
	if status == "connected":
		assert(get_tree().change_scene("res://CommandView.tscn") == OK)
	elif status == "disconnected":
		assert(get_tree().change_scene("res://Login.tscn") == OK)		
	pass

# Called every frame. 'delta' is the elapsed time since the previous frame.
#func _process(delta):
#	pass
