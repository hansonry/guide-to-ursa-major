extends Node

const MaskSign      : int = 0x80000000
const MaskMagnitude : int = 0x7FFFFFFF

func to_float(value: int, shift: int)->float:
	var signValue : int = value & MaskSign
	var magnitude : int = value & MaskMagnitude
	var result : float
	if shift > 0:
		var scale : float = 1 << shift;
		result = magnitude / scale
	elif shift < 0:
		var scale : float = 1 << (-shift)
		result = magnitude * scale
	else:
		result = magnitude
	if signValue != 0:
		result = -result
	return result
	
func from_float(value: float, shift: int)->int:
	var signValue : int = 0
	var magnitude : int
	if value < 0:
		value = -value
		signValue = MaskSign
	if shift > 0:
		var scale : int = 1 << shift
		magnitude = value * scale
	elif shift < 0:
		var scale : int = 1 << (-shift)
		magnitude = value / scale
	else:
		magnitude = value
	return magnitude | signValue

func to_vector3_from_int(x : int, y : int, z : int, shift: int) -> Vector3:
	return Vector3(to_float(x, shift),
				   to_float(y, shift),
				   to_float(z, shift))

func to_vector3(v : Dictionary, shift: int) -> Vector3:
	return Vector3(to_float(v.x, shift),
				   to_float(v.y, shift),
				   to_float(v.z, shift))

func from_vector3(v : Vector3, shift: int) -> Dictionary:
	return {"x": from_float(v.x, shift),
			"y": from_float(v.y, shift),
			"z": from_float(v.z, shift)}

func to_quat(q: Dictionary, shift: int) -> Quat:
	var qOut = Quat(to_float(q.x, shift),
				to_float(q.y, shift),
				to_float(q.z, shift),
				to_float(q.w, shift))
	return qOut.normalized()

func from_quat(q : Quat, shift : int) ->Dictionary:
	return {
		"w": from_float(q.w, shift),
		"x": from_float(q.x, shift),
		"y": from_float(q.y, shift),
		"z": from_float(q.z, shift)
	}
