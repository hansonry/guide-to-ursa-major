extends Spatial

const SceneShip = preload("res://Ship.tscn")

# Declare member variables here. Examples:
# var a = 2
# var b = "text"
var connection
var objects : Spatial
var camera : OrbitalCamera
var msg_const

var _user_id
var _ship_id
var _ship : Ship
var _ping_timer
var predicted_server_dt
var _next_command_id : int = 0

var _object_picker_ray : RayCast = null


# Called when the node enters the scene tree for the first time.
func _ready():
	_user_id = -1
	_ship_id = -1
	_ship = null
	connection         = get_node(@"/root/Connection")
	msg_const          = get_node(@"/root/MessageConstants")
	objects            = get_node(@"Objects")
	camera             = get_node(@"OrbitalCamera")
	_object_picker_ray = get_node(@"ObjectPickerRay")
	connection.connect("connection_state_change", self, "_connection_state_change")
	connection.connect("message_received",        self, "_message_received")
	connection.message_hold = false
	_ping_timer = -1
	predicted_server_dt = 1

func average_filter(old_value, new_value, strength):
	return ((old_value * (strength - 1)) + new_value) / strength

func _connection_state_change(status):
	if status ==  StreamPeerTCP.STATUS_NONE or status == StreamPeerTCP.STATUS_ERROR:
		print("Server Disconnected, moving to login screen")
		assert(get_tree().change_scene("res://Login.tscn") == OK)

func _message_received(msg):
	match msg.type:
		ClientMessage.MessageType.EntityCreate:
			_message_recived_ship_create(msg)
		ClientMessage.MessageType.EntityDestroy:
			pass
		ClientMessage.MessageType.EntityPlayerControl:
			var ship = _find_ship_by_id(msg.simId)
			if ship != null:
				print("Switching to our new Ship")
				camera.target_path = ship.get_path()
				_ship = ship
		ClientMessage.MessageType.EntityUpdate:
			var ship = _find_ship_by_id(msg.simId)
			if msg.serverUpdate:
				ship.instant_update(FixedPoint.to_vector3(msg.position, Config.Shift.Position),
									FixedPoint.to_vector3(msg.velocity, Config.Shift.Velocity),
									FixedPoint.to_quat(msg.rotation,    Config.Shift.Angle))
			else:
				ship.update(FixedPoint.to_vector3(msg.position, Config.Shift.Position),
							FixedPoint.to_vector3(msg.velocity, Config.Shift.Velocity),
							FixedPoint.to_quat(msg.rotation,    Config.Shift.Angle))
		ClientMessage.MessageType.Sync:
			var nextStep_seconds = msg.nextStepTime_ms / 1000.0
			for ship in objects.get_children():
				ship.synchronize(nextStep_seconds)
				

func _message_recived_ship_create(msg):
	var ship_instance = SceneShip.instance()
	ship_instance.id = msg.simId
	objects.add_child(ship_instance)
	print("Ship Created with simId: ", ship_instance.id)
	if(ship_instance.id == _ship_id):
		print("New Ship is our ship")
		camera.target_path = ship_instance.get_path()
		_ship = ship_instance

func _get_command_id():
	var id = _next_command_id
	_next_command_id = _next_command_id + 1
	return id
	
func _send_command_velocity(velocity: Vector3):
	var cmd = {
		"type": ClientMessage.MessageType.EntityPlayerCommandVelocity,
		"id": _get_command_id(),
		"targetVelocity": FixedPoint.from_vector3(velocity, Config.Shift.Velocity)}
	Connection.send_message(cmd)
	
func _send_command_position(target: Vector3):
	var cmd = {
		"type": ClientMessage.MessageType.EntityPlayerCommandPosition,
		"id": _get_command_id(),
		"targetPosition": FixedPoint.from_vector3(target, Config.Shift.Position)}
	Connection.send_message(cmd)

func _find_ship_by_id(ship_id):
	for child in objects.get_children():
		if child is Ship and child.id == ship_id:
			return child
	return null

# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	if _ping_timer >= 0:
		_ping_timer += delta
	pass

func _on_BTN_Stop_pressed():
	_send_command_velocity(Vector3(0, 0, 0))
	pass # Replace with function body.

# Input Section

func _clicked_in_space(event):
	var camNorm = camera.camera.project_ray_normal(event.position)
	_send_command_velocity(camNorm * 10)

func _clicked_entity(event, entity):
	print("Clicked Entity")
	_send_command_position(entity.transform.origin)
	
func _unhandled_input(event):
	if (event is InputEventMouseButton and 
		event.button_index == BUTTON_LEFT):
		if _object_picker_ray.is_colliding():
			var obj = _object_picker_ray.get_collider()
			var owner = obj.get_owner()
			if owner is Ship:
				_clicked_entity(event, owner)
			else:
				_clicked_in_space(event)
		else:
			_clicked_in_space(event)
	elif event is InputEventMouseMotion:
		# Preemptively move the ray beacuse Godot limmits Rays to only
		# physical processes
		var from = camera.camera.project_ray_origin(event.position)
		var to = from + camera.camera.project_ray_normal(event.position) * 1000
		_object_picker_ray.transform.origin = from
		_object_picker_ray.cast_to = to

func _on_ManualFlightLevel1_velocity_control_commanded(x, y, z):
	#print("Vel Command ", Vector3(x, y, z))
	pass

func _on_ManualFlightLevel1_velocity_direction_control_commanded(front, right, up):
	pass

func _on_ManualFlightLevel1_velocity_release_commanded():
	pass

func _on_ManualFlightLevel1_angular_velocity_control_commanded(yaw, pitch, roll):
	pass

func _on_ManualFlightLevel1_angular_velocity_release_commanded():
	pass

func _on_ManualFlightLevel2_position_control_commanded(x, y, z):
	#print("Position Command: ", x, y, z)
	pass

func _on_ManualFlightLevel2_Orientation_control_commanded(yaw, pitch, roll):
	#print("Orientation Command: ", yaw, pitch, roll)
	pass
