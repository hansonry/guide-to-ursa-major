extends Spatial

class_name Ship

# Declare member variables here. Examples:
# var a = 2
# var b = "text"
var id = -1;

var _lached_p : Vector3 = Vector3.ZERO;
var _lached_v : Vector3 = Vector3.ZERO;
var _lached_r : Quat = Quat.IDENTITY;

var next_p : Vector3 = Vector3(0, 0, 0)
var next_v : Vector3 = Vector3(0, 0, 0)
var next_r : Quat = Quat(0, 0, 0, 1)
var p : Vector3 = Vector3(0, 0, 0)
var v : Vector3 = Vector3(0, 0, 0)
var r : Quat = Quat(0, 0, 0, 1)
var time_so_far : float = 0
var _predicted_server_dt : float = 0
var _comp_p : Vector3 = Vector3(0, 0, 0)

# Called when the node enters the scene tree for the first time.
func _ready():
	pass

func instant_update(position : Vector3,
					velocity : Vector3,
					rotation: Quat):
	p = position
	_comp_p = position
	v = velocity
	r = rotation

func update(position: Vector3,
			velocity: Vector3,
			rotation: Quat):
	_lached_p = position
	_lached_v = velocity
	_lached_r = rotation
	
func synchronize(predicted_server_dt: float):
	# Make previous values the current values
	p = _comp_p
	v = next_v
	r = next_r
	
	# Setup Timers
	time_so_far = 0
	_predicted_server_dt = predicted_server_dt
	
	# Apply new next values
	next_p = _lached_p
	next_v = _lached_v
	next_r = _lached_r


func _cubic_bezier(p0: Vector3, p1: Vector3, p2: Vector3, p3: Vector3, t: float):
	var q0 = p0.linear_interpolate(p1, t)
	var q1 = p1.linear_interpolate(p2, t)
	var q2 = p2.linear_interpolate(p3, t)

	var r0 = q0.linear_interpolate(q1, t)
	var r1 = q1.linear_interpolate(q2, t)

	var s = r0.linear_interpolate(r1, t)
	return s
	
# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	# Physics Update
	var rot
	if time_so_far < _predicted_server_dt:
		
		var percent = time_so_far / _predicted_server_dt
		
		# Position
		var a = p      + (v      / 3)
		var b = next_p - (next_v / 3) 
		
		var old = _comp_p
		_comp_p = _cubic_bezier(p, a, b, next_p, percent)
		rot = r.slerp(next_r, percent)
		#print(percent, " ", (_comp_p - old).length())
		
		time_so_far += delta
	else:
		# Server is Late, make stuff up
		_comp_p = _comp_p + next_v * delta
		rot = next_r
	transform.origin = _comp_p
	transform.basis = Basis(rot)
	
