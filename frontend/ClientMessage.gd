extends Node

# This code is automaticaly generated using MessageCodeGeneratorGDScript.lua

# Enumerations
const AuthRequestType = {
	"LogIn": 1,
	"LogOut": 2,
}

const ConnectionState = {
	"Disconnected": 1,
	"Connecting": 2,
	"Connected": 3,
	"Disconnecting": 4,
}

# Message Definitions
const MessageType = {
	"Exit": 1,
	"HostInfo": 2,
	"ConnectionState": 3,
	"AuthRequest": 4,
	"AuthResponse": 5,
	"Sync": 6,
	"EntityCreate": 7,
	"EntityDestroy": 8,
	"EntityUpdate": 9,
	"EntityPlayerControl": 10,
	"EntityCommandFree": 20,
	"EntityCommandStop": 21,
	"EntityCommandVelocity": 22,
	"EntityCommandPosition": 23,
	"EntityPlayerCommandFree": 40,
	"EntityPlayerCommandStop": 41,
	"EntityPlayerCommandVelocity": 42,
	"EntityPlayerCommandPosition": 43,
}

# Enumeration ID to String Functions
func enum_value_to_string(enumeration : Dictionary, value: int):
	for key in enumeration.keys():
		if enumeration[key] == value:
			return key
	return "!UNKNWON!"

# Message Read/Write/Dump Functions
## Exit
func write_Exit(stream: StreamPeer, msg: Dictionary):
	MessageUtil.write_uint(stream, 2, MessageType.Exit)

func read_Exit(stream: StreamPeer) -> Dictionary:
	var msg = {
		"type": MessageType.Exit,
	}
	return msg

func dump_Exit(msg: Dictionary):
	print("Dump ClientMessage \"Exit\" 1:")

## HostInfo
func write_HostInfo(stream: StreamPeer, msg: Dictionary):
	MessageUtil.write_uint(stream, 2, MessageType.HostInfo)
	MessageUtil.write_bool(stream, msg.set)
	MessageUtil.write_string(stream, 1, msg.host)
	MessageUtil.write_string(stream, 1, msg.service)

func read_HostInfo(stream: StreamPeer) -> Dictionary:
	var msg = {
		"type": MessageType.HostInfo,
		"set": MessageUtil.read_bool(stream),
		"host": MessageUtil.read_string(stream, 1),
		"service": MessageUtil.read_string(stream, 1),
	}
	return msg

func dump_HostInfo(msg: Dictionary):
	print("Dump ClientMessage \"HostInfo\" 2:")
	print("   set:\t%s" % ["true" if msg.set else "false"])
	print("   host:\t\"%s\",\t%d" % [msg.host, msg.host.length()])
	print("   service:\t\"%s\",\t%d" % [msg.service, msg.service.length()])

## ConnectionState
func write_ConnectionState(stream: StreamPeer, msg: Dictionary):
	MessageUtil.write_uint(stream, 2, MessageType.ConnectionState)
	MessageUtil.write_bool(stream, msg.set)
	MessageUtil.write_bool(stream, msg.stayConnected)
	MessageUtil.write_uint(stream, 1, msg.state)

func read_ConnectionState(stream: StreamPeer) -> Dictionary:
	var msg = {
		"type": MessageType.ConnectionState,
		"set": MessageUtil.read_bool(stream),
		"stayConnected": MessageUtil.read_bool(stream),
		"state": MessageUtil.read_uint(stream, 1),
	}
	return msg

func dump_ConnectionState(msg: Dictionary):
	print("Dump ClientMessage \"ConnectionState\" 3:")
	print("   set:\t%s" % ["true" if msg.set else "false"])
	print("   stayConnected:\t%s" % ["true" if msg.stayConnected else "false"])
	print("   state:\t\"%s\",\t%d" % [enum_value_to_string(ConnectionState, msg.state), msg.state])

## AuthRequest
func write_AuthRequest(stream: StreamPeer, msg: Dictionary):
	MessageUtil.write_uint(stream, 2, MessageType.AuthRequest)
	MessageUtil.write_uint(stream, 1, msg.requestType)
	MessageUtil.write_string(stream, 1, msg.userName)
	MessageUtil.write_string(stream, 1, msg.userPassword)

func read_AuthRequest(stream: StreamPeer) -> Dictionary:
	var msg = {
		"type": MessageType.AuthRequest,
		"requestType": MessageUtil.read_uint(stream, 1),
		"userName": MessageUtil.read_string(stream, 1),
		"userPassword": MessageUtil.read_string(stream, 1),
	}
	return msg

func dump_AuthRequest(msg: Dictionary):
	print("Dump ClientMessage \"AuthRequest\" 4:")
	print("   requestType:\t\"%s\",\t%d" % [enum_value_to_string(AuthRequestType, msg.requestType), msg.requestType])
	print("   userName:\t\"%s\",\t%d" % [msg.userName, msg.userName.length()])
	print("   userPassword:\t\"%s\",\t%d" % [msg.userPassword, msg.userPassword.length()])

## AuthResponse
func write_AuthResponse(stream: StreamPeer, msg: Dictionary):
	MessageUtil.write_uint(stream, 2, MessageType.AuthResponse)
	MessageUtil.write_uint(stream, 1, msg.requestType)
	MessageUtil.write_bool(stream, msg.success)

func read_AuthResponse(stream: StreamPeer) -> Dictionary:
	var msg = {
		"type": MessageType.AuthResponse,
		"requestType": MessageUtil.read_uint(stream, 1),
		"success": MessageUtil.read_bool(stream),
	}
	return msg

func dump_AuthResponse(msg: Dictionary):
	print("Dump ClientMessage \"AuthResponse\" 5:")
	print("   requestType:\t\"%s\",\t%d" % [enum_value_to_string(AuthRequestType, msg.requestType), msg.requestType])
	print("   success:\t%s" % ["true" if msg.success else "false"])

## Sync
func write_Sync(stream: StreamPeer, msg: Dictionary):
	MessageUtil.write_uint(stream, 2, MessageType.Sync)
	MessageUtil.write_uint(stream, 1, msg.step)
	MessageUtil.write_uint(stream, 1, msg.lastClientCmdIdProccessed)
	MessageUtil.write_uint(stream, 2, msg.nextStepTime_ms)

func read_Sync(stream: StreamPeer) -> Dictionary:
	var msg = {
		"type": MessageType.Sync,
		"step": MessageUtil.read_uint(stream, 1),
		"lastClientCmdIdProccessed": MessageUtil.read_uint(stream, 1),
		"nextStepTime_ms": MessageUtil.read_uint(stream, 2),
	}
	return msg

func dump_Sync(msg: Dictionary):
	print("Dump ClientMessage \"Sync\" 6:")
	print("   step:\t0x%X,\t%d" % [msg.step, msg.step])
	print("   lastClientCmdIdProccessed:\t0x%X,\t%d" % [msg.lastClientCmdIdProccessed, msg.lastClientCmdIdProccessed])
	print("   nextStepTime_ms:\t0x%X,\t%d" % [msg.nextStepTime_ms, msg.nextStepTime_ms])

## EntityCreate
func write_EntityCreate(stream: StreamPeer, msg: Dictionary):
	MessageUtil.write_uint(stream, 2, MessageType.EntityCreate)
	MessageUtil.write_uint(stream, 4, msg.simId)
	MessageUtil.write_uint(stream, 4, msg.maxSpeed)
	MessageUtil.write_uint(stream, 4, msg.maxAcceleration)
	MessageUtil.write_uint(stream, 4, msg.stopDistance)

func read_EntityCreate(stream: StreamPeer) -> Dictionary:
	var msg = {
		"type": MessageType.EntityCreate,
		"simId": MessageUtil.read_uint(stream, 4),
		"maxSpeed": MessageUtil.read_uint(stream, 4),
		"maxAcceleration": MessageUtil.read_uint(stream, 4),
		"stopDistance": MessageUtil.read_uint(stream, 4),
	}
	return msg

func dump_EntityCreate(msg: Dictionary):
	print("Dump ClientMessage \"EntityCreate\" 7:")
	print("   simId:\t0x%X,\t%d" % [msg.simId, msg.simId])
	print("   maxSpeed:\t0x%X,\t%d" % [msg.maxSpeed, msg.maxSpeed])
	print("   maxAcceleration:\t0x%X,\t%d" % [msg.maxAcceleration, msg.maxAcceleration])
	print("   stopDistance:\t0x%X,\t%d" % [msg.stopDistance, msg.stopDistance])

## EntityDestroy
func write_EntityDestroy(stream: StreamPeer, msg: Dictionary):
	MessageUtil.write_uint(stream, 2, MessageType.EntityDestroy)
	MessageUtil.write_uint(stream, 4, msg.simId)

func read_EntityDestroy(stream: StreamPeer) -> Dictionary:
	var msg = {
		"type": MessageType.EntityDestroy,
		"simId": MessageUtil.read_uint(stream, 4),
	}
	return msg

func dump_EntityDestroy(msg: Dictionary):
	print("Dump ClientMessage \"EntityDestroy\" 8:")
	print("   simId:\t0x%X,\t%d" % [msg.simId, msg.simId])

## EntityUpdate
func write_EntityUpdate(stream: StreamPeer, msg: Dictionary):
	MessageUtil.write_uint(stream, 2, MessageType.EntityUpdate)
	MessageUtil.write_uint(stream, 4, msg.simId)
	MessageUtil.write_vector3(stream, msg.position)
	MessageUtil.write_vector3(stream, msg.velocity)
	MessageUtil.write_quat(stream, msg.rotation)
	MessageUtil.write_vector3(stream, msg.rotationalVelocity)
	MessageUtil.write_bool(stream, msg.serverUpdate)

func read_EntityUpdate(stream: StreamPeer) -> Dictionary:
	var msg = {
		"type": MessageType.EntityUpdate,
		"simId": MessageUtil.read_uint(stream, 4),
		"position": MessageUtil.read_vector3(stream),
		"velocity": MessageUtil.read_vector3(stream),
		"rotation": MessageUtil.read_quat(stream),
		"rotationalVelocity": MessageUtil.read_vector3(stream),
		"serverUpdate": MessageUtil.read_bool(stream),
	}
	return msg

func dump_EntityUpdate(msg: Dictionary):
	print("Dump ClientMessage \"EntityUpdate\" 9:")
	print("   simId:\t0x%X,\t%d" % [msg.simId, msg.simId])
	print("   position.x:\t0x%X,\t%d" % [msg.position.x, msg.position.x])
	print("   position.y:\t0x%X,\t%d" % [msg.position.y, msg.position.y])
	print("   position.z:\t0x%X,\t%d" % [msg.position.z, msg.position.z])
	print("   velocity.x:\t0x%X,\t%d" % [msg.velocity.x, msg.velocity.x])
	print("   velocity.y:\t0x%X,\t%d" % [msg.velocity.y, msg.velocity.y])
	print("   velocity.z:\t0x%X,\t%d" % [msg.velocity.z, msg.velocity.z])
	print("   rotation.w:\t0x%X,\t%d" % [msg.rotation.w, msg.rotation.w])
	print("   rotation.x:\t0x%X,\t%d" % [msg.rotation.x, msg.rotation.x])
	print("   rotation.y:\t0x%X,\t%d" % [msg.rotation.y, msg.rotation.y])
	print("   rotation.z:\t0x%X,\t%d" % [msg.rotation.z, msg.rotation.z])
	print("   rotationalVelocity.x:\t0x%X,\t%d" % [msg.rotationalVelocity.x, msg.rotationalVelocity.x])
	print("   rotationalVelocity.y:\t0x%X,\t%d" % [msg.rotationalVelocity.y, msg.rotationalVelocity.y])
	print("   rotationalVelocity.z:\t0x%X,\t%d" % [msg.rotationalVelocity.z, msg.rotationalVelocity.z])
	print("   serverUpdate:\t%s" % ["true" if msg.serverUpdate else "false"])

## EntityPlayerControl
func write_EntityPlayerControl(stream: StreamPeer, msg: Dictionary):
	MessageUtil.write_uint(stream, 2, MessageType.EntityPlayerControl)
	MessageUtil.write_uint(stream, 4, msg.simId)
	MessageUtil.write_bool(stream, msg.active)

func read_EntityPlayerControl(stream: StreamPeer) -> Dictionary:
	var msg = {
		"type": MessageType.EntityPlayerControl,
		"simId": MessageUtil.read_uint(stream, 4),
		"active": MessageUtil.read_bool(stream),
	}
	return msg

func dump_EntityPlayerControl(msg: Dictionary):
	print("Dump ClientMessage \"EntityPlayerControl\" 10:")
	print("   simId:\t0x%X,\t%d" % [msg.simId, msg.simId])
	print("   active:\t%s" % ["true" if msg.active else "false"])

## EntityCommandFree
func write_EntityCommandFree(stream: StreamPeer, msg: Dictionary):
	MessageUtil.write_uint(stream, 2, MessageType.EntityCommandFree)
	MessageUtil.write_uint(stream, 4, msg.simId)

func read_EntityCommandFree(stream: StreamPeer) -> Dictionary:
	var msg = {
		"type": MessageType.EntityCommandFree,
		"simId": MessageUtil.read_uint(stream, 4),
	}
	return msg

func dump_EntityCommandFree(msg: Dictionary):
	print("Dump ClientMessage \"EntityCommandFree\" 20:")
	print("   simId:\t0x%X,\t%d" % [msg.simId, msg.simId])

## EntityCommandStop
func write_EntityCommandStop(stream: StreamPeer, msg: Dictionary):
	MessageUtil.write_uint(stream, 2, MessageType.EntityCommandStop)
	MessageUtil.write_uint(stream, 4, msg.simId)

func read_EntityCommandStop(stream: StreamPeer) -> Dictionary:
	var msg = {
		"type": MessageType.EntityCommandStop,
		"simId": MessageUtil.read_uint(stream, 4),
	}
	return msg

func dump_EntityCommandStop(msg: Dictionary):
	print("Dump ClientMessage \"EntityCommandStop\" 21:")
	print("   simId:\t0x%X,\t%d" % [msg.simId, msg.simId])

## EntityCommandVelocity
func write_EntityCommandVelocity(stream: StreamPeer, msg: Dictionary):
	MessageUtil.write_uint(stream, 2, MessageType.EntityCommandVelocity)
	MessageUtil.write_vector3(stream, msg.targetVelocity)
	MessageUtil.write_uint(stream, 4, msg.simId)

func read_EntityCommandVelocity(stream: StreamPeer) -> Dictionary:
	var msg = {
		"type": MessageType.EntityCommandVelocity,
		"targetVelocity": MessageUtil.read_vector3(stream),
		"simId": MessageUtil.read_uint(stream, 4),
	}
	return msg

func dump_EntityCommandVelocity(msg: Dictionary):
	print("Dump ClientMessage \"EntityCommandVelocity\" 22:")
	print("   targetVelocity.x:\t0x%X,\t%d" % [msg.targetVelocity.x, msg.targetVelocity.x])
	print("   targetVelocity.y:\t0x%X,\t%d" % [msg.targetVelocity.y, msg.targetVelocity.y])
	print("   targetVelocity.z:\t0x%X,\t%d" % [msg.targetVelocity.z, msg.targetVelocity.z])
	print("   simId:\t0x%X,\t%d" % [msg.simId, msg.simId])

## EntityCommandPosition
func write_EntityCommandPosition(stream: StreamPeer, msg: Dictionary):
	MessageUtil.write_uint(stream, 2, MessageType.EntityCommandPosition)
	MessageUtil.write_vector3(stream, msg.targetPosition)
	MessageUtil.write_uint(stream, 4, msg.simId)

func read_EntityCommandPosition(stream: StreamPeer) -> Dictionary:
	var msg = {
		"type": MessageType.EntityCommandPosition,
		"targetPosition": MessageUtil.read_vector3(stream),
		"simId": MessageUtil.read_uint(stream, 4),
	}
	return msg

func dump_EntityCommandPosition(msg: Dictionary):
	print("Dump ClientMessage \"EntityCommandPosition\" 23:")
	print("   targetPosition.x:\t0x%X,\t%d" % [msg.targetPosition.x, msg.targetPosition.x])
	print("   targetPosition.y:\t0x%X,\t%d" % [msg.targetPosition.y, msg.targetPosition.y])
	print("   targetPosition.z:\t0x%X,\t%d" % [msg.targetPosition.z, msg.targetPosition.z])
	print("   simId:\t0x%X,\t%d" % [msg.simId, msg.simId])

## EntityPlayerCommandFree
func write_EntityPlayerCommandFree(stream: StreamPeer, msg: Dictionary):
	MessageUtil.write_uint(stream, 2, MessageType.EntityPlayerCommandFree)
	MessageUtil.write_uint(stream, 1, msg.id)

func read_EntityPlayerCommandFree(stream: StreamPeer) -> Dictionary:
	var msg = {
		"type": MessageType.EntityPlayerCommandFree,
		"id": MessageUtil.read_uint(stream, 1),
	}
	return msg

func dump_EntityPlayerCommandFree(msg: Dictionary):
	print("Dump ClientMessage \"EntityPlayerCommandFree\" 40:")
	print("   id:\t0x%X,\t%d" % [msg.id, msg.id])

## EntityPlayerCommandStop
func write_EntityPlayerCommandStop(stream: StreamPeer, msg: Dictionary):
	MessageUtil.write_uint(stream, 2, MessageType.EntityPlayerCommandStop)
	MessageUtil.write_uint(stream, 1, msg.id)

func read_EntityPlayerCommandStop(stream: StreamPeer) -> Dictionary:
	var msg = {
		"type": MessageType.EntityPlayerCommandStop,
		"id": MessageUtil.read_uint(stream, 1),
	}
	return msg

func dump_EntityPlayerCommandStop(msg: Dictionary):
	print("Dump ClientMessage \"EntityPlayerCommandStop\" 41:")
	print("   id:\t0x%X,\t%d" % [msg.id, msg.id])

## EntityPlayerCommandVelocity
func write_EntityPlayerCommandVelocity(stream: StreamPeer, msg: Dictionary):
	MessageUtil.write_uint(stream, 2, MessageType.EntityPlayerCommandVelocity)
	MessageUtil.write_vector3(stream, msg.targetVelocity)
	MessageUtil.write_uint(stream, 1, msg.id)

func read_EntityPlayerCommandVelocity(stream: StreamPeer) -> Dictionary:
	var msg = {
		"type": MessageType.EntityPlayerCommandVelocity,
		"targetVelocity": MessageUtil.read_vector3(stream),
		"id": MessageUtil.read_uint(stream, 1),
	}
	return msg

func dump_EntityPlayerCommandVelocity(msg: Dictionary):
	print("Dump ClientMessage \"EntityPlayerCommandVelocity\" 42:")
	print("   targetVelocity.x:\t0x%X,\t%d" % [msg.targetVelocity.x, msg.targetVelocity.x])
	print("   targetVelocity.y:\t0x%X,\t%d" % [msg.targetVelocity.y, msg.targetVelocity.y])
	print("   targetVelocity.z:\t0x%X,\t%d" % [msg.targetVelocity.z, msg.targetVelocity.z])
	print("   id:\t0x%X,\t%d" % [msg.id, msg.id])

## EntityPlayerCommandPosition
func write_EntityPlayerCommandPosition(stream: StreamPeer, msg: Dictionary):
	MessageUtil.write_uint(stream, 2, MessageType.EntityPlayerCommandPosition)
	MessageUtil.write_vector3(stream, msg.targetPosition)
	MessageUtil.write_uint(stream, 1, msg.id)

func read_EntityPlayerCommandPosition(stream: StreamPeer) -> Dictionary:
	var msg = {
		"type": MessageType.EntityPlayerCommandPosition,
		"targetPosition": MessageUtil.read_vector3(stream),
		"id": MessageUtil.read_uint(stream, 1),
	}
	return msg

func dump_EntityPlayerCommandPosition(msg: Dictionary):
	print("Dump ClientMessage \"EntityPlayerCommandPosition\" 43:")
	print("   targetPosition.x:\t0x%X,\t%d" % [msg.targetPosition.x, msg.targetPosition.x])
	print("   targetPosition.y:\t0x%X,\t%d" % [msg.targetPosition.y, msg.targetPosition.y])
	print("   targetPosition.z:\t0x%X,\t%d" % [msg.targetPosition.z, msg.targetPosition.z])
	print("   id:\t0x%X,\t%d" % [msg.id, msg.id])

# Master Read/Write/Dump Functions
func write(stream: StreamPeer, msg: Dictionary):
	match msg.type:
		MessageType.Exit:
			write_Exit(stream, msg)
		MessageType.HostInfo:
			write_HostInfo(stream, msg)
		MessageType.ConnectionState:
			write_ConnectionState(stream, msg)
		MessageType.AuthRequest:
			write_AuthRequest(stream, msg)
		MessageType.AuthResponse:
			write_AuthResponse(stream, msg)
		MessageType.Sync:
			write_Sync(stream, msg)
		MessageType.EntityCreate:
			write_EntityCreate(stream, msg)
		MessageType.EntityDestroy:
			write_EntityDestroy(stream, msg)
		MessageType.EntityUpdate:
			write_EntityUpdate(stream, msg)
		MessageType.EntityPlayerControl:
			write_EntityPlayerControl(stream, msg)
		MessageType.EntityCommandFree:
			write_EntityCommandFree(stream, msg)
		MessageType.EntityCommandStop:
			write_EntityCommandStop(stream, msg)
		MessageType.EntityCommandVelocity:
			write_EntityCommandVelocity(stream, msg)
		MessageType.EntityCommandPosition:
			write_EntityCommandPosition(stream, msg)
		MessageType.EntityPlayerCommandFree:
			write_EntityPlayerCommandFree(stream, msg)
		MessageType.EntityPlayerCommandStop:
			write_EntityPlayerCommandStop(stream, msg)
		MessageType.EntityPlayerCommandVelocity:
			write_EntityPlayerCommandVelocity(stream, msg)
		MessageType.EntityPlayerCommandPosition:
			write_EntityPlayerCommandPosition(stream, msg)
		_:
			assert(false, "Write Message Type Not supported: %d" % msg.type)

func read(stream: StreamPeer):
	var type = MessageUtil.read_uint(stream, 2)
	var msg = null
	match type:
		MessageType.Exit:
			msg = read_Exit(stream)
		MessageType.HostInfo:
			msg = read_HostInfo(stream)
		MessageType.ConnectionState:
			msg = read_ConnectionState(stream)
		MessageType.AuthRequest:
			msg = read_AuthRequest(stream)
		MessageType.AuthResponse:
			msg = read_AuthResponse(stream)
		MessageType.Sync:
			msg = read_Sync(stream)
		MessageType.EntityCreate:
			msg = read_EntityCreate(stream)
		MessageType.EntityDestroy:
			msg = read_EntityDestroy(stream)
		MessageType.EntityUpdate:
			msg = read_EntityUpdate(stream)
		MessageType.EntityPlayerControl:
			msg = read_EntityPlayerControl(stream)
		MessageType.EntityCommandFree:
			msg = read_EntityCommandFree(stream)
		MessageType.EntityCommandStop:
			msg = read_EntityCommandStop(stream)
		MessageType.EntityCommandVelocity:
			msg = read_EntityCommandVelocity(stream)
		MessageType.EntityCommandPosition:
			msg = read_EntityCommandPosition(stream)
		MessageType.EntityPlayerCommandFree:
			msg = read_EntityPlayerCommandFree(stream)
		MessageType.EntityPlayerCommandStop:
			msg = read_EntityPlayerCommandStop(stream)
		MessageType.EntityPlayerCommandVelocity:
			msg = read_EntityPlayerCommandVelocity(stream)
		MessageType.EntityPlayerCommandPosition:
			msg = read_EntityPlayerCommandPosition(stream)
		_:
			assert(false, "Read Message Type Not supported: %d" % type)
	return msg

func dump(msg: Dictionary):
	match msg.type:
		MessageType.Exit:
			dump_Exit(msg)
		MessageType.HostInfo:
			dump_HostInfo(msg)
		MessageType.ConnectionState:
			dump_ConnectionState(msg)
		MessageType.AuthRequest:
			dump_AuthRequest(msg)
		MessageType.AuthResponse:
			dump_AuthResponse(msg)
		MessageType.Sync:
			dump_Sync(msg)
		MessageType.EntityCreate:
			dump_EntityCreate(msg)
		MessageType.EntityDestroy:
			dump_EntityDestroy(msg)
		MessageType.EntityUpdate:
			dump_EntityUpdate(msg)
		MessageType.EntityPlayerControl:
			dump_EntityPlayerControl(msg)
		MessageType.EntityCommandFree:
			dump_EntityCommandFree(msg)
		MessageType.EntityCommandStop:
			dump_EntityCommandStop(msg)
		MessageType.EntityCommandVelocity:
			dump_EntityCommandVelocity(msg)
		MessageType.EntityCommandPosition:
			dump_EntityCommandPosition(msg)
		MessageType.EntityPlayerCommandFree:
			dump_EntityPlayerCommandFree(msg)
		MessageType.EntityPlayerCommandStop:
			dump_EntityPlayerCommandStop(msg)
		MessageType.EntityPlayerCommandVelocity:
			dump_EntityPlayerCommandVelocity(msg)
		MessageType.EntityPlayerCommandPosition:
			dump_EntityPlayerCommandPosition(msg)
		_:
			assert(false, "Dump Message Type Not supported: %d" % msg.type)

