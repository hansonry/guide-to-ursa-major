# Ship Controls


## Flight Controls

Ship flight will be controled by the server. The player will give a high
level command and the server will control the ship based on these commands.
The higher level the command the smoother the ship will operate as the 
server will only update the clients in very large time intervals. 

Conflicting controls will follow the last commands wins doctrine.


### Manual Flight Level 1 (MFL1)

Not the desired way to control the ship. Players attempting to control
the ship using these commands will get very inconsistant results. This 
will be useful for debuging, so as the game stabilizese these options will
probably be burried.

**Commands:**


* Velocity Control             - Keeping the ship in it's current orientation 
                                 and using the maximum allowed acceleration to 
                                 reach the targeted velocity based on world 
                                 coordinates (x, y, z).
* Directional Velocity Control - Keeping the ship in it's current orientation
                                 and using the maximum allowed acceleration to
                                 reach the targeted velocity based on ship
                                 coordinates (front, right, up).
* Velocity Release             - Stop attempting to control the ship velocity 
                                 (free float).
* Angular Velocity Control     - Using maximum allowed rotational acceleration 
                                 to reach the targed rotational velocity.
* Angular Velocity Release     - Stop attempting to control the ship rotational
                                 velocity (free rotate).

### Manual Flight Level 2 (MFL2)

Stil probably too low level for the player to control. These commands will
use Manual Flight Level 1 systems to assist and so using these commands will
cancle the Flight Level 1 commands and vice versa.


**Commands:**


* Position Control    - Keeping the ship in it's current orientation and using
                        the maximum allowed acceleration to reach the target
                        position. There is an optional max velocity parameter
                        that can be used to govern the ship speed. This command
                        uses Velocity Control in MFL1.
* Orientation Control - Using the maximum allowed rotational acceleration to
                        reach the target orientation. There is an optional max
                        angular velocity parameter that can be used to govern
                        the ships rotation. This command uses the Rotational
                        Velocity Control in MFL1.



### Automatic Flight (AF)

These are the commands the player would be mostl likely to use in a mature
game. 

**Commands:**

* Approach Position - The ship will attempt to get to the `specified position`
                      as quickly as possible. This includes rotating the
                      ship such that the maximum possible acceleration is
                      achieved. The ship will declerate before arriving and
                      stop when it arrives at the `specified position`. 
                      There is also an `approch vector`. This vector indicates 
                      how a ship may approach a position. If the 
                      `approach vector` is (0, 0, 0), then the ship may head 
                      directly to the `specified position`. If the 
                      `approach vector` is (100, 0, 0), then the ship needs to 
                      fly to (100, 0, 0) + `specified position` before it can 
                      begin heading to the `specified position`. An optional 
                      `max velocity` parameter may also be used to limit the 
                      speed.
* Orbit Target -      The ship will attempt to orbit (actively fly around)
                      a `target`. This is used in combat to avoid a constant
                      velocity. Parameters for `orbit distance` and 
                      `orbit speed` need to be provided.
* Fly Heading -       The ship will fly in the specifed world direction at
                      a provided speed. The ship may rotate to get the maximum
                      acceleration possible to reach the speed quickly.
                      Optional parameters for `evasivness` 
                      may be provided. Where 0 `evasivness` results in smooth
                      flight. Higher `evasivness` results in larger 
                      acceleration changes.
* Formation -         The ship will attempt to hold position realitive to a 
                      specified `target`. The `offset` vector specifies where 
                      that realitive position is. The `offset` vector is based 
                      on the `target` entities's coordinates 
                      (front, right, up). This is problably mostly useful for 
                      the coolness factor.

