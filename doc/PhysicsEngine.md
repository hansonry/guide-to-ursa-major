# Physics Engine

## Prerequisites

### Hermite Spline

[Cubic Hermite splines](https://en.wikipedia.org/wiki/Cubic_Hermite_spline) are
a way of computing a path given a start position, start velocity, end position,
and end velocity. The path is computed by using the start and end values and
a value `k` that is between 0 and 1 (inclusive), the result is a point on the
path.

They are the same as Bezier splines with a simple conversion:

```
Hermite(p1, v1, p2, v2, k) = Bezier(p1, p1 + (v1 / 3), p2 - (v2 / 3), p2, k)
```

This makes them easy to compute and has some nice properites.

We can think of this as a path travel though time. By default the path is 
traveled in 1 second. To extend this we have to ajust some things
about the path parameters and how fast we traverse the path. Let's assume 
the new length of time we want to simulate is `Ts` and the time passed so
far is `t`. In this case `k = t / Ts`. We will also need to ajust 
the start and end velocities in the folowing way `v1' = v1 * Ts` and 
`v2' = v2 * Ts`, where `v1` and `v2` are the original velocities and
`v1'` and `v2'` are the new ajusted velocities used in the Hermite function.


### Bezier Spline

Because the Hermite spline is a Bezier spline, the instantaneous velocity
at a given point in time can be found. The derivative of a Cubic Bezier spline
is a Quadratic Bezier spline with the values of `p1 - p0`, `p2 - p1`, `p3 - p2`,
where `p0`, `p1`, `p2`, and `p3` are the parameters used in the computation
for the Cubic Bezier spline.

### Max Velocity Point

When traveling between points, ships will acclerate and then decelerate
using maximum acceleration values. To get this point, we need the folowing
values:

* `(Ta)`  - Acceleration Time
* `(Td)`  - Decleration Time
* `(Pmv)` - The position of maximum velocity.  Vector in Rn space.
* `(V)`   - The maximum velocity.              Vector in Rn space.

We know the following values:

* `(Ps)` - The start position.       Vector in Rn space
* `(Pe)` - The end position.         Vector in Rn space
* `(Vs)` - The start velocity.       Vector in Rn space
* `(Ve)` - The end velocity.         Vector in Rn space
* `(A)`  - The acceleration to use.  Vector in Rn space

Using this information we can setup the following equations:

```
1. (Pmv) = (Ps) + (Vs)(Ta) + 1/2(A)(Ta)^2
2. (Pmv) = (Pe) - (Ve)(Td) - 1/2(A)(Td)^2
3. (Ta)  = ((V)  - (Vs)) / (A)
4. (Td)  = ((Ve) - (V))  / (A)
```

To solve this we will combine equations 3 and 4

```
3. (Ta) = ((V)  - (Vs)) / (A)

   (Ta)(A) = (V) - (Vs)
   (Ta)(A) = (V) - (Vs)

4. (V) = (Ta)(A) + (Vs)
5. (Td) = ((Ve) - (V))  / (A)

   (Td) = ((Ve) - ((Ta)(A) + (Vs))) / (A)
   (Td) = ((Ve) - (Ta)(A) - (Vs)) / (A)
   (Td) = ((Ve) - (Vs)) / (A) - (Ta)

// Lets create a variable to hold all the known values in this equation
// Tat - Total Acceleration Time
   let (Tat) = ((Ve) - (Vs)) / (A)

6. (Td) = (Att) - (Ta)
```

Now that we have equation 6 we can plug that into equation 2:

```
6. (Td) = (Att) - (Ta)
2. (Pmv) = (Pe) - (Ve)(Td) - 1/2(A)(Td)^2

   (Pmv) = (Pe) - (Ve)((Tat) - (Ta)) - 1/2(A)((Tat) - (Ta))^2

// FOIL out the squared term:
   ((Tat) - (Ta))^2 = (Tat)^2 - 2(Tat)(Ta) + (Ta)^2

   (Pmv) = (Pe) - (Ve)(Tat) + (Ve)(Ta) - 1/2(A)(Tat)^2 + (A)(Tat)(Ta) - 1/2(A)(Ta)^2

7. (Pmv) = (Pe) - (Ve)(Tat) - 1/2(A)(Tat)^2 + ((Ve) + (A)(Tat))(Ta) - 1/2(A)(Ta)^2
```

Now that we have equation 7 we can plug that with equation 1 and get a value
for `(Ta)`:

```
7. (Pmv) = (Pe) - (Ve)(Tat) - 1/2(A)(Tat)^2 + ((Ve) + (A)(Tat))(Ta) - 1/2(A)(Ta)^2
1. (Pmv) = (Ps) + (Vs)(Ta) + 1/2(A)(Ta)^2

   (Ps) + (Vs)(Ta) + 1/2(A)(Ta)^2 = (Pe) - (Ve)(Tat) - 1/2(A)(Tat)^2 + ((Ve) + (A)(Tat))(Ta) - 1/2(A)(Ta)^2

// Move everyting to one side

   0 = (Pe) - (Ps) - (Ve)(Tat) - 1/2(A)(Tat)^2 + ((Ve) + (A)(Tat))(Ta) - (Vs)(Ta) - 1/2(A)(Ta)^2 - 1/2(A)(Ta)^2
   0 = (Pe) - (Ps) - (Ve)(Tat) - 1/2(A)(Tat)^2 + ((Ve) + (A)(Tat) - (Vs))(Ta) - (A)(Ta)^2
   0 = (Ps) - (Pe) + (Ve)(Tat) + 1/2(A)(Tat)^2 + ((Vs) - (Ve) - (A)(Tat))(Ta) + (A)(Ta)^2

// Now we can apply the quadratic formula to solve for `(Ta)`

   (Ta) = (-((Vs) - (Ve) - (A)(Tat)) +- sqrt(((Vs) - (Ve) - (A)(Tat))^2 - 4(A)((Ps) - (Pe) + (Ve)(Tat) + 1/2(A)(Tat)^2))) / (2(A))
8. (Ta) = ((Ve) - (Vs) + (A)(Tat)) +- sqrt(sqrt(((Vs) - (Ve) - (A)(Tat))^2 + 4(A)((Pe) - (Ps) - (Ve)(Tat) - 1/2(A)(Tat)^2))) / (2(A))
```


**Steps To Compute Value**

1. Use equation 8 to get `(Ta)`. 
2. Use equation 4 to get `(V)`.
3. Use equation 6 to get `(Td)`.
4. Use equation 1 to get `(Pmv)`.

#### Ajust Maximum Velocity

The velocity `(V)` may be larger than the maximum velocity of the ship. If
That is the case, then the ship needs to hold a lower velocity until it
is time to decelerate.

We know the following values:

* `(Ps)` - The start position.       Vector in Rn space.
* `(Pe)` - The end position.         Vector in Rn space.
* `(Vs)` - The start velocity.       Vector in Rn space.
* `(Ve)` - The end velocity.         Vector in Rn space.
* `(A)`  - The acceleration to use.  Vector in Rn space.
* `(V)`  - The desired velocity.

We need the folowing values:

* `(Ta)`  - Acceleration Time
* `(Td)`  - Decleration Time
* `(Tv)   - Time for constant velocity
* `(Pvs)` - The position of constant velocity start.  Vector in Rn space.
* `(Pve)` - The position of constant velocity end.    Vector in Rn space.

Here is the equation set needed:

```
1. (Ta)  = ((V)  - (Vs)) / (A)
2. (Td)  = ((Ve) - (V))  / (A)
3. (Pvs) = (Ps) + (Vs)(Ta) + 1/2(A)(Ta)^2
4. (Pve) = (Pe) - (Ve)(Td) - 1/2(A)(Td)^2
5. (Tv)  = ((Pve) - (Pvs)) / (V)

```

## Path Computation

Computing a path between two points given a current velocitiy and a desired 
end velocitiy can be done by creating a cubic Hermite spline between the two 
points.

### Limmiting Velocity

It should be noted, that this path will occur in 1 second and there is no 
upper limmit on accelration or velocity. In order to satify the need for 
a maximum velocity, we need to find the time the curved path should take given
the maximum velocity desired. 

The first step in finding the correct time is to find the maximum velocity 
of the path. This can be done because the Hermite spline is a Bezier   
spline and you can find the derivative of a Bezier spline. For our purposes,
this will be a quadratic Bezier spline. Given that the velocities at the 
start and the end were chosen, they are already known. So the other maximum
we need to consider is the point most affected by `p1` in the derivative 
quadratic Bezier with paremeters `p0`, `p1`, and `p2`. To find this velocitiy,
use a value of `0.5` to compute the maximum point.

Now that we have a maximum velocitiy of the intial planned Hermite spline, `Vms`
and we have the desired maximum velocitiy of the path, `Vmp`, we can compute 
a new time `Ts` that will be travel time of the path, `Ts = Vms / Vmp`. 

We can now use the new `Ts` value to ajust the cubic Hermite spline to meet
our desired maximum velocitiy.
