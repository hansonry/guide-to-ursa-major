# Ideas for Guide to Ursa Major

This is a suggestive list. The actual ideas that will be worked on will appear in the [issue board](https://gitlab.com/hansonry/guide-to-ursa-major/-/boards/1933118).

**Note:** The issue board will be updated more frequently.

This file will contain lists of general, or more specific ideas that might or might not make it into the game. 

| Idea         | Category     | Description  |
|--------------|--------------|--------------|
| Indirect ship controls | Gameplay | A spaceship will have to be guided by instructions, no direct "move forward or move up" kind of controls |
| Skins for clients | Design | Every player can load a skin "file or zip" into the game to give the game a specific look and feel |
| Public development server | Development | For ease of development, a public development server with the server code can help with development of the games' features |
| Universe broken up into systems | Gameplay | |
| Mining | Gameplay | Players can mine for resources |
| Production | Gameplay | Players can set up production "plants" to automate resource production |
| PVP competition for resource & system control | Gameplay | Players can combat to loot others for resources or reign over a system |
| Ship combat | Gameplay | Ships can also take part in combat (e.g. with lasers or something) |