# Network Communication

For now communication is done over TCP. TCP is used because the game is less
of a fast response game and more of a slow state command game. This is useful
for the startup of the project because we don't need to deal with the
compications of UDP.

Between Godot and nodejs the least flexible system is Godot. So for now we will
be using Godot's 
[StreamPeerTCP](https://docs.godotengine.org/en/stable/classes/class_streampeertcp.html)
object to communicate with nodejs. It would be useful to be familar with 
[StreamPeer](https://docs.godotengine.org/en/stable/classes/class_streampeer.html#class-streampeer)
as well.

It seems the StreamPeer sends all data as little endian and uses IEEE floats.
although that is not really specified (maybe I am just geting lucky).

## Protocal

All messages contain a 32 bit ```length``` and payload. The payload will be
```length``` bytes long. After the message is complete another message can 
be sent. Note that this means the entire message will be 
(```length``` + 4) bytes long.


**Message**


```
[ 32 bits | <length> bytes ] 
[ length  | payload        ]
```

The payload may contain any information. However Godot specifies that strings
are prefixed with a 32 bit string length. It also appears that the strings
are not null terminated (contrary to the documentation).


**Godot String**
```
[ 32bits | <length> bytes        ]
[ length | string (not termated) ]
```


### Login

Uppon connecting the client will send the sever the following message

```
[ String      | String      | String          ]
[ "GUM Login" | <user name> | <user password> ]
```

For security and performance resons the server should drop the connection if
this is not the first message or the first parameter of the message is not
"GUM Login".

The server will then respond with either:

```
[ String    | String ]
[ "SUCCESS" | "OK"   ]
```

on a successful loging (username and password mataches database) or:

```
[ String | String     ]
[ "FAIL" | "NOTFOUND" ]
```

if the user name was not found or the password didn't match. For security
reasons the server will not tell the client that it has found a user name match.

### Gamplay

All entitys in the game should have a database id per table. The game currently
uses this to uniquly talk about entitys.

The following is the current state of things and may change:

The client is effectly a dumb terminal and will be told by the server to 
create, destroy, and update entitys. Note, that when the server tells a client
to destroy and entity, this does not mean the server has destroyed the enitity.
It could mean that the entity has left client radar range or something of that
nature. 

The message struture for gameplay is as follows:

```
[ 32 bites    | ??? bytes ]
[ messageType | payload   ]
```

The size and contents of the ```payload``` is based on the value of 
```messageType```.  I am not planing on listing those message details
until either the protocal churn slows down or it is nessary for documentation
reasons.

