# Development Process

## Project Structure

The project is broken down into a client and a server. 

The client source files as well as all the finished art 
assets (sounds, music, images, models) exist in a folder 
called `frontend`. 

The server source files exist in a folder called `backend`.

All art asset sources and refrences should be checked into
a diffrent respository 
[guide-to-ursa-major-art-source](https://gitlab.com/hansonry/guide-to-ursa-major-art-source)
to prevent the development respository from becoming too 
heavy. 

This fork owner realises that Gitlab provides a feature to
make a project URL. This fork owner will not use that
feature as it increases the barrier to legitimate 
forking of this project and makes the playing field uneven.

## Commiting Policy

1. All changes not done by the fork owner must be done in either a seperate
   branch or in another fork and must create a Merge Request.
2. The fork owner has the final say in all Merge Requests. It would super 
   helpfull if the fork owner expliained their decisions, but it isn't 
   required.
3. You are free to fork this project for whatever reason. This fork owner
   wants this project to be succesfull even if it isn't the result of
   this fork owner's work or direction.

## Feature Process

This is more of a guide on how this fork owner moves forward in development
so others can follow along. 

1. Planning    - Create a new Planning issue. This issue should contain some 
                 sort of high level description. On completion, there should
                 be a new or updated document in the `doc` folder containing
                 the new direction. New issues should be created to attempt
                 to complete the planned out tasks.
2. Development - Tasks are completed. Verification should be done to 
                 ensure tasks are completed well.
3. Review      - Play testing to make sure the feature works as intended
                 and to make sure the feature is "fun". If the new 
                 feature is not satisfactory is will be removed or
                 another feature will be planned.

# Community Rules

Developing a game is difficult and drama is hard to avoid some times. Lets all do our best!

1. This is an open source project and not a job. All contributors shall
   be treated with respect.
2. If you make a claim and it is contested, it is your job to provide
   evidence and convince others. If you are not convincing, that is your
   problem.
3. This development community should be a safe space for everyone. That means doing your
   best to not make others uncomfortable and unproductive. This also means, if you are 
   uncomfortable with something, you need to speak up. 
4. Never attribute to malice that which can be adequately explained by ignorance.
5. Ignorance while frustrating is normal and ok. However, no one is required
   to help you out of your ignorance. You can decided when to start and stop helping 
   someone for any reason.
