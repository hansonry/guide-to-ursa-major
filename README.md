# Guide to Ursa Major

An open source multiplayer space simulator. 

**License**: [GPL2](LICENSE)

## Screen Shots

It's ugly, but the truth:

**Action Shots**


![View1](doc/screenshots/readmeView1.png)

**Login and Register**

![Login](doc/screenshots/readmeLogin.png)


## Features

This game is currently in early development and isn't much of a game.

## Running the Game

### Loging in

Once you have the client running you will need to login to join a server.

Currently there is only one user named ```ryan``` who has a password ```1234```.
There is no way to currenly register new players. The client currenly expects
the server to be on the same computer (127.0.0.1). There is not way to change
that.


### Flying your Ship err Box

Once connected to the server you will be given a new ship to control. 

You can change the camera zoom by scrolling your mouse wheel. You can rotate 
the camera by pressing the right mouse button and moving your mouse. 

Left clicking will tell the ship to move in that direction. Clicking on 
the ```Stop``` button will stop the ship.

Currently there is nothing else you can do. Not even run into other ships.

## Client Setup

You will need to grab the Godot Game Engine https://godotengine.org/

Open it and import the project that lives in the folder ```frontend``` in
the root of the repository.

Press the white run game button on the upper right. 

## Server Setup

You will need to install nodejs and npm https://nodejs.org/

From the root of the repository in a console do the following:

1. Go to the ```backend``` folder in the repository (```cd backend```).
2. Run ```npm install``` to grab all the server dependencies. This only needs to be done once (per dependency change).
3. Run ```node index.js``` to run the server.

### Server configuration

Currently the server stores the state of the universe in the database 
```database/game.db``` under the ```backend``` folder of the repository. 
Just delete this if you want to reset the server.

## Development

### Game Direction

The game will borrow heavly from Eve Online. Expect the base gameplay loop
to look pretty similar. That being said this project is not attempting to 
clone Eve Online. So it may go in other directions.

The controls will be state or queue based. This means that the results of the
commands may not happen immediatly. This will result in more passive action
gameplay and it will allow the server to take needed time to handle large
amounts of interactions.

For example. The Player will not be able to directly chose when a weapon 
system will fire. Only that the wepan system should continusly fire or
to stop firing. The weapon system will fire as fast as possible. 

### Architecture

The game will attempt to be a single shard multiplayer experence. Being that
the capablities of the server host is unknown. The design of the system should 
be tollerant of really bad performace. The game should gracefully handle a
completly overloaded server. Note that this does not mean the player experence
will not degrate. But if the server does slow down due to load, the client will
slow down with it and not lock up or crash.

The universe is boken up into systems. Each system can be hosted by a diffrent
process, maybe even on a diffrent machine. There will be a root server that
will handle managing the system servers and any player registration. It may
be nessary to handel money and item transations on the root server.

The system server time steps will be large in the hopes that even bad hosts
will be able to handle a good number of clients. My current thought is 1 second 
for the network update and 0.25 seconds for phyical simulations. The client will
be responsible for making this look smooth. As for client prediction, there
are no plans yet as Eve has shown that with good feedback, players can be 
forgiving.

The final system will problably look something like this:

```
database
+- root server
   +- Connecting Client
   +- Connecting Client
   ...
+- system server
   +- Flying Client
   +- Docked Client
   ...   
+- system server
   +- Flying Client
   +- Docked Client
   ...
...   
```

#### Current Systems

The project currently uses nodejs for the backend and uses godot for the front 
end. These are not set in stone. The reason for using these technologies is 
to get something working quickly, so we can find out where improvments can be 
made to keep development fun and intresting. 

The database is currently sqlite.

### Contributing 

Contact us on our [Discord Server](https://discord.gg/KjSDX8) for more info, but if you want to take on [one of the issues](https://gitlab.com/hansonry/guide-to-ursa-major/-/issues?scope=all&utf8=%E2%9C%93&state=opened&assignee_id=None) that are currently open and assigned to nobody, feel free to discuss how you want to start tackling the issue in its comments. If you plan on contributing you should make yourself familar with the [Development Process](doc/DevelopmentProcess.md) this project uses.
