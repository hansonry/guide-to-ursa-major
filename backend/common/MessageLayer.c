#include <stdio.h>
#include <stdlib.h>

#include "Queue.h"
#include "TCPNetwork.h"

#include "MessageLayer.h"
#include "BytePacker.h"

QUEUE_CREATE_INLINE_FUNCTIONS(byte, uint8_t)

#define BUFFER_SIZE 512

struct messageLayer
{
   struct queue sendQueue;
   struct queue receiveQueue;
   void * userData;
   msglReceiveCallback receiveCallback;
   struct tcpNetworkSocket * socket;
   size_t bufferSize;
   uint8_t buffer[];
};

struct messageLayer * msglCreate(struct tcpNetworkSocket * socket,
                                 void * userData,
                                 msglReceiveCallback receiveCallback)
{
   struct messageLayer * layer = malloc(sizeof(struct messageLayer) + BUFFER_SIZE);
   
   queueInitDefaults_byte(&layer->sendQueue);
   queueInitDefaults_byte(&layer->receiveQueue);
   layer->receiveCallback = receiveCallback;
   layer->socket = socket;
   layer->bufferSize = BUFFER_SIZE;
   layer->userData = userData;
   
   return layer;
}
void msglDestory(struct messageLayer * layer)
{
   queueFree(&layer->sendQueue);
   queueFree(&layer->receiveQueue);
   free(layer);
}

static inline void msglPushMessageSize(struct queue * queue, uint16_t messageSize)
{
   uint8_t byteSize[2];
   struct bytepacker byp;
   bypInit(&byp, byteSize, 2);
   bypWriteInt16(&byp, messageSize);
   queuePushMany_byte(queue, byteSize, 2);
}

static inline bool msglPeekMessageSize(struct queue * queue, uint16_t * messageSize)
{
   uint8_t byteSize[2];
   size_t read = queuePeekMany_byte(queue, byteSize, 2);
   if(read != 2)
   {
      return false;
   }
   if(messageSize != NULL)
   {
      struct bytepacker byp;
      bypInit(&byp, byteSize, 2);
      (*messageSize) = bypReadInt16(&byp);
   }
   return true;
}

static inline void msglDumpMessage(const void * message, uint16_t messageSize)
{
   uint16_t offset = 0;
   const unsigned char * byteMessage = (const unsigned char *)message;
   printf("Dumping Message Size: %u\n", messageSize);
   printf("        0           4           8           C\n");
   while( offset < messageSize)
   {
      int byteCount = 0;
      printf("0x%04X:", offset);
      while(offset < messageSize && byteCount < 16)
      {
         printf(" %02X", byteMessage[offset]);
         
         byteCount ++;
         offset ++;
      }
      printf("\n");
   }
}

void msglQueueToSend(struct messageLayer * layer, const void * message, uint16_t messageSize)
{
   msglPushMessageSize(&layer->sendQueue, messageSize);
   queuePushMany(&layer->sendQueue, message, messageSize);
   //msglDumpMessage(message, messageSize);
}

static inline bool msglCheckAndNotify(struct messageLayer * layer)
{
   uint16_t messageSize;
   bool success = msglPeekMessageSize(&layer->receiveQueue, &messageSize);
   if(!success || (messageSize + 2) > queueGetCount(&layer->receiveQueue))
   {
      return false;
   }
   queuePopMany(&layer->receiveQueue, NULL, 2);
   if(layer->bufferSize < messageSize)
   {
      fprintf(stderr, "%s: %d: error: bufferSize %lu is larger than message size %u. Dropping Message.\n", __FILE__, __LINE__, layer->bufferSize, messageSize); 
      queuePopMany(&layer->receiveQueue, NULL, messageSize);
      return true;
   }
   queuePopMany(&layer->receiveQueue, layer->buffer, messageSize);
   if(layer->receiveCallback != NULL)
   {
      layer->receiveCallback(layer->userData, layer, layer->socket, layer->buffer, messageSize);
   }
   
   return true;
}

static inline void msglReceiveEverything(struct messageLayer * layer)
{
   ssize_t result = 1;
   
   while(result > 0)
   {
      result = tcpnSocketRead(layer->socket, layer->buffer, layer->bufferSize);
      if(result > 0)
      {
         queuePushMany(&layer->receiveQueue, layer->buffer, result);
         while(msglCheckAndNotify(layer));
      }
   }
}

static inline void msglSendOnce(struct messageLayer * layer)
{
   size_t toSend = queuePopMany_byte(&layer->sendQueue, layer->buffer, layer->bufferSize);
   if(toSend > 0)
   {
      tcpnSocketWrite(layer->socket, layer->buffer, toSend);
   }
}

void msglPump(struct messageLayer * layer)
{
   msglReceiveEverything(layer);
   msglSendOnce(layer);
}

void msglSend(struct messageLayer * layer)
{
   while(queueIsNotEmpty(&layer->sendQueue))
   {
      msglSendOnce(layer);
   }
}

void msglReceive(struct messageLayer * layer)
{
   msglReceiveEverything(layer);
}


size_t msglGetSendBufferSize(const struct messageLayer * layer)
{
   return queueGetCount(&layer->sendQueue);
}

size_t msglGetReceiveBufferSize(const struct messageLayer * layer)
{
   return queueGetCount(&layer->receiveQueue);
}

