local MessageCodeGeneratorC = {}


local function FirstLetterLowercase(str)
   return string.sub(str, 1, 1):lower() .. string.sub(str, 2)
end

function LookupEnumerationByName(enumerations, name)
   for i, v in ipairs(enumerations) do
      if v.name == name then
         return v
      end
   end
   return nil
end

local function LookupEnumerationByNameWithAssert(enumerations, name)
   return assert(LookupEnumerationByName(enumerations, name), 
                 "Failed to find matching enum to: " .. name)
end


local function GetSupportedSize(size)
   assert(size <= 32, "No support for sizes larger than 32 bits: " .. size)
   if size > 16 then
      return 32
   elseif size > 8 then
      return 16
   else
      return 8     
   end
end


local function GetCContainer(enumerations, param)
   local container = nil
   if param.type == "uint" then
      container = string.format("uint%d_t", GetSupportedSize(param.size))
   elseif param.type == "int" then
      container = string.format("uint%d_t", GetSupportedSize(param.size))
   elseif param.type == "bool" then
      container = "bool"
   elseif param.type == "string" then
      container = "const char *"
   elseif param.type == "vector3" then
      container = "struct msgI32Vector3"
   elseif param.type == "quat" then
      container = "struct msgI32Vector4"
   elseif param.type == "enum" then
      local enum = LookupEnumerationByNameWithAssert(enumerations, param.enum)
      container = string.format("uint%d_t", GetSupportedSize(enum.size))
   else
      assert(false, "Unexpected Parameter Type: " .. param.type)
   end
   return container
end

local function MakeTypeMacroName(moduleMacroPrefix, name)
   return string.format("%s_TYPE_%s", moduleMacroPrefix, name:upper())
end



local function generate(outputDir, moduleName, prefix, enumerations, messages, packer)

   local outputFilename = string.format("%s/%s.h", outputDir, moduleName)
   local fh = io.open(outputFilename, "wb")
   
   local moduleMacroPrefix = prefix:upper()
   
   local preprocessorProtectionName = string.format("__%s_H__", moduleName:upper())
   
   local messageUnionName = FirstLetterLowercase(moduleName);
   

   
   fh:write(string.format("#ifndef %s\n", preprocessorProtectionName))   
   fh:write(string.format("#define %s\n", preprocessorProtectionName))
   fh:write("// This file is automaticaly generated from by MessageCodeGeneratorC.lua\n")
   
   -- Includes
   fh:write("#include <stdint.h>\n")
   fh:write("#include <stdio.h>\n")
   fh:write("#include <string.h>\n")
   fh:write("#include <stdlib.h>\n")
   fh:write("#include <stdbool.h>\n")   
   fh:write("\n")
   for i, include in ipairs(packer.includes) do
      fh:write(string.format("#include %s\n", include))
   end
   fh:write("\n") 
  
   -- Message ID Defines
   fh:write("// Message Types\n")
   for msgIndex, msgValue in ipairs(messages) do
      local typeMacroName = MakeTypeMacroName(moduleMacroPrefix, msgValue.name)
      fh:write(string.format("#define %s %d\n", typeMacroName, msgValue.id))
   end 
   fh:write("\n")
  
   --fh:write("typedef uint16_t serverMessageType;\n")
   
   -- Write constants
   fh:write("// Enumerations\n")
   for enumIndex, enumValue in ipairs(enumerations) do
      fh:write(string.format("/// %s\n", enumValue.name))
      local enumMacroPrefix = enumValue.name:upper()
      for i, v in ipairs(enumValue.values) do
         local macroName = string.format("%s_%s_%s", moduleMacroPrefix, enumMacroPrefix, v.name:upper())
         fh:write(string.format("#define %s %d\n", macroName, v.value))
      end
      fh:write("\n")
   end
   fh:write("\n")
   
   -- Enumeration to String function
   for enumIndex, enumValue in ipairs(enumerations) do
      fh:write(string.format("/// %s\n", enumValue.name))
      local enumMacroPrefix = enumValue.name:upper()
      local enumContainer = string.format("uint%d_t", GetSupportedSize(enumValue.size))
      local enumToStringFunctionName = string.format("%s%sEnumToString", prefix, enumValue.name)
      fh:write("static inline\n")
      fh:write(string.format("const char * %s(%s value)\n", enumToStringFunctionName, enumContainer))
      fh:write("{\n")
      fh:write("   const char * name;\n")
      fh:write("   switch(value)\n")
      fh:write("   {\n")
      for i, v in ipairs(enumValue.values) do
         local macroName = string.format("%s_%s_%s", moduleMacroPrefix, enumMacroPrefix, v.name:upper())
         fh:write(string.format("      case %s: name = \"%s\"; break;\n", macroName, v.name))
      end
      fh:write("      default: name = \"!UNKNOWN!\"; break;\n")
      fh:write("   }\n")
      fh:write("   return name;\n")
      fh:write("}\n\n")
   end
   
   
   -- Write Utility Functions
   packer.createUtilFunctions(fh, prefix)
   
   local messageTypeLine = "   uint16_t type;\n";
   
   -- Write Messages
   fh:write("// Messages\n")
   for msgIndex, msgValue in ipairs(messages) do
      local structName = string.format("%s%s", prefix, msgValue.name)
      local paramName = FirstLetterLowercase(msgValue.name)
      fh:write(string.format("/// %s\n", msgValue.name))
      
      -- Write Structure
      fh:write(string.format("struct %s\n", structName))
      fh:write("{\n")
      fh:write(messageTypeLine)
      for paramIndex, paramValue in ipairs(msgValue.structure) do
         local container = GetCContainer(enumerations, paramValue)
         fh:write(string.format("   %s %s;\n", container, paramValue.name))

      end
      fh:write("};\n\n")
      
      local typeMacroName = MakeTypeMacroName(moduleMacroPrefix, msgValue.name)
      -- Write Writer
      fh:write("static inline\n")
      fh:write(string.format("void %sWrite%s(struct %s * bp, const struct %s * msg)\n", prefix, msgValue.name, packer.structName, structName))
      fh:write("{\n")
      local functionCall = packer.writeInt(prefix, "bp", 16, typeMacroName)
      fh:write(string.format("   %s;\n", functionCall))
      for paramIndex, paramValue in ipairs(msgValue.structure) do
         local paramVar = string.format("msg->%s", paramValue.name)
         if paramValue.type == "uint" or paramValue.type == "int" then
            local functionCall = packer.writeInt(prefix, "bp", paramValue.size, paramVar)
            fh:write(string.format("   %s;\n", functionCall))
         elseif paramValue.type == "bool" then
            local functionCall = packer.writeBool(prefix, "bp", paramVar)
            fh:write(string.format("   %s;\n", functionCall))
         elseif paramValue.type == "vector3" then
            local functionCallX = packer.writeInt(prefix, "bp", 32, string.format("%s.x", paramVar))
            local functionCallY = packer.writeInt(prefix, "bp", 32, string.format("%s.y", paramVar))
            local functionCallZ = packer.writeInt(prefix, "bp", 32, string.format("%s.z", paramVar))
            fh:write(string.format("   %s;\n   %s;\n   %s;\n", functionCallX, functionCallY, functionCallZ))
         elseif paramValue.type == "quat" then
            local functionCallW = packer.writeInt(prefix, "bp", 32, string.format("%s.w", paramVar))
            local functionCallX = packer.writeInt(prefix, "bp", 32, string.format("%s.x", paramVar))
            local functionCallY = packer.writeInt(prefix, "bp", 32, string.format("%s.y", paramVar))
            local functionCallZ = packer.writeInt(prefix, "bp", 32, string.format("%s.z", paramVar))
            fh:write(string.format("   %s;\n   %s;\n   %s;\n   %s;\n", functionCallW, functionCallX, functionCallY, functionCallZ))
         elseif paramValue.type == "enum" then
            local enum = LookupEnumerationByNameWithAssert(enumerations, paramValue.enum)
            local functionCall = packer.writeInt(prefix, "bp", enum.size, paramVar)
            fh:write(string.format("   %s;\n", functionCall))
         elseif paramValue.type == "string" then
            local functionCall = packer.writeString(prefix, "bp", paramValue.size, paramVar)
            fh:write(string.format("   %s;\n", functionCall))
         else
            assert(false, "Unexpected param type: " .. paramValue.type)
         end
      end
      fh:write("}\n\n")
      
      -- Write Reader
      local readString = false
      fh:write("static inline\n")
      fh:write(string.format("void %sRead%s(struct %s * bp, %sstruct %s * msg)\n", prefix, msgValue.name, packer.structName, packer.readParam, structName))
      fh:write("{\n")
      fh:write(string.format("   msg->type = %s;\n", typeMacroName))
      for paramIndex, paramValue in ipairs(msgValue.structure) do
         local paramVar = string.format("msg->%s", paramValue.name)
         if paramValue.type == "uint" or paramValue.type == "int" then
            local functionCall = packer.readInt(prefix, "bp", paramValue.size)
            fh:write(string.format("   %s = %s;\n", paramVar, functionCall))
         elseif paramValue.type == "bool" then
            local functionCall = packer.readBool(prefix, "bp")
            fh:write(string.format("   %s = %s;\n", paramVar, functionCall))
         elseif paramValue.type == "vector3" then
            local functionCall = packer.readInt(prefix, "bp", 32)
            fh:write(string.format("   %s.x = %s;\n", paramVar, functionCall))
            fh:write(string.format("   %s.y = %s;\n", paramVar, functionCall))
            fh:write(string.format("   %s.z = %s;\n", paramVar, functionCall))
         elseif paramValue.type == "quat" then
            local functionCall = packer.readInt(prefix, "bp", 32)
            fh:write(string.format("   %s.w = %s;\n", paramVar, functionCall))
            fh:write(string.format("   %s.x = %s;\n", paramVar, functionCall))
            fh:write(string.format("   %s.y = %s;\n", paramVar, functionCall))
            fh:write(string.format("   %s.z = %s;\n", paramVar, functionCall))
         elseif paramValue.type == "enum" then
            local enum = LookupEnumerationByNameWithAssert(enumerations, paramValue.enum)
            local functionCall = packer.readInt(prefix, "bp", enum.size)
            fh:write(string.format("   %s = %s;\n", paramVar, functionCall))
         elseif paramValue.type == "string" then
            readString = true
            local functionCall = packer.readString(prefix, "bp", paramValue.size)
            fh:write(string.format("   %s = %s;\n", paramVar, functionCall))
         else
            assert(false, "Unexpected param type: " .. paramValue.type)
         end
      end
      if not readString and packer.readParam ~= "" then
         fh:write("   (void)strb;\n")
      end

      fh:write("}\n\n")
      
      -- Write Dumper
      
      fh:write("static inline\n")
      fh:write(string.format("void %sDump%s(const struct %s * msg)\n", prefix, msgValue.name, structName))
      fh:write("{\n")
      fh:write(string.format("   printf(\"Dumping %s \\\"%s\\\" %d:\\n\");\n", moduleName, msgValue.name, msgValue.id))
      for paramIndex, paramValue in ipairs(msgValue.structure) do
         if paramValue.type == "uint" then
            fh:write(string.format("   printf(\"   %s:\\t0x%%X,\\t%%u\\n\", msg->%s, msg->%s);\n", paramValue.name, paramValue.name, paramValue.name))
         elseif paramValue.type == "int" then
            fh:write(string.format("   printf(\"   %s:\\t0x%%X,\\t%%d\\n\", msg->%s, msg->%s);\n", paramValue.name, paramValue.name, paramValue.name))
         elseif paramValue.type == "bool" then
            fh:write(string.format("   printf(\"   %s:\\t%%s\\n\", msg->%s ? \"true\" : \"false\");\n", paramValue.name, paramValue.name))
         elseif paramValue.type == "vector3" then
            fh:write(string.format("   printf(\"   %s.x:\\t0x%%X,\\t%%u\\n\", msg->%s.x, msg->%s.x);\n", paramValue.name, paramValue.name, paramValue.name))
            fh:write(string.format("   printf(\"   %s.y:\\t0x%%X,\\t%%u\\n\", msg->%s.y, msg->%s.y);\n", paramValue.name, paramValue.name, paramValue.name))
            fh:write(string.format("   printf(\"   %s.z:\\t0x%%X,\\t%%u\\n\", msg->%s.z, msg->%s.z);\n", paramValue.name, paramValue.name, paramValue.name))
         elseif paramValue.type == "quat" then
            fh:write(string.format("   printf(\"   %s.w:\\t0x%%X,\\t%%u\\n\", msg->%s.w, msg->%s.w);\n", paramValue.name, paramValue.name, paramValue.name))
            fh:write(string.format("   printf(\"   %s.x:\\t0x%%X,\\t%%u\\n\", msg->%s.x, msg->%s.x);\n", paramValue.name, paramValue.name, paramValue.name))
            fh:write(string.format("   printf(\"   %s.y:\\t0x%%X,\\t%%u\\n\", msg->%s.y, msg->%s.y);\n", paramValue.name, paramValue.name, paramValue.name))
            fh:write(string.format("   printf(\"   %s.z:\\t0x%%X,\\t%%u\\n\", msg->%s.z, msg->%s.z);\n", paramValue.name, paramValue.name, paramValue.name))
         elseif paramValue.type == "enum" then
            local enum = LookupEnumerationByNameWithAssert(enumerations, paramValue.enum)
            local enumToStringFunctionName = string.format("%s%sEnumToString", prefix, enum.name)
            fh:write(string.format("   printf(\"   %s:\\t\\\"%%s\\\",\\t%%u\\n\", %s(msg->%s), msg->%s);\n", paramValue.name, enumToStringFunctionName, paramValue.name, paramValue.name))
         elseif paramValue.type == "string" then
            readString = true
            fh:write(string.format("   printf(\"   %s:\\t\\\"%%s\\\",\\t%%lu\\n\", msg->%s, strlen(msg->%s));\n", paramValue.name, paramValue.name, paramValue.name))
         else
            assert(false, "Unexpected param type: " .. paramValue.type)
         end
      end

      fh:write("}\n\n")
      
   end
   
   
   -- Write Message Union
   fh:write(string.format("union %s\n", messageUnionName))
   fh:write("{\n")
   fh:write(messageTypeLine)
   for msgIndex, msgValue in ipairs(messages) do
      local structName = string.format("%s%s", prefix, msgValue.name)
      local paramName = FirstLetterLowercase(msgValue.name)

      fh:write(string.format("   struct %s %s;\n", structName, paramName))
   end
   fh:write("};\n\n")
   
   -- Write Message Write
   fh:write("static inline\n")
   fh:write(string.format("bool %sWrite(struct %s * bp, const union %s * msg, bool sizeCheck)\n", prefix, packer.structName, messageUnionName))
   fh:write("{\n")
   fh:write("   bool success = true;\n")
   fh:write("   switch(msg->type)\n")
   fh:write("   {\n")
   for msgIndex, msgValue in ipairs(messages) do
      local typeMacroName = MakeTypeMacroName(moduleMacroPrefix, msgValue.name)
      local functionName = string.format("%sWrite%s", prefix, msgValue.name, structName)
      local paramName = FirstLetterLowercase(msgValue.name)
      fh:write(string.format("      case %s: %s(bp, &msg->%s); break;\n", typeMacroName, functionName, paramName))
   end
   fh:write("      default:\n")
   fh:write("         success = false;\n")
   fh:write(string.format("         fprintf(stderr, \"%%s: %%d: error: %s Type %%u is not a valid message type\\n\", __FILE__, __LINE__, msg->type);\n", moduleName))
   fh:write("         break;\n")
   fh:write("   }\n\n")
   fh:write(string.format("   if(success && sizeCheck && %s(bp))\n", packer.overuseCheckFunctionName))
   fh:write("   {\n")
   fh:write("      success = false;\n")
   fh:write(string.format("      fprintf(stderr, \"%%s: %%d: error: %s Type %%u is larger than provided Buffer: %%lu\\n\", __FILE__, __LINE__, msg->type, bp->%s);\n", moduleName, packer.sizeInBytesMember))   
   fh:write("   }\n")
   fh:write("   return success;\n")
   fh:write("}\n\n")

   -- Write Message Read
   fh:write("static inline\n")
   fh:write(string.format("bool %sRead(struct %s * bp, %sunion %s * msg)\n", prefix, packer.structName, packer.readParam, messageUnionName))
   fh:write("{\n")
   fh:write("   bool success = true;\n")
   local functionCall = packer.readInt(prefix, "bp", 16)
   fh:write(string.format("   uint16_t type = %s;\n", functionCall))
   fh:write("   switch(type)\n")
   fh:write("   {\n")
   for msgIndex, msgValue in ipairs(messages) do
      local typeMacroName = MakeTypeMacroName(moduleMacroPrefix, msgValue.name)
      local functionName = string.format("%sRead%s", prefix, msgValue.name, structName)
      local paramName = FirstLetterLowercase(msgValue.name)
      local stringBuffer = ""
      if packer.readParam ~= "" then
         stringBuffer = "strb, "
      end
      fh:write(string.format("      case %s: %s(bp, %s&msg->%s); break;\n", typeMacroName, functionName, stringBuffer, paramName))
   end
   fh:write("      default:\n")
   fh:write("         success = false;\n")
   fh:write(string.format("         fprintf(stderr, \"%%s: %%d: error: %s Type %%u is not a valid message type\\n\", __FILE__, __LINE__, type);\n", moduleName))
   fh:write("         break;\n")
   fh:write("   }\n\n")
   fh:write(string.format("   if(success && %s(bp))\n", packer.overuseCheckFunctionName))
   fh:write("   {\n")
   fh:write("      success = false;\n")
   fh:write(string.format("      fprintf(stderr, \"%%s: %%d: error: %s Type %%u is larger than provided Buffer: %%lu\\n\", __FILE__, __LINE__, type, bp->%s);\n", moduleName, packer.sizeInBytesMember))   
   fh:write("   }\n")
   fh:write("   return success;\n")
   fh:write("}\n\n")
   
   -- Write Message Dump
   fh:write("static inline\n")
   fh:write(string.format("void %sDump(const union %s * msg)\n", prefix, messageUnionName))
   fh:write("{\n")
   fh:write("   switch(msg->type)\n")
   fh:write("   {\n")
   for msgIndex, msgValue in ipairs(messages) do
      local typeMacroName = MakeTypeMacroName(moduleMacroPrefix, msgValue.name)
      local functionName = string.format("%sDump%s", prefix, msgValue.name, structName)
      local paramName = FirstLetterLowercase(msgValue.name)
      fh:write(string.format("      case %s: %s(&msg->%s); break;\n", typeMacroName, functionName, paramName))
   end
   fh:write("      default:\n")
   fh:write(string.format("         fprintf(stderr, \"%%s: %%d: warning: %s Type %%u is not a valid message type\\n\", __FILE__, __LINE__, msg->type);\n", moduleName))
   fh:write("         break;\n")
   fh:write("   }\n")
   fh:write("}\n\n")


   fh:write(string.format("#endif // %s\n", preprocessorProtectionName))   
   fh:close()
end

function WriteBitPackerStringReaderFunction(fh, prefix)
   local stringReadFunctionName = prefix .. "ReadString"
   fh:write("static inline\n")
   fh:write(string.format("char * %s(struct bitpacker * bp, struct stringBuffer * strb, size_t sizeBitCount)\n", stringReadFunctionName))
   fh:write("{\n")
   fh:write("   size_t size = bpPeekByteArraySize(bp, sizeBitCount);\n")
   fh:write("   char * buffer = strbMalloc(strb, size);\n")
   fh:write("   (void)bpReadByteArray(bp, sizeBitCount, buffer, size);\n")
   fh:write("   buffer[size] = '\\0';\n")
   fh:write("   return buffer;\n")
   fh:write("}\n\n")
   return stringReadFunctionName
end

function MessageCodeGeneratorC.generateBitPacker(outputDir, moduleName, prefix, enumerations, messages)
   local bitPacker = {
      includes = {"\"CommonMessage.h\"", "\"BitPacker.h\"", "\"StringBuffer.h\""},
      structName = "bitpacker",
      readParam = "struct stringBuffer * strb, ",
      overuseCheckFunctionName = "bpHasDroppedBits",
      sizeInBytesMember = "sizeInBytes",
      createUtilFunctions = function(fh, prefix)
         WriteBitPackerStringReaderFunction(fh, prefix)
      end,
      writeInt = function(prefix, packerVar, size, param)
         return string.format("bpWriteInt(%s, %d, %s)", packerVar, size, param)
      end,
      readInt = function(prefix, packerVar, size)
         return string.format("bpReadInt(%s, %d)", packerVar, size)      
      end,
      writeString = function(prefix, packerVar, size, param)
         return string.format("bpWriteByteArray(%s, %d, %s, strlen(%s))", packerVar, size, param, param)               
      end,
      readString = function(prefix, packerVar, size)
         local stringReadFunctionName = prefix .. "ReadString"
         return string.format("%s(%s, strb, %d)", stringReadFunctionName, packerVar, size)               
      end,
      writeBool = function(prefix, packerVar, param)
         return string.format("bpWriteBit(%s, %s)", packerVar, param)
      end,
      readBool = function(prefix, packerVar)
         return string.format("bpReadBit(%s)", packerVar)
      end
   }
   
   generate(outputDir, moduleName, prefix, enumerations, messages, bitPacker)
end

function WriteBytePackerStringFunction(fh, prefix)
   local functionName = string.format("%sWriteString", prefix)
   fh:write("static inline\n")
   fh:write(string.format("void %s(struct bytepacker * byp, unsigned char sizeBytes, const char * str)\n", functionName))
   fh:write("{\n")
   fh:write("   if(str == NULL)\n")
   fh:write("   {\n")
   fh:write("      bypWriteData(byp, sizeBytes, NULL, 0);\n")
   fh:write("   }\n")
   fh:write("   else\n")
   fh:write("   {\n")
   fh:write("      bypWriteData(byp, sizeBytes, str, strlen(str) + 1);\n")
   fh:write("   }\n")
   fh:write("}\n\n")
end

function ReadBytePackerStringFunction(fh, prefix)
   local functionName = string.format("%sReadString", prefix)
   fh:write("static inline\n")
   fh:write(string.format("const char * %s(struct bytepacker * byp, unsigned char sizeBytes)\n", functionName))
   fh:write("{\n")
   fh:write("   size_t size;\n")
   fh:write("   const char * str = bypReadData(byp, sizeBytes, &size);\n")
   fh:write("   if(size == 0 || bypBytesDropped(byp))\n")
   fh:write("   {\n")
   fh:write("      str = NULL;\n")
   fh:write("   }\n")
   fh:write("   return str;\n")
   fh:write("}\n\n")
end

function MessageCodeGeneratorC.generateBytePacker(outputDir, moduleName, prefix, enumerations, messages)
   local bitPacker = {
      includes = {"<common/CommonMessage.h>", "<common/BytePacker.h>"},
      structName = "bytepacker",
      readParam = "",
      overuseCheckFunctionName = "bypBytesDropped",
      sizeInBytesMember = "size",
      createUtilFunctions = function(fh, prefix)
         WriteBytePackerStringFunction(fh, prefix)
         ReadBytePackerStringFunction(fh, prefix)
      end,
      writeInt = function(prefix, packerVar, size, param)
         local supporteSize = GetSupportedSize(size)
         return string.format("bypWriteInt%d(%s, %s)", supporteSize, packerVar, param)
      end,
      readInt = function(prefix, packerVar, size)
         local supporteSize = GetSupportedSize(size)
         return string.format("bypReadInt%d(%s)", supporteSize, packerVar)      
      end,
      writeString = function(prefix, packerVar, size, param)
         local functionName = string.format("%sWriteString", prefix)
         local supporteSize = GetSupportedSize(size) / 8
         return string.format("%s(%s, %d, %s)", functionName, packerVar, supporteSize, param)               
      end,
      readString = function(prefix, packerVar, size)
         local functionName = string.format("%sReadString", prefix)
         local supporteSize = GetSupportedSize(size) / 8
         local stringReadFunctionName = prefix .. "ReadString"
         return string.format("%s(%s, %d)", stringReadFunctionName, packerVar, supporteSize)               
      end,
      writeBool = function(prefix, packerVar, param)
         return string.format("bypWriteBool(%s, %s)", packerVar, param)
      end,
      readBool = function(prefix, packerVar)
         return string.format("bypReadBool(%s)", packerVar)
      end
   }
   
   generate(outputDir, moduleName, prefix, enumerations, messages, bitPacker)
end

return MessageCodeGeneratorC
