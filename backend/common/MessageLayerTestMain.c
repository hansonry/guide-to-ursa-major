#include <stdio.h>
#include <string.h>

#include "TCPNetwork.h"
#include "FakeTCPNetwork.h"
#include "MessageLayer.h"

#define PORT "1234"

static void clientCallback(void * userData, 
                           struct messageLayer * layer, 
                           struct tcpNetworkSocket * socket, 
                           void * message, uint16_t messageSize)
{
   printf("Client Callback Size: %u\n", messageSize);
   printf("Message: %s\n", (char *)message);
}

static void serverCallback(void * userData, 
                           struct messageLayer * layer, 
                           struct tcpNetworkSocket * socket,  
                           void * message, uint16_t messageSize)
{
   printf("Server Callback Size: %u\n", messageSize);
   printf("Message: %s\n", (char*)message);
}

int main(int argc, char * args[])
{
   struct tcpNetwork * net;
   struct tcpNetworkServer * server;
   struct tcpNetworkSocket * clientSocket, * serverSocket;
   struct messageLayer * clientLayer, * serverLayer;
   const char * message = "Hello! Test!";
   
   net = tcpnCreateFakeNetwork();
   
   server = tcpnCreateServer(net, NULL, PORT);
   clientSocket = tcpnCreateClient(net, NULL, PORT);
   serverSocket = tcpnServerAccept(server);
   clientLayer = msglCreate(clientSocket, NULL, clientCallback);
   serverLayer = msglCreate(serverSocket, NULL, serverCallback);
   
   msglQueueToSend(clientLayer, message, strlen(message) + 1);
   msglPump(clientLayer);
   msglPump(serverLayer);
   
   msglQueueToSend(serverLayer, message, strlen(message) + 1);
   msglPump(serverLayer);
   msglPump(clientLayer);
      
   msglDestory(clientLayer);
   msglDestory(serverLayer);
   tcpnSocketDestroy(clientSocket);
   tcpnSocketDestroy(serverSocket);
   tcpnServerDestroy(server);
   tcpnDestroy(net);
   printf("End of Test\n");
   return 0;
}
