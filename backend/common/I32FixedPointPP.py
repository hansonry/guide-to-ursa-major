#!/usr/bin/env python3
# Start off with defining the printer as a Python object.

class I32FixedPointPrinter:
    # The constructor takes the value and stores it for later.
    def __init__(self, val):
        self.val = val


    def to_string(self):
        value = int(self.val['value'])
        # This appears to truncate out the character representation
        # kinda hacky
        shift = int(self.val['shift'])
        sign = value & 0x80000000
        mag = value & 0x7FFFFFFF
        if shift > 0:
            pow2 = float(1 << shift)
            floatValue = mag / pow2
        else:
            pow2 = float(1 << (-shift))
            floatValue = mag * pow2
        
        if sign:
            floatValue = -floatValue
        #print(floatValue)
        #print(value)
        #print(shift)
        return '{:f} {{ 0x{:08X}, {:d} }}'.format(floatValue, value, shift)

# Next, define the lookup function that returns the
# printer object when it receives a i32FixedPoint.
    
# The function takes the GDB value-type, which, in
# our example is used to look for the i32FixedPoint.

def my_pp_func(val):
    #print(val)
    if val.type.tag == 'i32FixedPoint': 
        return I32FixedPointPrinter(val)
    return None

# Finally, append the pretty-printer as object/ function to 
# the list of registered GDB printers.

gdb.pretty_printers.append(my_pp_func)

# Our pretty-printer is now available when we debug 
# the inferior program in GDB.