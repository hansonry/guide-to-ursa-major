#include <signal.h>
#include <stddef.h>

#include "SignalInterrupt.h"

static siCallback Callback = NULL;

static volatile bool Interrupted = false;

static void Handler(int s)
{
   (void)s;
   Interrupted = true;
   if(Callback != NULL)
   {
      Callback();
   }

}

void siSetup(siCallback callback)
{
   struct sigaction sigIntHandler;
   Callback = callback;
   Interrupted = false;

   sigIntHandler.sa_handler = Handler;
   sigemptyset(&sigIntHandler.sa_mask);
   sigIntHandler.sa_flags = SA_RESETHAND;

   sigaction(SIGINT, &sigIntHandler, NULL);
}

bool siNotInterrupted(void)
{
   return !Interrupted;
}
