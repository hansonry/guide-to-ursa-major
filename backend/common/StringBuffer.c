#include <stdlib.h>
#include <string.h>

#include "StringBuffer.h"
#include "List.h"

#define STRINGBUFFER_DEFAULT_GROWBY 32

struct stringBufferChunk
{
   char * base;
   size_t size;
};

LIST_CREATE_INLINE_FUNCTIONS(chunk, struct stringBufferChunk);

struct stringBuffer
{
   char * base;
   size_t size;
   size_t nextIndex;
   size_t growBy;
   struct list bufferChunkList;
};

struct stringBuffer * strbCreate(size_t growBy)
{
   struct stringBuffer * strb = malloc(sizeof(struct stringBuffer));
   listInitDefaults_chunk(&strb->bufferChunkList);
   strb->base      = NULL;
   strb->nextIndex = 0;
   strb->size      = 0;
   
   if(growBy == 0)
   {
      strb->growBy = STRINGBUFFER_DEFAULT_GROWBY;
   }
   else
   {
      strb->growBy = growBy;
   }
   return strb;
}

static inline size_t strbSumAndFreeAllChunks(struct stringBuffer * strb)
{
   size_t count, i;
   struct stringBufferChunk * chunks;
   size_t sum = 0;
   
   chunks = listGetArray_chunk(&strb->bufferChunkList, &count);
   for(i = 0; i < count; i++)
   {
      sum += chunks[i].size;
      free(chunks[i].base);
   }
   
   listClear(&strb->bufferChunkList);
   return sum;
}

void strbDestroy(struct stringBuffer * strb)
{
   (void)strbSumAndFreeAllChunks(strb);
   listFree(&strb->bufferChunkList);
   if(strb->base != NULL)
   {
      free(strb->base);
   }
   free(strb);
}

void strbClear(struct stringBuffer * strb)
{
   size_t usedSize = strb->nextIndex + strbSumAndFreeAllChunks(strb);
   if(usedSize > strb->size)
   {
      strb->size = usedSize + strb->growBy;
      strb->base = realloc(strb->base, strb->size);
   }
   strb->nextIndex = 0;
}


char * strbMalloc(struct stringBuffer * strb, size_t stringLength)
{
   size_t neededSize = stringLength + 1;
   size_t newNextIndex = strb->nextIndex + neededSize;
   char * mem;
   if(newNextIndex <= strb->size)
   {
      mem = &strb->base[strb->nextIndex];
      strb->nextIndex = newNextIndex;
   }
   else
   {
      struct stringBufferChunk * chunk = listAdd_chunk(&strb->bufferChunkList, NULL);
      chunk->size = neededSize;
      chunk->base = malloc(chunk->size);
      mem = chunk->base;
   }
   
   mem[stringLength] = '\0';
   return mem;
}
