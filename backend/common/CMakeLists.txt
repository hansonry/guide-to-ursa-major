cmake_minimum_required(VERSION 2.8.9)
project(GuideToUrsaMajorCommon)

# Pick Correct Network Source Files
if(WIN32 OR MINGW)
   set(NETWORK_SRC WinTCPNetwork.c)
   set(NETWORK_LIB Ws2_32)
else()
   set(NETWORK_SRC NixTCPNetwork.c)
   set(NETWORK_LIB )
endif()

# Pick Correct Time And Sleep Source Files
if(WIN32 OR MINGW)
   set(TIMEANDSLEEP_SRC WinTimeAndSleep.c)
else()
   set(TIMEANDSLEEP_SRC NixTimeAndSleep.c)
endif()

# Pick Correct Signal Interrupt Source Files
if(WIN32 OR MINGW)
   set(SIGNALINTERRUPT_SRC WinSignalInterrupt.c)
else()
   set(SIGNALINTERRUPT_SRC NixSignalInterrupt.c)
endif()

add_library(container STATIC
            Queue.c
            List.c
            StringBuffer.c
            )

add_library(fixedpoint STATIC 
            I32FixedPoint.c
            )

target_link_libraries(fixedpoint m)

add_library(console STATIC 
            ${TIMEANDSLEEP_SRC}
            ${SIGNALINTERRUPT_SRC}
            TimeAndSleep.c
            )

add_library(networking STATIC 
            ${NETWORK_SRC}
            BitPacker.c
            BytePacker.c
            MessageLayer.c
            FakeTCPNetwork.c
            )

add_library(simulation STATIC 
            FlightSimulation.c
            )
target_link_libraries(simulation fixedpoint)
            
target_link_libraries(networking  ${NETWORK_LIB} container)




# Test Exes

add_executable(TCPNetworkTest
               TCPNetworkTestMain.c)
target_link_libraries(TCPNetworkTest networking console)

add_executable(MessageLayerTest
               MessageLayerTestMain.c)
target_link_libraries(MessageLayerTest networking)

add_executable(I32FixedPointTest
               I32FixedPointTestMain.c)
target_link_libraries(I32FixedPointTest fixedpoint)
