local NetworkMessageConfig         = require("NetworkMessageConfig")
local MessageCodeGeneratorC        = require("MessageCodeGeneratorC")
local MessageCodeGeneratorGDScript = require("MessageCodeGeneratorGDScript")

-- Generate Server Messages
MessageCodeGeneratorC.generateBitPacker(".", "ServerMessage", "servmsg",
                                        NetworkMessageConfig.Server.Enumerations,
                                        NetworkMessageConfig.Server.Messages)
                                        
-- Generate Client Messages
MessageCodeGeneratorC.generateBytePacker("../client", "ClientMessage", "clmsg",
                                         NetworkMessageConfig.Client.Enumerations,
                                         NetworkMessageConfig.Client.Messages)

MessageCodeGeneratorGDScript.generate("../../frontend", "ClientMessage",
                                      NetworkMessageConfig.Client.Enumerations,
                                      NetworkMessageConfig.Client.Messages)
