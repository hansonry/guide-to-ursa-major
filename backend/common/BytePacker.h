#ifndef __BYTEPACKER_H__
#define __BYTEPACKER_H__
#include <stddef.h>
#include <stdbool.h>
#include <stdint.h>

struct bytepacker
{
   uint8_t * buffer;
   size_t size;
   size_t index;
};

void bypInit(struct bytepacker * byp, void * buffer, size_t size);

void bypWriteInt32(struct bytepacker * byp, uint32_t value);
void bypWriteInt16(struct bytepacker * byp, uint16_t value);
void bypWriteInt8(struct bytepacker * byp, uint8_t value);
void bypWriteBool(struct bytepacker * byp, bool value);
void bypWriteData(struct bytepacker * byp, unsigned char sizeBytes, const void * buffer, size_t bufferSize);

uint32_t bypReadInt32(struct bytepacker * byp);
uint16_t bypReadInt16(struct bytepacker * byp);
uint8_t bypReadInt8(struct bytepacker * byp);
bool bypReadBool(struct bytepacker * byp);
const void * bypReadData(struct bytepacker * byp, unsigned char sizeBytes, size_t * size);

bool bypBytesDropped(const struct bytepacker * byp);

size_t bypGetUsedBytes(const struct bytepacker * byp);
size_t bypGetDesiredBytes(const struct bytepacker * byp);

int bypSeek(struct bytepacker * byp, int offset);
int bypSkip(struct bytepacker * byp, size_t toSkip);

void bypAtomicStart(const struct bytepacker * byp, size_t * state);

bool bypAtomicCommit(struct bytepacker * byp, const size_t * state, size_t extraRoom);

void bypAtomicAbort(struct bytepacker * byp, const size_t * state);


/// bytepackerbuffer

struct bytepackerbuffer
{
   void * base;
   size_t size;
   size_t growBy;
   bool firstWrite;
};

void bypbInit(struct bytepackerbuffer * bypb, size_t growBy, size_t initSize);
void bypbFree(struct bytepackerbuffer * bypb);

bool bypbNeedsWrite(struct bytepackerbuffer * bypb, struct bytepacker * byp);

#endif // __BYTEPACKER_H__
