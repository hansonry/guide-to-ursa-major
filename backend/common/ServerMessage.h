#ifndef __SERVERMESSAGE_H__
#define __SERVERMESSAGE_H__
// This file is automaticaly generated from by MessageCodeGeneratorC.lua
#include <stdint.h>
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <stdbool.h>

#include "CommonMessage.h"
#include "BitPacker.h"
#include "StringBuffer.h"

// Message Types
#define SERVMSG_TYPE_GREETING 1
#define SERVMSG_TYPE_KEEPALIVE 2
#define SERVMSG_TYPE_AUTHREQUEST 3
#define SERVMSG_TYPE_AUTHRESPONSE 4
#define SERVMSG_TYPE_SYNC 5
#define SERVMSG_TYPE_ENTITYCREATE 6
#define SERVMSG_TYPE_ENTITYDESTROY 7
#define SERVMSG_TYPE_ENTITYUPDATE 8
#define SERVMSG_TYPE_ENTITYPLAYERCONTROL 9
#define SERVMSG_TYPE_ENTITYCOMMANDFREE 20
#define SERVMSG_TYPE_ENTITYCOMMANDSTOP 21
#define SERVMSG_TYPE_ENTITYCOMMANDVELOCITY 22
#define SERVMSG_TYPE_ENTITYCOMMANDPOSITION 23
#define SERVMSG_TYPE_ENTITYPLAYERCOMMANDFREE 40
#define SERVMSG_TYPE_ENTITYPLAYERCOMMANDSTOP 41
#define SERVMSG_TYPE_ENTITYPLAYERCOMMANDVELOCITY 42
#define SERVMSG_TYPE_ENTITYPLAYERCOMMANDPOSITION 43

// Enumerations
/// AuthRequestType
#define SERVMSG_AUTHREQUESTTYPE_LOGIN 1
#define SERVMSG_AUTHREQUESTTYPE_LOGOUT 2


/// AuthRequestType
static inline
const char * servmsgAuthRequestTypeEnumToString(uint8_t value)
{
   const char * name;
   switch(value)
   {
      case SERVMSG_AUTHREQUESTTYPE_LOGIN: name = "LogIn"; break;
      case SERVMSG_AUTHREQUESTTYPE_LOGOUT: name = "LogOut"; break;
      default: name = "!UNKNOWN!"; break;
   }
   return name;
}

static inline
char * servmsgReadString(struct bitpacker * bp, struct stringBuffer * strb, size_t sizeBitCount)
{
   size_t size = bpPeekByteArraySize(bp, sizeBitCount);
   char * buffer = strbMalloc(strb, size);
   (void)bpReadByteArray(bp, sizeBitCount, buffer, size);
   buffer[size] = '\0';
   return buffer;
}

// Messages
/// Greeting
struct servmsgGreeting
{
   uint16_t type;
};

static inline
void servmsgWriteGreeting(struct bitpacker * bp, const struct servmsgGreeting * msg)
{
   bpWriteInt(bp, 16, SERVMSG_TYPE_GREETING);
}

static inline
void servmsgReadGreeting(struct bitpacker * bp, struct stringBuffer * strb, struct servmsgGreeting * msg)
{
   msg->type = SERVMSG_TYPE_GREETING;
   (void)strb;
}

static inline
void servmsgDumpGreeting(const struct servmsgGreeting * msg)
{
   printf("Dumping ServerMessage \"Greeting\" 1:\n");
}

/// KeepAlive
struct servmsgKeepAlive
{
   uint16_t type;
};

static inline
void servmsgWriteKeepAlive(struct bitpacker * bp, const struct servmsgKeepAlive * msg)
{
   bpWriteInt(bp, 16, SERVMSG_TYPE_KEEPALIVE);
}

static inline
void servmsgReadKeepAlive(struct bitpacker * bp, struct stringBuffer * strb, struct servmsgKeepAlive * msg)
{
   msg->type = SERVMSG_TYPE_KEEPALIVE;
   (void)strb;
}

static inline
void servmsgDumpKeepAlive(const struct servmsgKeepAlive * msg)
{
   printf("Dumping ServerMessage \"KeepAlive\" 2:\n");
}

/// AuthRequest
struct servmsgAuthRequest
{
   uint16_t type;
   uint8_t requestType;
   const char * userName;
   const char * userPassword;
};

static inline
void servmsgWriteAuthRequest(struct bitpacker * bp, const struct servmsgAuthRequest * msg)
{
   bpWriteInt(bp, 16, SERVMSG_TYPE_AUTHREQUEST);
   bpWriteInt(bp, 8, msg->requestType);
   bpWriteByteArray(bp, 8, msg->userName, strlen(msg->userName));
   bpWriteByteArray(bp, 8, msg->userPassword, strlen(msg->userPassword));
}

static inline
void servmsgReadAuthRequest(struct bitpacker * bp, struct stringBuffer * strb, struct servmsgAuthRequest * msg)
{
   msg->type = SERVMSG_TYPE_AUTHREQUEST;
   msg->requestType = bpReadInt(bp, 8);
   msg->userName = servmsgReadString(bp, strb, 8);
   msg->userPassword = servmsgReadString(bp, strb, 8);
}

static inline
void servmsgDumpAuthRequest(const struct servmsgAuthRequest * msg)
{
   printf("Dumping ServerMessage \"AuthRequest\" 3:\n");
   printf("   requestType:\t\"%s\",\t%u\n", servmsgAuthRequestTypeEnumToString(msg->requestType), msg->requestType);
   printf("   userName:\t\"%s\",\t%lu\n", msg->userName, strlen(msg->userName));
   printf("   userPassword:\t\"%s\",\t%lu\n", msg->userPassword, strlen(msg->userPassword));
}

/// AuthResponse
struct servmsgAuthResponse
{
   uint16_t type;
   uint8_t requestType;
   bool success;
};

static inline
void servmsgWriteAuthResponse(struct bitpacker * bp, const struct servmsgAuthResponse * msg)
{
   bpWriteInt(bp, 16, SERVMSG_TYPE_AUTHRESPONSE);
   bpWriteInt(bp, 8, msg->requestType);
   bpWriteBit(bp, msg->success);
}

static inline
void servmsgReadAuthResponse(struct bitpacker * bp, struct stringBuffer * strb, struct servmsgAuthResponse * msg)
{
   msg->type = SERVMSG_TYPE_AUTHRESPONSE;
   msg->requestType = bpReadInt(bp, 8);
   msg->success = bpReadBit(bp);
   (void)strb;
}

static inline
void servmsgDumpAuthResponse(const struct servmsgAuthResponse * msg)
{
   printf("Dumping ServerMessage \"AuthResponse\" 4:\n");
   printf("   requestType:\t\"%s\",\t%u\n", servmsgAuthRequestTypeEnumToString(msg->requestType), msg->requestType);
   printf("   success:\t%s\n", msg->success ? "true" : "false");
}

/// Sync
struct servmsgSync
{
   uint16_t type;
   uint8_t step;
   uint8_t lastClientCmdIdProccessed;
   uint16_t nextStepTime_ms;
};

static inline
void servmsgWriteSync(struct bitpacker * bp, const struct servmsgSync * msg)
{
   bpWriteInt(bp, 16, SERVMSG_TYPE_SYNC);
   bpWriteInt(bp, 8, msg->step);
   bpWriteInt(bp, 8, msg->lastClientCmdIdProccessed);
   bpWriteInt(bp, 16, msg->nextStepTime_ms);
}

static inline
void servmsgReadSync(struct bitpacker * bp, struct stringBuffer * strb, struct servmsgSync * msg)
{
   msg->type = SERVMSG_TYPE_SYNC;
   msg->step = bpReadInt(bp, 8);
   msg->lastClientCmdIdProccessed = bpReadInt(bp, 8);
   msg->nextStepTime_ms = bpReadInt(bp, 16);
   (void)strb;
}

static inline
void servmsgDumpSync(const struct servmsgSync * msg)
{
   printf("Dumping ServerMessage \"Sync\" 5:\n");
   printf("   step:\t0x%X,\t%u\n", msg->step, msg->step);
   printf("   lastClientCmdIdProccessed:\t0x%X,\t%u\n", msg->lastClientCmdIdProccessed, msg->lastClientCmdIdProccessed);
   printf("   nextStepTime_ms:\t0x%X,\t%u\n", msg->nextStepTime_ms, msg->nextStepTime_ms);
}

/// EntityCreate
struct servmsgEntityCreate
{
   uint16_t type;
   uint32_t simId;
   uint32_t maxSpeed;
   uint32_t maxAcceleration;
   uint32_t stopDistance;
};

static inline
void servmsgWriteEntityCreate(struct bitpacker * bp, const struct servmsgEntityCreate * msg)
{
   bpWriteInt(bp, 16, SERVMSG_TYPE_ENTITYCREATE);
   bpWriteInt(bp, 32, msg->simId);
   bpWriteInt(bp, 32, msg->maxSpeed);
   bpWriteInt(bp, 32, msg->maxAcceleration);
   bpWriteInt(bp, 32, msg->stopDistance);
}

static inline
void servmsgReadEntityCreate(struct bitpacker * bp, struct stringBuffer * strb, struct servmsgEntityCreate * msg)
{
   msg->type = SERVMSG_TYPE_ENTITYCREATE;
   msg->simId = bpReadInt(bp, 32);
   msg->maxSpeed = bpReadInt(bp, 32);
   msg->maxAcceleration = bpReadInt(bp, 32);
   msg->stopDistance = bpReadInt(bp, 32);
   (void)strb;
}

static inline
void servmsgDumpEntityCreate(const struct servmsgEntityCreate * msg)
{
   printf("Dumping ServerMessage \"EntityCreate\" 6:\n");
   printf("   simId:\t0x%X,\t%u\n", msg->simId, msg->simId);
   printf("   maxSpeed:\t0x%X,\t%u\n", msg->maxSpeed, msg->maxSpeed);
   printf("   maxAcceleration:\t0x%X,\t%u\n", msg->maxAcceleration, msg->maxAcceleration);
   printf("   stopDistance:\t0x%X,\t%u\n", msg->stopDistance, msg->stopDistance);
}

/// EntityDestroy
struct servmsgEntityDestroy
{
   uint16_t type;
   uint32_t simId;
};

static inline
void servmsgWriteEntityDestroy(struct bitpacker * bp, const struct servmsgEntityDestroy * msg)
{
   bpWriteInt(bp, 16, SERVMSG_TYPE_ENTITYDESTROY);
   bpWriteInt(bp, 32, msg->simId);
}

static inline
void servmsgReadEntityDestroy(struct bitpacker * bp, struct stringBuffer * strb, struct servmsgEntityDestroy * msg)
{
   msg->type = SERVMSG_TYPE_ENTITYDESTROY;
   msg->simId = bpReadInt(bp, 32);
   (void)strb;
}

static inline
void servmsgDumpEntityDestroy(const struct servmsgEntityDestroy * msg)
{
   printf("Dumping ServerMessage \"EntityDestroy\" 7:\n");
   printf("   simId:\t0x%X,\t%u\n", msg->simId, msg->simId);
}

/// EntityUpdate
struct servmsgEntityUpdate
{
   uint16_t type;
   uint32_t simId;
   struct msgI32Vector3 position;
   struct msgI32Vector3 velocity;
   struct msgI32Vector4 rotation;
   struct msgI32Vector3 rotationalVelocity;
};

static inline
void servmsgWriteEntityUpdate(struct bitpacker * bp, const struct servmsgEntityUpdate * msg)
{
   bpWriteInt(bp, 16, SERVMSG_TYPE_ENTITYUPDATE);
   bpWriteInt(bp, 32, msg->simId);
   bpWriteInt(bp, 32, msg->position.x);
   bpWriteInt(bp, 32, msg->position.y);
   bpWriteInt(bp, 32, msg->position.z);
   bpWriteInt(bp, 32, msg->velocity.x);
   bpWriteInt(bp, 32, msg->velocity.y);
   bpWriteInt(bp, 32, msg->velocity.z);
   bpWriteInt(bp, 32, msg->rotation.w);
   bpWriteInt(bp, 32, msg->rotation.x);
   bpWriteInt(bp, 32, msg->rotation.y);
   bpWriteInt(bp, 32, msg->rotation.z);
   bpWriteInt(bp, 32, msg->rotationalVelocity.x);
   bpWriteInt(bp, 32, msg->rotationalVelocity.y);
   bpWriteInt(bp, 32, msg->rotationalVelocity.z);
}

static inline
void servmsgReadEntityUpdate(struct bitpacker * bp, struct stringBuffer * strb, struct servmsgEntityUpdate * msg)
{
   msg->type = SERVMSG_TYPE_ENTITYUPDATE;
   msg->simId = bpReadInt(bp, 32);
   msg->position.x = bpReadInt(bp, 32);
   msg->position.y = bpReadInt(bp, 32);
   msg->position.z = bpReadInt(bp, 32);
   msg->velocity.x = bpReadInt(bp, 32);
   msg->velocity.y = bpReadInt(bp, 32);
   msg->velocity.z = bpReadInt(bp, 32);
   msg->rotation.w = bpReadInt(bp, 32);
   msg->rotation.x = bpReadInt(bp, 32);
   msg->rotation.y = bpReadInt(bp, 32);
   msg->rotation.z = bpReadInt(bp, 32);
   msg->rotationalVelocity.x = bpReadInt(bp, 32);
   msg->rotationalVelocity.y = bpReadInt(bp, 32);
   msg->rotationalVelocity.z = bpReadInt(bp, 32);
   (void)strb;
}

static inline
void servmsgDumpEntityUpdate(const struct servmsgEntityUpdate * msg)
{
   printf("Dumping ServerMessage \"EntityUpdate\" 8:\n");
   printf("   simId:\t0x%X,\t%u\n", msg->simId, msg->simId);
   printf("   position.x:\t0x%X,\t%u\n", msg->position.x, msg->position.x);
   printf("   position.y:\t0x%X,\t%u\n", msg->position.y, msg->position.y);
   printf("   position.z:\t0x%X,\t%u\n", msg->position.z, msg->position.z);
   printf("   velocity.x:\t0x%X,\t%u\n", msg->velocity.x, msg->velocity.x);
   printf("   velocity.y:\t0x%X,\t%u\n", msg->velocity.y, msg->velocity.y);
   printf("   velocity.z:\t0x%X,\t%u\n", msg->velocity.z, msg->velocity.z);
   printf("   rotation.w:\t0x%X,\t%u\n", msg->rotation.w, msg->rotation.w);
   printf("   rotation.x:\t0x%X,\t%u\n", msg->rotation.x, msg->rotation.x);
   printf("   rotation.y:\t0x%X,\t%u\n", msg->rotation.y, msg->rotation.y);
   printf("   rotation.z:\t0x%X,\t%u\n", msg->rotation.z, msg->rotation.z);
   printf("   rotationalVelocity.x:\t0x%X,\t%u\n", msg->rotationalVelocity.x, msg->rotationalVelocity.x);
   printf("   rotationalVelocity.y:\t0x%X,\t%u\n", msg->rotationalVelocity.y, msg->rotationalVelocity.y);
   printf("   rotationalVelocity.z:\t0x%X,\t%u\n", msg->rotationalVelocity.z, msg->rotationalVelocity.z);
}

/// EntityPlayerControl
struct servmsgEntityPlayerControl
{
   uint16_t type;
   uint32_t simId;
   bool active;
};

static inline
void servmsgWriteEntityPlayerControl(struct bitpacker * bp, const struct servmsgEntityPlayerControl * msg)
{
   bpWriteInt(bp, 16, SERVMSG_TYPE_ENTITYPLAYERCONTROL);
   bpWriteInt(bp, 32, msg->simId);
   bpWriteBit(bp, msg->active);
}

static inline
void servmsgReadEntityPlayerControl(struct bitpacker * bp, struct stringBuffer * strb, struct servmsgEntityPlayerControl * msg)
{
   msg->type = SERVMSG_TYPE_ENTITYPLAYERCONTROL;
   msg->simId = bpReadInt(bp, 32);
   msg->active = bpReadBit(bp);
   (void)strb;
}

static inline
void servmsgDumpEntityPlayerControl(const struct servmsgEntityPlayerControl * msg)
{
   printf("Dumping ServerMessage \"EntityPlayerControl\" 9:\n");
   printf("   simId:\t0x%X,\t%u\n", msg->simId, msg->simId);
   printf("   active:\t%s\n", msg->active ? "true" : "false");
}

/// EntityCommandFree
struct servmsgEntityCommandFree
{
   uint16_t type;
   uint32_t simId;
};

static inline
void servmsgWriteEntityCommandFree(struct bitpacker * bp, const struct servmsgEntityCommandFree * msg)
{
   bpWriteInt(bp, 16, SERVMSG_TYPE_ENTITYCOMMANDFREE);
   bpWriteInt(bp, 32, msg->simId);
}

static inline
void servmsgReadEntityCommandFree(struct bitpacker * bp, struct stringBuffer * strb, struct servmsgEntityCommandFree * msg)
{
   msg->type = SERVMSG_TYPE_ENTITYCOMMANDFREE;
   msg->simId = bpReadInt(bp, 32);
   (void)strb;
}

static inline
void servmsgDumpEntityCommandFree(const struct servmsgEntityCommandFree * msg)
{
   printf("Dumping ServerMessage \"EntityCommandFree\" 20:\n");
   printf("   simId:\t0x%X,\t%u\n", msg->simId, msg->simId);
}

/// EntityCommandStop
struct servmsgEntityCommandStop
{
   uint16_t type;
   uint32_t simId;
};

static inline
void servmsgWriteEntityCommandStop(struct bitpacker * bp, const struct servmsgEntityCommandStop * msg)
{
   bpWriteInt(bp, 16, SERVMSG_TYPE_ENTITYCOMMANDSTOP);
   bpWriteInt(bp, 32, msg->simId);
}

static inline
void servmsgReadEntityCommandStop(struct bitpacker * bp, struct stringBuffer * strb, struct servmsgEntityCommandStop * msg)
{
   msg->type = SERVMSG_TYPE_ENTITYCOMMANDSTOP;
   msg->simId = bpReadInt(bp, 32);
   (void)strb;
}

static inline
void servmsgDumpEntityCommandStop(const struct servmsgEntityCommandStop * msg)
{
   printf("Dumping ServerMessage \"EntityCommandStop\" 21:\n");
   printf("   simId:\t0x%X,\t%u\n", msg->simId, msg->simId);
}

/// EntityCommandVelocity
struct servmsgEntityCommandVelocity
{
   uint16_t type;
   struct msgI32Vector3 targetVelocity;
   uint32_t simId;
};

static inline
void servmsgWriteEntityCommandVelocity(struct bitpacker * bp, const struct servmsgEntityCommandVelocity * msg)
{
   bpWriteInt(bp, 16, SERVMSG_TYPE_ENTITYCOMMANDVELOCITY);
   bpWriteInt(bp, 32, msg->targetVelocity.x);
   bpWriteInt(bp, 32, msg->targetVelocity.y);
   bpWriteInt(bp, 32, msg->targetVelocity.z);
   bpWriteInt(bp, 32, msg->simId);
}

static inline
void servmsgReadEntityCommandVelocity(struct bitpacker * bp, struct stringBuffer * strb, struct servmsgEntityCommandVelocity * msg)
{
   msg->type = SERVMSG_TYPE_ENTITYCOMMANDVELOCITY;
   msg->targetVelocity.x = bpReadInt(bp, 32);
   msg->targetVelocity.y = bpReadInt(bp, 32);
   msg->targetVelocity.z = bpReadInt(bp, 32);
   msg->simId = bpReadInt(bp, 32);
   (void)strb;
}

static inline
void servmsgDumpEntityCommandVelocity(const struct servmsgEntityCommandVelocity * msg)
{
   printf("Dumping ServerMessage \"EntityCommandVelocity\" 22:\n");
   printf("   targetVelocity.x:\t0x%X,\t%u\n", msg->targetVelocity.x, msg->targetVelocity.x);
   printf("   targetVelocity.y:\t0x%X,\t%u\n", msg->targetVelocity.y, msg->targetVelocity.y);
   printf("   targetVelocity.z:\t0x%X,\t%u\n", msg->targetVelocity.z, msg->targetVelocity.z);
   printf("   simId:\t0x%X,\t%u\n", msg->simId, msg->simId);
}

/// EntityCommandPosition
struct servmsgEntityCommandPosition
{
   uint16_t type;
   struct msgI32Vector3 targetPosition;
   uint32_t simId;
};

static inline
void servmsgWriteEntityCommandPosition(struct bitpacker * bp, const struct servmsgEntityCommandPosition * msg)
{
   bpWriteInt(bp, 16, SERVMSG_TYPE_ENTITYCOMMANDPOSITION);
   bpWriteInt(bp, 32, msg->targetPosition.x);
   bpWriteInt(bp, 32, msg->targetPosition.y);
   bpWriteInt(bp, 32, msg->targetPosition.z);
   bpWriteInt(bp, 32, msg->simId);
}

static inline
void servmsgReadEntityCommandPosition(struct bitpacker * bp, struct stringBuffer * strb, struct servmsgEntityCommandPosition * msg)
{
   msg->type = SERVMSG_TYPE_ENTITYCOMMANDPOSITION;
   msg->targetPosition.x = bpReadInt(bp, 32);
   msg->targetPosition.y = bpReadInt(bp, 32);
   msg->targetPosition.z = bpReadInt(bp, 32);
   msg->simId = bpReadInt(bp, 32);
   (void)strb;
}

static inline
void servmsgDumpEntityCommandPosition(const struct servmsgEntityCommandPosition * msg)
{
   printf("Dumping ServerMessage \"EntityCommandPosition\" 23:\n");
   printf("   targetPosition.x:\t0x%X,\t%u\n", msg->targetPosition.x, msg->targetPosition.x);
   printf("   targetPosition.y:\t0x%X,\t%u\n", msg->targetPosition.y, msg->targetPosition.y);
   printf("   targetPosition.z:\t0x%X,\t%u\n", msg->targetPosition.z, msg->targetPosition.z);
   printf("   simId:\t0x%X,\t%u\n", msg->simId, msg->simId);
}

/// EntityPlayerCommandFree
struct servmsgEntityPlayerCommandFree
{
   uint16_t type;
   uint8_t id;
};

static inline
void servmsgWriteEntityPlayerCommandFree(struct bitpacker * bp, const struct servmsgEntityPlayerCommandFree * msg)
{
   bpWriteInt(bp, 16, SERVMSG_TYPE_ENTITYPLAYERCOMMANDFREE);
   bpWriteInt(bp, 8, msg->id);
}

static inline
void servmsgReadEntityPlayerCommandFree(struct bitpacker * bp, struct stringBuffer * strb, struct servmsgEntityPlayerCommandFree * msg)
{
   msg->type = SERVMSG_TYPE_ENTITYPLAYERCOMMANDFREE;
   msg->id = bpReadInt(bp, 8);
   (void)strb;
}

static inline
void servmsgDumpEntityPlayerCommandFree(const struct servmsgEntityPlayerCommandFree * msg)
{
   printf("Dumping ServerMessage \"EntityPlayerCommandFree\" 40:\n");
   printf("   id:\t0x%X,\t%u\n", msg->id, msg->id);
}

/// EntityPlayerCommandStop
struct servmsgEntityPlayerCommandStop
{
   uint16_t type;
   uint8_t id;
};

static inline
void servmsgWriteEntityPlayerCommandStop(struct bitpacker * bp, const struct servmsgEntityPlayerCommandStop * msg)
{
   bpWriteInt(bp, 16, SERVMSG_TYPE_ENTITYPLAYERCOMMANDSTOP);
   bpWriteInt(bp, 8, msg->id);
}

static inline
void servmsgReadEntityPlayerCommandStop(struct bitpacker * bp, struct stringBuffer * strb, struct servmsgEntityPlayerCommandStop * msg)
{
   msg->type = SERVMSG_TYPE_ENTITYPLAYERCOMMANDSTOP;
   msg->id = bpReadInt(bp, 8);
   (void)strb;
}

static inline
void servmsgDumpEntityPlayerCommandStop(const struct servmsgEntityPlayerCommandStop * msg)
{
   printf("Dumping ServerMessage \"EntityPlayerCommandStop\" 41:\n");
   printf("   id:\t0x%X,\t%u\n", msg->id, msg->id);
}

/// EntityPlayerCommandVelocity
struct servmsgEntityPlayerCommandVelocity
{
   uint16_t type;
   struct msgI32Vector3 targetVelocity;
   uint8_t id;
};

static inline
void servmsgWriteEntityPlayerCommandVelocity(struct bitpacker * bp, const struct servmsgEntityPlayerCommandVelocity * msg)
{
   bpWriteInt(bp, 16, SERVMSG_TYPE_ENTITYPLAYERCOMMANDVELOCITY);
   bpWriteInt(bp, 32, msg->targetVelocity.x);
   bpWriteInt(bp, 32, msg->targetVelocity.y);
   bpWriteInt(bp, 32, msg->targetVelocity.z);
   bpWriteInt(bp, 8, msg->id);
}

static inline
void servmsgReadEntityPlayerCommandVelocity(struct bitpacker * bp, struct stringBuffer * strb, struct servmsgEntityPlayerCommandVelocity * msg)
{
   msg->type = SERVMSG_TYPE_ENTITYPLAYERCOMMANDVELOCITY;
   msg->targetVelocity.x = bpReadInt(bp, 32);
   msg->targetVelocity.y = bpReadInt(bp, 32);
   msg->targetVelocity.z = bpReadInt(bp, 32);
   msg->id = bpReadInt(bp, 8);
   (void)strb;
}

static inline
void servmsgDumpEntityPlayerCommandVelocity(const struct servmsgEntityPlayerCommandVelocity * msg)
{
   printf("Dumping ServerMessage \"EntityPlayerCommandVelocity\" 42:\n");
   printf("   targetVelocity.x:\t0x%X,\t%u\n", msg->targetVelocity.x, msg->targetVelocity.x);
   printf("   targetVelocity.y:\t0x%X,\t%u\n", msg->targetVelocity.y, msg->targetVelocity.y);
   printf("   targetVelocity.z:\t0x%X,\t%u\n", msg->targetVelocity.z, msg->targetVelocity.z);
   printf("   id:\t0x%X,\t%u\n", msg->id, msg->id);
}

/// EntityPlayerCommandPosition
struct servmsgEntityPlayerCommandPosition
{
   uint16_t type;
   struct msgI32Vector3 targetPosition;
   uint8_t id;
};

static inline
void servmsgWriteEntityPlayerCommandPosition(struct bitpacker * bp, const struct servmsgEntityPlayerCommandPosition * msg)
{
   bpWriteInt(bp, 16, SERVMSG_TYPE_ENTITYPLAYERCOMMANDPOSITION);
   bpWriteInt(bp, 32, msg->targetPosition.x);
   bpWriteInt(bp, 32, msg->targetPosition.y);
   bpWriteInt(bp, 32, msg->targetPosition.z);
   bpWriteInt(bp, 8, msg->id);
}

static inline
void servmsgReadEntityPlayerCommandPosition(struct bitpacker * bp, struct stringBuffer * strb, struct servmsgEntityPlayerCommandPosition * msg)
{
   msg->type = SERVMSG_TYPE_ENTITYPLAYERCOMMANDPOSITION;
   msg->targetPosition.x = bpReadInt(bp, 32);
   msg->targetPosition.y = bpReadInt(bp, 32);
   msg->targetPosition.z = bpReadInt(bp, 32);
   msg->id = bpReadInt(bp, 8);
   (void)strb;
}

static inline
void servmsgDumpEntityPlayerCommandPosition(const struct servmsgEntityPlayerCommandPosition * msg)
{
   printf("Dumping ServerMessage \"EntityPlayerCommandPosition\" 43:\n");
   printf("   targetPosition.x:\t0x%X,\t%u\n", msg->targetPosition.x, msg->targetPosition.x);
   printf("   targetPosition.y:\t0x%X,\t%u\n", msg->targetPosition.y, msg->targetPosition.y);
   printf("   targetPosition.z:\t0x%X,\t%u\n", msg->targetPosition.z, msg->targetPosition.z);
   printf("   id:\t0x%X,\t%u\n", msg->id, msg->id);
}

union serverMessage
{
   uint16_t type;
   struct servmsgGreeting greeting;
   struct servmsgKeepAlive keepAlive;
   struct servmsgAuthRequest authRequest;
   struct servmsgAuthResponse authResponse;
   struct servmsgSync sync;
   struct servmsgEntityCreate entityCreate;
   struct servmsgEntityDestroy entityDestroy;
   struct servmsgEntityUpdate entityUpdate;
   struct servmsgEntityPlayerControl entityPlayerControl;
   struct servmsgEntityCommandFree entityCommandFree;
   struct servmsgEntityCommandStop entityCommandStop;
   struct servmsgEntityCommandVelocity entityCommandVelocity;
   struct servmsgEntityCommandPosition entityCommandPosition;
   struct servmsgEntityPlayerCommandFree entityPlayerCommandFree;
   struct servmsgEntityPlayerCommandStop entityPlayerCommandStop;
   struct servmsgEntityPlayerCommandVelocity entityPlayerCommandVelocity;
   struct servmsgEntityPlayerCommandPosition entityPlayerCommandPosition;
};

static inline
bool servmsgWrite(struct bitpacker * bp, const union serverMessage * msg, bool sizeCheck)
{
   bool success = true;
   switch(msg->type)
   {
      case SERVMSG_TYPE_GREETING: servmsgWriteGreeting(bp, &msg->greeting); break;
      case SERVMSG_TYPE_KEEPALIVE: servmsgWriteKeepAlive(bp, &msg->keepAlive); break;
      case SERVMSG_TYPE_AUTHREQUEST: servmsgWriteAuthRequest(bp, &msg->authRequest); break;
      case SERVMSG_TYPE_AUTHRESPONSE: servmsgWriteAuthResponse(bp, &msg->authResponse); break;
      case SERVMSG_TYPE_SYNC: servmsgWriteSync(bp, &msg->sync); break;
      case SERVMSG_TYPE_ENTITYCREATE: servmsgWriteEntityCreate(bp, &msg->entityCreate); break;
      case SERVMSG_TYPE_ENTITYDESTROY: servmsgWriteEntityDestroy(bp, &msg->entityDestroy); break;
      case SERVMSG_TYPE_ENTITYUPDATE: servmsgWriteEntityUpdate(bp, &msg->entityUpdate); break;
      case SERVMSG_TYPE_ENTITYPLAYERCONTROL: servmsgWriteEntityPlayerControl(bp, &msg->entityPlayerControl); break;
      case SERVMSG_TYPE_ENTITYCOMMANDFREE: servmsgWriteEntityCommandFree(bp, &msg->entityCommandFree); break;
      case SERVMSG_TYPE_ENTITYCOMMANDSTOP: servmsgWriteEntityCommandStop(bp, &msg->entityCommandStop); break;
      case SERVMSG_TYPE_ENTITYCOMMANDVELOCITY: servmsgWriteEntityCommandVelocity(bp, &msg->entityCommandVelocity); break;
      case SERVMSG_TYPE_ENTITYCOMMANDPOSITION: servmsgWriteEntityCommandPosition(bp, &msg->entityCommandPosition); break;
      case SERVMSG_TYPE_ENTITYPLAYERCOMMANDFREE: servmsgWriteEntityPlayerCommandFree(bp, &msg->entityPlayerCommandFree); break;
      case SERVMSG_TYPE_ENTITYPLAYERCOMMANDSTOP: servmsgWriteEntityPlayerCommandStop(bp, &msg->entityPlayerCommandStop); break;
      case SERVMSG_TYPE_ENTITYPLAYERCOMMANDVELOCITY: servmsgWriteEntityPlayerCommandVelocity(bp, &msg->entityPlayerCommandVelocity); break;
      case SERVMSG_TYPE_ENTITYPLAYERCOMMANDPOSITION: servmsgWriteEntityPlayerCommandPosition(bp, &msg->entityPlayerCommandPosition); break;
      default:
         success = false;
         fprintf(stderr, "%s: %d: error: ServerMessage Type %u is not a valid message type\n", __FILE__, __LINE__, msg->type);
         break;
   }

   if(success && sizeCheck && bpHasDroppedBits(bp))
   {
      success = false;
      fprintf(stderr, "%s: %d: error: ServerMessage Type %u is larger than provided Buffer: %lu\n", __FILE__, __LINE__, msg->type, bp->sizeInBytes);
   }
   return success;
}

static inline
bool servmsgRead(struct bitpacker * bp, struct stringBuffer * strb, union serverMessage * msg)
{
   bool success = true;
   uint16_t type = bpReadInt(bp, 16);
   switch(type)
   {
      case SERVMSG_TYPE_GREETING: servmsgReadGreeting(bp, strb, &msg->greeting); break;
      case SERVMSG_TYPE_KEEPALIVE: servmsgReadKeepAlive(bp, strb, &msg->keepAlive); break;
      case SERVMSG_TYPE_AUTHREQUEST: servmsgReadAuthRequest(bp, strb, &msg->authRequest); break;
      case SERVMSG_TYPE_AUTHRESPONSE: servmsgReadAuthResponse(bp, strb, &msg->authResponse); break;
      case SERVMSG_TYPE_SYNC: servmsgReadSync(bp, strb, &msg->sync); break;
      case SERVMSG_TYPE_ENTITYCREATE: servmsgReadEntityCreate(bp, strb, &msg->entityCreate); break;
      case SERVMSG_TYPE_ENTITYDESTROY: servmsgReadEntityDestroy(bp, strb, &msg->entityDestroy); break;
      case SERVMSG_TYPE_ENTITYUPDATE: servmsgReadEntityUpdate(bp, strb, &msg->entityUpdate); break;
      case SERVMSG_TYPE_ENTITYPLAYERCONTROL: servmsgReadEntityPlayerControl(bp, strb, &msg->entityPlayerControl); break;
      case SERVMSG_TYPE_ENTITYCOMMANDFREE: servmsgReadEntityCommandFree(bp, strb, &msg->entityCommandFree); break;
      case SERVMSG_TYPE_ENTITYCOMMANDSTOP: servmsgReadEntityCommandStop(bp, strb, &msg->entityCommandStop); break;
      case SERVMSG_TYPE_ENTITYCOMMANDVELOCITY: servmsgReadEntityCommandVelocity(bp, strb, &msg->entityCommandVelocity); break;
      case SERVMSG_TYPE_ENTITYCOMMANDPOSITION: servmsgReadEntityCommandPosition(bp, strb, &msg->entityCommandPosition); break;
      case SERVMSG_TYPE_ENTITYPLAYERCOMMANDFREE: servmsgReadEntityPlayerCommandFree(bp, strb, &msg->entityPlayerCommandFree); break;
      case SERVMSG_TYPE_ENTITYPLAYERCOMMANDSTOP: servmsgReadEntityPlayerCommandStop(bp, strb, &msg->entityPlayerCommandStop); break;
      case SERVMSG_TYPE_ENTITYPLAYERCOMMANDVELOCITY: servmsgReadEntityPlayerCommandVelocity(bp, strb, &msg->entityPlayerCommandVelocity); break;
      case SERVMSG_TYPE_ENTITYPLAYERCOMMANDPOSITION: servmsgReadEntityPlayerCommandPosition(bp, strb, &msg->entityPlayerCommandPosition); break;
      default:
         success = false;
         fprintf(stderr, "%s: %d: error: ServerMessage Type %u is not a valid message type\n", __FILE__, __LINE__, type);
         break;
   }

   if(success && bpHasDroppedBits(bp))
   {
      success = false;
      fprintf(stderr, "%s: %d: error: ServerMessage Type %u is larger than provided Buffer: %lu\n", __FILE__, __LINE__, type, bp->sizeInBytes);
   }
   return success;
}

static inline
void servmsgDump(const union serverMessage * msg)
{
   switch(msg->type)
   {
      case SERVMSG_TYPE_GREETING: servmsgDumpGreeting(&msg->greeting); break;
      case SERVMSG_TYPE_KEEPALIVE: servmsgDumpKeepAlive(&msg->keepAlive); break;
      case SERVMSG_TYPE_AUTHREQUEST: servmsgDumpAuthRequest(&msg->authRequest); break;
      case SERVMSG_TYPE_AUTHRESPONSE: servmsgDumpAuthResponse(&msg->authResponse); break;
      case SERVMSG_TYPE_SYNC: servmsgDumpSync(&msg->sync); break;
      case SERVMSG_TYPE_ENTITYCREATE: servmsgDumpEntityCreate(&msg->entityCreate); break;
      case SERVMSG_TYPE_ENTITYDESTROY: servmsgDumpEntityDestroy(&msg->entityDestroy); break;
      case SERVMSG_TYPE_ENTITYUPDATE: servmsgDumpEntityUpdate(&msg->entityUpdate); break;
      case SERVMSG_TYPE_ENTITYPLAYERCONTROL: servmsgDumpEntityPlayerControl(&msg->entityPlayerControl); break;
      case SERVMSG_TYPE_ENTITYCOMMANDFREE: servmsgDumpEntityCommandFree(&msg->entityCommandFree); break;
      case SERVMSG_TYPE_ENTITYCOMMANDSTOP: servmsgDumpEntityCommandStop(&msg->entityCommandStop); break;
      case SERVMSG_TYPE_ENTITYCOMMANDVELOCITY: servmsgDumpEntityCommandVelocity(&msg->entityCommandVelocity); break;
      case SERVMSG_TYPE_ENTITYCOMMANDPOSITION: servmsgDumpEntityCommandPosition(&msg->entityCommandPosition); break;
      case SERVMSG_TYPE_ENTITYPLAYERCOMMANDFREE: servmsgDumpEntityPlayerCommandFree(&msg->entityPlayerCommandFree); break;
      case SERVMSG_TYPE_ENTITYPLAYERCOMMANDSTOP: servmsgDumpEntityPlayerCommandStop(&msg->entityPlayerCommandStop); break;
      case SERVMSG_TYPE_ENTITYPLAYERCOMMANDVELOCITY: servmsgDumpEntityPlayerCommandVelocity(&msg->entityPlayerCommandVelocity); break;
      case SERVMSG_TYPE_ENTITYPLAYERCOMMANDPOSITION: servmsgDumpEntityPlayerCommandPosition(&msg->entityPlayerCommandPosition); break;
      default:
         fprintf(stderr, "%s: %d: warning: ServerMessage Type %u is not a valid message type\n", __FILE__, __LINE__, msg->type);
         break;
   }
}

#endif // __SERVERMESSAGE_H__
