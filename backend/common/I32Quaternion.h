#ifndef __I32QUATERNION_H__
#define __I32QUATERNION_H__
#include <stdint.h>
#include <math.h>

#include "I32FixedPoint.h"
#include "I32Vector3.h"

struct i32Quat
{
   int32_t w;
   int32_t x;
   int32_t y;
   int32_t z;
   int8_t shift;
};

static inline
struct i32Quat * i32QuatSetI(struct i32Quat * quat, int32_t w, int32_t x, int32_t y, int32_t z, int8_t shift)
{
   quat->w = w;
   quat->x = x;
   quat->y = y;
   quat->z = z;
   quat->shift = shift;
   return quat;
}

static inline
struct i32Quat * i32QuatSetV(struct i32Quat * quat, int32_t w, const struct i32Vector3 * v)
{
   quat->w = w;
   quat->x = v->x;
   quat->y = v->y;
   quat->z = v->z;
   quat->shift = v->shift;
   return quat;
}

static inline
struct i32Quat * i32QuatSetIdentity(struct i32Quat * quat, int8_t shift)
{
   quat->w = i32fpSetI(1, shift);
   quat->x = 0;
   quat->y = 0;
   quat->z = 0;
   quat->shift = shift;
   return quat;
}

static inline
struct i32Quat * i32QuatSetF(struct i32Quat * quat, float w, float x, float y, float z, int8_t shift)
{
   quat->shift = shift;
   quat->w = i32fpSetD(w, shift);
   quat->x = i32fpSetD(x, shift);
   quat->y = i32fpSetD(y, shift);
   quat->z = i32fpSetD(z, shift);
   return quat;
}

static inline
struct i32Quat * i32QuatSetQ(struct i32Quat * dest, const struct i32Quat * src)
{
   dest->w     = src->w;
   dest->x     = src->x;
   dest->y     = src->y;
   dest->z     = src->z;
   dest->shift = src->shift;
   return dest;
}

static inline 
void i32QuatGetFp(const struct i32Quat * q, struct i32FixedPoint * w, 
                                            struct i32FixedPoint * x, 
                                            struct i32FixedPoint * y, 
                                            struct i32FixedPoint * z)
{
   if(w != NULL)
   {
      i32fpSetS(w, q->w, q->shift);
   }
   if(x != NULL)
   {
      i32fpSetS(x, q->x, q->shift);
   }
   if(y != NULL)
   {
      i32fpSetS(y, q->y, q->shift);
   }
   if(z != NULL)
   {
      i32fpSetS(z, q->z, q->shift);
   }
}

static inline 
void i32QuatGetFpA(const struct i32Quat * q, struct i32FixedPoint * fpv)
{
   i32fpSetS(&fpv[0], q->w, q->shift);
   i32fpSetS(&fpv[1], q->x, q->shift);
   i32fpSetS(&fpv[2], q->y, q->shift);
   i32fpSetS(&fpv[3], q->z, q->shift);
}

static inline 
struct i32Quat * i32QuatSetFpA(struct i32Quat * q, struct i32FixedPoint * fpv)
{
   i32fpSetToCommonShift(fpv, 4, 0);
   q->shift = fpv[0].shift;
   q->w     = fpv[0].value;
   q->x     = fpv[1].value;
   q->y     = fpv[2].value;
   q->z     = fpv[3].value;
   return q;
}

static inline 
struct i32Quat * i32QuatSeFitFpA(struct i32Quat * q, struct i32FixedPoint * fpv, int8_t shift)
{
   i32fpSetToCommonShiftFit(fpv, 4, shift);
   q->shift = shift;
   q->w     = fpv[0].value;
   q->x     = fpv[1].value;
   q->y     = fpv[2].value;
   q->z     = fpv[3].value;
   return q;
}


static inline
struct i32Quat * i32QuatSetAxisAngleF(struct i32Quat * quat, float axis_x, float axis_y, float axis_z, float angle, int8_t shift)
{
   double halfAngle = angle / 2;
   double hs = sin(halfAngle);
   quat->w     = i32fpSetD(cos(halfAngle), shift);
   quat->x     = i32fpSetD(axis_x * hs,    shift);
   quat->y     = i32fpSetD(axis_y * hs,    shift);
   quat->z     = i32fpSetD(axis_z * hs,    shift);
   quat->shift = shift;
   return quat;
}

static inline
struct i32Quat * i32QuatSetAxisAngleV(struct i32Quat * quat, const struct i32Vector3 * axis, const struct i32FixedPoint * angle)
{
   struct i32FixedPoint fps, fpc, halfAngle, v[4];
   
   // halfAngle = angle / 2
   i32fpCopy(&halfAngle, angle);
   halfAngle.shift += 1;
   
   
   i32fpCosSin(&halfAngle, &fpc, &fps);
   
   i32v3GetFpA(axis, &v[1]);
   
   i32fpCopy(&v[0], &fpc);
   i32fpMultiplyS(&v[1], &fps);
   i32fpMultiplyS(&v[2], &fps);
   i32fpMultiplyS(&v[3], &fps);
   
   return i32QuatSetFpA(quat, v);
}

static inline 
struct i32Quat * i32QuatSetShift(struct i32Quat * quat, int8_t newShift)
{
   quat->w = i32fpSetShift(quat->w, quat->shift, newShift);
   quat->x = i32fpSetShift(quat->x, quat->shift, newShift);
   quat->y = i32fpSetShift(quat->y, quat->shift, newShift);
   quat->z = i32fpSetShift(quat->z, quat->shift, newShift);
   quat->shift = newShift;
   return quat;
}




static inline 
struct i32FixedPoint * i32QuatLength2(const struct i32Quat * quat, struct i32FixedPoint * length2)
{
   struct i32FixedPoint fp[4];
   i32QuatGetFpA(quat, fp);
   
   (void)i32fpMultiplyS(&fp[0], &fp[0]);
   (void)i32fpMultiplyS(&fp[1], &fp[1]);
   (void)i32fpMultiplyS(&fp[2], &fp[2]);
   (void)i32fpMultiplyS(&fp[3], &fp[3]);
   
   i32fpSetToCommonShift(fp, 4, 3);
   (void)i32fpZero(length2, fp[0].shift);
   (void)i32fpAddS(length2, &fp[0]);
   (void)i32fpAddS(length2, &fp[1]);
   (void)i32fpAddS(length2, &fp[2]);
   (void)i32fpAddS(length2, &fp[3]);
   
   return length2;
}


static inline 
struct i32FixedPoint * i32QuatLength(const struct i32Quat * quat, struct i32FixedPoint * length)
{
   (void)i32QuatLength2(quat, length);
   return i32fpSqrt(length);
}


static inline 
struct i32Quat * i32QuatDivide(struct i32Quat * quat, const struct i32FixedPoint * k)
{
   int8_t resultShift;
   struct i32FixedPoint fp[4];
   i32QuatGetFpA(quat, fp);
   (void)i32fpDivideS(&fp[0], k);
   (void)i32fpDivideS(&fp[1], k);
   (void)i32fpDivideS(&fp[2], k);
   (void)i32fpDivideS(&fp[3], k);
   return i32QuatSetFpA(quat, fp);
}

static inline 
struct i32Quat * i32QuatDivideFit(struct i32Quat * quat, const struct i32FixedPoint * k, int8_t resultShift)
{
   quat->w = i32fpDivideFit(quat->w, quat->shift, k->value, k->shift, resultShift);
   quat->x = i32fpDivideFit(quat->x, quat->shift, k->value, k->shift, resultShift);
   quat->y = i32fpDivideFit(quat->y, quat->shift, k->value, k->shift, resultShift);
   quat->z = i32fpDivideFit(quat->z, quat->shift, k->value, k->shift, resultShift);
   quat->shift = resultShift;
   return quat;
}


static inline 
struct i32Quat * i32QuatNormalize(struct i32Quat * quat)
{
   struct i32FixedPoint length;
   (void)i32QuatLength(quat, &length);
   if(i32fpIsZeroS(&length))
   {
      return i32QuatSetIdentity(quat, quat->shift);
   }
   return i32QuatDivide(quat, &length);
}

static inline 
struct i32Quat * i32QuatNormalizeFit(struct i32Quat * quat, int8_t resultShift)
{
   struct i32FixedPoint length;
   (void)i32QuatLength(quat, &length);
   if(i32fpIsZeroS(&length))
   {
      return i32QuatSetIdentity(quat, quat->shift);
   }
   return i32QuatDivideFit(quat, &length, resultShift);
}


static inline
struct i32Quat * i32QuatAdd(struct i32Quat * a, const struct i32Quat * b)
{
   a->w = i32fpAdd(a->w, a->shift, b->w, b->shift);
   a->x = i32fpAdd(a->x, a->shift, b->x, b->shift);
   a->y = i32fpAdd(a->y, a->shift, b->y, b->shift);
   a->z = i32fpAdd(a->z, a->shift, b->z, b->shift);
   return a;
}

static inline 
struct i32Quat * i32QuatInvert(struct i32Quat * quat)
{
   quat->x = i32fpInvert(quat->x);
   quat->y = i32fpInvert(quat->y);
   quat->z = i32fpInvert(quat->z);
   return quat;
}

static inline 
struct i32Quat * i32QuatMultiply(struct i32Quat * a, const struct i32Quat * b)
{
   struct i32FixedPoint fpA[4], fpB[4], fpX[4], fpOut[4];
   i32QuatGetFpA(a, fpA);
   i32QuatGetFpA(b, fpB);
   
   
   // w = a->w * b->w - a->x * b->x - a->y * b->y - a->z * b->z;
   (void)i32fpCopyMany(fpX, fpA, 4);

   (void)i32fpMultiplyS(&fpX[0], &fpB[0]);
   (void)i32fpMultiplyS(&fpX[1], &fpB[1]);
   (void)i32fpMultiplyS(&fpX[2], &fpB[2]);
   (void)i32fpMultiplyS(&fpX[3], &fpB[3]);
   (void)i32fpInvertS(&fpX[1]);
   (void)i32fpInvertS(&fpX[2]);
   (void)i32fpInvertS(&fpX[3]);
   
   (void)i32fpSum(&fpOut[0], fpX, 4, 3);
   
   // x = a->w * b->x + a->x * b->w + a->y * b->z - a->z * b->y;
   (void)i32fpCopyMany(fpX, fpA, 4);

   (void)i32fpMultiplyS(&fpX[0], &fpB[1]);
   (void)i32fpMultiplyS(&fpX[1], &fpB[0]);
   (void)i32fpMultiplyS(&fpX[2], &fpB[3]);
   (void)i32fpMultiplyS(&fpX[3], &fpB[2]);
   (void)i32fpInvertS(&fpX[3]);
   
   (void)i32fpSum(&fpOut[1], fpX, 4, 3);

   // y = a->w * b->y - a->x * b->z + a->y * b->w + a->z * b->x;
   (void)i32fpCopyMany(fpX, fpA, 4);

   (void)i32fpMultiplyS(&fpX[0], &fpB[2]);
   (void)i32fpMultiplyS(&fpX[1], &fpB[3]);
   (void)i32fpMultiplyS(&fpX[2], &fpB[0]);
   (void)i32fpMultiplyS(&fpX[3], &fpB[1]);
   (void)i32fpInvertS(&fpX[1]);
   
   (void)i32fpSum(&fpOut[2], fpX, 4, 3);

   // z = a->w * b->z + a->x * b->y - a->y * b->x + a->z * b->w;
   (void)i32fpCopyMany(fpX, fpA, 4);

   (void)i32fpMultiplyS(&fpX[0], &fpB[3]);
   (void)i32fpMultiplyS(&fpX[1], &fpB[2]);
   (void)i32fpMultiplyS(&fpX[2], &fpB[1]);
   (void)i32fpMultiplyS(&fpX[3], &fpB[0]);
   (void)i32fpInvertS(&fpX[2]);
   
   (void)i32fpSum(&fpOut[3], fpX, 4, 3);

   return i32QuatSetFpA(a, fpOut);
}

static inline 
struct i32Quat * i32QuatMultiplyFix(struct i32Quat * a, const struct i32Quat * b, int8_t shift)
{
   (void)i32QuatMultiply(a, b);
   return i32QuatSetShift(a, shift);
}

static inline
struct i32Vector3 * i32QuatTransform(const struct i32Quat * quat, struct i32Vector3 * v)
{
   struct i32Quat vectQuat, temp, inverse;
   i32QuatSetV(&vectQuat, 0, v);
   
   // Prepare Inverse
   i32QuatSetQ(&inverse, quat);
   i32QuatInvert(&inverse);
   
   i32QuatSetQ(&temp, quat);
   i32QuatMultiply(&temp, &vectQuat);
   i32QuatMultiply(&temp, &inverse);
   v->x = temp.x;
   v->y = temp.y;
   v->z = temp.z;
   v->shift = temp.shift;
   return v;
}


static inline
struct i32Vector3 * i32QuatTransformInverse(const struct i32Quat * quat, struct i32Vector3 * v)
{
   struct i32Quat vectQuat, temp;
   i32QuatSetV(&vectQuat, 0, v);
   
   // Prepare Inverse
   i32QuatSetQ(&temp, quat);
   i32QuatInvert(&temp);
   
   i32QuatMultiply(&temp, &vectQuat);
   i32QuatMultiply(&temp, quat);
   v->x = temp.x;
   v->y = temp.y;
   v->z = temp.z;
   v->shift = temp.shift;
   return v;
}

#endif // __I32QUATERNION_H__
