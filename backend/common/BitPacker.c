#include <stdio.h>
#include <string.h>

#include "BitPacker.h"

void bpInit(struct bitpacker * bp, void * buffer, size_t size)
{
   bp->buffer      = (unsigned char *)buffer;
   bp->sizeInBytes = size;
   bp->index.byte  = 0;
   bp->index.bit   = 0;
}

void bpGetIndex(const struct bitpacker * bp, struct bpaddress * address)
{
   address->byte = bp->index.byte;
   address->bit  = bp->index.bit;
}

void bpSetIndex(struct bitpacker * bp, const struct bpaddress * address)
{
   bp->index.byte = address->byte;
   bp->index.bit  = address->bit;
}

bool bpHasDroppedBits(const struct bitpacker * bp)
{
   return bp->index.byte > bp->sizeInBytes ||
          (bp->index.byte == bp->sizeInBytes &&
           bp->index.bit > 0);
}

void bpMoveToNextByte(struct bitpacker * bp)
{
   bp->index.byte ++;
   bp->index.bit = 0;
}

void bpMoveBits(struct bitpacker * bp, int bits)
{
   bits += bp->index.bit;
   while(bits >= 8)
   {
      bits -= 8;
      bp->index.byte ++;
   }
   
   while(bits < 0)
   {
      bits += 8;
      if(bp->index.byte > 0)
      {
         bp->index.byte --;
      }
      else
      {
         bits = 0;
      }
   }
   bp->index.bit = bits;
}
size_t bpGetIndexSizeInBytes(const struct bpaddress * address)
{
   size_t addressByte;
   if(address->bit > 0)
   {
      addressByte = address->byte + 1;
   }
   else
   {
      addressByte = address->byte;
   }
   return addressByte;
}

size_t bpGetCurrentUsageInBytes(const struct bitpacker * bp)
{
   size_t indexByte;
   indexByte = bpGetIndexSizeInBytes(&bp->index);
   
   if(indexByte > bp->sizeInBytes)
   {
      return bp->sizeInBytes;
   }
   return indexByte;
}

unsigned long bpGetCurrentUsageInBits(const struct bitpacker * bp)
{
   return (unsigned long)bp->index.byte * 8 + (unsigned long)bp->index.bit;
}

bool bpHasRoomForBits(const struct bitpacker * bp, unsigned int bits)
{
   size_t byte = bp->index.byte;
   bits += bp->index.bit;
   while(bits >= 8)
   {
      bits -= 8;
      byte ++;
   }
   return byte < bp->sizeInBytes ||
          (byte == bp->sizeInBytes &&
           bits == 0);
}


void bpFillWith(struct bitpacker * bp, unsigned char fill)
{
   memset(bp->buffer, fill, bp->sizeInBytes);
}

void bpResetIndex(struct bitpacker * bp)
{
   bp->index.byte = 0;
   bp->index.bit  = 0;
}


static inline void bpMoveForwardOneBit(struct bitpacker * bp)
{
   bp->index.bit ++;
   if(bp->index.bit >= 8)
   {
      bp->index.bit = 0;
      bp->index.byte ++;
   }
}

static inline void bpInlineWriteBit(struct bitpacker * bp, int value)
{
   if(bp->index.byte < bp->sizeInBytes)
   {
      const unsigned char mask = 1 << bp->index.bit;
      if(value)
      {
         bp->buffer[bp->index.byte] |= mask;
      }
      else
      {
         bp->buffer[bp->index.byte] &= ~mask;
      }
   }
   bpMoveForwardOneBit(bp);   
}

static inline int bpInlineReadBit(struct bitpacker * bp)
{
   const unsigned char mask = 1 << bp->index.bit;
   int value;
   if(bp->index.byte >= bp->sizeInBytes)
   {
      return 0;
   }
   value = (bp->buffer[bp->index.byte] & mask) == mask;
   bpMoveForwardOneBit(bp); 
   return value;
}

void bpWriteBit(struct bitpacker * bp, int value)
{
   bpInlineWriteBit(bp, value);
}

int  bpReadBit(struct bitpacker * bp)
{
   return bpInlineReadBit(bp);
}

void bpWriteInt(struct bitpacker * bp, unsigned int bits, unsigned long long value)
{
   unsigned int i;
   unsigned long long mask = ((unsigned long long)1) << (bits - 1);
   for(i = 0; i < bits; i++)
   {
      bpInlineWriteBit(bp, (value & mask) == mask);
      mask = mask >> 1;
   }
}

unsigned long long bpReadInt(struct bitpacker * bp, unsigned int bits)
{
   unsigned int i;
   unsigned long long value = 0;
   for(i = 0; i < bits; i++)
   {
      value = value << 1;
      value |= bpInlineReadBit(bp) ? 1 : 0;
   }
   return value;
}

static inline void bpPrintByte(unsigned char value)
{
   int i;
   for(i = 0; i < 8; i++)
   {
      printf("%i", value & 1);
      value = value >> 1;
   }
}

void bpWriteByteArray(struct bitpacker * bp, unsigned int sizeBits, const void * data, size_t size)
{
   size_t i;
   const unsigned char * byteData = (const unsigned char *)data;
   bpWriteInt(bp, sizeBits, size);

   for(i = 0; i < size; i++)
   {
      bpWriteInt(bp, 8, byteData[i]);
   }
}

size_t bpPeekByteArraySize(struct bitpacker * bp, unsigned int sizeBits)
{
   size_t size;
   struct bpaddress state;
   bpGetIndex(bp, &state);

   size = (size_t)bpReadInt(bp, sizeBits);

   bpSetIndex(bp, &state);
   return size;
}

size_t bpReadByteArray(struct bitpacker * bp, unsigned int sizeBits, void * data, size_t size)
{
   size_t i;
   size_t dataSize;
   size_t copySize;
   int bytesToShift;
   unsigned char * byteData = (unsigned char *)data;
   dataSize = (size_t)bpReadInt(bp, sizeBits);
   
   if(dataSize > size)
   {
      copySize = size;
   }
   else
   {
      copySize = dataSize;
   }

   bytesToShift = dataSize - copySize;

   for(i = 0; i < copySize; i++)
   {
      byteData[i] = (unsigned char)bpReadInt(bp, 8);
   }

   bpMoveBits(bp, bytesToShift * 8);
   
   return dataSize;
}

void bpDumpBitData(const struct bitpacker * bp)
{
   DumpBitData(bp->buffer, bp->sizeInBytes);
}

void bpAtomicStart(struct bitpacker * bp, struct bpaddress * address)
{
   bpGetIndex(bp, address);
}


bool bpAtomicCommit(struct bitpacker * bp, const struct bpaddress * address, unsigned int promiseBits)
{
   if(!bpHasRoomForBits(bp, promiseBits))
   {
      bpSetIndex(bp, address);
      return false;
   }
   return true;
}

void bpAtomicAbort(struct bitpacker * bp, const struct bpaddress * address)
{
   bpSetIndex(bp, address);
}

void DumpBitData(const void * buffer, size_t size)
{
   size_t index, bytesOnLine;
   const unsigned char * byteBuffer = (const unsigned char *) buffer;
   printf("\nDumping Data for buffer 0x%p of size %d\n", buffer, (int)size);
   
   printf("          0        1        2        3\n");
   index = 0;
   while(index < size)
   {
      printf("  0x%04X: ", (unsigned int)index);
      bytesOnLine = 0;
      while(index < size && bytesOnLine < 4)
      {
         bpPrintByte(byteBuffer[index]);
         
         if(bytesOnLine < 3)
         {
            printf(" ");
         }
         bytesOnLine ++;
         index ++;
      }
      printf("\n");
      
   }
}

/// bitpackerbuffer
#include <stdlib.h>

#define BPB_DEFAULT_GROWBY   32
#define BPB_DEFAULT_INITSIZE 32

void bpbInit(struct bitpackerbuffer * bpb, size_t growBy, size_t initSize)
{
   if(initSize == 0)
   {
      bpb->size = BPB_DEFAULT_INITSIZE;
   }
   else
   {
      bpb->size = initSize;
   }
   
   if(growBy == 0)
   {
      bpb->growBy = BPB_DEFAULT_GROWBY;
   }
   else
   {
      bpb->growBy = growBy;
   }
   
   bpb->firstWrite = true;
   
   bpb->base = malloc(bpb->size);
}

void bpbFree(struct bitpackerbuffer * bpb)
{
   free(bpb->base);
   bpb->base = NULL;
   bpb->size = 0;
}

bool bpbNeedsWrite(struct bitpackerbuffer * bpb, struct bitpacker * bp)
{
   if(bpb->firstWrite)
   {
      bpb->firstWrite = false;
      bpInit(bp, bpb->base, bpb->size);
      return true;
   }
   if(bpHasDroppedBits(bp))
   {
      bpb->size = bpGetIndexSizeInBytes(&bp->index) + bpb->growBy;
      bpb->base = realloc(bpb->base, bpb->size);
      bpInit(bp, bpb->base, bpb->size);
      return true;
   }
   bpb->firstWrite = true;
   return false;
}
