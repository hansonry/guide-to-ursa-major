#include <limits.h>

#include "TimeAndSleep.h"

static inline unsigned int tasWrappedDiffrence(unsigned int now, unsigned int before)
{
   unsigned int delta;
   if(now >= before)
   {
      delta = now - before;
   }
   else
   {
      delta = (UINT_MAX - before) + now + 1;
   }
   return delta;
}

unsigned int tasGetMillisecondDelta(unsigned int * previousMilliseconds)
{
   unsigned int now = tasGetMilliseconds();
   unsigned int delta;
   delta = tasWrappedDiffrence(now, *previousMilliseconds);
   *previousMilliseconds = now;
   return delta;
}

unsigned int tasSleepUntil(unsigned int targetMillisconds, 
                           unsigned int * previousMilliseconds)
{
   unsigned int overshoot;
   unsigned int now = tasGetMilliseconds();
   unsigned int delta = tasWrappedDiffrence(now, *previousMilliseconds);
   if(delta < targetMillisconds)
   {
      overshoot = 0;
      unsigned int toSleep = targetMillisconds - delta;
      tasSleep(toSleep);
   }
   else
   {
      overshoot = delta - targetMillisconds;
   }
   (*previousMilliseconds) += targetMillisconds;
   return overshoot;
}
