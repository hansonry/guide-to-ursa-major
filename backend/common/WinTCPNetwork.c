#define WIN32_LEAN_AND_MEAN

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include <winsock2.h>
#include <ws2tcpip.h>

#include "TCPNetwork.h"

#pragma comment (lib, "Ws2_32.lib")
#pragma comment (lib, "Mswsock.lib")
#pragma comment (lib, "AdvApi32.lib")

struct winTCPNetwork;
struct winTCPNetworkSocket;
struct winTCPNetworkServer;

struct winTCPNetwork
{
   struct tcpNetwork parent;
};

struct winTCPNetworkSocket
{
   struct tcpNetworkSocket parent;
   struct winTCPNetwork * network;
   SOCKET socketfd;
   bool isOpen;
};
struct winTCPNetworkServer
{
   struct tcpNetworkServer parent;
   struct winTCPNetwork * network;
   SOCKET socketfd;
};

static unsigned int CreateCount = 0;

static void WinSockStart(void) 
{
   WSADATA wsa;
   if(CreateCount == 0)
   {
      if(WSAStartup(MAKEWORD(2,2),&wsa) != 0)
      {
         fprintf(stderr, "Failed to Startup Winsock. Code: %d\n", WSAGetLastError());
      }
   }
   CreateCount ++;
}

static void WinSockStop(void) 
{
   if(CreateCount == 1)
   {
      WSACleanup();
   }
   if(CreateCount > 0)
   {
      CreateCount --;
   }  
}
#define CAST_TO(type, dest, src) type * dest = (type *)src

/// winTCPNetworkSocket

static void wtcpnSocketDestroy(struct tcpNetworkSocket * pSocket)
{
   CAST_TO(struct winTCPNetworkSocket, socket, pSocket);
   closesocket(socket->socketfd);
   free(socket);
}

static struct tcpNetwork * wtcpnSocketGetNetwork(const struct tcpNetworkSocket * pSocket)
{
   CAST_TO(struct winTCPNetworkSocket, socket, pSocket);
   return &socket->network->parent;
}

static bool wtcpnSocketIsOpen(struct tcpNetworkSocket * pSocket)
{
   CAST_TO(struct winTCPNetworkSocket, socket, pSocket);
   return socket->isOpen;
}

static ssize_t wtcpnSocketRead(struct tcpNetworkSocket * pSocket, void * buffer, size_t size)
{
   CAST_TO(struct winTCPNetworkSocket, socket, pSocket);
   ssize_t result = recv(socket->socketfd, buffer, size, 0);
   if(result == SOCKET_ERROR)
   {
      int error = WSAGetLastError();
      if(error == WSAEWOULDBLOCK)
      {
         result = 0;
      }
      else
      {
         //printf("Read Failed Socket Closed %d\n", error);
         socket->isOpen = false;
      }
   }
   else if(result == 0)
   {
      socket->isOpen = false;
   }
   return result;
}

static bool wtcpnSocketWrite(struct tcpNetworkSocket * pSocket, const void * buffer, size_t size)
{
   CAST_TO(struct winTCPNetworkSocket, socket, pSocket);
   const unsigned char *  byteBufer = (const unsigned char *)buffer;
   ssize_t result;

   result = send(socket->socketfd, buffer, size, 0);
   if(result < 0)
   {
      int error = WSAGetLastError();
      //printf("Write Failed Socket Closed %d\n", error);
      socket->isOpen = false;
      return false;
   }
   return true;
}

static inline bool SetSocketNonblocking(SOCKET socketfd)
{
   u_long iMode = 1;
   bool failed = ioctlsocket(socketfd, FIONBIO, &iMode) == SOCKET_ERROR;
   if(failed)
   {
      fprintf(stderr, "Failed to set socket read non-block. Code: %d\n", WSAGetLastError());
   }
   return failed;
}

static inline bool DisableNaggleAlgorithm(SOCKET socketfd)
{
   BOOL enabled = TRUE;
   bool failed = setsockopt(socketfd, IPPROTO_TCP, TCP_NODELAY, (void*)&enabled, sizeof(BOOL)) == SOCKET_ERROR;
   if(failed)
   {
      fprintf(stderr, "Failed to disable naggle Algorithm. Code: %d\n", WSAGetLastError());
   }
   return failed;
}

static struct winTCPNetworkSocket * CreateSocketFromHandle(struct winTCPNetwork * network, SOCKET socketfd)
{
   struct winTCPNetworkSocket * socket = malloc(sizeof(struct winTCPNetworkSocket));
   struct tcpNetworkSocket * pSocket = &socket->parent;
   
   socket->network  = network;
   socket->socketfd = socketfd;
   socket->isOpen   = true;
   

   pSocket->fpDestroy    = wtcpnSocketDestroy;
   pSocket->fpGetNetwork = wtcpnSocketGetNetwork;
   pSocket->fpIsOpen     = wtcpnSocketIsOpen;
   pSocket->fpRead       = wtcpnSocketRead;
   pSocket->fpWrite      = wtcpnSocketWrite;
   
   return socket;
}

// winTCPNetworkServer

static void wtcpnServerDestroy(struct tcpNetworkServer * pServer)
{
   CAST_TO(struct winTCPNetworkServer, server, pServer);
   closesocket(server->socketfd);
   free(server);
}
static struct tcpNetwork * wtcpnServerGetNetwork(const struct tcpNetworkServer * pServer)
{
   CAST_TO(struct winTCPNetworkServer, server, pServer);
   return &server->network->parent;
}

static struct tcpNetworkSocket * wtcpnServerAccept(struct tcpNetworkServer * pServer)
{
   CAST_TO(struct winTCPNetworkServer, server, pServer);
   struct winTCPNetworkSocket * wSocket;
   
   SOCKET new_fd;
   struct sockaddr_storage their_addr;
   socklen_t sin_size = sizeof(struct sockaddr_storage);
   
   new_fd = accept(server->socketfd, (struct sockaddr *)&their_addr, &sin_size);
   if(new_fd == -1)
   {
      return NULL;
   }
   
   if(SetSocketNonblocking(new_fd) ||
      DisableNaggleAlgorithm(new_fd))
   {
      closesocket(new_fd);
      return NULL;
   }
   wSocket = CreateSocketFromHandle(server->network, new_fd);
   return &wSocket->parent;
}

/// winTCPNetwork

static inline void WriteOutAddress(struct addrinfo * info)
{
   char buff[INET6_ADDRSTRLEN];
   int port = -1;
   inet_ntop(info->ai_family, info->ai_addr, buff, INET6_ADDRSTRLEN);
   if(info->ai_family == AF_INET)
   {
      port = ntohs(((struct sockaddr_in*)info->ai_addr)->sin_port);
   }
   else
   {
      port = ntohs(((struct sockaddr_in6*)info->ai_addr)->sin6_port);
   }
   printf("Address: %s :: %d\n", buff, port);
}

static struct tcpNetworkServer * wtcpnCreateServer(struct tcpNetwork * ptcpNet, const char * host, const char * service)
{
   CAST_TO(struct winTCPNetwork, tcpNet, ptcpNet);
   struct winTCPNetworkServer * server;
   struct tcpNetworkServer * pServer;
   
   SOCKET sockfd;
   struct addrinfo hints, *servinfo, * p;
   char yes = 1;
   int rv;
   
   memset(&hints, 0, sizeof(struct addrinfo));
   hints.ai_family = AF_UNSPEC;
   hints.ai_socktype = SOCK_STREAM;
   hints.ai_flags = AI_PASSIVE; // use my IP
   
   if((rv = getaddrinfo(host, service, &hints, &servinfo)) != 0)
   {
      fprintf(stderr, "getaddrinfo: %s\n", gai_strerror(rv));
      return NULL;
   }
   
   for(p = servinfo; p != NULL; p = p->ai_next)
   {
      if ((sockfd = socket(p->ai_family, p->ai_socktype,
              p->ai_protocol)) == -1) 
      {
         perror("server: socket");
         continue;
      }

      if(setsockopt(sockfd, SOL_SOCKET, SO_REUSEADDR, &yes,
              sizeof(char)) == -1) 
      {
         perror("setsockopt");
         freeaddrinfo(servinfo);
         return NULL;
      }

      if(bind(sockfd, p->ai_addr, p->ai_addrlen) == -1) 
      {
         closesocket(sockfd);
         perror("server: bind");
         continue;
      }

      break;
   }
   freeaddrinfo(servinfo);
   if(p == NULL)
   {
      fprintf(stderr, "server: failed to bind\n");
      return NULL;
   }
   
   if(SetSocketNonblocking(sockfd))
   {
      closesocket(sockfd);
      return NULL;
   }
   
   if(listen(sockfd, 10) == -1)
   {
      perror("listen");
      return NULL;
   }
   
   server = malloc(sizeof(struct winTCPNetworkServer));
   pServer = &server->parent;
   
   server->network  = tcpNet;
   server->socketfd = sockfd;
   
   pServer->fpDestroy    = wtcpnServerDestroy;
   pServer->fpGetNetwork = wtcpnServerGetNetwork;
   pServer->fpAccept     = wtcpnServerAccept;
   
   return pServer;
}

static struct tcpNetworkSocket * wtcpnCreateClient(struct tcpNetwork * ptcpNet, const char * host, const char * service)
{
   CAST_TO(struct winTCPNetwork, tcpNet, ptcpNet);
   struct winTCPNetworkSocket * wSocket;
   
   SOCKET sockfd;
   struct addrinfo hints, *servinfo, * p;
   int rv;
   
   memset(&hints, 0, sizeof(struct addrinfo));
   hints.ai_family = AF_UNSPEC;
   hints.ai_socktype = SOCK_STREAM;
   
   if((rv = getaddrinfo(host, service, &hints, &servinfo)) != 0)
   {
      fprintf(stderr, "getaddrinfo: %s\n", gai_strerror(rv));
      return NULL;
   }
   
   for(p = servinfo; p != NULL; p = p->ai_next) 
   {
      if((sockfd = socket(p->ai_family, p->ai_socktype,
             p->ai_protocol)) == -1) 
      {
         perror("client: socket");
         continue;
      }

      if(connect(sockfd, p->ai_addr, p->ai_addrlen) == -1) 
      {
         fprintf(stderr, "Failed To Connect: %d\n", WSAGetLastError());
         closesocket(sockfd);
         continue;
      }

      break;
   }

   freeaddrinfo(servinfo);
   if(p == NULL) {
      fprintf(stderr, "client: failed to connect\n");
      return NULL;
   }
   
   if(SetSocketNonblocking(sockfd) ||
      DisableNaggleAlgorithm(sockfd))
   {
      closesocket(sockfd);
      return NULL;
   }
   
   wSocket = CreateSocketFromHandle(tcpNet, sockfd);   
   return &wSocket->parent;
}

static void wtcpnDestroy(struct tcpNetwork * ptcpNet)
{
   CAST_TO(struct winTCPNetwork, tcpNet, ptcpNet);
   WinSockStop();
   free(tcpNet);
}

struct tcpNetwork * tcpnCreateNativeNetwork(void)
{
   struct winTCPNetwork * tcpNet = malloc(sizeof(struct winTCPNetwork));
   struct tcpNetwork * ptcpNet = &tcpNet->parent;
   WinSockStart();
   
   ptcpNet->fpDestroy      = wtcpnDestroy;
   ptcpNet->fpCreateServer = wtcpnCreateServer;
   ptcpNet->fpCreateClient = wtcpnCreateClient;
   return ptcpNet;
}
