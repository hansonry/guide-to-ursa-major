#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include <unistd.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <netinet/tcp.h>
#include <netdb.h>
#include <arpa/inet.h>
#include <fcntl.h>
#include <errno.h>

#include "TCPNetwork.h"


struct nixTCPNetwork;
struct nixTCPNetworkSocket;
struct nixTCPNetworkServer;

struct nixTCPNetwork
{
   struct tcpNetwork parent;
};

struct nixTCPNetworkSocket
{
   struct tcpNetworkSocket parent;
   struct nixTCPNetwork * network;
   int socketfd;
   bool isOpen;
};
struct nixTCPNetworkServer
{
   struct tcpNetworkServer parent;
   struct nixTCPNetwork * network;
   int socketfd;
};

#define CAST_TO(type, dest, src) type * dest = (type *)src

/// nixTCPNetworkSocket

static void ntcpnSocketDestroy(struct tcpNetworkSocket * pSocket)
{
   CAST_TO(struct nixTCPNetworkSocket, socket, pSocket);
   close(socket->socketfd);
   free(socket);
}

static struct tcpNetwork * ntcpnSocketGetNetwork(const struct tcpNetworkSocket * pSocket)
{
   CAST_TO(struct nixTCPNetworkSocket, socket, pSocket);
   return &socket->network->parent;
}

static bool ntcpnSocketIsOpen(struct tcpNetworkSocket * pSocket)
{
   CAST_TO(struct nixTCPNetworkSocket, socket, pSocket);
   return socket->isOpen;
}

static ssize_t ntcpnSocketRead(struct tcpNetworkSocket * pSocket, void * buffer, size_t size)
{
   CAST_TO(struct nixTCPNetworkSocket, socket, pSocket);
   ssize_t result = recv(socket->socketfd, buffer, size, 0);
   if(result < 0)
   {
      if(errno == EWOULDBLOCK || errno == EAGAIN)
      {
         result = 0;
      }
      else
      {
         //printf("Read Close Socket Here\n");
         socket->isOpen = false;
      }
   }
   else if(result == 0)
   {
      //printf("Read Close Socket Here2\n");
      socket->isOpen = false;
      result = -1;
   }
   return result;
}

static bool ntcpnSocketWrite(struct tcpNetworkSocket * pSocket, const void * buffer, size_t size)
{
   CAST_TO(struct nixTCPNetworkSocket, socket, pSocket);
   const unsigned char *  byteBufer = (const unsigned char *)buffer;
   ssize_t result = 0;
   result = send(socket->socketfd, buffer, size, 0);
   if(result < 0)
   {
      if(errno == EWOULDBLOCK || errno == EAGAIN)
      {
         result = 0;
      }
      else
      {
         //printf("Write Close Socket Here2 %d\n", errno);
         socket->isOpen = false;
         return false;
      }
   }
   return true;
}

static inline bool SetSocketNonblocking(int socketfd)
{
   u_long iMode = 1;
   int flags = fcntl(socketfd, F_GETFL, 0);
   flags = flags | O_NONBLOCK;
   bool failed = fcntl(socketfd, F_SETFL, flags) == -1;
   if(failed)
   {
      fprintf(stderr, "Failed to set socket read non-block. Code: %d\n", errno);
   }
   return failed;
}

static inline bool DisableNaggleAlgorithm(int socketfd)
{
   int enabled = 1;
   bool failed = setsockopt(socketfd, IPPROTO_TCP, TCP_NODELAY, (void*)&enabled, sizeof(int)) == -1;
   if(failed)
   {
      fprintf(stderr, "Failed to disable naggle Algorithm. Code: %d\n", errno);
   }
   return failed;
}

static struct nixTCPNetworkSocket * CreateSocketFromHandle(struct nixTCPNetwork * network, int socketfd)
{
   struct nixTCPNetworkSocket * socket = malloc(sizeof(struct nixTCPNetworkSocket));
   struct tcpNetworkSocket * pSocket = &socket->parent;
   
   socket->network  = network;
   socket->socketfd = socketfd;
   socket->isOpen   = true;
   

   pSocket->fpDestroy    = ntcpnSocketDestroy;
   pSocket->fpGetNetwork = ntcpnSocketGetNetwork;
   pSocket->fpIsOpen     = ntcpnSocketIsOpen;
   pSocket->fpRead       = ntcpnSocketRead;
   pSocket->fpWrite      = ntcpnSocketWrite;
   
   return socket;
}

// nixTCPNetworkServer

static void ntcpnServerDestroy(struct tcpNetworkServer * pServer)
{
   CAST_TO(struct nixTCPNetworkServer, server, pServer);
   close(server->socketfd);
   free(server);
}
static struct tcpNetwork * ntcpnServerGetNetwork(const struct tcpNetworkServer * pServer)
{
   CAST_TO(struct nixTCPNetworkServer, server, pServer);
   return &server->network->parent;
}

static struct tcpNetworkSocket * ntcpnServerAccept(struct tcpNetworkServer * pServer)
{
   CAST_TO(struct nixTCPNetworkServer, server, pServer);
   struct nixTCPNetworkSocket * wSocket;
   
   int new_fd;
   struct sockaddr_storage their_addr;
   socklen_t sin_size = sizeof(struct sockaddr_storage);
   
   new_fd = accept(server->socketfd, (struct sockaddr *)&their_addr, &sin_size);
   if(new_fd == -1)
   {
      return NULL;
   }
   
   if(SetSocketNonblocking(new_fd) ||
      DisableNaggleAlgorithm(new_fd))
   {
      close(new_fd);
      return NULL;
   }
   wSocket = CreateSocketFromHandle(server->network, new_fd);
   return &wSocket->parent;
}

/// nixTCPNetwork

static inline void WriteOutAddress(struct addrinfo * info)
{
   char buff[INET6_ADDRSTRLEN];
   int port = -1;
   inet_ntop(info->ai_family, info->ai_addr, buff, INET6_ADDRSTRLEN);
   if(info->ai_family == AF_INET)
   {
      port = ntohs(((struct sockaddr_in*)info->ai_addr)->sin_port);
   }
   else
   {
      port = ntohs(((struct sockaddr_in6*)info->ai_addr)->sin6_port);
   }
   printf("Address: %s :: %d\n", buff, port);
}

static struct tcpNetworkServer * ntcpnCreateServer(struct tcpNetwork * ptcpNet, const char * host, const char * service)
{
   CAST_TO(struct nixTCPNetwork, tcpNet, ptcpNet);
   struct nixTCPNetworkServer * server;
   struct tcpNetworkServer * pServer;
   
   int sockfd;
   struct addrinfo hints, *servinfo, * p;
   int yes = 1;
   int rv;
   
   memset(&hints, 0, sizeof(struct addrinfo));
   hints.ai_family = AF_UNSPEC;
   hints.ai_socktype = SOCK_STREAM;
   hints.ai_flags = AI_PASSIVE; // use my IP
   
   if((rv = getaddrinfo(host, service, &hints, &servinfo)) != 0)
   {
      fprintf(stderr, "getaddrinfo: %s\n", gai_strerror(rv));
      return NULL;
   }
   
   for(p = servinfo; p != NULL; p = p->ai_next)
   {
      if ((sockfd = socket(p->ai_family, p->ai_socktype,
              p->ai_protocol)) == -1) 
      {
         perror("server: socket");
         continue;
      }

      if(setsockopt(sockfd, SOL_SOCKET, SO_REUSEADDR, &yes,
              sizeof(int)) == -1) 
      {
         perror("setsockopt");
         freeaddrinfo(servinfo);
         return NULL;
      }

      if(bind(sockfd, p->ai_addr, p->ai_addrlen) == -1) 
      {
         close(sockfd);
         perror("server: bind");
         continue;
      }

      break;
   }
   freeaddrinfo(servinfo);
   if(p == NULL)
   {
      fprintf(stderr, "server: failed to bind\n");
      return NULL;
   }
   
   if(SetSocketNonblocking(sockfd))
   {
      close(sockfd);
      return NULL;
   }
   
   if(listen(sockfd, 10) == -1)
   {
      perror("listen");
      return NULL;
   }
   
   server = malloc(sizeof(struct nixTCPNetworkServer));
   pServer = &server->parent;
   
   server->network  = tcpNet;
   server->socketfd = sockfd;
   
   pServer->fpDestroy    = ntcpnServerDestroy;
   pServer->fpGetNetwork = ntcpnServerGetNetwork;
   pServer->fpAccept     = ntcpnServerAccept;
   
   return pServer;
}

static struct tcpNetworkSocket * ntcpnCreateClient(struct tcpNetwork * ptcpNet, const char * host, const char * service)
{
   CAST_TO(struct nixTCPNetwork, tcpNet, ptcpNet);
   struct nixTCPNetworkSocket * wSocket;
   
   int sockfd;
   struct addrinfo hints, *servinfo, * p;
   int rv;
   
   memset(&hints, 0, sizeof(struct addrinfo));
   hints.ai_family = AF_UNSPEC;
   hints.ai_socktype = SOCK_STREAM;
   
   if((rv = getaddrinfo(host, service, &hints, &servinfo)) != 0)
   {
      fprintf(stderr, "getaddrinfo: %s\n", gai_strerror(rv));
      return NULL;
   }
   
   for(p = servinfo; p != NULL; p = p->ai_next) 
   {
      if((sockfd = socket(p->ai_family, p->ai_socktype,
             p->ai_protocol)) == -1) 
      {
         perror("client: socket");
         continue;
      }

      if(connect(sockfd, p->ai_addr, p->ai_addrlen) == -1) 
      {
         fprintf(stderr, "Failed To Connect: %d\n", errno);
         close(sockfd);
         continue;
      }

      break;
   }

   freeaddrinfo(servinfo);
   if(p == NULL) {
      fprintf(stderr, "client: failed to connect\n");
      return NULL;
   }
   
   if(SetSocketNonblocking(sockfd) ||
      DisableNaggleAlgorithm(sockfd))
   {
      close(sockfd);
      return NULL;
   }
   
   wSocket = CreateSocketFromHandle(tcpNet, sockfd);   
   return &wSocket->parent;
}

static void ntcpnDestroy(struct tcpNetwork * ptcpNet)
{
   CAST_TO(struct nixTCPNetwork, tcpNet, ptcpNet);
   free(tcpNet);
}

struct tcpNetwork * tcpnCreateNativeNetwork(void)
{
   struct nixTCPNetwork * tcpNet = malloc(sizeof(struct nixTCPNetwork));
   struct tcpNetwork * ptcpNet = &tcpNet->parent;
   
   ptcpNet->fpDestroy      = ntcpnDestroy;
   ptcpNet->fpCreateServer = ntcpnCreateServer;
   ptcpNet->fpCreateClient = ntcpnCreateClient;
   return ptcpNet;
}
