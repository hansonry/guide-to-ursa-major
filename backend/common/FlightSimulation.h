#ifndef __FLIGHTSIMULATION_H__
#define __FLIGHTSIMULATION_H__
#include <stdbool.h>

#include "I32Vector3.h"
#include "I32Quaternion.h"

enum fsimCommandType
{
   eFSCT_free,
   eFSCT_stop,
   eFSCT_velocity,
   eFSCT_position
};

struct fsimCommandVelocity
{
   enum fsimCommandType type;
   struct i32Vector3    velocity;
};

struct fsimCommandPosition
{
   enum fsimCommandType type;
   struct i32Vector3    position;
};

union fsimCommand
{
   enum fsimCommandType       type;
   struct fsimCommandVelocity velocity;
   struct fsimCommandPosition position;
};

struct fsimEntity
{
   struct i32Vector3 position;
   struct i32Vector3 velocity;
   struct i32Quat rotation;
   struct i32Vector3 rotationalVelocity;
};

struct fsimDynamics
{
   struct i32FixedPoint maxSpeed;
   struct i32FixedPoint maxAcceleration;
   struct i32FixedPoint stopDistance;
};

void fsimUpdate(struct fsimEntity * entity,
                const struct fsimDynamics * dynamics, 
                const union fsimCommand * command, 
                unsigned int dt_ms);

void fsimCommandSet(union fsimCommand * dest, const union fsimCommand * src);
void fsimCommandFree(union fsimCommand * command);
void fsimCommandStop(union fsimCommand * command);
void fsimCommandVelocity(union fsimCommand * command, const struct i32Vector3 * velocity);
void fsimCommandPosition(union fsimCommand * command, const struct i32Vector3 * position);

bool fsimCommandEqual(const union fsimCommand * a, const union fsimCommand * b);

#endif // __FLIGHTSIMULATION_H__

