#include <time.h>
#include <unistd.h>

#include "TimeAndSleep.h"

unsigned int tasGetMilliseconds(void)
{
   struct timespec ts;
   (void)clock_gettime(CLOCK_MONOTONIC, &ts);
   return (unsigned int)(ts.tv_nsec / 1000000) + 
          ((unsigned int)ts.tv_sec * 1000); 
}

void tasSleep(unsigned int milliseconds)
{
   const useconds_t usec = milliseconds * 1000;
   (void)usleep(usec);
}

