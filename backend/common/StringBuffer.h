#ifndef __STRINGBUFFER_H__
#define __STRINGBUFFER_H__
#include <stddef.h>

struct stringBuffer;

struct stringBuffer * strbCreate(size_t growBy);
void strbDestroy(struct stringBuffer * strb);

void strbClear(struct stringBuffer * strb);
char * strbMalloc(struct stringBuffer * strb, size_t stringLength);

#endif // __STRINGBUFFER_H__
