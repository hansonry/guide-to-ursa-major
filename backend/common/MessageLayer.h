#ifndef __MESSAGELAYER_H__
#define __MESSAGELAYER_H__
#include <stdint.h>
#include <stddef.h>

struct tcpNetworkSocket;
struct messageLayer;

typedef void (*msglReceiveCallback)(void * userData, 
                                    struct messageLayer * layer, 
                                    struct tcpNetworkSocket * socket, 
                                    void * message, uint16_t messageSize); 

struct messageLayer * msglCreate(struct tcpNetworkSocket * socket,
                                 void * userData,
                                 msglReceiveCallback receiveCallback);
void msglDestory(struct messageLayer * layer);

void msglQueueToSend(struct messageLayer * layer, const void * message, uint16_t messageSize);

void msglPump(struct messageLayer * layer);
void msglSend(struct messageLayer * layer);
void msglReceive(struct messageLayer * layer);


size_t msglGetSendBufferSize(const struct messageLayer * layer);
size_t msglGetReceiveBufferSize(const struct messageLayer * layer);



#endif // __MESSAGELAYER_H__
