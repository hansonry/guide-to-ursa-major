local I32FixedPoint = {}

local SignedBitMask    = 0x80000000
local MagnitudeBitMask = 0x7FFFFFFF

function I32FixedPoint.fromDouble(value, shift)
   local sign = 0
   local mag = value
   if value < 0 then
      value = -value
      sign = SignedBitMask
   end
   if shift > 0 then
      local scale = 1 << shift
      mag = value * scale
   elseif shift < 0 then
      local scale = 1 << (-shift)
      mag = value / scale
   end
   local fpValue = sign | (math.floor(mag) & MagnitudeBitMask)
   return {value = fpValue, shift = shift}
end

return I32FixedPoint
