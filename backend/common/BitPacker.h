#ifndef __BITPACKER_H__
#define __BITPACKER_H__
#include <stddef.h>
#include <stdbool.h>

struct bpaddress
{
   size_t byte;
   unsigned char bit;
};

struct bitpacker
{
   unsigned char * buffer;
   size_t sizeInBytes;
   struct bpaddress index;
};

void bpInit(struct bitpacker * bp, void * buffer, size_t size);

void bpGetIndex(const struct bitpacker * bp, struct bpaddress * address);
void bpSetIndex(struct bitpacker * bp, const struct bpaddress * address);

bool bpHasDroppedBits(const struct bitpacker * bp);

void bpMoveToNextByte(struct bitpacker * bp);
void bpMoveBits(struct bitpacker * bp, int bits);

size_t bpGetIndexSizeInBytes(const struct bpaddress * address);
size_t bpGetCurrentUsageInBytes(const struct bitpacker * bp);
unsigned long bpGetCurrentUsageInBits(const struct bitpacker * bp);
bool bpHasRoomForBits(const struct bitpacker * bp, unsigned int bits);

void bpFillWith(struct bitpacker * bp, unsigned char fill);
void bpResetIndex(struct bitpacker * bp);

void bpWriteBit(struct bitpacker * bp, int value);
int  bpReadBit(struct bitpacker * bp);

void bpWriteInt(struct bitpacker * bp, unsigned int bits, unsigned long long value);
unsigned long long bpReadInt(struct bitpacker * bp, unsigned int bits);

void bpWriteByteArray(struct bitpacker * bp, unsigned int sizeBits, const void * data, size_t size);
size_t bpPeekByteArraySize(struct bitpacker * bp, unsigned int sizeBits);
size_t bpReadByteArray(struct bitpacker * bp, unsigned int sizeBits, void * data, size_t size);

void bpDumpBitData(const struct bitpacker * bp);

void bpAtomicStart(struct bitpacker * bp, struct bpaddress * address);

// Returns false if the commit failed
bool bpAtomicCommit(struct bitpacker * bp, const struct bpaddress * address, unsigned int promiseBits);

void bpAtomicAbort(struct bitpacker * bp, const struct bpaddress * address);



void DumpBitData(const void * buffer, size_t size);

/// bitpackerbuffer
struct bitpackerbuffer
{
   void * base;
   size_t size;
   size_t growBy;
   bool firstWrite;
};

void bpbInit(struct bitpackerbuffer * bpb, size_t growBy, size_t initSize);
void bpbFree(struct bitpackerbuffer * bpb);

bool bpbNeedsWrite(struct bitpackerbuffer * bpb, struct bitpacker * bp);

#endif // __BITPACKER_H__
