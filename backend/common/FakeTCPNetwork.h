#ifndef __FAKETCPNETWORK_H__
#define __FAKETCPNETWORK_H__

struct tcpNetwork;

struct tcpNetwork * tcpnCreateFakeNetwork(void);

#endif // __FAKETCPNETWORK_H__
