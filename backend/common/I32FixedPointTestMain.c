#include <stdio.h>
#include <math.h>

#include "I32FixedPoint.h"
#include "I32Vector3.h"
#include "I32Quaternion.h"

static inline
const char * toBoolString(bool b)
{
   if(b) return "true";
   return "false";
}

static inline
double rangeNormalize(double value, double min, double max)
{
   double diff, shift, count;
   
   diff = max - min;
   shift = value - min;
   count = floor(shift / diff);
   count = count * diff;
   shift = shift - count;
   return shift + min;
}



static void testNumbers3(const struct i32FixedPoint * fpa,
                         const struct i32FixedPoint * fpb,
                         const struct i32FixedPoint * fpc)
{
   double valueA, valueB, valueC;
   double result, fpResult;
   struct i32FixedPoint temp;
   
   valueA = i32fpGetDS(fpa);
   valueB = i32fpGetDS(fpb);
   valueC = i32fpGetDS(fpc);
   printf("\nTesting A: %f (%u, %d), B: %f (%u, %d), C: %f (%u, %d)\n", valueA, fpa->value, fpa->shift,
                                                                        valueB, fpb->value, fpb->shift,
                                                                        valueC, fpc->value, fpc->shift);

   result = rangeNormalize(valueA, valueB, valueC);
   (void)i32fpCopy(&temp, fpa);
   (void)i32fpRangeNormalize(&temp, fpb, fpc);
   fpResult = i32fpGetDS(&temp);
   printf("   rangeNormalize(A, B, C) = %f ~ %f (%u, %d) [%.2f]\n", result, fpResult, temp.value, temp.shift, result - fpResult);
}

static void testNumbers3D(double a, int8_t aShift,
                          double b, int8_t bShift,
                          double c, int8_t cShift)
{
   struct i32FixedPoint fpa, fpb, fpc;
   (void)i32fpSetDS(&fpa, a, aShift);
   (void)i32fpSetDS(&fpb, b, bShift);
   (void)i32fpSetDS(&fpc, c, cShift);
   testNumbers3(&fpa, &fpb, &fpc);
}


static void testNumbers2(const struct i32FixedPoint * fpa,
                         const struct i32FixedPoint * fpb)
{
   struct i32FixedPoint fpc, fpd;
   double a, b;
   double result, fixedResult, fixedA, fixedB;
   bool boolResult, boolFixedResult;

   a = i32fpGetDS(fpa);
   b = i32fpGetDS(fpb);
   printf("\nTesting A: %f (0x%08X, %d), B: %f (0x%08X, %d)\n", a, fpa->value, fpa->shift, b, fpb->value, fpb->shift);

   i32fpCopy(&fpc, fpa);
   i32fpAddS(&fpc, fpb);
   fixedResult = i32fpGetDS(&fpc);
   result = a + b;
   printf("A + B  = %f ~ (%f, %d) [%.2f]\n", result, fixedResult, fpc.shift, result - fixedResult); 
   
   i32fpCopy(&fpc, fpa);
   i32fpSubtractS(&fpc, fpb);
   fixedResult = i32fpGetDS(&fpc);
   result = a - b;
   printf("A - B  = %f ~ (%f, %d) [%.2f]\n", result, fixedResult, fpc.shift, result - fixedResult); 

   i32fpCopy(&fpc, fpa);
   i32fpMultiplyS(&fpc, fpb);
   fixedResult = i32fpGetDS(&fpc);
   result = a * b;
   printf("A * B  = %f ~ (%f, %d) [%.2f]\n", result, fixedResult, fpc.shift, result - fixedResult); 
   
   i32fpCopy(&fpc, fpa);
   i32fpDivideS(&fpc, fpb);
   fixedResult = i32fpGetDS(&fpc);
   result = a / b;
   printf("A / B  = %f ~ (%f, %d) [%.2f]\n", result, fixedResult, fpc.shift, result - fixedResult); 
   
   boolResult = a == b;
   boolFixedResult = i32fpEqualToS(fpa, fpb);
   printf("A == B = %-5s ~ %-5s [%s]\n", toBoolString(boolResult), toBoolString(boolFixedResult), toBoolString(boolResult == boolFixedResult)); 
   
   boolResult = a != b;
   boolFixedResult = i32fpNotEqualToS(fpa, fpb);
   printf("A != B = %-5s ~ %-5s [%s]\n", toBoolString(boolResult), toBoolString(boolFixedResult), toBoolString(boolResult == boolFixedResult)); 
   
   boolResult = a > b;
   boolFixedResult = i32fpGreaterThanS(fpa, fpb);
   printf("A > B  = %-5s ~ %-5s [%s]\n", toBoolString(boolResult), toBoolString(boolFixedResult), toBoolString(boolResult == boolFixedResult)); 
   
   boolResult = a < b;
   boolFixedResult = i32fpLessThanS(fpa, fpb);
   printf("A < B  = %-5s ~ %-5s [%s]\n", toBoolString(boolResult), toBoolString(boolFixedResult), toBoolString(boolResult == boolFixedResult)); 

   result = sqrt(a);
   (void)i32fpCopy(&fpc, fpa);
   (void)i32fpSqrt(&fpc);
   fixedResult = i32fpGetDS(&fpc);
   printf("sqrt(A) = %f ~ (%f, %d) [%.2f]\n", result, fixedResult, fpc.shift, result - fixedResult); 
   
   result = sqrt(b);
   (void)i32fpCopy(&fpc, fpb);
   (void)i32fpSqrt(&fpc);
   fixedResult = i32fpGetDS(&fpc);
   printf("sqrt(B) = %f ~ (%f, %d) [%.2f]\n", result, fixedResult, fpc.shift, result - fixedResult); 
   
   result = floor(a);
   (void)i32fpCopy(&fpc, fpa);
   (void)i32fpFloorS(&fpc);
   fixedResult = i32fpGetDS(&fpc);
   printf("floor(A) = %f ~ (%f, %d) [%.2f]\n", result, fixedResult, fpc.shift, result - fixedResult); 

   result = floor(b);
   (void)i32fpCopy(&fpc, fpb);
   (void)i32fpFloorS(&fpc);
   fixedResult = i32fpGetDS(&fpc);
   printf("floor(B) = %f ~ (%f, %d) [%.2f]\n", result, fixedResult, fpc.shift, result - fixedResult); 

   result = sin(a);
   (void)i32fpCopy(&fpc, fpa);
   (void)i32fpSin(&fpc);
   fixedResult = i32fpGetDS(&fpc);
   printf("sin(A) = %f ~ (%f, %d) [%.2f]\n", result, fixedResult, fpc.shift, result - fixedResult); 

   result = sin(b);
   (void)i32fpCopy(&fpc, fpb);
   (void)i32fpSin(&fpc);
   fixedResult = i32fpGetDS(&fpc);
   printf("sin(B) = %f ~ (%f, %d) [%.2f]\n", result, fixedResult, fpc.shift, result - fixedResult); 


   result = cos(a);
   (void)i32fpCopy(&fpc, fpa);
   (void)i32fpCos(&fpc);
   fixedResult = i32fpGetDS(&fpc);
   printf("cos(A) = %f ~ (%f, %d) [%.2f]\n", result, fixedResult, fpc.shift, result - fixedResult); 

   result = cos(b);
   (void)i32fpCopy(&fpc, fpb);
   (void)i32fpCos(&fpc);
   fixedResult = i32fpGetDS(&fpc);
   printf("cos(B) = %f ~ (%f, %d) [%.2f]\n", result, fixedResult, fpc.shift, result - fixedResult); 

   i32fpCopy(&fpc, fpa);
   i32fpModS(&fpc, fpb);
   fixedResult = i32fpGetDS(&fpc);
   result = fmod(a,  b);
   printf("fmod(A, B)  = %f ~ (%f, %d) [%.2f]\n", result, fixedResult, fpc.shift, result - fixedResult); 
   
   
   i32fpToRectCoords(fpa, fpb, &fpc, &fpd);
   fixedResult = i32fpGetDS(&fpc);
   result = cos(a) * b;
   printf("cos(A) * B  = %f ~ (%f, %d) [%.2f]\n", result, fixedResult, fpc.shift, result - fixedResult); 


   fixedResult = i32fpGetDS(&fpd);
   result = sin(a) * b;
   printf("sin(A) * B  = %f ~ (%f, %d) [%.2f]\n", result, fixedResult, fpd.shift, result - fixedResult); 
   
   i32fpToPolarCoords(fpa, fpb, &fpc, &fpd);
   fixedResult = i32fpGetDS(&fpc);
   result = atan2(b, a);
   printf("atan2(b, a) = %f ~ (%f, %d) [%.2f]\n", result, fixedResult, fpc.shift, result - fixedResult); 


   fixedResult = i32fpGetDS(&fpd);
   result = sqrt(a * a + b * b);
   printf("sqrt(A^2 + B^2) = %f ~ (%f, %d) [%.2f]\n", result, fixedResult, fpd.shift, result - fixedResult); 
   
}

static void testNumbers2D(double a, int8_t aShift,
                          double b, int8_t bShift)
{
   struct i32FixedPoint fpa, fpb;
   (void)i32fpSetDS(&fpa, a, aShift);
   (void)i32fpSetDS(&fpb, b, bShift);
   testNumbers2(&fpa, &fpb);
}

static void testNumbers2I(uint32_t a, int8_t aShift,
                          uint32_t b, int8_t bShift)
{
   struct i32FixedPoint fpa, fpb;
   fpa.value = a;
   fpa.shift = aShift;
   fpb.value = b;
   fpb.shift = bShift;
   testNumbers2(&fpa, &fpb);
}

static
void testScaleAndAdd(void)
{
   struct i32FixedPoint fpv, fpp, fpc;
   printf("\nTest Scale And Add\n");
   fpp.value = 0x80000005;
   fpp.shift = 0;//1;
   
   fpv.value = 0x80000001;
   fpv.shift = 2;
   

   printf("Pos: %f [0x%08X, %d]\n", i32fpGetDS(&fpp), fpp.value, fpp.shift);
   printf("Vel: %f [0x%08X, %d]\n", i32fpGetDS(&fpv), fpv.value, fpv.shift);

   (void)i32fpAddScaledSI2(&fpp, &fpv, 1000, 1000);
   printf("Result: %f [0x%08X, %d]\n", i32fpGetDS(&fpp), fpp.value, fpp.shift);

   (void)i32fpAddScaledSI2(&fpp, &fpv, 1000, 1000);
   printf("Result: %f [0x%08X, %d]\n", i32fpGetDS(&fpp), fpp.value, fpp.shift);

   (void)i32fpAddScaledSI2(&fpp, &fpv, 1000, 1000);
   printf("Result: %f [0x%08X, %d]\n", i32fpGetDS(&fpp), fpp.value, fpp.shift);

   (void)i32fpAddScaledSI2(&fpp, &fpv, 1000, 1000);
   printf("Result: %f [0x%08X, %d]\n", i32fpGetDS(&fpp), fpp.value, fpp.shift);
 
}

static 
void testQuaternion(void)
{
   struct i32Quat q;
   struct i32Vector3 v;
   
   printf("\nTest Quaternions:\n");
   printf("{0, 1, 0} @ pi / 2\n");
   (void)i32QuatSetAxisAngleF(&q, 0, 1, 0, 1.57079632679489661923, 16); // pi / 2
   
   (void)i32v3SetF(&v, 1, 0, 0, 16);
   I32FP_VECTOR_DEBUG_PRINT(&v);
   
   (void)i32QuatTransform(&q, &v);
   I32FP_VECTOR_DEBUG_PRINT(&v);
   
   (void)i32v3SetF(&v, 1, 0, 0, 16);
   (void)i32QuatTransformInverse(&q, &v);
   I32FP_VECTOR_DEBUG_PRINT(&v); 
   
   printf("\n{0, 1, 0} @ pi / 4\n");
   (void)i32QuatSetAxisAngleF(&q, 0, 1, 0, 0.78539816339744830962, 16); // pi / 4

   (void)i32v3SetF(&v, 0, 0, 1, 16);
   I32FP_VECTOR_DEBUG_PRINT(&v);
   
   (void)i32QuatTransform(&q, &v);
   I32FP_VECTOR_DEBUG_PRINT(&v);
   
   (void)i32v3SetF(&v, 0, 0, 1, 16);
   (void)i32QuatTransformInverse(&q, &v);
   I32FP_VECTOR_DEBUG_PRINT(&v); 

}

int main(int argc, char * args[])
{
   testNumbers2D( 16, 0, 4, 0);
   
   testNumbers2D( 2, 0, 1, 0);
   
   testNumbers2D(10.6, 2, 5.2, 2);
   
   testNumbers2D(11, 0, 7, 0);
   
   testNumbers2D(1.571429343532, 29, 1.571429343532, 29);
   
   testNumbers2D(-5.2, 16, 36.45, 16);
   
   testNumbers2D(-27.2, 16, -5.45, 16);

   testNumbers2D(34.563, 16, -5.45, 16);

   testNumbers2D(5003, -8, -458694, -8);

   testNumbers2D( -45.435, 16, 2000, -4);
   
   testNumbers2D( 170, 3, 100, 1);
   
   testNumbers2D( 0.65, 31, 10, 2);
   
   testNumbers2D( 1.5708, 16, 3.1416, 16);

   testNumbers2D( 4.7124, 16, 6.2832, 16);
   
   testNumbers2D( -34.3, 16, 6.2832, 16);
  
   testNumbers2D( 0.785398, 16, 1, 16);
   
   testNumbers2D( 1.57079632679, 16, 2, 16);
   
   testNumbers2D( 1, 16, 5, 16);
   
   testNumbers2D( 0.33, 16, 0.5, 16);
   
   testNumbers2D( 1, 16, 1, 16);
   
   testNumbers2D( -2, 16, 1, 16);

   testNumbers2D( -2, 16, -1, 16);
   
   
   testNumbers3D(15, 0, 0, 0, 10, 0);
   
   testNumbers3D(15, 0, -3.14, 16, 3.14, 16);
   
   testNumbers3D(0.0003, 23, -3.14, 16, 3.14, 16);
   
   testNumbers3D(22, 16, 0, 16, 1.5708, 16);

   
   testScaleAndAdd();

   testQuaternion();

   printf("\nEnd of Test\n");
   return 0;
}

