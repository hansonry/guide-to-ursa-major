#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "TCPNetwork.h"
#include "FakeTCPNetwork.h"
#include "Queue.h"

QUEUE_CREATE_INLINE_FUNCTIONS(byte, unsigned char)

struct fakeTCPNetwork;
struct fakeTCPNetworkSocket;
struct fakeTCPNetworkServer;
struct fakeTCPNetworkPipe;

struct fakeTCPNetwork
{
   struct tcpNetwork parent;
   struct fakeTCPNetworkPipe * root;
};

struct fakeTCPNetworkSocket
{
   struct tcpNetworkSocket parent;
   struct fakeTCPNetwork * network;
   struct fakeTCPNetworkPipe * pipe;
   bool isServer;
   bool isOpen;
};
struct fakeTCPNetworkServer
{
   struct tcpNetworkServer parent;
   struct fakeTCPNetwork * network;
   char * service;
};

struct fakeTCPNetworkPipe
{
   struct fakeTCPNetworkPipe * next;
   struct fakeTCPNetworkSocket * client;
   struct fakeTCPNetworkSocket * server;
   struct queue toClient;
   struct queue toServer;
   char * service;
   bool connecting;
};

#define CAST_TO(type, dest, src) type * dest = (type *)src


/// fakeTCPNetworkPipe

static inline void ftcpPipeDestory(struct fakeTCPNetworkPipe * pipe)
{
   free(pipe->service);
   queueFree(&pipe->toClient);
   queueFree(&pipe->toServer);
   free(pipe);
}

static inline void ftcpnCheckAndDestroyPipe(struct fakeTCPNetwork * tcpNet, bool force)
{
   struct fakeTCPNetworkPipe * pipe, *prevPipe, *delPipe;
   
   prevPipe = NULL;
   pipe = tcpNet->root;
   while(pipe != NULL)
   {
      if((pipe->client == NULL && pipe->server == NULL) || force)
      {
         delPipe = pipe;
         pipe = pipe->next;
         if(prevPipe == NULL)
         {
            tcpNet->root = pipe;
         }
         else
         {
            prevPipe->next = pipe;
         }
         
         ftcpPipeDestory(delPipe);
      }
      else
      {
         prevPipe = pipe;
         pipe = pipe->next;
      }
   }
}

/// fakeTCPNetworkSocket

static void ftcpnSocketDestroy(struct tcpNetworkSocket * pSocket)
{
   CAST_TO(struct fakeTCPNetworkSocket, socket, pSocket);
   if(socket->isServer)
   {
      socket->pipe->server = NULL;
   }
   else
   {
      socket->pipe->client = NULL;
   }
   ftcpnCheckAndDestroyPipe(socket->network, false);
   free(socket);
}

static struct tcpNetwork * ftcpnSocketGetNetwork(const struct tcpNetworkSocket * pSocket)
{
   CAST_TO(struct fakeTCPNetworkSocket, socket, pSocket);
   return &socket->network->parent;
}

static bool ftcpnSocketIsOpen(struct tcpNetworkSocket * pSocket)
{
   CAST_TO(struct fakeTCPNetworkSocket, socket, pSocket);
   return socket->isOpen;
}

static ssize_t ftcpnSocketRead(struct tcpNetworkSocket * pSocket, void * buffer, size_t size)
{
   CAST_TO(struct fakeTCPNetworkSocket, socket, pSocket);
   ssize_t result;
   if(socket->isServer)
   {
      if(socket->pipe->client == NULL)
      {
         socket->isOpen = false;
         return -1;
      }
      result = queuePopMany(&socket->pipe->toServer, buffer, size);
   }
   else
   {
      if(socket->pipe->server == NULL)
      {
         socket->isOpen = false;
         return -1;
      }
      result = queuePopMany(&socket->pipe->toClient, buffer, size);
   }   
   return result;
}

static bool ftcpnSocketWrite(struct tcpNetworkSocket * pSocket, const void * buffer, size_t size)
{
   CAST_TO(struct fakeTCPNetworkSocket, socket, pSocket);
   if(socket->isServer)
   {
      if(socket->pipe->client == NULL)
      {
         socket->isOpen = false;
         return false;
      }
      queuePushMany(&socket->pipe->toClient, buffer, size);
   }
   else
   {
      if(socket->pipe->server == NULL)
      {
         socket->isOpen = false;
         return false;
      }
      queuePushMany(&socket->pipe->toServer, buffer, size);
   }   
   return true;
}
static inline struct fakeTCPNetworkPipe * ftcpnCreateAndAddPipe(struct fakeTCPNetwork * tcpNet, const char * service)
{
   struct fakeTCPNetworkPipe * pipe = malloc(sizeof(struct fakeTCPNetworkPipe));
   pipe->next = tcpNet->root;
   pipe->client = NULL;
   pipe->server = NULL;
   queueInitDefaults_byte(&pipe->toClient);
   queueInitDefaults_byte(&pipe->toServer);
   pipe->service = strdup(service);
   pipe->connecting = true;
   
   tcpNet->root = pipe;
   return pipe;
}

static struct fakeTCPNetworkSocket * CreateSocket(struct fakeTCPNetwork * network, struct fakeTCPNetworkPipe * pipe, bool isServer)
{
   struct fakeTCPNetworkSocket * socket = malloc(sizeof(struct fakeTCPNetworkSocket));
   struct tcpNetworkSocket * pSocket = &socket->parent;
   
   socket->network  = network;
   socket->pipe     = pipe;
   socket->isOpen   = true;
   socket->isServer = isServer;
   
   if(isServer)
   {
      pipe->server = socket;
   }
   else
   {
      pipe->client = socket;
   }

   pSocket->fpDestroy    = ftcpnSocketDestroy;
   pSocket->fpGetNetwork = ftcpnSocketGetNetwork;
   pSocket->fpIsOpen     = ftcpnSocketIsOpen;
   pSocket->fpRead       = ftcpnSocketRead;
   pSocket->fpWrite      = ftcpnSocketWrite;
   
   return socket;
}

// fakeTCPNetworkServer

static void ftcpnServerDestroy(struct tcpNetworkServer * pServer)
{
   CAST_TO(struct fakeTCPNetworkServer, server, pServer);
   free(server->service);
   free(server);
}
static struct tcpNetwork * ftcpnServerGetNetwork(const struct tcpNetworkServer * pServer)
{
   CAST_TO(struct fakeTCPNetworkServer, server, pServer);
   return &server->network->parent;
}

static struct tcpNetworkSocket * ftcpnServerAccept(struct tcpNetworkServer * pServer)
{
   CAST_TO(struct fakeTCPNetworkServer, server, pServer);
   struct fakeTCPNetworkSocket * socket;
   struct fakeTCPNetworkPipe * pipe;
   
   // Find pipe without a server using the same service
   pipe = server->network->root;
   while(pipe != NULL)
   {
      if(pipe->connecting && pipe->client != NULL && pipe->server == NULL &&
         strcmp(pipe->service, server->service) == 0)
      {
         break;
      }
   }
   
   if(pipe == NULL)
   {
      return NULL;
   }
   
   pipe->connecting = false;
   socket = CreateSocket(server->network, pipe, true);

   return &socket->parent;
}

/// fakeTCPNetwork

static struct tcpNetworkServer * ftcpnCreateServer(struct tcpNetwork * ptcpNet, const char * host, const char * service)
{
   CAST_TO(struct fakeTCPNetwork, tcpNet, ptcpNet);
   struct fakeTCPNetworkServer * server = malloc(sizeof(struct fakeTCPNetworkServer));
   struct tcpNetworkServer * pServer = &server->parent;
   (void)host;
   
   server->network = tcpNet;
   server->service = strdup(service);

   pServer->fpDestroy    = ftcpnServerDestroy;
   pServer->fpGetNetwork = ftcpnServerGetNetwork;
   pServer->fpAccept     = ftcpnServerAccept;
   
   return pServer;
}



static struct tcpNetworkSocket * ftcpnCreateClient(struct tcpNetwork * ptcpNet, const char * host, const char * service)
{
   CAST_TO(struct fakeTCPNetwork, tcpNet, ptcpNet);
   struct fakeTCPNetworkPipe * pipe = ftcpnCreateAndAddPipe(tcpNet, service);
   struct fakeTCPNetworkSocket * socket = CreateSocket(tcpNet, pipe, false);
   (void)host;
     
   return &socket->parent;
}

static void ftcpnDestroy(struct tcpNetwork * ptcpNet)
{
   CAST_TO(struct fakeTCPNetwork, tcpNet, ptcpNet);
   ftcpnCheckAndDestroyPipe(tcpNet, true);
   free(tcpNet);
}

struct tcpNetwork * tcpnCreateFakeNetwork(void)
{
   struct fakeTCPNetwork * tcpNet = malloc(sizeof(struct fakeTCPNetwork));
   struct tcpNetwork * ptcpNet = &tcpNet->parent;
   
   tcpNet->root = NULL;
   
   ptcpNet->fpDestroy      = ftcpnDestroy;
   ptcpNet->fpCreateServer = ftcpnCreateServer;
   ptcpNet->fpCreateClient = ftcpnCreateClient;
   return ptcpNet;
}
