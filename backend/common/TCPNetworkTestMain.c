#include <stdio.h>
#include <string.h>

#include "TCPNetwork.h"
#include "SignalInterrupt.h"

#define PORT "11234"

static void client(struct tcpNetwork * net)
{
   struct tcpNetworkSocket * socket;
   const char * message = "Hi mom!";
   ssize_t size;
   unsigned char buffer[1024];
   
   socket = tcpnCreateClient(net, "localhost", PORT);
   printf("Created Client: %p\n", socket);
   tcpnSocketWrite(socket, message, strlen(message) + 1);
   printf("Sent message: %s\n", message);
   siSetup(NULL);
   while(siNotInterrupted())
   {
      size = tcpnSocketRead(socket, buffer, 1024);
      if(size > 0)
      {
         printf("Received: %s\n", buffer);
      }
   }
   tcpnSocketDestroy(socket);
}

static void echoServer(struct tcpNetwork * net)
{
   struct tcpNetworkServer * server;
   struct tcpNetworkSocket * socket[50];
   struct tcpNetworkSocket * newSocket;
   unsigned char buffer[1024];
   ssize_t size;
   int next = 0;
   int i;
   server = tcpnCreateServer(net, NULL, PORT);
   printf("Created Server %p\n", server);
   siSetup(NULL);
   while(siNotInterrupted())
   {
      newSocket = tcpnServerAccept(server);
      if(newSocket != NULL)
      {
         socket[next] = newSocket;
         next ++;
      }
      
      for(i = 0; i < next; i++)
      {
         size = tcpnSocketRead(socket[i], buffer, 1024);
         if(size > 0)
         {
            printf("Received Message \"%s\" from %p\n", buffer, socket[i]);
            tcpnSocketWrite(socket[i], buffer, size);
         }
      }
   }
   
   for(i = 0; i < next; i++)
   {
      tcpnSocketDestroy(socket[i]);
   }
   tcpnServerDestroy(server);

}

int main(int argc, char * args[])
{
   struct tcpNetwork * net;

   net = tcpnCreateNativeNetwork();
   
   if(argc > 1 && strcmp(args[1], "echo") == 0)
   {
      echoServer(net);
   }
   else
   {
      printf("Client selected\n");
      client(net);
   }
   tcpnDestroy(net);
   printf("End Of Test\n");
   return 0;
}
