#include <profileapi.h>
#include <synchapi.h>

#include "TimeAndSleep.h"

LARGE_INTEGER PerfFreqCountsPerSecond = { 0 };
LARGE_INTEGER HalfFreqCountsPerSecond;

unsigned int tasGetMilliseconds(void)
{
   LARGE_INTEGER counts;
   if(PerfFreqCountsPerSecond.QuadPart == 0)
   {
      (void)QueryPerformanceFrequency(&PerfFreqCountsPerSecond);
      HalfFreqCountsPerSecond.QuadPart = PerfFreqCountsPerSecond.QuadPart / 2;
   }
   (void)QueryPerformanceCounter(&counts);
   return (unsigned int)(((counts.QuadPart * 1000) + HalfFreqCountsPerSecond.QuadPart) / PerfFreqCountsPerSecond.QuadPart);
}

void tasSleep(unsigned int milliseconds)
{
   Sleep(milliseconds);
}

