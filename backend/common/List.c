#include <stdlib.h>
#include <string.h>

#include "List.h"

#define LIST_DEFAULT_GROWBY   32
#define LIST_DEFAULT_INITSIZE 32

typedef unsigned char byte;

void listInit(struct list * list, size_t elementSize, size_t growBy, size_t initSize)
{
   if(initSize == 0)
   {
      list->size = LIST_DEFAULT_INITSIZE;
   }
   else
   {
      list->size = initSize;
   }

   if(growBy == 0)
   {
      list->growBy = LIST_DEFAULT_GROWBY;
   }
   else
   {
      list->growBy = growBy;
   }

   list->elementSize = elementSize;
   list->count = 0;
   list->base = malloc(list->elementSize * list->size);
}

void listFree(struct list * list)
{
   free(list->base);
   list->base        = NULL;
   list->size        = 0;
   list->count       = 0;
   list->elementSize = 0;
   list->growBy      = 0;
}

static inline void listGrow(struct list * list, size_t fit)
{
   size_t newSize = fit + list->growBy;
   list->base = realloc(list->base, list->elementSize * newSize);
   list->size = newSize;
}

static inline void listCheckAndGrow(struct list * list, size_t size)
{
   if(size > list->size)
   {
      listGrow(list, size);
   }
}

static inline void * getOffset(const void * base, size_t elementSize, size_t index)
{
   return &((byte*)base)[elementSize * index];
}

static inline void * listGetWithoutCheck(const struct list * list, size_t index)
{
   return getOffset(list->base, list->elementSize, index);
}

void * listAdd(struct list * list, size_t * index)
{
   size_t iIndex;
   listCheckAndGrow(list, list->count + 1);

   iIndex = list->count;
   list->count ++;

   if(index != NULL)
   {
      (*index) = iIndex;
   }

   return listGetWithoutCheck(list, iIndex);
   
}

void * listAddCopy(struct list * list, const void * element, size_t * index)
{
   void * mem = listAdd(list, index);
   memcpy(mem, element, list->elementSize);
   return mem;
}

void * listAddMany(struct list * list, const void * elements, size_t elementCount, size_t * firstIndex)
{
   size_t iFirstIndex;
   void * mem;
   listCheckAndGrow(list, list->count + elementCount);
   
   iFirstIndex = list->count;
   list->count += elementCount;

   mem = listGetWithoutCheck(list, iFirstIndex);
   
   memcpy(mem, elements, list->elementSize * elementCount);

   if(firstIndex != NULL)
   {
      (*firstIndex) = iFirstIndex;
   }
   return mem;
}

void * listGet(const struct list * list, size_t index)
{
   if(index >= list->count)
   {
      return NULL;
   }
   return listGetWithoutCheck(list, index);
}

void * listGetArray(const struct list * list, size_t * count)
{
   if(count != NULL)
   {
      (*count) = list->count;
   }
   return (void*)list->base;
}

size_t listGetCount(const struct list * list)
{
   return list->count;
}

static inline void listSwapNoCheck(struct list * list, size_t index1, size_t index2)
{
   void * mem1 = listGetWithoutCheck(list, index1);
   void * mem2 = listGetWithoutCheck(list, index2);
   void * swapSpace;
   listCheckAndGrow(list, list->count + 1);
   swapSpace = listGetWithoutCheck(list, list->count);
   memcpy(swapSpace, mem1,      list->elementSize);
   memcpy(mem1,      mem2,      list->elementSize);
   memcpy(mem2,      swapSpace, list->elementSize);
}

bool listRemoveFast(struct list * list, size_t index)
{
   if(index >= list->count)
   {
      return false;
   }
   list->count --;
   if(list->count != index)
   {
      listSwapNoCheck(list, index, list->count);
   }
   return true;
}

static inline void listShiftDown(struct list * list, size_t start, size_t amount)
{
   size_t i;
   size_t end = start - amount;
   for(i = start; i < list->count; i ++)
   {
      void * src = listGetWithoutCheck(list, i);
      void * dest = listGetWithoutCheck(list, i - amount);
      memcpy(dest, src, list->elementSize);
   } 
}

bool listRemoveOrdered(struct list * list, size_t index)
{
   if(index >= list->count)
   {
      return false;
   }

   listShiftDown(list, index + 1, 1);

   list->count --;

   return true;
}

static inline void listShiftUp(struct list * list, size_t start, size_t amount)
{
   size_t i;
   for(i = list->count - 1; i < list->count && i >= start; i --)
   {
      void * src = listGetWithoutCheck(list, i);
      void * dest = listGetWithoutCheck(list, i + amount);
      memcpy(dest, src, list->elementSize);
   }
}

void * listInsert(struct list * list, size_t index)
{
   if(index > list->count)
   {
      return NULL;
   }

   listCheckAndGrow(list, list->count + 1);
   listShiftUp(list, index, 1);
   list->count ++;
   return listGetWithoutCheck(list, index); 
}

void * listInsertCopy(struct list * list, size_t index, const void * element)
{
   void * mem = listInsert(list, index);
   if(mem == NULL)
   {
      return NULL;
   }
   memcpy(mem, element, list->elementSize);
   return mem;
}

void listClear(struct list * list)
{
   list->count = 0;
}

bool listSwap(struct list * list, size_t index1, size_t index2)
{
   if(index1 >= list->count ||
      index2 >= list->count)
   {
      return false;
   }
   listSwapNoCheck(list, index1, index2);
   return true;
}

