local MessageCodeGeneratorGDScript = {}

local function LookupEnumerationByName(enumerations, name)
   for i, v in ipairs(enumerations) do
      if v.name == name then
         return v
      end
   end
   return nil
end

local function LookupEnumerationByNameWithAssert(enumerations, name)
   return assert(LookupEnumerationByName(enumerations, name), 
                 "Failed to find matching enum to: " .. name)
end

local function GetSupportedSize(size)
   assert(size <= 32, "No support for sizes larger than 32 bits: " .. size)
   if size > 16 then
      return 4
   elseif size > 8 then
      return 2
   else
      return 1     
   end
end

function MessageCodeGeneratorGDScript.generate(outputDir, moduleName, enumerations, messages)
   local outputFilename = string.format("%s/%s.gd", outputDir, moduleName)
   local fh = io.open(outputFilename, "wb")
   
   fh:write("extends Node\n\n")
   fh:write("# This code is automaticaly generated using MessageCodeGeneratorGDScript.lua\n\n")
   
   fh:write("# Enumerations\n")
   for enumIndex, enumValue in ipairs(enumerations) do
      fh:write(string.format("const %s = {\n", enumValue.name))
      for i, v in ipairs(enumValue.values) do
         fh:write(string.format("\t\"%s\": %d,\n", v.name, v.value))
      end
      fh:write("}\n\n")
   end


   fh:write("# Message Definitions\n")
   fh:write("const MessageType = {\n")
   for msgIndex, msgValue in ipairs(messages) do
      fh:write(string.format("\t\"%s\": %d,\n", msgValue.name, msgValue.id))
   end 
   fh:write("}\n\n")
   
   fh:write("# Enumeration ID to String Functions\n")
   fh:write("func enum_value_to_string(enumeration : Dictionary, value: int):\n")
   fh:write("\tfor key in enumeration.keys():\n")
   fh:write("\t\tif enumeration[key] == value:\n")
   fh:write("\t\t\treturn key\n")
   fh:write("\treturn \"!UNKNWON!\"\n\n")
   
   fh:write("# Message Read/Write/Dump Functions\n")
   for msgIndex, msgValue in ipairs(messages) do
      fh:write(string.format("## %s\n", msgValue.name))
      
      -- Message Specific Write
      fh:write(string.format("func write_%s(stream: StreamPeer, msg: Dictionary):\n", msgValue.name))
      fh:write(string.format("\tMessageUtil.write_uint(stream, 2, MessageType.%s)\n", msgValue.name))
      for paramIndex, paramValue in ipairs(msgValue.structure) do
         local paramVar = string.format("msg.%s", paramValue.name)
         if paramValue.type == "uint" then
            local byteSize = GetSupportedSize(paramValue.size)
            fh:write(string.format("\tMessageUtil.write_uint(stream, %d, %s)\n", byteSize, paramVar))
         elseif paramValue.type == "int" then
            local byteSize = GetSupportedSize(paramValue.size)
            fh:write(string.format("\tMessageUtil.write_int(stream, %d, %s)\n", byteSize, paramVar))
         elseif paramValue.type == "bool" then
            fh:write(string.format("\tMessageUtil.write_bool(stream, %s)\n", paramVar))
         elseif paramValue.type == "vector3" then
            fh:write(string.format("\tMessageUtil.write_vector3(stream, %s)\n", paramVar))
         elseif paramValue.type == "quat" then
            fh:write(string.format("\tMessageUtil.write_quat(stream, %s)\n", paramVar))
         elseif paramValue.type == "enum" then
            local enum = LookupEnumerationByNameWithAssert(enumerations, paramValue.enum)
            local byteSize = GetSupportedSize(enum.size)
            fh:write(string.format("\tMessageUtil.write_uint(stream, %d, %s)\n", byteSize, paramVar))
         elseif paramValue.type == "string" then
            local byteSize = GetSupportedSize(paramValue.size)
            fh:write(string.format("\tMessageUtil.write_string(stream, %d, %s)\n", byteSize, paramVar))
         else
            assert(false, "Unexpected param type: " .. paramValue.type)
         end      
      end
      fh:write("\n")
      
      -- Message Specific Read
      fh:write(string.format("func read_%s(stream: StreamPeer) -> Dictionary:\n", msgValue.name))
      fh:write("\tvar msg = {\n")
      fh:write(string.format("\t\t\"type\": MessageType.%s,\n", msgValue.name))
      for paramIndex, paramValue in ipairs(msgValue.structure) do
         if paramValue.type == "uint" then
            local byteSize = GetSupportedSize(paramValue.size)
            fh:write(string.format("\t\t\"%s\": MessageUtil.read_uint(stream, %d),\n", paramValue.name, byteSize))
         elseif paramValue.type == "int" then
            local byteSize = GetSupportedSize(paramValue.size)
            fh:write(string.format("\t\t\"%s\": MessageUtil.read_int(stream, %d),\n", paramValue.name, byteSize))
         elseif paramValue.type == "bool" then
            fh:write(string.format("\t\t\"%s\": MessageUtil.read_bool(stream),\n", paramValue.name))
         elseif paramValue.type == "vector3" then
            fh:write(string.format("\t\t\"%s\": MessageUtil.read_vector3(stream),\n", paramValue.name))
         elseif paramValue.type == "quat" then
            fh:write(string.format("\t\t\"%s\": MessageUtil.read_quat(stream),\n", paramValue.name))
         elseif paramValue.type == "enum" then
            local enum = LookupEnumerationByNameWithAssert(enumerations, paramValue.enum)
            local byteSize = GetSupportedSize(enum.size)
            fh:write(string.format("\t\t\"%s\": MessageUtil.read_uint(stream, %d),\n", paramValue.name, byteSize))
         elseif paramValue.type == "string" then
            local byteSize = GetSupportedSize(paramValue.size)
            fh:write(string.format("\t\t\"%s\": MessageUtil.read_string(stream, %d),\n", paramValue.name, byteSize))
         else
            assert(false, "Unexpected param type: " .. paramValue.type)
         end      
      end
      fh:write("\t}\n")
      fh:write("\treturn msg\n")
      fh:write("\n")
      
      -- Message Specific Dump
      fh:write(string.format("func dump_%s(msg: Dictionary):\n", msgValue.name))
      fh:write(string.format("\tprint(\"Dump %s \\\"%s\\\" %d:\")\n", moduleName, msgValue.name, msgValue.id))
      for paramIndex, paramValue in ipairs(msgValue.structure) do
         local paramVar = string.format("msg.%s", paramValue.name)
         if paramValue.type == "uint" or paramValue.type == "int" then
            fh:write(string.format("\tprint(\"   %s:\\t0x%%X,\\t%%d\" %% [%s, %s])\n", paramValue.name, paramVar, paramVar))
         elseif paramValue.type == "bool" then
            fh:write(string.format("\tprint(\"   %s:\\t%%s\" %% [\"true\" if %s else \"false\"])\n", paramValue.name, paramVar))
         elseif paramValue.type == "vector3" then
            fh:write(string.format("\tprint(\"   %s.x:\\t0x%%X,\\t%%d\" %% [%s.x, %s.x])\n", paramValue.name, paramVar, paramVar))
            fh:write(string.format("\tprint(\"   %s.y:\\t0x%%X,\\t%%d\" %% [%s.y, %s.y])\n", paramValue.name, paramVar, paramVar))
            fh:write(string.format("\tprint(\"   %s.z:\\t0x%%X,\\t%%d\" %% [%s.z, %s.z])\n", paramValue.name, paramVar, paramVar))
         elseif paramValue.type == "quat" then
            fh:write(string.format("\tprint(\"   %s.w:\\t0x%%X,\\t%%d\" %% [%s.w, %s.w])\n", paramValue.name, paramVar, paramVar))
            fh:write(string.format("\tprint(\"   %s.x:\\t0x%%X,\\t%%d\" %% [%s.x, %s.x])\n", paramValue.name, paramVar, paramVar))
            fh:write(string.format("\tprint(\"   %s.y:\\t0x%%X,\\t%%d\" %% [%s.y, %s.y])\n", paramValue.name, paramVar, paramVar))
            fh:write(string.format("\tprint(\"   %s.z:\\t0x%%X,\\t%%d\" %% [%s.z, %s.z])\n", paramValue.name, paramVar, paramVar))
         elseif paramValue.type == "enum" then
            --local enum = LookupEnumerationByNameWithAssert(enumerations, paramValue.enum)
            --local byteSize = GetSupportedSize(enum.size)
            fh:write(string.format("\tprint(\"   %s:\\t\\\"%%s\\\",\\t%%d\" %% [enum_value_to_string(%s, %s), %s])\n", paramValue.name, paramValue.enum, paramVar, paramVar))
         elseif paramValue.type == "string" then
            fh:write(string.format("\tprint(\"   %s:\\t\\\"%%s\\\",\\t%%d\" %% [%s, %s.length()])\n", paramValue.name, paramVar, paramVar))
         else
            assert(false, "Unexpected param type: " .. paramValue.type)
         end
      end
      fh:write("\n")  
   end


   fh:write("# Master Read/Write/Dump Functions\n")
   
   -- Master Write
   fh:write("func write(stream: StreamPeer, msg: Dictionary):\n")
   fh:write("\tmatch msg.type:\n")
   for msgIndex, msgValue in ipairs(messages) do
      fh:write(string.format("\t\tMessageType.%s:\n", msgValue.name))
      fh:write(string.format("\t\t\twrite_%s(stream, msg)\n", msgValue.name))
   end
   fh:write("\t\t_:\n")
   fh:write("\t\t\tassert(false, \"Write Message Type Not supported: %d\" % msg.type)\n\n")
   
   -- Master Read
   fh:write("func read(stream: StreamPeer):\n")
   fh:write("\tvar type = MessageUtil.read_uint(stream, 2)\n")
   fh:write("\tvar msg = null\n")
   fh:write("\tmatch type:\n")
   for msgIndex, msgValue in ipairs(messages) do
      fh:write(string.format("\t\tMessageType.%s:\n", msgValue.name))
      fh:write(string.format("\t\t\tmsg = read_%s(stream)\n", msgValue.name))
   end
   fh:write("\t\t_:\n")
   fh:write("\t\t\tassert(false, \"Read Message Type Not supported: %d\" % type)\n")
   fh:write("\treturn msg\n\n")
   
   -- Master Dump
   -- Master Write
   fh:write("func dump(msg: Dictionary):\n")
   fh:write("\tmatch msg.type:\n")
   for msgIndex, msgValue in ipairs(messages) do
      fh:write(string.format("\t\tMessageType.%s:\n", msgValue.name))
      fh:write(string.format("\t\t\tdump_%s(msg)\n", msgValue.name))
   end
   fh:write("\t\t_:\n")
   fh:write("\t\t\tassert(false, \"Dump Message Type Not supported: %d\" % msg.type)\n\n")
   
   fh:close()
end

return MessageCodeGeneratorGDScript
