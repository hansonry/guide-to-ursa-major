#ifndef __I32FIXEDPOINT_H__
#define __I32FIXEDPOINT_H__
#include <stdint.h>
#include <stddef.h>
#include <stdbool.h>

#define I32FP_MASK_SIGNED_BIT    ((uint32_t)0x80000000)
#define I64FP_MASK_SIGNED_BIT    ((uint64_t)0x8000000000000000)
#define I32FP_ALL_FS             ((uint32_t)0xFFFFFFFF)
#define I64FP_ALL_FS             ((uint64_t)0xFFFFFFFFFFFFFFFF)
#define I32FP_MAX_MAGNITUDE      ((uint32_t)0x7FFFFFFF)
#define I32FP_MAGNITUE_HIGH_BIT  ((uint32_t)0x40000000)
#define I32FP_MAX_SHIFT          127  

#define I32FP_DEBUG_PRINT(valuePtr) printf("> %s = %f { 0x%08X, %d }\n", #valuePtr, i32fpGetDS(valuePtr), (valuePtr)->value, (valuePtr)->shift)

struct i32FixedPoint
{
   uint32_t value;
   int8_t shift;
};

static inline
uint32_t i32fpSetD(double value, int8_t shift)
{
   uint32_t magnatude;
   uint32_t sign;
   if(value < 0)
   {
      sign = I32FP_MASK_SIGNED_BIT;
      value = -value;
   }
   else
   {
      sign = 0;
   }
   
   if(shift > 0)
   {
      uint32_t scale = 1 << shift;
      magnatude = (uint32_t)(value * scale);
   }
   else if(shift < 0)
   {
      uint32_t scale = 1 << (-shift);
      magnatude = (uint32_t)(value / scale);
   }
   else
   {
      magnatude = (uint32_t)value;
   }
   return (magnatude & (~I32FP_MASK_SIGNED_BIT)) | sign;
}

static inline
uint32_t i32fpSetI(int value, int8_t shift)
{
   uint32_t magnatude;
   uint32_t sign;
   if(value > 0)
   {
      sign = 0;
   }
   else
   {
      sign = I32FP_MASK_SIGNED_BIT;
      value = -value;
   }
   
   if(shift > 0)
   {
      magnatude = ((uint32_t)value) << shift;
   }
   else if(shift < 0)
   {
      uint32_t halfShift = 1 << (1 - shift);
      magnatude = (((uint32_t)value) + halfShift) >> (-shift);
   }
   else
   {
      magnatude = (uint32_t)value;
   }
   return (magnatude & (~I32FP_MASK_SIGNED_BIT)) | sign;
}

static inline
double i32fpGetD(uint32_t value, int8_t shift)
{
   double result;
   uint32_t magnatude = value & (~I32FP_MASK_SIGNED_BIT);
   if(shift > 0)
   {
      double scale = ((uint64_t)1) << shift;
      result = magnatude / scale;
   }
   else if(shift < 0)
   {
      double scale = ((uint64_t)1) << (-shift);
      result = magnatude * scale;
   }
   else
   {
      result = magnatude;
   }
   if(value & I32FP_MASK_SIGNED_BIT)
   {
      result = -result;
   }
   return result;
}

static inline
struct i32FixedPoint * i32fpSetDS(struct i32FixedPoint * fp, double value, int8_t shift)
{
   fp->shift = shift;
   fp->value = i32fpSetD(value, fp->shift);
   return fp;
}
static inline
struct i32FixedPoint * i32fpSetIS(struct i32FixedPoint * fp, int value, int8_t shift)
{
   fp->shift = shift;
   fp->value = i32fpSetI(value, fp->shift);
   return fp;
}

static inline
struct i32FixedPoint * i32fpSetS(struct i32FixedPoint * fp, uint32_t value, int8_t shift)
{
   fp->shift = shift;
   fp->value = value;
   return fp;
}

static inline
double i32fpGetDS(const struct i32FixedPoint * fp)
{
   return i32fpGetD(fp->value, fp->shift);
}

static inline
struct i32FixedPoint * i32fpCopy(struct i32FixedPoint * dest, const struct i32FixedPoint * src)
{
   dest->value = src->value;
   dest->shift = src->shift;
   return dest;
}

static inline
struct i32FixedPoint * i32fpCopyMany(struct i32FixedPoint * dest, const struct i32FixedPoint * src, unsigned int count)
{
   unsigned int i;
   for(i = 0; i < count; i++)
   {
      dest[i].value = src[i].value;
      dest[i].shift = src[i].shift;
   }
   return dest;
}

static inline
struct i32FixedPoint * i32fpCopyNullCheck(struct i32FixedPoint * dest, const struct i32FixedPoint * src)
{
   if(src == NULL)
   {
      return NULL;
   }
   dest->value = src->value;
   dest->shift = src->shift;
   return dest;
}

static inline
struct i32FixedPoint * i32fpZero(struct i32FixedPoint * fp, int8_t shift)
{
   fp->value = 0;
   fp->shift = shift;
   return fp;
}

static inline
struct i32FixedPoint * i32fpOne(struct i32FixedPoint * fp, int8_t shift)
{
   fp->shift = shift;
   fp->value = i32fpSetI(1, shift);
   return fp;
}

static inline
int32_t i32fpRoundedShiftRight(uint32_t value, uint8_t shift)
{
   uint32_t sign = value & I32FP_MASK_SIGNED_BIT;
   if(shift >= 32)
   {
      return sign;
   }
   uint32_t magnatude = value & (~I32FP_MASK_SIGNED_BIT);
   uint32_t halfShift = 1 << (shift - 1);
   return ((magnatude + halfShift) >> shift) | sign;
}

static inline
int32_t i32fpSignedShiftLeft(uint32_t value, uint8_t shift)
{
   uint32_t sign = value & I32FP_MASK_SIGNED_BIT;
   if(shift >= 32)
   {
      return sign;
   }
   return (value << shift) | sign;
}


static inline
int64_t i64fpRoundedShiftRight(uint64_t value, uint8_t shift)
{
   uint64_t sign = value & I64FP_MASK_SIGNED_BIT;
   if(shift >= 64)
   {
      return sign;
   }
   uint64_t magnatude = value & (uint64_t)(~I64FP_MASK_SIGNED_BIT);
   uint64_t halfShift = 1 << (shift - 1);
   return ((magnatude + halfShift) >> shift) | sign;
}

static inline
uint64_t i64fpSignedShiftLeft(uint64_t value, uint8_t shift)
{
   uint32_t sign = value & I64FP_MASK_SIGNED_BIT;
   if(shift >= 64)
   {
      return sign;
   }
   return (value << shift) | sign;
}

static inline
uint32_t i32fpSetShift(uint32_t value, int8_t shift, int8_t newShift)
{
   int8_t diff = newShift - shift;
   if(diff < 0)
   {
      return i32fpRoundedShiftRight(value, -diff);
   }
   if(diff > 0)
   {
      return i32fpSignedShiftLeft(value, diff);
   }
   return value;
}

static inline 
void i32fpGetSignAndMagnatude(uint32_t value, uint32_t * sign, uint32_t * magnatude)
{
   if(sign != NULL)
   {
      (*sign) = value & I32FP_MASK_SIGNED_BIT;
   }
   if(magnatude != NULL)
   {
      (*magnatude) = value & (~I32FP_MASK_SIGNED_BIT);
   }
}

static inline
bool i32fpShiftToMaxPrecision(uint32_t * value, int8_t * shift)
{
   uint32_t sign, mag;
   i32fpGetSignAndMagnatude(*value, &sign, &mag);
   if(mag == 0)
   {
      return false;
   }
   while((mag & I32FP_MAGNITUE_HIGH_BIT) == 0)
   {
      mag = mag << 1;
      (*shift) ++;
   }
   (*value) = sign | mag;
   return true;
}

static inline
bool i32fpShiftToMaxPrecisionS(struct i32FixedPoint * fp)
{
   return i32fpShiftToMaxPrecision(&fp->value, &fp->shift);
}

static inline
void i32fpShiftForCompair(uint32_t * a, int8_t * aShift, uint32_t * b, int8_t * bShift)
{
   if((*aShift) != (*bShift))
   {
      uint32_t bSign, bMag;
      i32fpGetSignAndMagnatude(*b, &bSign, &bMag);
      
      while(((*aShift) > (*bShift)) && 
            (bMag & I32FP_MAGNITUE_HIGH_BIT) == 0)
      {
         bMag = bMag << 1;
         (*bShift) ++;
      }
      (*a) = i32fpSetShift(*a, *aShift, *bShift);
      (*aShift) = (*bShift);
      (*b) = bMag | bSign;
   }
}

static inline
uint64_t i64fpSetShift(uint64_t value, int8_t shift, int8_t newShift)
{
   int8_t diff = newShift - shift;
   if(diff < 0)
   {
      return i64fpRoundedShiftRight(value, -diff);      
   }
   if(diff > 0)
   {
      return i64fpSignedShiftLeft(value, diff);
   }
   return value;
}

static inline
uint64_t i64fpPushInto32Bits(uint64_t value, int8_t shift, int8_t * resultShift)
{
   uint64_t magnatude = value & (uint64_t)(~I64FP_MASK_SIGNED_BIT);
   uint64_t sign = value & I64FP_MASK_SIGNED_BIT;
   while(magnatude > I32FP_MAX_MAGNITUDE)
   {
      magnatude = i64fpRoundedShiftRight(magnatude, 1);
      shift --;
   }
   if(resultShift != NULL)
   {
      (*resultShift) = shift;
   }
   return magnatude | sign;
}

static inline
uint32_t i32fpMultiply(uint32_t a, int8_t aShift, uint32_t b, int8_t bShift, int8_t * resultShift)
{
   uint32_t aSign, aMag, bSign, bMag;
   i32fpGetSignAndMagnatude(a, &aSign, &aMag);
   i32fpGetSignAndMagnatude(b, &bSign, &bMag);
   int8_t multResultShift = aShift + bShift;
   int8_t lResultShift;
   uint64_t resultMag = (uint64_t)aMag * (uint64_t)bMag;
   uint32_t resultSign;
   if((aSign || bSign) && !(aSign && bSign))
   {
      resultSign = I32FP_MASK_SIGNED_BIT;
   }
   else
   {
      resultSign = 0;
   }
   
   return (uint32_t)i64fpPushInto32Bits(resultMag, multResultShift, resultShift) | resultSign;
}

static inline
uint32_t i32fpMultiplyFit(uint32_t a, int8_t aShift, uint32_t b, int8_t bShift, int8_t resultShift)
{
   int8_t multShift;
   uint32_t result = i32fpMultiply(a, aShift, b, bShift, &multShift);
   return i32fpSetShift(result, multShift, resultShift);
}

static inline
struct i32FixedPoint * i32fpMultiplyS(struct i32FixedPoint * a, const struct i32FixedPoint * b)
{
   a->value = i32fpMultiply(a->value, a->shift, b->value, b->shift, &a->shift);
   return a;
}

static inline
struct i32FixedPoint * i32fpMultiplyFitS(struct i32FixedPoint * a, const struct i32FixedPoint * b, int8_t resultShift)
{
   a->value = i32fpMultiplyFit(a->value, a->shift, b->value, b->shift, resultShift);
   return a;
}

static inline
struct i32FixedPoint * i32fpSetShiftS(struct i32FixedPoint * fp, int8_t newShift)
{
   fp->value = i32fpSetShift(fp->value, fp->shift, newShift);
   fp->shift = newShift;
   return fp;
}

static inline
uint32_t i32fpAdd(uint32_t a, int8_t aShift, uint32_t b, int8_t bShift)
{
   uint32_t aSign, aMag, bSign, bMag;
   i32fpGetSignAndMagnatude(a, &aSign, &aMag);
   i32fpGetSignAndMagnatude(b, &bSign, &bMag);
   uint32_t result;
   uint32_t resultSign;
   
   bMag = i32fpSetShift(bMag, bShift, aShift);
   
   if(aSign && bSign)
   {
      result = aMag + bMag; 
      resultSign = I32FP_MASK_SIGNED_BIT; 
   }
   else if(aSign && !bSign)
   {
      if(aMag > bMag)
      {
         result = aMag - bMag;
         resultSign = I32FP_MASK_SIGNED_BIT;
      }
      else
      {
         result = bMag - aMag;
         resultSign = 0;
      }
   }
   else if(!aSign && bSign)
   {
      if(aMag >= bMag)
      {
         result = aMag - bMag;
         resultSign = 0;
      }
      else
      {
         result = bMag - aMag;
         resultSign = I32FP_MASK_SIGNED_BIT;
      }
   }
   else
   {
      result = aMag + bMag; 
      resultSign = 0;
   }
   
   if(result == 0)
   {
      resultSign = 0;
   }
   
   return result | resultSign;
}

static inline
struct i32FixedPoint * i32fpAddS(struct i32FixedPoint * a, const struct i32FixedPoint * b)
{
   a->value = i32fpAdd(a->value, a->shift, b->value, b->shift);
   return a;
}



static inline
uint32_t i32fpInvert(uint32_t value)
{
   uint32_t sign, mag;
   i32fpGetSignAndMagnatude(value, &sign, &mag);
   sign = sign ^ I32FP_MASK_SIGNED_BIT;
   return mag | sign;
}

static inline
struct i32FixedPoint * i32fpInvertS(struct i32FixedPoint * fp)
{
   fp->value = i32fpInvert(fp->value);
   return fp;
}

static inline
uint32_t i32fpAbsoluteValue(uint32_t value)
{
   return value & (~I32FP_MASK_SIGNED_BIT);
}

static inline
struct i32FixedPoint * i32fpAbsoluteValueS(struct i32FixedPoint * fp)
{
   fp->value = i32fpAbsoluteValue(fp->value);
   return fp;
}

static inline
uint32_t i32fpSubtract(uint32_t a, int8_t aShift, uint32_t b, int8_t bShift)
{
   b = i32fpInvert(b);
   return i32fpAdd(a, aShift, b, bShift);
}

static inline
struct i32FixedPoint * i32fpSubtractS(struct i32FixedPoint * a, const struct i32FixedPoint * b)
{
   a->value = i32fpSubtract(a->value, a->shift, b->value, b->shift);
   return a;
}

static inline
uint32_t i32fpDivide(int32_t a, uint32_t aShift, int32_t b, uint32_t bShift, int8_t * resultShift)
{
   uint32_t aSign, aMag, bSign, bMag;
   uint64_t aMagScaled, divResultMag, divResultSign;
   uint32_t resultSign;
   int8_t aShiftScaled = aShift + 32;
   int8_t divShift = aShiftScaled - bShift;
   i32fpGetSignAndMagnatude(a, &aSign, &aMag);
   i32fpGetSignAndMagnatude(b, &bSign, &bMag);
   aMagScaled = ((uint64_t)aMag) << 32;
   divResultMag = (aMagScaled + (bMag >> 1)) / bMag;
   if((aSign || bSign) && !(aSign && bSign))
   {
      resultSign = I32FP_MASK_SIGNED_BIT;
   }
   else
   {
      resultSign = 0;
   }
   
   return (uint32_t)i64fpPushInto32Bits(divResultMag, divShift, resultShift) | resultSign;
}

static inline
uint32_t i32fpDivideFit(int32_t a, uint32_t aShift, int32_t b, uint32_t bShift, int8_t resultShift)
{
   int8_t divShift;
   uint32_t divResult = i32fpDivide(a, aShift, b, bShift, &divShift);
   return i32fpSetShift(divResult, divShift, resultShift);
}

static inline
struct i32FixedPoint * i32fpDivideFixS(struct i32FixedPoint * a, const struct i32FixedPoint * b, int8_t resultShift)
{
   a->value = i32fpDivideFit(a->value, a->shift, b->value, b->shift, resultShift);
   a->shift = resultShift;
   return a;
}

static inline
struct i32FixedPoint * i32fpDivideS(struct i32FixedPoint * a, const struct i32FixedPoint * b)
{
   a->value = i32fpDivide(a->value, a->shift, b->value, b->shift, &a->shift);
   return a;
}

static inline
uint32_t i32fpScaleI2(uint32_t value, int8_t shift, int32_t mult, int32_t div, int8_t * resultShift)
{
   uint32_t multValue = i32fpSetI(mult, 0);
   uint32_t divValue = i32fpSetI(div, 0);
   value = i32fpMultiply(value, shift, multValue, 0, &shift);
   value = i32fpDivide(value, shift, divValue, 0, &shift);
   if(resultShift != NULL)
   {
      (*resultShift) = shift;
   }
   return value;
}

static inline
uint32_t i32fpScaleFitI2(uint32_t value, int8_t shift, int32_t mult, int32_t div, int8_t resultShift)
{
   value = i32fpScaleI2(value, shift, mult, div, &shift);
   return i32fpSetShift(value, shift, resultShift);
}

static inline
uint32_t i32fpAddScaledI2(uint32_t a, int8_t aShift, uint32_t b, int8_t bShift, int32_t mult, int32_t div)
{
   int8_t scaledShift;
   uint32_t scaledValue = i32fpScaleI2(b, bShift, mult, div, &scaledShift);
   return i32fpAdd(a, aShift, scaledValue, scaledShift);
}

static inline
struct i32FixedPoint * i32fpAddScaledSI2(struct i32FixedPoint * a, const struct i32FixedPoint * b, int32_t mult, int32_t div)
{
   a->value = i32fpAddScaledI2(a->value, a->shift, b->value, b->shift, mult, div);
   return a;
}

static inline
struct i32FixedPoint * i32fpScaleSI2(struct i32FixedPoint * fp, int32_t mult, int32_t div)
{
   fp->value = i32fpScaleI2(fp->value, fp->shift, mult, div, &fp->shift);
   return fp;
}

static inline
void i32fpSetToCommonShiftFit(struct i32FixedPoint * fpns, unsigned int count, int8_t newShift)
{
   unsigned int i;
   for(i = 0; i < count; i++)
   {
      (void)i32fpSetShiftS(&fpns[i], newShift);
   }
}

static inline
void i32fpSetToCommonShift(struct i32FixedPoint * fpns, unsigned int count, uint8_t extra)
{
   unsigned int i;
   int8_t minShift = I32FP_MAX_SHIFT;
   int8_t newShift;
   
   for(i = 0; i < count; i++)
   {
      if(!i32fpShiftToMaxPrecisionS(&fpns[i]))
      {
         fpns[i].shift = I32FP_MAX_SHIFT;
      }
   }
   
   for(i = 0; i < count; i++)
   {
      if(fpns[i].shift < minShift)
      {
         minShift = fpns[i].shift;
      }
   }

   newShift = minShift - extra;
   i32fpSetToCommonShiftFit(fpns, count, newShift);
}

static inline
bool i32fpEqualTo(uint32_t a, int8_t aShift, uint32_t b, int8_t bShift)
{
   uint32_t aSign, aMag, bSign, bMag;
   
   b = i32fpSetShift(b, bShift, aShift);
   i32fpGetSignAndMagnatude(a, &aSign, &aMag);
   i32fpGetSignAndMagnatude(b, &bSign, &bMag);

   if(aMag == 0) aSign = 0;
   if(bMag == 0) bSign = 0;

   return aMag == bMag && aSign == bSign;
}

static inline
bool i32fpNotEqualTo(uint32_t a, int8_t aShift, uint32_t b, int8_t bShift)
{
   return !i32fpEqualTo(a, aShift, b, bShift);
}

static inline
bool i32fpGreaterThan(uint32_t a, int8_t aShift, uint32_t b, int8_t bShift)
{
   uint32_t aSign, aMag, bSign, bMag;

   i32fpShiftForCompair(&a, &aShift, &b, &bShift);

   i32fpGetSignAndMagnatude(a, &aSign, &aMag);
   i32fpGetSignAndMagnatude(b, &bSign, &bMag);

   if(aMag == 0 && bMag == 0)
   {
      return false;
   }
   if(aSign && bSign)
   {
      return aMag < bMag;
   }
   if(aSign)
   {
      return false;
   }
   if(bSign)
   {
      return true;
   }
   return aMag > bMag;
}

static inline
bool i32fpLessThan(uint32_t a, int8_t aShift, uint32_t b, int8_t bShift)
{
   uint32_t aSign, aMag, bSign, bMag;
   
   i32fpShiftForCompair(&a, &aShift, &b, &bShift);

   i32fpGetSignAndMagnatude(a, &aSign, &aMag);
   i32fpGetSignAndMagnatude(b, &bSign, &bMag);
   
   if(aMag == 0 && bMag == 0)
   {
      return false;
   }
   if(aSign && bSign)
   {
      return aMag > bMag;
   }
   if(aSign)
   {
      return true;
   }
   if(bSign)
   {
      return false;
   }
   return aMag < bMag;
}

static inline
bool i32fpGreaterThanOrEqualTo(uint32_t a, int8_t aShift, uint32_t b, int8_t bShift)
{
   return !i32fpLessThan(a, aShift, b, bShift);
}

static inline
bool i32fpLessThanOrEqualTo(uint32_t a, int8_t aShift, uint32_t b, int8_t bShift)
{
   return !i32fpGreaterThan(a, aShift, b, bShift);
}

static inline
bool i32fpEqualToS(const struct i32FixedPoint * a, const struct i32FixedPoint * b)
{
   return i32fpEqualTo(a->value, a->shift, b->value, b->shift);
}

static inline
bool i32fpNotEqualToS(const struct i32FixedPoint * a, const struct i32FixedPoint * b)
{
   return i32fpNotEqualTo(a->value, a->shift, b->value, b->shift);
}

static inline
bool i32fpGreaterThanS(const struct i32FixedPoint * a, const struct i32FixedPoint * b)
{
   return i32fpGreaterThan(a->value, a->shift, b->value, b->shift);
}

static inline
bool i32fpLessThanS(const struct i32FixedPoint * a, const struct i32FixedPoint * b)
{
   return i32fpLessThan(a->value, a->shift, b->value, b->shift);
}

static inline
bool i32fpGreaterThanOrEqualToS(const struct i32FixedPoint * a, const struct i32FixedPoint * b)
{
   return i32fpGreaterThanOrEqualTo(a->value, a->shift, b->value, b->shift);
}

static inline
bool i32fpLessThanOrEqualToS(const struct i32FixedPoint * a, const struct i32FixedPoint * b)
{
   return i32fpLessThanOrEqualTo(a->value, a->shift, b->value, b->shift);
}

static inline
bool i32fpIsZero(uint32_t value)
{
   uint32_t sign, mag;
   i32fpGetSignAndMagnatude(value, &sign, &mag);
   return mag == 0;
}

static inline
bool i32fpIsZeroS(const struct i32FixedPoint * fp)
{
   return i32fpIsZero(fp->value);
}

static inline
bool i32fpIsNegative(uint32_t value)
{
   uint32_t sign, mag;
   i32fpGetSignAndMagnatude(value, &sign, &mag);
   return sign == I32FP_MASK_SIGNED_BIT;
}

static inline
bool i32fpIsNegativeS(const struct i32FixedPoint * fp)
{
   return i32fpIsNegative(fp->value);
}



static inline
struct i32FixedPoint * i32fpLerp(struct i32FixedPoint * result, 
                                 const struct i32FixedPoint * p1,
                                 const struct i32FixedPoint * p2,
                                 const struct i32FixedPoint * percentage)
{
   // result = p2 - p1
   (void)i32fpCopy(result, p2);
   (void)i32fpSubtractS(result, p1);
   
   // result = result * percentage
   (void)i32fpMultiplyFitS(result, percentage, p2->shift);
   
   // return result + p1
   return i32fpAddS(result, p1);
}

static inline
struct i32FixedPoint * i32fpLookup2d(struct i32FixedPoint * result,
                                     const struct i32FixedPoint * xs,
                                     const struct i32FixedPoint * ys,
                                     size_t pointCount,
                                     const struct i32FixedPoint * x)
{
   size_t i;
   if(pointCount == 0)
   {
      return NULL;
   }
   if(pointCount == 1 || i32fpLessThanS(x, &xs[0]))
   {
      return i32fpCopy(result, &ys[0]);
   }

   for(i = 1; i < pointCount; i++)
   {
      if(i32fpLessThanS(x, &xs[i]))
      {
         const size_t prevI = i - 1;
         struct i32FixedPoint top, bottom;
         
         // top = x - xs[1]
         (void)i32fpCopy(&top, x);
         (void)i32fpSubtractS(&top, &xs[prevI]);
         
         // bottom = xs[2] - xs[1]
         (void)i32fpCopy(&bottom, &xs[i]);
         (void)i32fpSubtractS(&bottom, &xs[prevI]);
         
         // top = top / bottom
         (void)i32fpDivideS(&top, &bottom);
         
         return i32fpLerp(result, &ys[prevI], &ys[i], &top);
      }
   }
   
   return i32fpCopy(result, &ys[pointCount - 1]);
}

static inline
uint32_t i32fpFloor(uint32_t value, int8_t shift)
{
   uint32_t mask;
   uint32_t sign, mag;
   if(shift <= 0)
   {
      return value;
   }
   i32fpGetSignAndMagnatude(value, &sign, &mag);
   if(shift > 31)
   {
      mask = 0;
   }
   else
   {
      mask = ((uint32_t)0xFFFFFFFF) << shift;
   }
   if(sign)
   {
      mag += (1 << (shift));
   }
   return sign | (mag & mask);
}

static inline
struct i32FixedPoint * i32fpFloorS(struct i32FixedPoint * value)
{
   value->value = i32fpFloor(value->value, value->shift);
   return value;
}

static inline
uint32_t i32fpMod(uint32_t a, int8_t aShift, uint32_t b, int8_t bShift, int8_t * resultShift)
{
   uint32_t r;
   uint32_t mask;
   uint32_t sign, mag;
   int8_t rs;
   // a / b
   r = i32fpDivide(a, aShift, b, bShift, &rs);
   
   // Remove the whole number part leaving the fracional part
   i32fpGetSignAndMagnatude(r, &sign, &mag);
   
   if(rs > 31)
   {
      mask = 0xFFFFFFFF;
   }
   else
   {
      mask = ~(((uint32_t)0xFFFFFFFF) << rs);
   }
   
   r = sign | (mag & mask);
   
   // multiply faction part by B
   
   r = i32fpMultiply(r, rs, b, bShift, &rs);
   
   if(resultShift != NULL)
   {
      (*resultShift) = rs;
   }
   return r;
}

static inline 
struct i32FixedPoint * i32fpModS(struct i32FixedPoint * a, 
                                 const struct i32FixedPoint * b)
{
   a->value = i32fpMod(a->value, a->shift, b->value, b->shift, &a->shift);
   return a;
}

static inline
struct i32FixedPoint * i32fpRangeNormalize(struct i32FixedPoint * value,
                                           const struct i32FixedPoint * min,
                                           const struct i32FixedPoint * max)
{
   struct i32FixedPoint diff, shifted;
   
   // diff = max - min
   (void)i32fpCopy(&diff, max);
   (void)i32fpSubtractS(&diff, min);
   
   // shifted = value - min
   (void)i32fpCopy(&shifted, value);
   if(shifted.shift > min->shift)
   {
      (void)i32fpSetShiftS(&shifted, min->shift);
   }
   (void)i32fpSubtractS(&shifted, min);
   
   // shifted = fmod(shifted, diff)
   (void)i32fpModS(&shifted, &diff);
   if(shifted.shift > max->shift)
   {
      (void)i32fpSetShiftS(&shifted, max->shift);
   }
   
   
   // if shifted < 0 then shifted = shifted + diff
   if((shifted.value & I32FP_MASK_SIGNED_BIT) != 0)
   {
      (void)i32fpAddS(&shifted, &diff);
   }
     
   // value = shifted + min
   (void)i32fpCopy(value, &shifted);
   return i32fpAddS(value, min); 
}

// Math Functions
struct i32FixedPoint * i32fpSin(struct i32FixedPoint * value);
struct i32FixedPoint * i32fpCos(struct i32FixedPoint * value);
struct i32FixedPoint * i32fpSqrt(struct i32FixedPoint * value);

void i32fpToRectCoords(const struct i32FixedPoint * angle_in,
                       const struct i32FixedPoint * radius_in,
                       struct i32FixedPoint * x_out,
                       struct i32FixedPoint * y_out);

void i32fpToPolarCoords(const struct i32FixedPoint * x_in,
                        const struct i32FixedPoint * y_in,
                        struct i32FixedPoint * angle_out,
                        struct i32FixedPoint * radius_out);

static inline
void i32fpCosSin(const struct i32FixedPoint * angle,
                 struct i32FixedPoint * fpCos,
                 struct i32FixedPoint * fpSin)
{
   struct i32FixedPoint one;
   (void)i32fpOne(&one, 28);   
   i32fpToRectCoords(angle, &one, fpCos, fpSin);
}

static inline
struct i32FixedPoint * i32fpAtan(struct i32FixedPoint * value)
{
   struct i32FixedPoint x, y;
   (void)i32fpCopy(&y, value);
   (void)i32fpOne(&x, value->shift);
   i32fpToPolarCoords(&x, &y, value, NULL);
   return value;
}

static inline
struct i32FixedPoint * i32fpSum(struct i32FixedPoint * result, 
                                const struct i32FixedPoint * values, 
                                unsigned int size,
                                uint8_t extraShift)
{
   int8_t minShift = I32FP_MAX_SHIFT;
   unsigned int i;
   for(i = 0; i < size; i++)
   {
      if(values[i].shift < minShift)
      {
         minShift = values[i].shift;
      }
   }
   
   (void)i32fpZero(result, minShift - extraShift);
   for(i = 0; i < size; i++)
   {
      (void)i32fpAddS(result, &values[i]);
   }
   return result;
}


#endif // __I32FIXEDPOINT_H__
