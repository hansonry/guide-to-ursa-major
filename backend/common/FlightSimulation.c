#include <string.h>

#include "FlightSimulation.h"
#include "Config.h"

#define GET_VECTOR_PARAMS(vect) i32fpGetD((vect).x, (vect).shift), \
                                i32fpGetD((vect).y, (vect).shift), \
                                i32fpGetD((vect).z, (vect).shift)

#define FSIM_DEBUG_PRINT  0
#if FSIM_DEBUG_PRINT == 1
#include <stdio.h>
#endif // FSIM_DEBUG_PRINT

static inline 
void fsimIntegrateV3(struct i32Vector3 * v, 
                     const struct i32Vector3 * delta,
                     unsigned int dt_ms)
{
   (void)i32v3AddScaled(v, delta, dt_ms, 1000);
}

static inline
void fsimIntegrateQuat(struct i32Quat * rotation,
                       const struct i32Vector3 * rotationalVelocity,
                       unsigned int dt_ms)
{
   struct i32Quat rotVel;
   struct i32FixedPoint rotVelVect[4];
   i32v3GetFpA(rotationalVelocity, &rotVelVect[1]);
   
   
   (void)i32fpZero(&rotVelVect[0], rotationalVelocity->shift);
   (void)i32fpScaleSI2(&rotVelVect[1], dt_ms, 2000);
   (void)i32fpScaleSI2(&rotVelVect[2], dt_ms, 2000);
   (void)i32fpScaleSI2(&rotVelVect[3], dt_ms, 2000);
   (void)i32QuatSetFpA(&rotVel, rotVelVect);
   
   (void)i32QuatMultiply(&rotVel, rotation);
   
   (void)i32QuatAdd(rotation, &rotVel);
   
   (void)i32QuatNormalizeFit(rotation, CONFIG_SHIFT_ANGLE);
}

static inline 
void fsimIntegrate(struct fsimEntity * entity, unsigned int dt_ms,
                   const struct i32Vector3 * acceleration,
                   const struct i32Vector3 * rotationalAcceleration)
{
   fsimIntegrateV3(&entity->velocity,   acceleration,                dt_ms);
   fsimIntegrateV3(&entity->position,   &entity->velocity,           dt_ms);
   fsimIntegrateQuat(&entity->rotation, &entity->rotationalVelocity, dt_ms);
}


static inline
void fsimComputeDesiredVelocity(struct fsimEntity * entity,
                                const struct fsimDynamics * dynamics,
                                const struct i32Vector3 * desiredVelocity,
                                unsigned int dt_ms,
                                struct i32Vector3 * acceleration,
                                struct i32Vector3 * rotationalAcceleration)
{
   struct i32Vector3 velocityDiff, desiredAcceleration;
   // TODO: Limmit Target velocity to maximum Velocity
   // Maybe maximum velocity is not a simulation responsiblity but
   // a server command responsiblity.
   
   // velocityDiff = desiredVelocity - velocity
   (void)i32v3SetV(&velocityDiff, desiredVelocity);
   (void)i32v3SubtractV(&velocityDiff, &entity->velocity);
   
   // Convert to an acceleration
   (void)i32v3SetV(&desiredAcceleration, &velocityDiff);
   (void)i32v3ScaleI2(&desiredAcceleration, 1000, dt_ms);

   // Limmit acceleration to max acceleration
   (void)i32v3MaxLength(&desiredAcceleration, &dynamics->maxAcceleration);

   // Add to acceleration
   (void)i32v3AddV(acceleration, &desiredAcceleration);
   
#if FSIM_DEBUG_PRINT == 1
   printf("Desired Velocity Calculations:\n");
   printf("   Desired Velocity:     (%f, %f, %f) @ %d\n", GET_VECTOR_PARAMS(*desiredVelocity),    desiredVelocity->shift);
   printf("   Velocity Diff:        (%f, %f, %f) @ %d\n", GET_VECTOR_PARAMS(velocityDiff),        velocityDiff.shift);
   printf("   Desired Acceleration: (%f, %f, %f) @ %d\n", GET_VECTOR_PARAMS(desiredAcceleration), desiredAcceleration.shift);
   printf("   Acceleration:         (%f, %f, %f) @ %d\n", GET_VECTOR_PARAMS(*acceleration),       acceleration->shift);
   printf("\n");
#endif // FSIM_DEBUG_PRINT
}

static inline
void fsimHandleCommand(struct fsimEntity * entity,
                       const struct fsimDynamics * dynamics,
                       const union fsimCommand * command,
                       unsigned int dt_ms,
                       struct i32Vector3 * acceleration,
                       struct i32Vector3 * rotationalAcceleration)
{
   switch(command->type)
   {
   case eFSCT_stop:
      break;
   case eFSCT_velocity:
      fsimComputeDesiredVelocity(entity, 
                                 dynamics, 
                                 &command->velocity.velocity,
                                 dt_ms,
                                 acceleration,
                                 rotationalAcceleration);
      break;
   case eFSCT_position:
      {
         struct i32Vector3 targetOffset, desiredVelocity;
         struct i32FixedPoint distanceToTarget, rampedSpeed, clippedSpeed, zero;

         // targetOffset = targetPosition - position
         (void)i32v3SetV(&targetOffset, &command->position.position);
         (void)i32v3SubtractV(&targetOffset, &entity->position);

         // distanceToTarget = length(targetOffset)
         (void)i32v3LengthFit(&distanceToTarget, &targetOffset, CONFIG_SHIFT_POSITION);
         

         // rampedSpeed = maxSpeed * distanceToTarget / stopDistance
         (void)i32fpCopy(&rampedSpeed, &dynamics->maxSpeed);
         (void)i32fpMultiplyS(&rampedSpeed, &distanceToTarget);
         (void)i32fpDivideS(&rampedSpeed, &dynamics->stopDistance);

         // clippedSpeed = min(rampedSpeed, maxSpeed)
         if(i32fpGreaterThanS(&rampedSpeed, &dynamics->maxSpeed))
         {
            (void)i32fpCopy(&clippedSpeed, &dynamics->maxSpeed);
         }
         else
         {
            (void)i32fpCopy(&clippedSpeed, &rampedSpeed);
         }

         // desiredVelocity = targetOffset * clippedSpeed / distanceToTarget
         (void)i32fpZero(&zero, 0);
         if(i32fpEqualToS(&distanceToTarget, &zero))
         {
            i32v3SetI(&desiredVelocity, 0, 0, 0, CONFIG_SHIFT_VELOCITY);
         }
         else
         {
            struct i32FixedPoint temp;
            
            (void)i32fpCopy(&temp, &clippedSpeed);
            (void)i32fpDivideS(&temp, &distanceToTarget);
            (void)i32v3SetV(&desiredVelocity, &targetOffset);
            (void)i32v3ScaleFpFit(&desiredVelocity, &temp, CONFIG_SHIFT_VELOCITY);
         }
         
#if FSIM_DEBUG_PRINT == 1
         printf("Position Command Calculations:\n");
         printf("   Target Offset:      (%f, %f, %f) @ %d\n", GET_VECTOR_PARAMS(targetOffset),    targetOffset.shift);
         printf("   Distance To Target: %f @ %d\n",           i32fpGetDS(&distanceToTarget),      distanceToTarget.shift);
         printf("   RampedSpeed:        %f @ %d\n",           i32fpGetDS(&rampedSpeed),           rampedSpeed.shift);
         printf("   Clipped Speed:      %f @ %d\n",           i32fpGetDS(&clippedSpeed),          clippedSpeed.shift);
         printf("   Target Velocity:    (%f, %f, %f) @ %d\n", GET_VECTOR_PARAMS(desiredVelocity), desiredVelocity.shift);
         printf("\n");
#endif // FSIM_DEBUG_PRINT
         
         fsimComputeDesiredVelocity(entity, 
                                    dynamics, 
                                    &desiredVelocity,
                                    dt_ms,
                                    acceleration,
                                    rotationalAcceleration);

      }
      break;
   }
}

void fsimUpdate(struct fsimEntity * entity,
                const struct fsimDynamics * dynamics, 
                const union fsimCommand * command, 
                unsigned int dt_ms)
{
   struct i32Vector3 acceleration           = I32V3_MAKE(0, 0, 0, CONFIG_SHIFT_ACCELERATION);
   struct i32Vector3 rotationalAcceleration = I32V3_MAKE(0, 0, 0, CONFIG_SHIFT_ANGULAR_ACCELERATION);
   if(command != NULL)
   {
      fsimHandleCommand(entity, dynamics, command, dt_ms,
                        &acceleration, 
                        &rotationalAcceleration);
   }

      fsimIntegrate(entity, dt_ms, &acceleration, &rotationalAcceleration);
}

void fsimCommandSet(union fsimCommand * dest, const union fsimCommand * src)
{
   memcpy(dest, src, sizeof(union fsimCommand));
}

void fsimCommandFree(union fsimCommand * command)
{
   command->type = eFSCT_free;
}

void fsimCommandStop(union fsimCommand * command)
{
   command->type = eFSCT_stop;
}

void fsimCommandVelocity(union fsimCommand * command, const struct i32Vector3 * velocity)
{
   command->type = eFSCT_velocity;
   i32v3SetV(&command->velocity.velocity, velocity);
}

void fsimCommandPosition(union fsimCommand * command, const struct i32Vector3 * position)
{
   command->type = eFSCT_position;
   i32v3SetV(&command->position.position, position);
}

bool fsimCommandEqual(const union fsimCommand * a, const union fsimCommand * b)
{
   if(a->type != b->type)
   {
      return false;
   }
   switch(a->type)
   {
   case eFSCT_stop:
   case eFSCT_free:
      return true;
      break;
   case eFSCT_velocity:
      return i32v3IsEqual(&a->velocity.velocity, &b->velocity.velocity);
      break;
   case eFSCT_position:
      return i32v3IsEqual(&a->position.position, &b->position.position);
      break;
   }
}

