#ifndef __QUEUE_H__
#define __QUEUE_H__
#include <stddef.h>
#include <stdbool.h>

struct queue
{
   void * base;
   size_t size;
   size_t count;
   size_t head;
   size_t tail;
   size_t elementSize;
   size_t growBy;
};

void queueInit(struct queue * queue, size_t elementSize, size_t initSize, size_t growBy);
void queueInitDefaults(struct queue * queue, size_t elementSize);
void queueFree(struct queue * queue);

void * queuePushCopy(struct queue * queue, const void * element);
void * queuePush(struct queue * queue);
void queuePushMany(struct queue * queue, const void * elements, size_t elementCount);

void * queuePeek(const struct queue * queue);
void * queuePeekCopy(const struct queue * queue, void * element);
size_t queuePeekMany(const struct queue * queue, void * elements, size_t elementCount);
void * queuePop(struct queue * queue);
void * queuePopCopy(struct queue * queue, void * element);
size_t queuePopMany(struct queue * queue, void * elements, size_t elementCount);

void queueClear(struct queue * queue);

size_t queueGetCount(const struct queue * queue);
size_t queueGetFree(const struct queue * queue);
bool   queueIsNotEmpty(const struct queue * queue);

void * queuePeekAt(const struct queue * queue, size_t index);

#define QUEUE_CREATE_INLINE_FUNCTIONS(name, type)                                                              \
static inline void queueInit_ ## name (struct queue * queue, size_t initSize, size_t growBy)                   \
{                                                                                                              \
   queueInit(queue, sizeof(type), initSize, growBy);                                                           \
}                                                                                                              \
                                                                                                               \
static inline void queueInitDefaults_ ## name (struct queue * queue)                                           \
{                                                                                                              \
   queueInitDefaults(queue, sizeof(type));                                                                     \
}                                                                                                              \
                                                                                                               \
static inline type * queuePushCopy_ ## name (struct queue * queue, const type * element)                       \
{                                                                                                              \
   return queuePushCopy(queue, element);                                                                       \
}                                                                                                              \
                                                                                                               \
static inline type * queuePush_ ## name (struct queue * queue)                                                 \
{                                                                                                              \
   return queuePush(queue);                                                                                    \
}                                                                                                              \
                                                                                                               \
static inline void queuePushMany_ ## name (struct queue * queue, const type * elements, size_t elementCount)   \
{                                                                                                              \
   queuePushMany(queue, elements, elementCount);                                                               \
}                                                                                                              \
                                                                                                               \
static inline type * queuePeek_ ## name (const struct queue * queue)                                           \
{                                                                                                              \
   return queuePeek(queue);                                                                                    \
}                                                                                                              \
                                                                                                               \
static inline type * queuePeekCopy_ ## name (const struct queue * queue, type * element)                       \
{                                                                                                              \
   return queuePeekCopy(queue, element);                                                                       \
}                                                                                                              \
                                                                                                               \
static inline size_t queuePeekMany_ ## name (const struct queue * queue, type * elements, size_t elementCount) \
{                                                                                                              \
   return queuePeekMany(queue, elements, elementCount);                                                        \
}                                                                                                              \
                                                                                                               \
static inline type * queuePop_ ## name (struct queue * queue)                                                  \
{                                                                                                              \
   return queuePop(queue);                                                                                     \
}                                                                                                              \
                                                                                                               \
static inline type * queuePopCopy_ ## name (struct queue * queue, type * element)                              \
{                                                                                                              \
   return queuePopCopy(queue, element);                                                                        \
}                                                                                                              \
                                                                                                               \
static inline size_t queuePopMany_ ## name (struct queue * queue, type * elements, size_t elementCount)        \
{                                                                                                              \
   return queuePopMany(queue, elements, elementCount);                                                         \
}                                                                                                              \
                                                                                                               \
static inline type * queuePeekAt_ ## name (const struct queue * queue, size_t index)                           \
{                                                                                                              \
   return queuePeekAt(queue, index);                                                                           \
}                                                                                                              \
                                                                                                               \
static inline void queuePushValue_ ## name (struct queue * queue, type value)                                  \
{                                                                                                              \
   (void)queuePushCopy(queue, &value);                                                                         \
}                                                                                                              \
                                                                                                               \
static inline type queuePeekValue_ ## name (const struct queue * queue, bool * success)                        \
{                                                                                                              \
   type value;                                                                                                 \
   void * result = queuePeekCopy(queue, &value);                                                               \
   if(success != NULL) (*success) = (result != NULL);                                                          \
   return value;                                                                                               \
}                                                                                                              \
                                                                                                               \
static inline type queuePopValue_ ## name (struct queue * queue, bool * success)                               \
{                                                                                                              \
   type value;                                                                                                 \
   void * result = queuePopCopy(queue, &value);                                                                \
   if(success != NULL) (*success) = (result != NULL);                                                          \
   return value;                                                                                               \
}

#endif // __QUEUE_H__
