#include <string.h>

#include "BytePacker.h"

void bypInit(struct bytepacker * byp, void * buffer, size_t size)
{
   byp->buffer = buffer;
   byp->size   = size;
   byp->index  = 0;
}

static inline void bypWriteByte(struct bytepacker * byp, uint8_t value)
{
   if(byp->index < byp->size)
   {
      byp->buffer[byp->index] = value;
   }
   byp->index ++;
}

static inline uint8_t bypReadByte(struct bytepacker * byp)
{
   uint8_t value = 0;
   if(byp->index < byp->size)
   {
      value = byp->buffer[byp->index];
   }
   byp->index ++;
   return value;
}

static inline void bypWriteInt(struct bytepacker * byp, unsigned char byteCount, unsigned long long value)
{
   unsigned char i;
   for(i = 0; i < byteCount; i++)
   {
      bypWriteByte(byp, (uint8_t)value);
      value >>= 8;
   }
}
static inline unsigned long long bypReadInt(struct bytepacker * byp, unsigned char byteCount)
{
   unsigned char i;
   unsigned long long value = 0;
   unsigned int shift = 0;
   for(i = 0; i < byteCount; i ++)
   {
      unsigned long long byte = ((unsigned long long)bypReadByte(byp)) & 0xFF;
      value |= (byte << shift);
      shift += 8;
   }
   return value;
}


void bypWriteInt32(struct bytepacker * byp, uint32_t value)
{
   bypWriteInt(byp, 4, value);
}

void bypWriteInt16(struct bytepacker * byp, uint16_t value)
{
   bypWriteInt(byp, 2, value);
}

void bypWriteInt8(struct bytepacker * byp, uint8_t value)
{
   bypWriteByte(byp, value);
}
void bypWriteBool(struct bytepacker * byp, bool value)
{
   bypWriteByte(byp, value ? 1 : 0);
}

void bypWriteData(struct bytepacker * byp, unsigned char sizeBytes, const void * buffer, size_t bufferSize)
{
   size_t i;
   const unsigned char * byteBuffer = (const unsigned char *)buffer;
   bypWriteInt(byp, sizeBytes, bufferSize);
   for(i = 0; i < bufferSize; i++)
   {
      bypWriteByte(byp, byteBuffer[i]);
   }
}

uint32_t bypReadInt32(struct bytepacker * byp)
{
   return bypReadInt(byp, 4);
}

uint16_t bypReadInt16(struct bytepacker * byp)
{
   return bypReadInt(byp, 2);
}
uint8_t bypReadInt8(struct bytepacker * byp)
{
   return bypReadByte(byp);
}

bool bypReadBool(struct bytepacker * byp)
{
   uint8_t value = bypReadByte(byp);
   return value != 0;
}

const void * bypReadData(struct bytepacker * byp, unsigned char sizeBytes, size_t * size)
{
   size_t lSize = bypReadInt(byp, sizeBytes);
   const void * buffer = &byp->buffer[byp->index];
   byp->index += lSize;
   if(size != NULL)
   {
      (*size) = lSize;
   }
   return buffer;
}

bool bypBytesDropped(const struct bytepacker * byp)
{
   return byp->index > byp->size;
}

size_t bypGetUsedBytes(const struct bytepacker * byp)
{
   size_t usedBytes = byp->index;
   if(usedBytes > byp->size)
   {
      usedBytes = byp->size;
   }
   return usedBytes;
}

size_t bypGetDesiredBytes(const struct bytepacker * byp)
{
   return byp->index;
}

int bypSeek(struct bytepacker * byp, int offset)
{
   int seeked = offset;
   if(offset > 0)
   {
      byp->index += offset;
   }
   else if(offset < 0)
   {
      size_t offsetSize = -offset;
      
      if(offsetSize >= byp->index)
      {
         seeked = -(int)byp->index;
         byp->index = 0;
      }
      else
      {
         byp->index += offset;
      }
   }
   return seeked;
}

int bypSkip(struct bytepacker * byp, size_t toSkip)
{
   byp->index += toSkip;
}

void bypAtomicStart(const struct bytepacker * byp, size_t * state)
{
   (*state) = byp->index;
}

bool bypAtomicCommit(struct bytepacker * byp, const size_t * state, size_t extraRoom)
{
   if((byp->index + extraRoom) > byp->size)
   {
      byp->index = *state;
      return false;
   }
   return true;
}

void bypAtomicAbort(struct bytepacker * byp, const size_t * state)
{
   byp->index = *state;
}

/// bytepackerbuffer
#include <stdlib.h>

#define BYPB_DEFAULT_GROWBY   32
#define BYPB_DEFAULT_INITSIZE 32

void bypbInit(struct bytepackerbuffer * bypb, size_t growBy, size_t initSize)
{
   if(growBy == 0)
   {
      bypb->growBy = BYPB_DEFAULT_GROWBY;
   }
   else
   {
      bypb->growBy = growBy;
   }
   
   if(initSize == 0)
   {
      bypb->size = BYPB_DEFAULT_INITSIZE;
   }
   else
   {
      bypb->size = initSize;
   }
   
   bypb->firstWrite = true;
   bypb->base = malloc(bypb->size);
}

void bypbFree(struct bytepackerbuffer * bypb)
{
   free(bypb->base);
   bypb->base = NULL;
   bypb->size = 0;
}


bool bypbNeedsWrite(struct bytepackerbuffer * bypb, struct bytepacker * byp)
{
   if(bypb->firstWrite)
   {
      bypInit(byp, bypb->base, bypb->size);
      bypb->firstWrite = false;
      return true;
   }
   if(bypBytesDropped(byp))
   {
      bypb->size = bypGetDesiredBytes(byp) + bypb->growBy;
      bypb->base = realloc(bypb->base, bypb->size);
      bypInit(byp, bypb->base, bypb->size);
      return true;
   }
   bypb->firstWrite = true;
   return false;
}

