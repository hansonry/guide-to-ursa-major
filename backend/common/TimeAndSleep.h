#ifndef __TIMEANDSLEEP_H__
#define __TIMEANDSLEEP_H__

// System Specific
unsigned int tasGetMilliseconds(void);

void tasSleep(unsigned int milliseconds);

// Utility Functions
unsigned int tasGetMillisecondDelta(unsigned int * previousMilliseconds);

unsigned int tasSleepUntil(unsigned int targetMillisconds, 
                           unsigned int * previousMilliseconds);

#endif // __TIMEANDSLEEP_H__
