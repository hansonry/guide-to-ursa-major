#ifndef __SIGNALINTERUPT_H__
#define __SIGNALINTERUPT_H__
#include <stdbool.h>

typedef void (*siCallback)(void);

void siSetup(siCallback callback);

bool siNotInterrupted(void);

#endif // __SIGNALINTERUPT_H__
