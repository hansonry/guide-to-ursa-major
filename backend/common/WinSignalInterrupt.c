#include <windows.h>

#include "SignalInterrupt.h"

static siCallback Callback = NULL;

static volatile bool Interrupted = false;

static BOOL WINAPI CtrlHandler(DWORD fdwCtrlType)
{
   if(Interrupted)
   {
      return FALSE;
   }
   if(fdwCtrlType == CTRL_C_EVENT ||
      fdwCtrlType == CTRL_CLOSE_EVENT)
   {
      Interrupted = true;
      if(Callback != NULL)
      {
         Callback();
      }
      return TRUE;
   }
   return FALSE;
}

void siSetup(siCallback callback)
{
   Callback = callback;
   Interrupted = false;
   SetConsoleCtrlHandler(CtrlHandler, TRUE);
}

bool siNotInterrupted(void)
{
   return !Interrupted;
}

