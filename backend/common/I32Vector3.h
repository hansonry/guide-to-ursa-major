#ifndef __IVECTOR3_H__
#define __IVECTOR3_H__
#include <stdint.h>
#include <math.h>
#include <stdbool.h>

#include "I32FixedPoint.h"

#define I32FP_VECTOR_DEBUG_PRINT(valuePtr) printf("> %s = { %f, %f, %f } @ %d\n", #valuePtr, i32fpGetD((valuePtr)->x, (valuePtr)->shift), \
                                                                                             i32fpGetD((valuePtr)->y, (valuePtr)->shift), \
                                                                                             i32fpGetD((valuePtr)->z, (valuePtr)->shift), \
                                                                                             (valuePtr)->shift)

struct i32Vector3
{
   uint32_t x;
   uint32_t y;
   uint32_t z;
   int8_t shift;
};


#define I32V3_MAKE(x, y, z, shift) {x, y, z, shift}

static inline 
struct i32Vector3 * i32v3SetI(struct i32Vector3 * v, uint32_t x, uint32_t y, uint32_t z, uint8_t shift)
{
   v->x = x;
   v->y = y;
   v->z = z;
   v->shift = shift;
   return v;
}

static inline 
struct i32Vector3 * i32v3SetF(struct i32Vector3 * v, float x, float y, float z, uint8_t shift)
{

   v->x = i32fpSetD(x, shift);
   v->y = i32fpSetD(y, shift);
   v->z = i32fpSetD(z, shift);
   v->shift = shift;
   return v;
}

static inline 
struct i32Vector3 * i32v3SetV(struct i32Vector3 * vDest, const struct i32Vector3 * vSrc)
{
   vDest->x     = vSrc->x;
   vDest->y     = vSrc->y;
   vDest->z     = vSrc->z;
   vDest->shift = vSrc->shift;
   return vDest;
}

static inline 
struct i32Vector3 * i32v3SetShift(struct i32Vector3 * v, int8_t newShift)
{   
   v->x = i32fpSetShift(v->x, v->shift, newShift);
   v->y = i32fpSetShift(v->y, v->shift, newShift);
   v->z = i32fpSetShift(v->z, v->shift, newShift);
   return v;
}


static inline 
void i32v3GetF(const struct i32Vector3 * v, float * x, float * y, float * z)
{
   if(x != NULL)
   {
      (*x) = i32fpGetD(v->x, v->shift);
   }
   if(y != NULL)
   {
      (*y) = i32fpGetD(v->y, v->shift);
   }
   if(z != NULL)
   {
      (*z) = i32fpGetD(v->z, v->shift);
   }
}

static inline 
void i32v3GetFp(const struct i32Vector3 * v, struct i32FixedPoint * x, struct i32FixedPoint * y, struct i32FixedPoint * z)
{
   if(x != NULL)
   {
      i32fpSetS(x, v->x, v->shift);
   }
   if(y != NULL)
   {
      i32fpSetS(y, v->y, v->shift);
   }
   if(z != NULL)
   {
      i32fpSetS(z, v->z, v->shift);
   }
}

static inline 
void i32v3GetFpA(const struct i32Vector3 * v, struct i32FixedPoint * fpv)
{
   i32fpSetS(&fpv[0], v->x, v->shift);
   i32fpSetS(&fpv[1], v->y, v->shift);
   i32fpSetS(&fpv[2], v->z, v->shift);
}

static inline 
void i32v3SetFpA(struct i32Vector3 * v, struct i32FixedPoint * fpv)
{
   i32fpSetToCommonShift(fpv, 3, 0);
   v->shift = fpv[0].shift;
   v->x     = fpv[0].value;
   v->y     = fpv[1].value;
   v->z     = fpv[2].value;
}

static inline 
void i32v3SetFitFpA(struct i32Vector3 * v, struct i32FixedPoint * fpv, int8_t shift)
{
   i32fpSetToCommonShiftFit(fpv, 3, shift);
   v->shift = shift;
   v->x     = fpv[0].value;
   v->y     = fpv[1].value;
   v->z     = fpv[2].value;
}

static inline 
struct i32Vector3 * i32v3Invert(struct i32Vector3 * v)
{
   v->x = i32fpInvert(v->x);
   v->y = i32fpInvert(v->y);
   v->z = i32fpInvert(v->z);
   return v;
}



static inline 
struct i32Vector3 * i32v3AddV(struct i32Vector3 * a, const struct i32Vector3 * b)
{   
   a->x = i32fpAdd(a->x, a->shift, b->x, b->shift);
   a->y = i32fpAdd(a->y, a->shift, b->y, b->shift);
   a->z = i32fpAdd(a->z, a->shift, b->z, b->shift);
   return a;
}

static inline 
struct i32Vector3 * i32v3AddF3(struct i32Vector3 * v, float x, float y, float z)
{
   struct i32Vector3 b;
   i32v3SetF(&b, x, y, z, v->shift);
   return i32v3AddV(v, &b);
}


static inline 
struct i32Vector3 * i32v3SubtractF3(struct i32Vector3 * v, float x, float y, float z)
{
   struct i32Vector3 b;
   i32v3SetF(&b, -x, -y, -z, v->shift);
   return i32v3AddV(v, &b);
}

static inline 
struct i32Vector3 * i32v3SubtractV(struct i32Vector3 * a, const struct i32Vector3 * b)
{
   a->x = i32fpSubtract(a->x, a->shift, b->x, b->shift);
   a->y = i32fpSubtract(a->y, a->shift, b->y, b->shift);
   a->z = i32fpSubtract(a->z, a->shift, b->z, b->shift);
   return a;
}

static inline 
struct i32Vector3 * i32v3ScaleFitV(struct i32Vector3 * a, const struct i32Vector3 * b, uint8_t resultShift)
{
   a->x = i32fpMultiplyFit(a->x, a->shift, b->x, b->shift, resultShift);
   a->y = i32fpMultiplyFit(a->y, a->shift, b->y, b->shift, resultShift);
   a->z = i32fpMultiplyFit(a->z, a->shift, b->z, b->shift, resultShift);
   a->shift = resultShift;
   return a;
}

static inline 
struct i32Vector3 * i32v3ScaleFpFit(struct i32Vector3 * v, const struct i32FixedPoint * k, uint8_t resultShift)
{
   v->x = i32fpMultiplyFit(v->x, v->shift, k->value, k->shift, resultShift);
   v->y = i32fpMultiplyFit(v->y, v->shift, k->value, k->shift, resultShift);
   v->z = i32fpMultiplyFit(v->z, v->shift, k->value, k->shift, resultShift);
   v->shift = resultShift;
   return v;
}

static inline 
struct i32Vector3 * i32v3DivideFpFit(struct i32Vector3 * v, const struct i32FixedPoint * k, int8_t resultShift)
{
   v->x = i32fpDivideFit(v->x, v->shift, k->value, k->shift, resultShift);
   v->y = i32fpDivideFit(v->y, v->shift, k->value, k->shift, resultShift);
   v->z = i32fpDivideFit(v->z, v->shift, k->value, k->shift, resultShift);
   v->shift = resultShift;
   return v;
}


static inline 
struct i32FixedPoint * i32v3DotFit(struct i32FixedPoint * dest, const struct i32Vector3 * a, const struct i32Vector3 * b, int8_t resultShift)
{
   struct i32FixedPoint fpAx, fpAy, fpAz, fpBx, fpBy, fpBz;
   // Get All the components
   i32v3GetFp(a, &fpAx, &fpAy, &fpAz);
   i32v3GetFp(b, &fpBx, &fpBy, &fpBz);
   
   // Multiply components together
   (void)i32fpMultiplyS(&fpAx, &fpBx);
   (void)i32fpMultiplyS(&fpAy, &fpBy);
   (void)i32fpMultiplyS(&fpAz, &fpBz);
   
   // Sum up products
   (void)i32fpZero(dest, resultShift);
   (void)i32fpAddS(dest, &fpAx);
   (void)i32fpAddS(dest, &fpAy);
   (void)i32fpAddS(dest, &fpAy);
   return dest;
}

static inline 
struct i32Vector3 * i32v3CrossFit(struct i32Vector3 * a, const struct i32Vector3 * b, int8_t resultShift)
{
   struct i32FixedPoint fpAx, fpAy, fpAz, fpBx, fpBy, fpBz, fpAx1, fpAy1, fpAz1;
   // Get All the components
   i32v3GetFp(a, &fpAx, &fpAy, &fpAz);
   i32v3GetFp(b, &fpBx, &fpBy, &fpBz);
   
   (void)i32fpCopy(&fpAx1, &fpAx);
   (void)i32fpCopy(&fpAy1, &fpAy);
   (void)i32fpCopy(&fpAz1, &fpAz);
   
   (void)i32fpMultiplyS(&fpAy, &fpBz);
   (void)i32fpMultiplyS(&fpAz, &fpBx);
   (void)i32fpMultiplyS(&fpAx, &fpBy);
   
   (void)i32fpMultiplyS(&fpAz1, &fpBy);
   (void)i32fpMultiplyS(&fpAx1, &fpBz);
   (void)i32fpMultiplyS(&fpAy1, &fpBx);
   
   (void)i32fpZero(&fpBx, resultShift);
   (void)i32fpZero(&fpBy, resultShift);
   (void)i32fpZero(&fpBz, resultShift);
   
   (void)i32fpAddS(&fpBx, &fpAy);
   (void)i32fpAddS(&fpBy, &fpAz);
   (void)i32fpAddS(&fpBz, &fpAx);

   (void)i32fpSubtractS(&fpBx, &fpAz1);
   (void)i32fpSubtractS(&fpBy, &fpAx1);
   (void)i32fpSubtractS(&fpBz, &fpAy1);
   
   //x = a->y * b->z - a->z * b->y;
   //y = a->z * b->x - a->x * b->z;
   //z = a->x * b->y - a->y * b->x;
   
   a->shift = resultShift;
   a->x = fpBx.value;
   a->y = fpBy.value;
   a->z = fpBz.value;
   return a;
}

static inline
struct i32FixedPoint * i32v3Length2Fit(struct i32FixedPoint * dest, const struct i32Vector3 * v, int8_t resultShift)
{
   struct i32FixedPoint fpX, fpY, fpZ;
   i32v3GetFp(v, &fpX, &fpY, &fpZ);
   
   (void)i32fpMultiplyS(&fpX, &fpX);
   (void)i32fpMultiplyS(&fpY, &fpY);
   (void)i32fpMultiplyS(&fpZ, &fpZ);
   
   (void)i32fpZero(dest, resultShift);
   (void)i32fpAddS(dest, &fpX);
   (void)i32fpAddS(dest, &fpY);
   (void)i32fpAddS(dest, &fpZ);
   
   return dest;
}


static inline
struct i32FixedPoint * i32v3LengthFit(struct i32FixedPoint * dest, const struct i32Vector3 * v, int8_t resultShift)
{
   (void)i32v3Length2Fit(dest, v, resultShift);
   (void)i32fpSqrt(dest);
   return i32fpSetShiftS(dest, resultShift);
}

static inline
struct i32Vector3 * i32v3NormalizeFit(struct i32Vector3 * v, int8_t resultShift)
{
   struct i32FixedPoint length, zero;
   (void)i32v3LengthFit(&length, v, resultShift);
   (void)i32fpZero(&zero, 0);
   if(i32fpEqualToS(&length, &zero))
   {
      return i32v3SetI(v, 0, 0, 0, resultShift);
   }
   return i32v3DivideFpFit(v, &length, resultShift); 
}

static inline
struct i32Vector3 * i32v3AddScaled(struct i32Vector3 * a, const struct i32Vector3 * b, int32_t mult, int32_t div)
{
   a->x = i32fpAddScaledI2(a->x, a->shift, b->x, b->shift, mult, div);
   a->y = i32fpAddScaledI2(a->y, a->shift, b->y, b->shift, mult, div);
   a->z = i32fpAddScaledI2(a->z, a->shift, b->z, b->shift, mult, div);
   return a;
}

static inline
struct i32Vector3 * i32v3ScaleI2(struct i32Vector3 * v, int32_t mult, int32_t div)
{
   struct i32FixedPoint fpv[3];
  
   i32v3GetFpA(v, fpv);
 
   i32fpScaleSI2(&fpv[0], mult, div);
   i32fpScaleSI2(&fpv[1], mult, div);
   i32fpScaleSI2(&fpv[2], mult, div);

   i32v3SetFpA(v, fpv);

   return v;
}

static inline
struct i32Vector3 * i32v3MaxLength(struct i32Vector3 * v, const struct i32FixedPoint * length)
{
   struct i32FixedPoint compLength;

   i32v3LengthFit(&compLength, v, length->shift);
   if(i32fpGreaterThanS(&compLength, length))
   {
      (void)i32fpDivideS(&compLength, length); 
      (void)i32v3DivideFpFit(v, &compLength, v->shift);
   }
   return v;
}


static inline
bool i32v3IsEqual(const struct i32Vector3 * a, const struct i32Vector3 * b)
{
   return i32fpEqualTo(a->x, a->shift, b->x, b->shift) &&
          i32fpEqualTo(a->y, a->shift, b->y, b->shift) &&
          i32fpEqualTo(a->z, a->shift, b->z, b->shift);
}

#endif // __IVECTOR3_H__
