#include <stdlib.h>
#include <string.h>

#include "Queue.h"

typedef unsigned char byte;

#define DEFAULT_GROWBY   32
#define DEFAULT_INITSIZE 32

void queueInit(struct queue * queue, size_t elementSize, size_t initSize, size_t growBy)
{
   if(initSize == 0)
   {
      queue->size = DEFAULT_INITSIZE;
   }
   else
   {
      queue->size = initSize;
   }

   if(growBy == 0)
   {
      queue->growBy = DEFAULT_GROWBY;
   }
   else
   {
      queue->growBy = growBy;
   }
   
   queue->elementSize = elementSize;
   queue->count       = 0;
   queue->head        = 0;
   queue->tail        = 0;
   
   queue->base = malloc(queue->elementSize * queue->size);
}

void queueInitDefaults(struct queue * queue, size_t elementSize)
{
   queueInit(queue, elementSize, 0, 0);
}

void queueFree(struct queue * queue)
{
   free(queue->base);
   queue->base        = NULL;
   queue->size        = 0;
   queue->count       = 0;
   queue->head        = 0;
   queue->tail        = 0;
   queue->growBy      = 0;
   queue->elementSize = 0;
}

static inline void queueAddToIndex(const struct queue * queue, size_t * index, size_t value)
{
   (*index) = ((*index) + value) % queue->size;
}

#define GET_OFFSET(base, elementSize, index) (&((byte*)(base))[(elementSize) * (index)])

static inline void * queueGetIndexNoCheck(const struct queue * queue, size_t index)
{
   return GET_OFFSET(queue->base, queue->elementSize, index);
}

static inline void queueGrowToFit(struct queue * queue, size_t count)
{
   size_t newSize = count + queue->growBy;
   queue->base = realloc(queue->base, queue->elementSize * newSize);
   
   // Check to see if we should move up the top elements
   if(queue->count == queue->size || queue->head < queue->tail)
   {
      size_t oldIndex;
      const size_t sizeDiff = newSize - queue->size;
      for(oldIndex = queue->size - 1; 
          oldIndex < queue->size && oldIndex >= queue->tail; 
          oldIndex --)
      {
         const size_t newIndex = oldIndex + sizeDiff;
         void * oldElement = queueGetIndexNoCheck(queue, oldIndex);
         void * newElement = queueGetIndexNoCheck(queue, newIndex);
         memcpy(newElement, oldElement, queue->elementSize);
      }
      queue->tail += sizeDiff;
   }
   queue->size = newSize;
}

static inline void queueMakeRoomFor(struct queue * queue, size_t count)
{
   if(count >= queue->size)
   {
      queueGrowToFit(queue, count);
   }
}

static inline void * queuePushNoSizeCheck(struct queue * queue)
{
   size_t index;
   if(queue->count >= queue->size)
   {
      return NULL;
   }
   index = queue->head;
   queueAddToIndex(queue, &queue->head, 1);
   queue->count ++;
   return queueGetIndexNoCheck(queue, index);
}

void * queuePushCopy(struct queue * queue, const void * element)
{
   void * mem;
   queueMakeRoomFor(queue, queue->count + 1); 
   mem = queuePushNoSizeCheck(queue);
   if(mem == NULL)
   {
      return NULL;
   }
   memcpy(mem, element, queue->elementSize);
   return mem;
}

void * queuePush(struct queue * queue)
{
   queueMakeRoomFor(queue, queue->count + 1);   
   return queuePushNoSizeCheck(queue);
}

void queuePushMany(struct queue * queue, const void * elements, size_t elementCount)
{
   size_t i;
   queueMakeRoomFor(queue, queue->count + elementCount);
   for(i = 0; i < elementCount; i++)
   {
      void * mem = queuePushNoSizeCheck(queue);
      memcpy(mem, GET_OFFSET(elements, queue->elementSize, i), queue->elementSize);
   }
}

void * queuePeek(const struct queue * queue)
{
   if(queue->count == 0)
   {
      return NULL;
   }
   return queueGetIndexNoCheck(queue, queue->tail);
}

void * queuePeekCopy(const struct queue * queue, void * element)
{
   void * mem = queuePeek(queue);
   if(mem == NULL)
   {
      return NULL;
   }
   memcpy(element, mem, queue->elementSize);
   return element;
}
size_t queuePeekMany(const struct queue * queue, void * elements, size_t elementCount)
{
   size_t peekCount;
   size_t i;
   size_t index;
   if(elementCount > queue->count)
   {
      peekCount = queue->count;
   }
   else
   {
      peekCount = elementCount;
   }
   index = queue->tail;
   for(i = 0; i < peekCount; i++)
   {
      memcpy(GET_OFFSET(elements, queue->elementSize, i), 
             queueGetIndexNoCheck(queue, index),
             queue->elementSize);
      queueAddToIndex(queue, &index, 1);
   }
   
   return peekCount;
}

void * queuePop(struct queue * queue)
{
   size_t index;
   if(queue->count == 0)
   {
      return NULL;
   }
   index = queue->tail;
   queueAddToIndex(queue, &queue->tail, 1);
   queue->count --;
   return queueGetIndexNoCheck(queue, index);
}

void * queuePopCopy(struct queue * queue, void * element)
{
   void * mem = queuePeek(queue);
   if(mem == NULL)
   {
      return NULL;
   }
   memcpy(element, mem, queue->elementSize);
   (void)queuePop(queue);
   return element;
}

size_t queuePopMany(struct queue * queue, void * elements, size_t elementCount)
{
   size_t popedCount;
   size_t i;
   if(elementCount > queue->count)
   {
      popedCount = queue->count;
   }
   else
   {
      popedCount = elementCount;
   }
   if(elements == NULL)
   {
      for(i = 0; i < popedCount; i++)
      {
         (void)queuePop(queue);
      }
   }
   else
   {
      for(i = 0; i < popedCount; i++)
      {
         (void)queuePopCopy(queue, GET_OFFSET(elements, queue->elementSize, i));
      }
   }
   return popedCount;
}

void queueClear(struct queue * queue)
{
   queue->count = 0;
   queue->head  = 0;
   queue->tail  = 0;
}

size_t queueGetCount(const struct queue * queue)
{
   return queue->count;
}

size_t queueGetFree(const struct queue * queue)
{
   return queue->size - queue->count;
}

bool queueIsNotEmpty(const struct queue * queue)
{
   return queue->count > 0;
}

void * queuePeekAt(const struct queue * queue, size_t index)
{
   size_t queueIndex;
   if(index >= queue->count)
   {
      return NULL;
   }
   
   queueIndex = queue->tail;
   queueAddToIndex(queue, &queueIndex, index);
   return queueGetIndexNoCheck(queue, queueIndex);
}
