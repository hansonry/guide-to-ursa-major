#ifndef __TCPNETWORK_H__
#define __TCPNETWORK_H__
#include <stdbool.h>
#include <stddef.h>

struct tcpNetwork;
struct tcpNetworkSocket;
struct tcpNetworkServer;

struct tcpNetwork
{
   void (*fpDestroy)(struct tcpNetwork * tcpNet);
   struct tcpNetworkServer * (*fpCreateServer)(struct tcpNetwork * tcpNet, const char * host, const char * service);
   struct tcpNetworkSocket * (*fpCreateClient)(struct tcpNetwork * tcpNet, const char * host, const char * service);
};

struct tcpNetworkSocket
{
   void (*fpDestroy)(struct tcpNetworkSocket * socket);
   struct tcpNetwork * (*fpGetNetwork)(const struct tcpNetworkSocket * socket);
   bool (*fpIsOpen)(struct tcpNetworkSocket * socket);
   ssize_t (*fpRead)(struct tcpNetworkSocket * socket, void * buffer, size_t size);
   bool (*fpWrite)(struct tcpNetworkSocket * socket, const void * buffer, size_t size);
};

struct tcpNetworkServer
{
   void (*fpDestroy)(struct tcpNetworkServer * server);
   struct tcpNetwork * (*fpGetNetwork)(const struct tcpNetworkServer * server);
   struct tcpNetworkSocket * (*fpAccept)(struct tcpNetworkServer * server);
};

struct tcpNetwork * tcpnCreateNativeNetwork(void);

/// tcpNetwork
static inline void tcpnDestroy(struct tcpNetwork * tcpNet)
{
   tcpNet->fpDestroy(tcpNet);
}

static inline struct tcpNetworkServer * tcpnCreateServer(struct tcpNetwork * tcpNet, const char * host, const char * service)
{
   return tcpNet->fpCreateServer(tcpNet, host, service);
}

static inline struct tcpNetworkSocket * tcpnCreateClient(struct tcpNetwork * tcpNet, const char * host, const char * service)
{
   return tcpNet->fpCreateClient(tcpNet, host, service);
}

/// tcpNetworkSocket

static inline void tcpnSocketDestroy(struct tcpNetworkSocket * socket)
{
   socket->fpDestroy(socket);
}

static inline struct tcpNetwork * tcpnSocketGetNetwork(const struct tcpNetworkSocket * socket)
{
   return socket->fpGetNetwork(socket);
}

static inline bool tcpnSocketIsOpen(struct tcpNetworkSocket * socket)
{
   return socket->fpIsOpen(socket);
}

static inline ssize_t tcpnSocketRead(struct tcpNetworkSocket * socket, void * buffer, size_t size)
{
   return socket->fpRead(socket, buffer, size);
}

static inline bool tcpnSocketWrite(struct tcpNetworkSocket * socket, const void * buffer, size_t size)
{
   return socket->fpWrite(socket, buffer, size);
}

/// tcpNetworkServer


static inline void tcpnServerDestroy(struct tcpNetworkServer * server)
{
   server->fpDestroy(server);
}
static inline struct tcpNetwork * tcpnServerGetNetwork(const struct tcpNetworkServer * server)
{
   return server->fpGetNetwork(server);
}

static inline struct tcpNetworkSocket * tcpnServerAccept(struct tcpNetworkServer * server)
{
   return server->fpAccept(server);
}


#endif // __TCPNETWORK_H__
