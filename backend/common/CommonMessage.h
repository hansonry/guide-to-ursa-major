#ifndef __COMMONMESSAGE_H__
#define __COMMONMESSAGE_H__
#include <stdint.h>
#include <string.h>

struct baseMsg
{
   uint16_t type;
   uint8_t rest[];
};

struct msgI32Vector3
{
   int32_t x;
   int32_t y;
   int32_t z;
};

struct msgI32Vector4
{
   int32_t w;
   int32_t x;
   int32_t y;
   int32_t z;
};

static inline 
void msgI32Vector3Copy(struct msgI32Vector3 * dest, const struct msgI32Vector3 * src)
{
   dest->x = src->x;
   dest->y = src->y;
   dest->z = src->z;
}

static inline 
void msgI32Vector4Copy(struct msgI32Vector4 * dest, const struct msgI32Vector4 * src)
{
   dest->w = src->w;
   dest->x = src->x;
   dest->y = src->y;
   dest->z = src->z;
}

static inline
void msgTypeSafeCopy(void * to, const void * from, size_t size)
{
   struct baseMsg * bmTo = (struct baseMsg*)to;
   const struct baseMsg * bmFrom = (const struct baseMsg*)from;
   uint16_t type = bmTo->type;

   memcpy(bmTo, bmFrom, size);
   
   bmTo->type = type;
}

#endif // __COMMONMESSAGE_H__
