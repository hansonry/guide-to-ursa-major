
-- Utiltity Functions --

local function TableConcat(t1, t2)
   for i=1, #t2 do
      t1[#t1 + 1] = t2[i]
   end
   return t1
end

local function TableJoin(t1, t2, t3, t4)
   local newTable = {}
   if t1 ~= nil then TableConcat(newTable, t1) end
   if t2 ~= nil then TableConcat(newTable, t2) end
   if t3 ~= nil then TableConcat(newTable, t3) end
   if t4 ~= nil then TableConcat(newTable, t4) end
   return newTable
end


-- Data Types --
local CommonEnumerations = {
   { name = "AuthRequestType", size = 8, values = { 
         { name = "LogIn",  value = 1 },
         { name = "LogOut", value = 2 }
      }
   },
}
local ServerEnumerations = TableJoin(CommonEnumerations, {})

local ClientEnumerations = TableJoin(CommonEnumerations,
   {
      { name = "ConnectionState", size = 8, values = { 
            { name = "Disconnected",  value = 1 },
            { name = "Connecting",    value = 2 },
            { name = "Connected",     value = 3 },
            { name = "Disconnecting", value = 4 }
         }
      },
   }
)

-- Command Structures --

local CommandStructFree = {}

local CommandStructStop = {}

local CommandStructVelocity = {
   { name = "targetVelocity",  type = "vector3" }
}

local CommandStructPosition = {
   { name = "targetPosition",  type = "vector3" }
}

-- Common Message --

local AuthRequestMessageStructure = {
   { name = "requestType",  type = "enum", enum = "AuthRequestType" },
   { name = "userName",     type = "string", size = 8 },
   { name = "userPassword", type = "string", size = 8 }
}

local AuthResponseMessageStructure = {
   { name = "requestType", type = "enum", enum = "AuthRequestType" },
   { name = "success",     type = "bool" }
}

local SyncMessageStructure = {
   { name = "step",                      type = "uint", size = 8 },
   { name = "lastClientCmdIdProccessed", type = "uint", size = 8 },
   { name = "nextStepTime_ms",           type = "uint", size = 16 },
}

local EntityCreateMessageStructure = {
   { name = "simId",           type = "uint", size = 32 },
   { name = "maxSpeed",        type = "uint", size = 32 },
   { name = "maxAcceleration", type = "uint", size = 32 },
   { name = "stopDistance",    type = "uint", size = 32 }
   -- TODO = Probably need some kind of type enumeration
}

local EntityDestroyMessageStructure = {
   { name = "simId",           type = "uint", size = 32 }
}

local CommandStructure = {
  { name = "simId",  type = "uint", size = 32 },
}

local EntityCommandFreeMessageStructure     = TableJoin(CommandStructFree,     CommandStructure)
local EntityCommandStopMessageStructure     = TableJoin(CommandStructStop,     CommandStructure)
local EntityCommandVelocityMessageStructure = TableJoin(CommandStructVelocity, CommandStructure)
local EntityCommandPositionMessageStructure = TableJoin(CommandStructPosition, CommandStructure)

local EntityPlayerControlMessageStructure = {
   { name = "simId",  type = "uint", size = 32 },
   { name = "active", type = "bool" }
}

local PlayerCommandStructure = {
   { name = "id", type = "uint", size = 8 },
}

local EntityPlayerCommandFreeMessageStructure     = TableJoin(CommandStructFree,     PlayerCommandStructure)
local EntityPlayerCommandStopMessageStructure     = TableJoin(CommandStructStop,     PlayerCommandStructure)
local EntityPlayerCommandVelocityMessageStructure = TableJoin(CommandStructVelocity, PlayerCommandStructure)
local EntityPlayerCommandPositionMessageStructure = TableJoin(CommandStructPosition, PlayerCommandStructure)

local EntityUpdateMessageStructure = {
   { name = "simId",              type = "uint", size = 32 },
   { name = "position",           type = "vector3" },
   { name = "velocity",           type = "vector3" },
   { name = "rotation",           type = "quat"    },
   { name = "rotationalVelocity", type = "vector3" },
}


-- Server Messages --


local GreetingServerMessageStructure = {}

local KeepAliveServerMessageStructure = {}

local EntityUpdateServerMessageStructure = TableJoin({}, EntityUpdateMessageStructure)


-- Client Messages --

local ExitClientMessageStructure = {}

local HostInfoClientMessageStructure = {
   { name = "set",     type = "bool" },
   { name = "host",    type = "string", size = 8 },
   { name = "service", type = "string", size = 8 }
}

local ConnectionStateClientMessageStructure = {
   { name = "set",           type = "bool" },
   { name = "stayConnected", type = "bool" },
   { name = "state",         type = "enum", enum = "ConnectionState"}
}

local EntityUpdateClientMessageStructure = TableJoin(EntityUpdateMessageStructure, {
   { name = "serverUpdate", type = "bool" }
})

-- NetworkMessageConfig --

local CommandAndPlayerCommandMessages = {
   { name = "EntityCommandFree",           id = 20, structure = EntityCommandFreeMessageStructure },
   { name = "EntityCommandStop",           id = 21, structure = EntityCommandStopMessageStructure },
   { name = "EntityCommandVelocity",       id = 22, structure = EntityCommandVelocityMessageStructure },          
   { name = "EntityCommandPosition",       id = 23, structure = EntityCommandPositionMessageStructure },          
   { name = "EntityPlayerCommandFree",     id = 40, structure = EntityPlayerCommandFreeMessageStructure },
   { name = "EntityPlayerCommandStop",     id = 41, structure = EntityPlayerCommandStopMessageStructure },
   { name = "EntityPlayerCommandVelocity", id = 42, structure = EntityPlayerCommandVelocityMessageStructure },
   { name = "EntityPlayerCommandPosition", id = 43, structure = EntityPlayerCommandPositionMessageStructure },
}

local NetworkMessageConfig = {
   Server = {
      Enumerations = ServerEnumerations,
      Messages = TableJoin({
         { name = "Greeting",            id = 1,  structure = GreetingServerMessageStructure },
         { name = "KeepAlive",           id = 2,  structure = KeepAliveServerMessageStructure },
         { name = "AuthRequest",         id = 3,  structure = AuthRequestMessageStructure },
         { name = "AuthResponse",        id = 4,  structure = AuthResponseMessageStructure },      
         { name = "Sync",                id = 5,  structure = SyncMessageStructure },      
         { name = "EntityCreate",        id = 6,  structure = EntityCreateMessageStructure },      
         { name = "EntityDestroy",       id = 7,  structure = EntityDestroyMessageStructure },      
         { name = "EntityUpdate",        id = 8,  structure = EntityUpdateServerMessageStructure },      
         { name = "EntityPlayerControl", id = 9,  structure = EntityPlayerControlMessageStructure },      
      }, CommandAndPlayerCommandMessages)      
   },
   Client = {
      Enumerations = ClientEnumerations,
      Messages = TableJoin({
         { name = "Exit",                id = 1,  structure = ExitClientMessageStructure },
         { name = "HostInfo",            id = 2,  structure = HostInfoClientMessageStructure },
         { name = "ConnectionState",     id = 3,  structure = ConnectionStateClientMessageStructure },
         { name = "AuthRequest",         id = 4,  structure = AuthRequestMessageStructure },
         { name = "AuthResponse",        id = 5,  structure = AuthResponseMessageStructure },
         { name = "Sync",                id = 6,  structure = SyncMessageStructure },
         { name = "EntityCreate",        id = 7,  structure = EntityCreateMessageStructure },
         { name = "EntityDestroy",       id = 8,  structure = EntityDestroyMessageStructure },
         { name = "EntityUpdate",        id = 9,  structure = EntityUpdateClientMessageStructure },
         { name = "EntityPlayerControl", id = 10, structure = EntityPlayerControlMessageStructure },
      }, CommandAndPlayerCommandMessages)
   }  
}

return NetworkMessageConfig

