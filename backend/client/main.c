// Client Main
#include <stdio.h>

#include <common/TCPNetwork.h>
#include <common/MessageLayer.h>
#include <common/SignalInterrupt.h>
#include <common/TimeAndSleep.h>
#include <common/Config.h>
#include <common/ServerMessage.h>
#include <common/List.h>
#include <common/FlightSimulation.h>
#include "ClientMessage.h"

#define CLIENT_CHECK_RATE_MS 100
#define CLIENT_FRONTEND_SERVER_PORT "5486"

struct networking;
struct simulation;
struct simEntity;

LIST_CREATE_INLINE_FUNCTIONS(simEntityPtr, struct simEntity *)

void SimulationUpdate(struct simulation * simulation);
void SimulationSendUpdatesToFrontEnd(struct simulation * simulation, 
                                     struct networking * networking);
struct simEntity * SimulationCreateNewEntity(struct simulation * simulation, 
                                             uint32_t simId);
struct simEntity * SimulationLookupSimId(struct simulation * simulation,
                                         uint32_t simId);

struct simEntity
{
   uint32_t simId;
   struct fsimEntity fsim;
   struct fsimDynamics dynamics;
   union fsimCommand activeCommand;
};

struct simulation
{
   struct networking * networking;
   struct list entityList;
   uint32_t controlledSimId;
   bool controlActive;
};

// =============================== Netowrking =============================== //
struct networking
{
   struct simulation * simulation;
   struct tcpNetwork * net;
   struct tcpNetworkServer * frontEndServer;
   struct tcpNetworkSocket * socket;
   struct tcpNetworkSocket * frontEndSocket;
   struct bitpackerbuffer serverSendBuffer;
   struct messageLayer * msgLayer;
   struct messageLayer * frontEndMsgLayer;
   char * serverHost;
   char * serverService;
   struct bytepackerbuffer frontEndSendBuffer;
   struct stringBuffer * stringBuffer;
   bool stayConnected;
   bool exitRequested;
   uint8_t connectionState;
   unsigned int stepsSinceLastSentMessage;
};

static void ServerMessageReceived(void * userData, 
                                  struct messageLayer * layer, 
                                  struct tcpNetworkSocket * socket, 
                                  void * message, uint16_t messageSize);

static inline bool NetworkingIsServerSocketConnected(struct networking * networking)
{
   return networking->socket != NULL && tcpnSocketIsOpen(networking->socket);
}

static inline bool NetworkingIsFrontEndSocketConnected(struct networking * networking)
{
   return networking->frontEndSocket != NULL && tcpnSocketIsOpen(networking->frontEndSocket);
}

static inline void NetworkingSendServerMessage(struct messageLayer * layer, struct bitpackerbuffer * bpb, union serverMessage * msg)
{
   struct bitpacker bp;
   if(layer != NULL)
   {
      while(bpbNeedsWrite(bpb, &bp))
      {
         servmsgWrite(&bp, msg, false);
      }
      msglQueueToSend(layer, bp.buffer, bpGetCurrentUsageInBytes(&bp));
   }
}

static inline void SendFrontEndMessage(struct messageLayer * layer, struct bytepackerbuffer * bypb, union clientMessage * msg)
{
   struct bytepacker byp;
   if(layer != NULL)
   {
      while(bypbNeedsWrite(bypb, &byp))
      {
         clmsgWrite(&byp, msg, false);
      }
      msglQueueToSend(layer, byp.buffer, bypGetUsedBytes(&byp));
      printf("Sent to Front End:\n");
      clmsgDump(msg);
   }
}

static inline void NetworkingSendFrontEndMessage(struct networking * networking, union clientMessage * msg)
{
   SendFrontEndMessage(networking->frontEndMsgLayer, &networking->frontEndSendBuffer, msg);
}

static inline void NetworkingSendToServer(struct networking * networking, union serverMessage * msg)
{
   NetworkingSendServerMessage(networking->msgLayer, &networking->serverSendBuffer, msg);
   networking->stepsSinceLastSentMessage = 0;
}

static inline void NetworkSendGreeting(struct networking * networking)
{
   union serverMessage greeting;
   greeting.type = SERVMSG_TYPE_GREETING;
   NetworkingSendToServer(networking, &greeting);
   
}

static inline void NetworkingSetHost(struct networking * networking, const char * host, const char * service)
{
   if(networking->serverHost != host)
   {
      if(networking->serverHost != NULL)
      {
         free(networking->serverHost);
      }
      networking->serverHost = strdup(host);
   }
   if(networking->serverService != service)
   {
      if(networking->serverService != NULL)
      {
         free(networking->serverService);
      }
      networking->serverService = strdup(service);
   }
}

static inline void NetworkingSetConnectionState(struct networking * networking, 
                                                uint8_t newConnectionState)
{
   if(networking->connectionState != newConnectionState)
   {
      union clientMessage msg;
      networking->connectionState = newConnectionState;
      msg.type                          = CLMSG_TYPE_CONNECTIONSTATE;
      msg.connectionState.set           = false;
      msg.connectionState.stayConnected = networking->stayConnected;
      msg.connectionState.state         = networking->connectionState;
      
      NetworkingSendFrontEndMessage(networking, &msg);
   }
}

static inline void NetworkHostDisconnectAndDestroy(struct networking * networking)
{
   if(networking->msgLayer != NULL)
   {
      msglDestory(networking->msgLayer);
      networking->msgLayer = NULL;
   }

   if(networking->socket != NULL)
   {
      tcpnSocketDestroy(networking->socket);
      networking->socket = NULL;
   }
   NetworkingSetConnectionState(networking, CLMSG_CONNECTIONSTATE_DISCONNECTED);
}

static inline bool NetworkingAttemptToConnect(struct networking * networking)
{
   NetworkHostDisconnectAndDestroy(networking);
   
   NetworkingSetConnectionState(networking, CLMSG_CONNECTIONSTATE_CONNECTING);
   printf("Attempting to connect to host \"%s\" with service \"%s\"...\n", networking->serverHost, networking->serverService);
   networking->socket = tcpnCreateClient(networking->net, networking->serverHost, networking->serverService);
   if(networking->socket == NULL)
   {
      NetworkingSetConnectionState(networking, CLMSG_CONNECTIONSTATE_DISCONNECTED);
      return false;
   }
   networking->stepsSinceLastSentMessage = 0;
   networking->msgLayer = msglCreate(networking->socket, networking, ServerMessageReceived);   
   NetworkSendGreeting(networking);
   NetworkingSetConnectionState(networking, CLMSG_CONNECTIONSTATE_CONNECTED);
   return true;
}

static inline void NetowrkingSendKeepAliveToServer(struct networking * networking)
{
   union serverMessage msg;
   msg.type = SERVMSG_TYPE_KEEPALIVE;
   NetworkingSendToServer(networking, &msg);
}

static inline
void NetworkHandleCommandServerMessage(struct networking * networking, const union serverMessage * msg)
{
   union clientMessage cMsg;
   struct simEntity * ent;
   struct i32Vector3 velocity, position;

   switch(msg->type)
   {
   case SERVMSG_TYPE_ENTITYCOMMANDFREE:
      ent = SimulationLookupSimId(networking->simulation, 
                                  msg->entityCommandFree.simId);
      fsimCommandFree(&ent->activeCommand);
      break;
   case SERVMSG_TYPE_ENTITYCOMMANDSTOP:
      ent = SimulationLookupSimId(networking->simulation, 
                                  msg->entityCommandStop.simId);
      fsimCommandStop(&ent->activeCommand);
      break;
   case SERVMSG_TYPE_ENTITYCOMMANDVELOCITY:
      ent = SimulationLookupSimId(networking->simulation, 
                                  msg->entityCommandVelocity.simId);
      i32v3SetI(&velocity, msg->entityCommandVelocity.targetVelocity.x,
                           msg->entityCommandVelocity.targetVelocity.y,
                           msg->entityCommandVelocity.targetVelocity.z,
                           CONFIG_SHIFT_VELOCITY);
      fsimCommandVelocity(&ent->activeCommand, &velocity);
      break;
   case SERVMSG_TYPE_ENTITYCOMMANDPOSITION:
      ent = SimulationLookupSimId(networking->simulation, 
                                  msg->entityCommandPosition.simId);
      i32v3SetI(&position, msg->entityCommandPosition.targetPosition.x,
                           msg->entityCommandPosition.targetPosition.y,
                           msg->entityCommandPosition.targetPosition.z,
                           CONFIG_SHIFT_POSITION);
      fsimCommandPosition(&ent->activeCommand, &position);
      break;
   default:
      fprintf(stderr, "%s: %d: error: Server Command message not handled: %u\n", __FILE__, __LINE__, msg->type);
      break;
   }
}

static void NetworkHandleServerMessage(struct networking * networking, const union serverMessage * msg)
{
   union clientMessage cMsg;
   struct simEntity * ent;
   struct i32Vector3 velocity;
   printf("\nReceived Message From Server:\n");
   servmsgDump(msg);
   
   switch(msg->type)
   {
   case SERVMSG_TYPE_SYNC:
      if(NetworkingIsFrontEndSocketConnected(networking))
      {
         // Run updates:
         SimulationUpdate(networking->simulation);
         
         SimulationSendUpdatesToFrontEnd(networking->simulation, networking);
         
         cMsg.type                           = CLMSG_TYPE_SYNC;
         cMsg.sync.step                      = msg->sync.step;
         cMsg.sync.lastClientCmdIdProccessed = msg->sync.lastClientCmdIdProccessed;
         cMsg.sync.nextStepTime_ms           = msg->sync.nextStepTime_ms;
         NetworkingSendFrontEndMessage(networking, &cMsg);
      }
      networking->stepsSinceLastSentMessage ++;
      if(networking->stepsSinceLastSentMessage > 7)
      {
         NetowrkingSendKeepAliveToServer(networking);
      }
      break;
   case SERVMSG_TYPE_GREETING:
      printf("Server Greeted\n");
      break;
   case SERVMSG_TYPE_AUTHRESPONSE:
      cMsg.type = CLMSG_TYPE_AUTHRESPONSE;
      cMsg.authResponse.requestType = msg->authResponse.requestType;
      cMsg.authResponse.success     = msg->authResponse.success;
      NetworkingSendFrontEndMessage(networking, &cMsg);
      break;
   case SERVMSG_TYPE_ENTITYCREATE:
      cMsg.type = CLMSG_TYPE_ENTITYCREATE;
      msgTypeSafeCopy(&cMsg.entityCreate, &msg->entityCreate, 
                      sizeof(struct servmsgEntityCreate));
      
      
      NetworkingSendFrontEndMessage(networking, &cMsg);
      
      ent = SimulationCreateNewEntity(networking->simulation, 
                                      msg->entityCreate.simId);

      i32fpSetS(&ent->dynamics.maxSpeed,        msg->entityCreate.maxSpeed,
                                                CONFIG_SHIFT_VELOCITY);
      i32fpSetS(&ent->dynamics.maxAcceleration, msg->entityCreate.maxAcceleration, 
                                                CONFIG_SHIFT_ACCELERATION);
      i32fpSetS(&ent->dynamics.stopDistance,    msg->entityCreate.stopDistance, 
                                                CONFIG_SHIFT_POSITION);

      break;
   case SERVMSG_TYPE_ENTITYDESTROY:
      cMsg.type = CLMSG_TYPE_ENTITYDESTROY;
      cMsg.entityDestroy.simId = msg->entityDestroy.simId;
      NetworkingSendFrontEndMessage(networking, &cMsg); 
      break;
   case SERVMSG_TYPE_ENTITYUPDATE:
      ent = SimulationLookupSimId(networking->simulation, 
                                  msg->entityUpdate.simId);
      i32v3SetI(&ent->fsim.position, msg->entityUpdate.position.x,
                                     msg->entityUpdate.position.y,
                                     msg->entityUpdate.position.z,
                                     CONFIG_SHIFT_POSITION);
      i32v3SetI(&ent->fsim.velocity, msg->entityUpdate.velocity.x,
                                     msg->entityUpdate.velocity.y,
                                     msg->entityUpdate.velocity.z,
                                     CONFIG_SHIFT_VELOCITY);
      i32QuatSetI(&ent->fsim.rotation, msg->entityUpdate.rotation.w,
                                       msg->entityUpdate.rotation.x,
                                       msg->entityUpdate.rotation.y,
                                       msg->entityUpdate.rotation.z,
                                       CONFIG_SHIFT_ANGLE);
      i32v3SetI(&ent->fsim.rotationalVelocity, msg->entityUpdate.rotationalVelocity.x,
                                               msg->entityUpdate.rotationalVelocity.y,
                                               msg->entityUpdate.rotationalVelocity.z,
                                               CONFIG_SHIFT_ANGULAR_VELOCITY);
      
      
      cMsg.type = CLMSG_TYPE_ENTITYUPDATE;
      cMsg.entityUpdate.serverUpdate = true;
      msgTypeSafeCopy(&cMsg.entityUpdate, 
                      &msg->entityUpdate, 
                      sizeof(struct servmsgEntityUpdate));

      
      NetworkingSendFrontEndMessage(networking, &cMsg);
      break;
   case SERVMSG_TYPE_ENTITYPLAYERCONTROL:
      cMsg.type = CLMSG_TYPE_ENTITYPLAYERCONTROL;
      msgTypeSafeCopy(&cMsg.entityPlayerControl, &msg->entityPlayerControl,
                      sizeof(struct servmsgEntityPlayerControl));
      NetworkingSendFrontEndMessage(networking, &cMsg);
      
      networking->simulation->controlledSimId = msg->entityPlayerControl.simId;
      networking->simulation->controlActive   = msg->entityPlayerControl.active;
      break;
   case SERVMSG_TYPE_ENTITYCOMMANDFREE:
      cMsg.type = CLMSG_TYPE_ENTITYCOMMANDFREE;
      msgTypeSafeCopy(&cMsg.entityCommandFree,
                      &msg->entityCommandFree,
                      sizeof(struct servmsgEntityCommandFree));
      NetworkingSendFrontEndMessage(networking, &cMsg);
      NetworkHandleCommandServerMessage(networking, msg);
      break;
   case SERVMSG_TYPE_ENTITYCOMMANDSTOP:
      cMsg.type = CLMSG_TYPE_ENTITYCOMMANDSTOP;
      msgTypeSafeCopy(&cMsg.entityCommandStop,
                      &msg->entityCommandStop,
                      sizeof(struct servmsgEntityCommandStop));
      NetworkingSendFrontEndMessage(networking, &cMsg); 
      NetworkHandleCommandServerMessage(networking, msg);
      break;
   case SERVMSG_TYPE_ENTITYCOMMANDVELOCITY:
      cMsg.type = CLMSG_TYPE_ENTITYCOMMANDVELOCITY;
      msgTypeSafeCopy(&cMsg.entityCommandVelocity,
                      &msg->entityCommandVelocity,
                      sizeof(struct servmsgEntityCommandVelocity));
      NetworkingSendFrontEndMessage(networking, &cMsg);
      NetworkHandleCommandServerMessage(networking, msg);
      break;
   case SERVMSG_TYPE_ENTITYCOMMANDPOSITION:
      cMsg.type = CLMSG_TYPE_ENTITYCOMMANDPOSITION;
      msgTypeSafeCopy(&cMsg.entityCommandPosition,
                      &msg->entityCommandPosition,
                      sizeof(struct servmsgEntityCommandPosition));
      NetworkingSendFrontEndMessage(networking, &cMsg);
      NetworkHandleCommandServerMessage(networking, msg);
      break;

   default:
      fprintf(stderr, "%s: %d: warning: Server message not handled: %u\n", __FILE__, __LINE__, msg->type);
      servmsgDump(msg);
   }
}

static void ServerMessageReceived(void * userData, 
                                  struct messageLayer * layer, 
                                  struct tcpNetworkSocket * socket, 
                                  void * message, uint16_t messageSize)
{
   struct networking * networking = (struct networking *)userData;
   struct bitpacker bp;
   union serverMessage serverMessage;
   bpInit(&bp, message, messageSize);
   if(servmsgRead(&bp, networking->stringBuffer, &serverMessage))
   {
      NetworkHandleServerMessage(networking, &serverMessage);
   }
   strbClear(networking->stringBuffer);
}



static void NetworkHandleFrontEndMessage(struct networking * networking, const union clientMessage * msg)
{
   union clientMessage cMsg;
   union serverMessage sMsg;
   printf("\nReceived Message From FrontEnd:\n");
   clmsgDump(msg);
   switch(msg->type)
   {
   case CLMSG_TYPE_EXIT:
      printf("Exit requested from front end\n"); 
      networking->exitRequested = true;
      break;
   case CLMSG_TYPE_HOSTINFO:
      if(msg->hostInfo.set)
      {
         NetworkingSetHost(networking, msg->hostInfo.host, msg->hostInfo.service);
      }
      else
      {
         cMsg.type             = CLMSG_TYPE_HOSTINFO;
         cMsg.hostInfo.set     = false;
         cMsg.hostInfo.host    = networking->serverHost;
         cMsg.hostInfo.service = networking->serverService;
         NetworkingSendFrontEndMessage(networking, &cMsg);
      }
      break;
   case CLMSG_TYPE_CONNECTIONSTATE:
      if(msg->connectionState.set)
      {
         networking->stayConnected = msg->connectionState.stayConnected;
         if(msg->connectionState.state == CLMSG_CONNECTIONSTATE_DISCONNECTED)
         {
            NetworkHostDisconnectAndDestroy(networking);
         }
         else if(msg->connectionState.state  == CLMSG_CONNECTIONSTATE_CONNECTED &&
                 networking->connectionState != CLMSG_CONNECTIONSTATE_CONNECTED)
         {
            NetworkingAttemptToConnect(networking);
         }
      }
      else
      {
         cMsg.type                          = CLMSG_TYPE_CONNECTIONSTATE;
         cMsg.connectionState.set           = false;
         cMsg.connectionState.stayConnected = networking->stayConnected;
         cMsg.connectionState.state         = networking->connectionState;
         NetworkingSendFrontEndMessage(networking, &cMsg);
      }
      break;
   case CLMSG_TYPE_AUTHREQUEST:
      sMsg.type = SERVMSG_TYPE_AUTHREQUEST;
      sMsg.authRequest.requestType  = msg->authRequest.requestType;
      sMsg.authRequest.userName     = msg->authRequest.userName;
      sMsg.authRequest.userPassword = msg->authRequest.userPassword;
      NetworkingSendToServer(networking, &sMsg);
      break;
   case CLMSG_TYPE_ENTITYPLAYERCOMMANDFREE:
      sMsg.type = SERVMSG_TYPE_ENTITYPLAYERCOMMANDFREE;
      msgTypeSafeCopy(&sMsg.entityPlayerCommandFree,
                      &msg->entityPlayerCommandFree,
                      sizeof(struct servmsgEntityPlayerCommandFree));
      NetworkingSendToServer(networking, &sMsg);
      break;
   case CLMSG_TYPE_ENTITYPLAYERCOMMANDSTOP:
      sMsg.type = SERVMSG_TYPE_ENTITYPLAYERCOMMANDSTOP;
      msgTypeSafeCopy(&sMsg.entityPlayerCommandStop,
                      &msg->entityPlayerCommandStop,
                      sizeof(struct servmsgEntityPlayerCommandStop));
      NetworkingSendToServer(networking, &sMsg);
      break;
   case CLMSG_TYPE_ENTITYPLAYERCOMMANDVELOCITY:
      sMsg.type = SERVMSG_TYPE_ENTITYPLAYERCOMMANDVELOCITY;
      msgTypeSafeCopy(&sMsg.entityPlayerCommandVelocity,
                      &msg->entityPlayerCommandVelocity,
                      sizeof(struct servmsgEntityPlayerCommandVelocity));
      NetworkingSendToServer(networking, &sMsg);
      break;
   case CLMSG_TYPE_ENTITYPLAYERCOMMANDPOSITION:
      sMsg.type = SERVMSG_TYPE_ENTITYPLAYERCOMMANDPOSITION;
      msgTypeSafeCopy(&sMsg.entityPlayerCommandPosition,
                      &msg->entityPlayerCommandPosition,
                      sizeof(struct servmsgEntityPlayerCommandPosition));
      NetworkingSendToServer(networking, &sMsg);
      break;

   default:
      fprintf(stderr, "%s: %d: warning: Front End message not handled: %u\n", __FILE__, __LINE__, msg->type);
      clmsgDump(msg);
   }   
}

static void FrontEndMessageReceived(void * userData, 
                                    struct messageLayer * layer, 
                                    struct tcpNetworkSocket * socket, 
                                    void * message, uint16_t messageSize)
{
   struct networking * networking = (struct networking *)userData;
   struct bytepacker byp;
   union clientMessage clientMessage;
   bypInit(&byp, message, messageSize);
   if(clmsgRead(&byp, &clientMessage))
   {
      NetworkHandleFrontEndMessage(networking, &clientMessage);
   }
}



void NetworkingSetup(struct networking * networking)
{
   networking->net = tcpnCreateNativeNetwork();
   bpbInit(&networking->serverSendBuffer, 0, 0);
   bypbInit(&networking->frontEndSendBuffer, 0, 0);
   networking->stringBuffer = strbCreate(0);
   networking->socket = NULL;
   networking->msgLayer = NULL;
   networking->serverHost = NULL;
   networking->serverService = NULL;
   networking->stayConnected = false;
   networking->connectionState = CLMSG_CONNECTIONSTATE_DISCONNECTED;
   NetworkingSetHost(networking, "localhost", CONFIG_DEFAULT_PORT);
   
   networking->frontEndServer = tcpnCreateServer(networking->net, NULL, CLIENT_FRONTEND_SERVER_PORT);
   networking->frontEndSocket = NULL;
   networking->frontEndMsgLayer = NULL;
   networking->exitRequested = false;
}

void NetworkingTeardown(struct networking * networking)
{
   if(networking->frontEndMsgLayer != NULL)
   {
      msglDestory(networking->frontEndMsgLayer);
   }

   if(networking->frontEndSocket != NULL)
   {
      tcpnSocketDestroy(networking->frontEndSocket);
   }
   
   if(networking->msgLayer != NULL)
   {
      msglDestory(networking->msgLayer);
   }

   if(networking->socket != NULL)
   {
      tcpnSocketDestroy(networking->socket);
   }
   
   tcpnServerDestroy(networking->frontEndServer);
   strbDestroy(networking->stringBuffer);
   bpbFree(&networking->serverSendBuffer);
   bypbFree(&networking->frontEndSendBuffer);
   tcpnDestroy(networking->net);
   free(networking->serverHost);
   free(networking->serverService);
}
static inline 
void NetworkDumpCurrentStatusToFrontend(struct networking * networking)
{
   union clientMessage cMsg;
   size_t count, i;
   struct simEntity ** entities;
   // Dump all Ships
   entities = listGetArray_simEntityPtr(&networking->simulation->entityList, &count);
   for(i = 0; i < count; i++)
   {
      struct simEntity * entity = entities[i];
      cMsg.type = CLMSG_TYPE_ENTITYCREATE;
      cMsg.entityCreate.simId           = entity->simId;
      cMsg.entityCreate.maxSpeed        = entity->dynamics.maxSpeed.value;
      cMsg.entityCreate.maxAcceleration = entity->dynamics.maxAcceleration.value;
      NetworkingSendFrontEndMessage(networking, &cMsg);
   }
   
   // Dump Commanded Ship
   cMsg.type = CLMSG_TYPE_ENTITYPLAYERCONTROL;
   cMsg.entityPlayerControl.simId  = networking->simulation->controlledSimId;
   cMsg.entityPlayerControl.active = networking->simulation->controlActive;
   NetworkingSendFrontEndMessage(networking, &cMsg);
}

void NetworkingAccept(struct networking * networking)
{
   if(networking->frontEndSocket == NULL)
   {
      networking->frontEndSocket = tcpnServerAccept(networking->frontEndServer);
      if(networking->frontEndSocket != NULL)
      {
         union clientMessage msg;
         printf("Front End Connected\n");
         networking->frontEndMsgLayer = msglCreate(networking->frontEndSocket, 
                                                   networking, 
                                                   FrontEndMessageReceived);
         
         msg.type             = CLMSG_TYPE_HOSTINFO;
         msg.hostInfo.set     = false;
         msg.hostInfo.host    = networking->serverHost;
         msg.hostInfo.service = networking->serverService;

         NetworkingSendFrontEndMessage(networking, &msg);

         msg.type                          = CLMSG_TYPE_CONNECTIONSTATE;
         msg.connectionState.set           = false;
         msg.connectionState.stayConnected = networking->stayConnected;
         msg.connectionState.state         = networking->connectionState;
         
         NetworkingSendFrontEndMessage(networking, &msg);
         
         if(networking->connectionState == CLMSG_CONNECTIONSTATE_CONNECTED)
         {
            NetworkDumpCurrentStatusToFrontend(networking);
         }
      }
   }
}

void NetworkReceive(struct networking * networking)
{
   if(NetworkingIsServerSocketConnected(networking))
   {
      msglReceive(networking->msgLayer);
   }
   if(NetworkingIsFrontEndSocketConnected(networking))
   {
      msglReceive(networking->frontEndMsgLayer);
   }
}

void NetworkSend(struct networking * networking)
{
   if(NetworkingIsServerSocketConnected(networking))
   {
      msglSend(networking->msgLayer);
   }
   if(NetworkingIsFrontEndSocketConnected(networking))
   {
      msglSend(networking->frontEndMsgLayer);
   }
}

void NetworkCheckConnection(struct networking * networking)
{
   if(!NetworkingIsServerSocketConnected(networking))
   {
      if(networking->connectionState == CLMSG_CONNECTIONSTATE_CONNECTED)
      {
         printf("Server Disconnected!\n");
      }
      NetworkHostDisconnectAndDestroy(networking);
      
      if(networking->stayConnected)
      {
         NetworkingAttemptToConnect(networking);
      }
   }
   if(!NetworkingIsFrontEndSocketConnected(networking))
   {
      if(networking->frontEndSocket != NULL)
      {
         printf("Front End Disconnected\n");
         msglDestory(networking->frontEndMsgLayer);
         networking->frontEndMsgLayer = NULL;
         tcpnSocketDestroy(networking->frontEndSocket);
         networking->frontEndSocket = NULL;
      }
   }
}
// =============================== Simulation =============================== //





void SimulationSetup(struct simulation * simulation)
{
   listInitDefaults_simEntityPtr(&simulation->entityList);
}

void SimulationTeardown(struct simulation * simulation)
{
   listFree(&simulation->entityList);
}

void SimulationUpdate(struct simulation * simulation)
{
   size_t count, i;
   struct simEntity ** entities;
   entities = listGetArray_simEntityPtr(&simulation->entityList, &count);
   for(i = 0; i < count; i++)
   {
      fsimUpdate(&entities[i]->fsim, 
                 &entities[i]->dynamics,
                 &entities[i]->activeCommand, 
                 CONFIG_SIMULATION_TIME_PER_STEP_MS);
   }
}

void SimulationSendUpdatesToFrontEnd(struct simulation * simulation, 
                                     struct networking * networking)
{
   union clientMessage cMsg;
   size_t count, i;
   struct simEntity ** entities;
   cMsg.type                      = CLMSG_TYPE_ENTITYUPDATE;
   cMsg.entityUpdate.serverUpdate = false;
   
   entities = listGetArray_simEntityPtr(&simulation->entityList, &count);
   for(i = 0; i < count; i++)
   {
      cMsg.entityUpdate.simId                = entities[i]->simId;
      cMsg.entityUpdate.position.x           = entities[i]->fsim.position.x;
      cMsg.entityUpdate.position.y           = entities[i]->fsim.position.y;
      cMsg.entityUpdate.position.z           = entities[i]->fsim.position.z;
      cMsg.entityUpdate.velocity.x           = entities[i]->fsim.velocity.x;
      cMsg.entityUpdate.velocity.y           = entities[i]->fsim.velocity.y;
      cMsg.entityUpdate.velocity.z           = entities[i]->fsim.velocity.z;
      cMsg.entityUpdate.rotation.w           = entities[i]->fsim.rotation.w;
      cMsg.entityUpdate.rotation.x           = entities[i]->fsim.rotation.x;
      cMsg.entityUpdate.rotation.y           = entities[i]->fsim.rotation.y;
      cMsg.entityUpdate.rotation.z           = entities[i]->fsim.rotation.z;
      cMsg.entityUpdate.rotationalVelocity.x = entities[i]->fsim.rotationalVelocity.x;
      cMsg.entityUpdate.rotationalVelocity.y = entities[i]->fsim.rotationalVelocity.y;
      cMsg.entityUpdate.rotationalVelocity.z = entities[i]->fsim.rotationalVelocity.z;
      
      NetworkingSendFrontEndMessage(networking, &cMsg);
   }
}

struct simEntity * SimulationCreateNewEntity(struct simulation * simulation, 
                                             uint32_t simId)
{
   struct simEntity * ent = malloc(sizeof(struct simEntity));
   ent->simId = simId;
   fsimCommandFree(&ent->activeCommand);
   listAddValue_simEntityPtr(&simulation->entityList, ent, NULL);
   return ent;
}

struct simEntity * SimulationLookupSimId(struct simulation * simulation,
                                         uint32_t simId)
{
   size_t count, i;
   struct simEntity ** entities;
   entities = listGetArray_simEntityPtr(&simulation->entityList, &count);
   for(i = 0; i < count; i++)
   {
      if(entities[i]->simId == simId)
      {
         return entities[i];
      }
   }
   return NULL;
}

// ================================== main ================================== //


int main(int argc, char * args[])
{
   unsigned int limiterTimer_ms;
   struct networking networking;
   struct simulation simulation;
   printf("Setting Up Client...\n");
   
   NetworkingSetup(&networking);
   SimulationSetup(&simulation);
   networking.simulation = &simulation;
   simulation.networking = &networking;

   printf("Client Entering Main Loop\n");
   siSetup(NULL);
   limiterTimer_ms = tasGetMilliseconds();
   while(siNotInterrupted() && !networking.exitRequested)
   {
      NetworkCheckConnection(&networking);
      NetworkingAccept(&networking);
      NetworkReceive(&networking);
      // TODO: Simulation Maybe? Could be done when we recieve server step
      NetworkSend(&networking);
      tasSleepUntil(CLIENT_CHECK_RATE_MS, &limiterTimer_ms);
   }
   printf("\nClient Exiting Main Loop\n");
   
   NetworkingTeardown(&networking);
   SimulationTeardown(&simulation);
   
   printf("End of Client\n");
   return 0;
}
