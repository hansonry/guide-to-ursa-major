#ifndef __CLIENTMESSAGE_H__
#define __CLIENTMESSAGE_H__
// This file is automaticaly generated from by MessageCodeGeneratorC.lua
#include <stdint.h>
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <stdbool.h>

#include <common/CommonMessage.h>
#include <common/BytePacker.h>

// Message Types
#define CLMSG_TYPE_EXIT 1
#define CLMSG_TYPE_HOSTINFO 2
#define CLMSG_TYPE_CONNECTIONSTATE 3
#define CLMSG_TYPE_AUTHREQUEST 4
#define CLMSG_TYPE_AUTHRESPONSE 5
#define CLMSG_TYPE_SYNC 6
#define CLMSG_TYPE_ENTITYCREATE 7
#define CLMSG_TYPE_ENTITYDESTROY 8
#define CLMSG_TYPE_ENTITYUPDATE 9
#define CLMSG_TYPE_ENTITYPLAYERCONTROL 10
#define CLMSG_TYPE_ENTITYCOMMANDFREE 20
#define CLMSG_TYPE_ENTITYCOMMANDSTOP 21
#define CLMSG_TYPE_ENTITYCOMMANDVELOCITY 22
#define CLMSG_TYPE_ENTITYCOMMANDPOSITION 23
#define CLMSG_TYPE_ENTITYPLAYERCOMMANDFREE 40
#define CLMSG_TYPE_ENTITYPLAYERCOMMANDSTOP 41
#define CLMSG_TYPE_ENTITYPLAYERCOMMANDVELOCITY 42
#define CLMSG_TYPE_ENTITYPLAYERCOMMANDPOSITION 43

// Enumerations
/// AuthRequestType
#define CLMSG_AUTHREQUESTTYPE_LOGIN 1
#define CLMSG_AUTHREQUESTTYPE_LOGOUT 2

/// ConnectionState
#define CLMSG_CONNECTIONSTATE_DISCONNECTED 1
#define CLMSG_CONNECTIONSTATE_CONNECTING 2
#define CLMSG_CONNECTIONSTATE_CONNECTED 3
#define CLMSG_CONNECTIONSTATE_DISCONNECTING 4


/// AuthRequestType
static inline
const char * clmsgAuthRequestTypeEnumToString(uint8_t value)
{
   const char * name;
   switch(value)
   {
      case CLMSG_AUTHREQUESTTYPE_LOGIN: name = "LogIn"; break;
      case CLMSG_AUTHREQUESTTYPE_LOGOUT: name = "LogOut"; break;
      default: name = "!UNKNOWN!"; break;
   }
   return name;
}

/// ConnectionState
static inline
const char * clmsgConnectionStateEnumToString(uint8_t value)
{
   const char * name;
   switch(value)
   {
      case CLMSG_CONNECTIONSTATE_DISCONNECTED: name = "Disconnected"; break;
      case CLMSG_CONNECTIONSTATE_CONNECTING: name = "Connecting"; break;
      case CLMSG_CONNECTIONSTATE_CONNECTED: name = "Connected"; break;
      case CLMSG_CONNECTIONSTATE_DISCONNECTING: name = "Disconnecting"; break;
      default: name = "!UNKNOWN!"; break;
   }
   return name;
}

static inline
void clmsgWriteString(struct bytepacker * byp, unsigned char sizeBytes, const char * str)
{
   if(str == NULL)
   {
      bypWriteData(byp, sizeBytes, NULL, 0);
   }
   else
   {
      bypWriteData(byp, sizeBytes, str, strlen(str) + 1);
   }
}

static inline
const char * clmsgReadString(struct bytepacker * byp, unsigned char sizeBytes)
{
   size_t size;
   const char * str = bypReadData(byp, sizeBytes, &size);
   if(size == 0 || bypBytesDropped(byp))
   {
      str = NULL;
   }
   return str;
}

// Messages
/// Exit
struct clmsgExit
{
   uint16_t type;
};

static inline
void clmsgWriteExit(struct bytepacker * bp, const struct clmsgExit * msg)
{
   bypWriteInt16(bp, CLMSG_TYPE_EXIT);
}

static inline
void clmsgReadExit(struct bytepacker * bp, struct clmsgExit * msg)
{
   msg->type = CLMSG_TYPE_EXIT;
}

static inline
void clmsgDumpExit(const struct clmsgExit * msg)
{
   printf("Dumping ClientMessage \"Exit\" 1:\n");
}

/// HostInfo
struct clmsgHostInfo
{
   uint16_t type;
   bool set;
   const char * host;
   const char * service;
};

static inline
void clmsgWriteHostInfo(struct bytepacker * bp, const struct clmsgHostInfo * msg)
{
   bypWriteInt16(bp, CLMSG_TYPE_HOSTINFO);
   bypWriteBool(bp, msg->set);
   clmsgWriteString(bp, 1, msg->host);
   clmsgWriteString(bp, 1, msg->service);
}

static inline
void clmsgReadHostInfo(struct bytepacker * bp, struct clmsgHostInfo * msg)
{
   msg->type = CLMSG_TYPE_HOSTINFO;
   msg->set = bypReadBool(bp);
   msg->host = clmsgReadString(bp, 1);
   msg->service = clmsgReadString(bp, 1);
}

static inline
void clmsgDumpHostInfo(const struct clmsgHostInfo * msg)
{
   printf("Dumping ClientMessage \"HostInfo\" 2:\n");
   printf("   set:\t%s\n", msg->set ? "true" : "false");
   printf("   host:\t\"%s\",\t%lu\n", msg->host, strlen(msg->host));
   printf("   service:\t\"%s\",\t%lu\n", msg->service, strlen(msg->service));
}

/// ConnectionState
struct clmsgConnectionState
{
   uint16_t type;
   bool set;
   bool stayConnected;
   uint8_t state;
};

static inline
void clmsgWriteConnectionState(struct bytepacker * bp, const struct clmsgConnectionState * msg)
{
   bypWriteInt16(bp, CLMSG_TYPE_CONNECTIONSTATE);
   bypWriteBool(bp, msg->set);
   bypWriteBool(bp, msg->stayConnected);
   bypWriteInt8(bp, msg->state);
}

static inline
void clmsgReadConnectionState(struct bytepacker * bp, struct clmsgConnectionState * msg)
{
   msg->type = CLMSG_TYPE_CONNECTIONSTATE;
   msg->set = bypReadBool(bp);
   msg->stayConnected = bypReadBool(bp);
   msg->state = bypReadInt8(bp);
}

static inline
void clmsgDumpConnectionState(const struct clmsgConnectionState * msg)
{
   printf("Dumping ClientMessage \"ConnectionState\" 3:\n");
   printf("   set:\t%s\n", msg->set ? "true" : "false");
   printf("   stayConnected:\t%s\n", msg->stayConnected ? "true" : "false");
   printf("   state:\t\"%s\",\t%u\n", clmsgConnectionStateEnumToString(msg->state), msg->state);
}

/// AuthRequest
struct clmsgAuthRequest
{
   uint16_t type;
   uint8_t requestType;
   const char * userName;
   const char * userPassword;
};

static inline
void clmsgWriteAuthRequest(struct bytepacker * bp, const struct clmsgAuthRequest * msg)
{
   bypWriteInt16(bp, CLMSG_TYPE_AUTHREQUEST);
   bypWriteInt8(bp, msg->requestType);
   clmsgWriteString(bp, 1, msg->userName);
   clmsgWriteString(bp, 1, msg->userPassword);
}

static inline
void clmsgReadAuthRequest(struct bytepacker * bp, struct clmsgAuthRequest * msg)
{
   msg->type = CLMSG_TYPE_AUTHREQUEST;
   msg->requestType = bypReadInt8(bp);
   msg->userName = clmsgReadString(bp, 1);
   msg->userPassword = clmsgReadString(bp, 1);
}

static inline
void clmsgDumpAuthRequest(const struct clmsgAuthRequest * msg)
{
   printf("Dumping ClientMessage \"AuthRequest\" 4:\n");
   printf("   requestType:\t\"%s\",\t%u\n", clmsgAuthRequestTypeEnumToString(msg->requestType), msg->requestType);
   printf("   userName:\t\"%s\",\t%lu\n", msg->userName, strlen(msg->userName));
   printf("   userPassword:\t\"%s\",\t%lu\n", msg->userPassword, strlen(msg->userPassword));
}

/// AuthResponse
struct clmsgAuthResponse
{
   uint16_t type;
   uint8_t requestType;
   bool success;
};

static inline
void clmsgWriteAuthResponse(struct bytepacker * bp, const struct clmsgAuthResponse * msg)
{
   bypWriteInt16(bp, CLMSG_TYPE_AUTHRESPONSE);
   bypWriteInt8(bp, msg->requestType);
   bypWriteBool(bp, msg->success);
}

static inline
void clmsgReadAuthResponse(struct bytepacker * bp, struct clmsgAuthResponse * msg)
{
   msg->type = CLMSG_TYPE_AUTHRESPONSE;
   msg->requestType = bypReadInt8(bp);
   msg->success = bypReadBool(bp);
}

static inline
void clmsgDumpAuthResponse(const struct clmsgAuthResponse * msg)
{
   printf("Dumping ClientMessage \"AuthResponse\" 5:\n");
   printf("   requestType:\t\"%s\",\t%u\n", clmsgAuthRequestTypeEnumToString(msg->requestType), msg->requestType);
   printf("   success:\t%s\n", msg->success ? "true" : "false");
}

/// Sync
struct clmsgSync
{
   uint16_t type;
   uint8_t step;
   uint8_t lastClientCmdIdProccessed;
   uint16_t nextStepTime_ms;
};

static inline
void clmsgWriteSync(struct bytepacker * bp, const struct clmsgSync * msg)
{
   bypWriteInt16(bp, CLMSG_TYPE_SYNC);
   bypWriteInt8(bp, msg->step);
   bypWriteInt8(bp, msg->lastClientCmdIdProccessed);
   bypWriteInt16(bp, msg->nextStepTime_ms);
}

static inline
void clmsgReadSync(struct bytepacker * bp, struct clmsgSync * msg)
{
   msg->type = CLMSG_TYPE_SYNC;
   msg->step = bypReadInt8(bp);
   msg->lastClientCmdIdProccessed = bypReadInt8(bp);
   msg->nextStepTime_ms = bypReadInt16(bp);
}

static inline
void clmsgDumpSync(const struct clmsgSync * msg)
{
   printf("Dumping ClientMessage \"Sync\" 6:\n");
   printf("   step:\t0x%X,\t%u\n", msg->step, msg->step);
   printf("   lastClientCmdIdProccessed:\t0x%X,\t%u\n", msg->lastClientCmdIdProccessed, msg->lastClientCmdIdProccessed);
   printf("   nextStepTime_ms:\t0x%X,\t%u\n", msg->nextStepTime_ms, msg->nextStepTime_ms);
}

/// EntityCreate
struct clmsgEntityCreate
{
   uint16_t type;
   uint32_t simId;
   uint32_t maxSpeed;
   uint32_t maxAcceleration;
   uint32_t stopDistance;
};

static inline
void clmsgWriteEntityCreate(struct bytepacker * bp, const struct clmsgEntityCreate * msg)
{
   bypWriteInt16(bp, CLMSG_TYPE_ENTITYCREATE);
   bypWriteInt32(bp, msg->simId);
   bypWriteInt32(bp, msg->maxSpeed);
   bypWriteInt32(bp, msg->maxAcceleration);
   bypWriteInt32(bp, msg->stopDistance);
}

static inline
void clmsgReadEntityCreate(struct bytepacker * bp, struct clmsgEntityCreate * msg)
{
   msg->type = CLMSG_TYPE_ENTITYCREATE;
   msg->simId = bypReadInt32(bp);
   msg->maxSpeed = bypReadInt32(bp);
   msg->maxAcceleration = bypReadInt32(bp);
   msg->stopDistance = bypReadInt32(bp);
}

static inline
void clmsgDumpEntityCreate(const struct clmsgEntityCreate * msg)
{
   printf("Dumping ClientMessage \"EntityCreate\" 7:\n");
   printf("   simId:\t0x%X,\t%u\n", msg->simId, msg->simId);
   printf("   maxSpeed:\t0x%X,\t%u\n", msg->maxSpeed, msg->maxSpeed);
   printf("   maxAcceleration:\t0x%X,\t%u\n", msg->maxAcceleration, msg->maxAcceleration);
   printf("   stopDistance:\t0x%X,\t%u\n", msg->stopDistance, msg->stopDistance);
}

/// EntityDestroy
struct clmsgEntityDestroy
{
   uint16_t type;
   uint32_t simId;
};

static inline
void clmsgWriteEntityDestroy(struct bytepacker * bp, const struct clmsgEntityDestroy * msg)
{
   bypWriteInt16(bp, CLMSG_TYPE_ENTITYDESTROY);
   bypWriteInt32(bp, msg->simId);
}

static inline
void clmsgReadEntityDestroy(struct bytepacker * bp, struct clmsgEntityDestroy * msg)
{
   msg->type = CLMSG_TYPE_ENTITYDESTROY;
   msg->simId = bypReadInt32(bp);
}

static inline
void clmsgDumpEntityDestroy(const struct clmsgEntityDestroy * msg)
{
   printf("Dumping ClientMessage \"EntityDestroy\" 8:\n");
   printf("   simId:\t0x%X,\t%u\n", msg->simId, msg->simId);
}

/// EntityUpdate
struct clmsgEntityUpdate
{
   uint16_t type;
   uint32_t simId;
   struct msgI32Vector3 position;
   struct msgI32Vector3 velocity;
   struct msgI32Vector4 rotation;
   struct msgI32Vector3 rotationalVelocity;
   bool serverUpdate;
};

static inline
void clmsgWriteEntityUpdate(struct bytepacker * bp, const struct clmsgEntityUpdate * msg)
{
   bypWriteInt16(bp, CLMSG_TYPE_ENTITYUPDATE);
   bypWriteInt32(bp, msg->simId);
   bypWriteInt32(bp, msg->position.x);
   bypWriteInt32(bp, msg->position.y);
   bypWriteInt32(bp, msg->position.z);
   bypWriteInt32(bp, msg->velocity.x);
   bypWriteInt32(bp, msg->velocity.y);
   bypWriteInt32(bp, msg->velocity.z);
   bypWriteInt32(bp, msg->rotation.w);
   bypWriteInt32(bp, msg->rotation.x);
   bypWriteInt32(bp, msg->rotation.y);
   bypWriteInt32(bp, msg->rotation.z);
   bypWriteInt32(bp, msg->rotationalVelocity.x);
   bypWriteInt32(bp, msg->rotationalVelocity.y);
   bypWriteInt32(bp, msg->rotationalVelocity.z);
   bypWriteBool(bp, msg->serverUpdate);
}

static inline
void clmsgReadEntityUpdate(struct bytepacker * bp, struct clmsgEntityUpdate * msg)
{
   msg->type = CLMSG_TYPE_ENTITYUPDATE;
   msg->simId = bypReadInt32(bp);
   msg->position.x = bypReadInt32(bp);
   msg->position.y = bypReadInt32(bp);
   msg->position.z = bypReadInt32(bp);
   msg->velocity.x = bypReadInt32(bp);
   msg->velocity.y = bypReadInt32(bp);
   msg->velocity.z = bypReadInt32(bp);
   msg->rotation.w = bypReadInt32(bp);
   msg->rotation.x = bypReadInt32(bp);
   msg->rotation.y = bypReadInt32(bp);
   msg->rotation.z = bypReadInt32(bp);
   msg->rotationalVelocity.x = bypReadInt32(bp);
   msg->rotationalVelocity.y = bypReadInt32(bp);
   msg->rotationalVelocity.z = bypReadInt32(bp);
   msg->serverUpdate = bypReadBool(bp);
}

static inline
void clmsgDumpEntityUpdate(const struct clmsgEntityUpdate * msg)
{
   printf("Dumping ClientMessage \"EntityUpdate\" 9:\n");
   printf("   simId:\t0x%X,\t%u\n", msg->simId, msg->simId);
   printf("   position.x:\t0x%X,\t%u\n", msg->position.x, msg->position.x);
   printf("   position.y:\t0x%X,\t%u\n", msg->position.y, msg->position.y);
   printf("   position.z:\t0x%X,\t%u\n", msg->position.z, msg->position.z);
   printf("   velocity.x:\t0x%X,\t%u\n", msg->velocity.x, msg->velocity.x);
   printf("   velocity.y:\t0x%X,\t%u\n", msg->velocity.y, msg->velocity.y);
   printf("   velocity.z:\t0x%X,\t%u\n", msg->velocity.z, msg->velocity.z);
   printf("   rotation.w:\t0x%X,\t%u\n", msg->rotation.w, msg->rotation.w);
   printf("   rotation.x:\t0x%X,\t%u\n", msg->rotation.x, msg->rotation.x);
   printf("   rotation.y:\t0x%X,\t%u\n", msg->rotation.y, msg->rotation.y);
   printf("   rotation.z:\t0x%X,\t%u\n", msg->rotation.z, msg->rotation.z);
   printf("   rotationalVelocity.x:\t0x%X,\t%u\n", msg->rotationalVelocity.x, msg->rotationalVelocity.x);
   printf("   rotationalVelocity.y:\t0x%X,\t%u\n", msg->rotationalVelocity.y, msg->rotationalVelocity.y);
   printf("   rotationalVelocity.z:\t0x%X,\t%u\n", msg->rotationalVelocity.z, msg->rotationalVelocity.z);
   printf("   serverUpdate:\t%s\n", msg->serverUpdate ? "true" : "false");
}

/// EntityPlayerControl
struct clmsgEntityPlayerControl
{
   uint16_t type;
   uint32_t simId;
   bool active;
};

static inline
void clmsgWriteEntityPlayerControl(struct bytepacker * bp, const struct clmsgEntityPlayerControl * msg)
{
   bypWriteInt16(bp, CLMSG_TYPE_ENTITYPLAYERCONTROL);
   bypWriteInt32(bp, msg->simId);
   bypWriteBool(bp, msg->active);
}

static inline
void clmsgReadEntityPlayerControl(struct bytepacker * bp, struct clmsgEntityPlayerControl * msg)
{
   msg->type = CLMSG_TYPE_ENTITYPLAYERCONTROL;
   msg->simId = bypReadInt32(bp);
   msg->active = bypReadBool(bp);
}

static inline
void clmsgDumpEntityPlayerControl(const struct clmsgEntityPlayerControl * msg)
{
   printf("Dumping ClientMessage \"EntityPlayerControl\" 10:\n");
   printf("   simId:\t0x%X,\t%u\n", msg->simId, msg->simId);
   printf("   active:\t%s\n", msg->active ? "true" : "false");
}

/// EntityCommandFree
struct clmsgEntityCommandFree
{
   uint16_t type;
   uint32_t simId;
};

static inline
void clmsgWriteEntityCommandFree(struct bytepacker * bp, const struct clmsgEntityCommandFree * msg)
{
   bypWriteInt16(bp, CLMSG_TYPE_ENTITYCOMMANDFREE);
   bypWriteInt32(bp, msg->simId);
}

static inline
void clmsgReadEntityCommandFree(struct bytepacker * bp, struct clmsgEntityCommandFree * msg)
{
   msg->type = CLMSG_TYPE_ENTITYCOMMANDFREE;
   msg->simId = bypReadInt32(bp);
}

static inline
void clmsgDumpEntityCommandFree(const struct clmsgEntityCommandFree * msg)
{
   printf("Dumping ClientMessage \"EntityCommandFree\" 20:\n");
   printf("   simId:\t0x%X,\t%u\n", msg->simId, msg->simId);
}

/// EntityCommandStop
struct clmsgEntityCommandStop
{
   uint16_t type;
   uint32_t simId;
};

static inline
void clmsgWriteEntityCommandStop(struct bytepacker * bp, const struct clmsgEntityCommandStop * msg)
{
   bypWriteInt16(bp, CLMSG_TYPE_ENTITYCOMMANDSTOP);
   bypWriteInt32(bp, msg->simId);
}

static inline
void clmsgReadEntityCommandStop(struct bytepacker * bp, struct clmsgEntityCommandStop * msg)
{
   msg->type = CLMSG_TYPE_ENTITYCOMMANDSTOP;
   msg->simId = bypReadInt32(bp);
}

static inline
void clmsgDumpEntityCommandStop(const struct clmsgEntityCommandStop * msg)
{
   printf("Dumping ClientMessage \"EntityCommandStop\" 21:\n");
   printf("   simId:\t0x%X,\t%u\n", msg->simId, msg->simId);
}

/// EntityCommandVelocity
struct clmsgEntityCommandVelocity
{
   uint16_t type;
   struct msgI32Vector3 targetVelocity;
   uint32_t simId;
};

static inline
void clmsgWriteEntityCommandVelocity(struct bytepacker * bp, const struct clmsgEntityCommandVelocity * msg)
{
   bypWriteInt16(bp, CLMSG_TYPE_ENTITYCOMMANDVELOCITY);
   bypWriteInt32(bp, msg->targetVelocity.x);
   bypWriteInt32(bp, msg->targetVelocity.y);
   bypWriteInt32(bp, msg->targetVelocity.z);
   bypWriteInt32(bp, msg->simId);
}

static inline
void clmsgReadEntityCommandVelocity(struct bytepacker * bp, struct clmsgEntityCommandVelocity * msg)
{
   msg->type = CLMSG_TYPE_ENTITYCOMMANDVELOCITY;
   msg->targetVelocity.x = bypReadInt32(bp);
   msg->targetVelocity.y = bypReadInt32(bp);
   msg->targetVelocity.z = bypReadInt32(bp);
   msg->simId = bypReadInt32(bp);
}

static inline
void clmsgDumpEntityCommandVelocity(const struct clmsgEntityCommandVelocity * msg)
{
   printf("Dumping ClientMessage \"EntityCommandVelocity\" 22:\n");
   printf("   targetVelocity.x:\t0x%X,\t%u\n", msg->targetVelocity.x, msg->targetVelocity.x);
   printf("   targetVelocity.y:\t0x%X,\t%u\n", msg->targetVelocity.y, msg->targetVelocity.y);
   printf("   targetVelocity.z:\t0x%X,\t%u\n", msg->targetVelocity.z, msg->targetVelocity.z);
   printf("   simId:\t0x%X,\t%u\n", msg->simId, msg->simId);
}

/// EntityCommandPosition
struct clmsgEntityCommandPosition
{
   uint16_t type;
   struct msgI32Vector3 targetPosition;
   uint32_t simId;
};

static inline
void clmsgWriteEntityCommandPosition(struct bytepacker * bp, const struct clmsgEntityCommandPosition * msg)
{
   bypWriteInt16(bp, CLMSG_TYPE_ENTITYCOMMANDPOSITION);
   bypWriteInt32(bp, msg->targetPosition.x);
   bypWriteInt32(bp, msg->targetPosition.y);
   bypWriteInt32(bp, msg->targetPosition.z);
   bypWriteInt32(bp, msg->simId);
}

static inline
void clmsgReadEntityCommandPosition(struct bytepacker * bp, struct clmsgEntityCommandPosition * msg)
{
   msg->type = CLMSG_TYPE_ENTITYCOMMANDPOSITION;
   msg->targetPosition.x = bypReadInt32(bp);
   msg->targetPosition.y = bypReadInt32(bp);
   msg->targetPosition.z = bypReadInt32(bp);
   msg->simId = bypReadInt32(bp);
}

static inline
void clmsgDumpEntityCommandPosition(const struct clmsgEntityCommandPosition * msg)
{
   printf("Dumping ClientMessage \"EntityCommandPosition\" 23:\n");
   printf("   targetPosition.x:\t0x%X,\t%u\n", msg->targetPosition.x, msg->targetPosition.x);
   printf("   targetPosition.y:\t0x%X,\t%u\n", msg->targetPosition.y, msg->targetPosition.y);
   printf("   targetPosition.z:\t0x%X,\t%u\n", msg->targetPosition.z, msg->targetPosition.z);
   printf("   simId:\t0x%X,\t%u\n", msg->simId, msg->simId);
}

/// EntityPlayerCommandFree
struct clmsgEntityPlayerCommandFree
{
   uint16_t type;
   uint8_t id;
};

static inline
void clmsgWriteEntityPlayerCommandFree(struct bytepacker * bp, const struct clmsgEntityPlayerCommandFree * msg)
{
   bypWriteInt16(bp, CLMSG_TYPE_ENTITYPLAYERCOMMANDFREE);
   bypWriteInt8(bp, msg->id);
}

static inline
void clmsgReadEntityPlayerCommandFree(struct bytepacker * bp, struct clmsgEntityPlayerCommandFree * msg)
{
   msg->type = CLMSG_TYPE_ENTITYPLAYERCOMMANDFREE;
   msg->id = bypReadInt8(bp);
}

static inline
void clmsgDumpEntityPlayerCommandFree(const struct clmsgEntityPlayerCommandFree * msg)
{
   printf("Dumping ClientMessage \"EntityPlayerCommandFree\" 40:\n");
   printf("   id:\t0x%X,\t%u\n", msg->id, msg->id);
}

/// EntityPlayerCommandStop
struct clmsgEntityPlayerCommandStop
{
   uint16_t type;
   uint8_t id;
};

static inline
void clmsgWriteEntityPlayerCommandStop(struct bytepacker * bp, const struct clmsgEntityPlayerCommandStop * msg)
{
   bypWriteInt16(bp, CLMSG_TYPE_ENTITYPLAYERCOMMANDSTOP);
   bypWriteInt8(bp, msg->id);
}

static inline
void clmsgReadEntityPlayerCommandStop(struct bytepacker * bp, struct clmsgEntityPlayerCommandStop * msg)
{
   msg->type = CLMSG_TYPE_ENTITYPLAYERCOMMANDSTOP;
   msg->id = bypReadInt8(bp);
}

static inline
void clmsgDumpEntityPlayerCommandStop(const struct clmsgEntityPlayerCommandStop * msg)
{
   printf("Dumping ClientMessage \"EntityPlayerCommandStop\" 41:\n");
   printf("   id:\t0x%X,\t%u\n", msg->id, msg->id);
}

/// EntityPlayerCommandVelocity
struct clmsgEntityPlayerCommandVelocity
{
   uint16_t type;
   struct msgI32Vector3 targetVelocity;
   uint8_t id;
};

static inline
void clmsgWriteEntityPlayerCommandVelocity(struct bytepacker * bp, const struct clmsgEntityPlayerCommandVelocity * msg)
{
   bypWriteInt16(bp, CLMSG_TYPE_ENTITYPLAYERCOMMANDVELOCITY);
   bypWriteInt32(bp, msg->targetVelocity.x);
   bypWriteInt32(bp, msg->targetVelocity.y);
   bypWriteInt32(bp, msg->targetVelocity.z);
   bypWriteInt8(bp, msg->id);
}

static inline
void clmsgReadEntityPlayerCommandVelocity(struct bytepacker * bp, struct clmsgEntityPlayerCommandVelocity * msg)
{
   msg->type = CLMSG_TYPE_ENTITYPLAYERCOMMANDVELOCITY;
   msg->targetVelocity.x = bypReadInt32(bp);
   msg->targetVelocity.y = bypReadInt32(bp);
   msg->targetVelocity.z = bypReadInt32(bp);
   msg->id = bypReadInt8(bp);
}

static inline
void clmsgDumpEntityPlayerCommandVelocity(const struct clmsgEntityPlayerCommandVelocity * msg)
{
   printf("Dumping ClientMessage \"EntityPlayerCommandVelocity\" 42:\n");
   printf("   targetVelocity.x:\t0x%X,\t%u\n", msg->targetVelocity.x, msg->targetVelocity.x);
   printf("   targetVelocity.y:\t0x%X,\t%u\n", msg->targetVelocity.y, msg->targetVelocity.y);
   printf("   targetVelocity.z:\t0x%X,\t%u\n", msg->targetVelocity.z, msg->targetVelocity.z);
   printf("   id:\t0x%X,\t%u\n", msg->id, msg->id);
}

/// EntityPlayerCommandPosition
struct clmsgEntityPlayerCommandPosition
{
   uint16_t type;
   struct msgI32Vector3 targetPosition;
   uint8_t id;
};

static inline
void clmsgWriteEntityPlayerCommandPosition(struct bytepacker * bp, const struct clmsgEntityPlayerCommandPosition * msg)
{
   bypWriteInt16(bp, CLMSG_TYPE_ENTITYPLAYERCOMMANDPOSITION);
   bypWriteInt32(bp, msg->targetPosition.x);
   bypWriteInt32(bp, msg->targetPosition.y);
   bypWriteInt32(bp, msg->targetPosition.z);
   bypWriteInt8(bp, msg->id);
}

static inline
void clmsgReadEntityPlayerCommandPosition(struct bytepacker * bp, struct clmsgEntityPlayerCommandPosition * msg)
{
   msg->type = CLMSG_TYPE_ENTITYPLAYERCOMMANDPOSITION;
   msg->targetPosition.x = bypReadInt32(bp);
   msg->targetPosition.y = bypReadInt32(bp);
   msg->targetPosition.z = bypReadInt32(bp);
   msg->id = bypReadInt8(bp);
}

static inline
void clmsgDumpEntityPlayerCommandPosition(const struct clmsgEntityPlayerCommandPosition * msg)
{
   printf("Dumping ClientMessage \"EntityPlayerCommandPosition\" 43:\n");
   printf("   targetPosition.x:\t0x%X,\t%u\n", msg->targetPosition.x, msg->targetPosition.x);
   printf("   targetPosition.y:\t0x%X,\t%u\n", msg->targetPosition.y, msg->targetPosition.y);
   printf("   targetPosition.z:\t0x%X,\t%u\n", msg->targetPosition.z, msg->targetPosition.z);
   printf("   id:\t0x%X,\t%u\n", msg->id, msg->id);
}

union clientMessage
{
   uint16_t type;
   struct clmsgExit exit;
   struct clmsgHostInfo hostInfo;
   struct clmsgConnectionState connectionState;
   struct clmsgAuthRequest authRequest;
   struct clmsgAuthResponse authResponse;
   struct clmsgSync sync;
   struct clmsgEntityCreate entityCreate;
   struct clmsgEntityDestroy entityDestroy;
   struct clmsgEntityUpdate entityUpdate;
   struct clmsgEntityPlayerControl entityPlayerControl;
   struct clmsgEntityCommandFree entityCommandFree;
   struct clmsgEntityCommandStop entityCommandStop;
   struct clmsgEntityCommandVelocity entityCommandVelocity;
   struct clmsgEntityCommandPosition entityCommandPosition;
   struct clmsgEntityPlayerCommandFree entityPlayerCommandFree;
   struct clmsgEntityPlayerCommandStop entityPlayerCommandStop;
   struct clmsgEntityPlayerCommandVelocity entityPlayerCommandVelocity;
   struct clmsgEntityPlayerCommandPosition entityPlayerCommandPosition;
};

static inline
bool clmsgWrite(struct bytepacker * bp, const union clientMessage * msg, bool sizeCheck)
{
   bool success = true;
   switch(msg->type)
   {
      case CLMSG_TYPE_EXIT: clmsgWriteExit(bp, &msg->exit); break;
      case CLMSG_TYPE_HOSTINFO: clmsgWriteHostInfo(bp, &msg->hostInfo); break;
      case CLMSG_TYPE_CONNECTIONSTATE: clmsgWriteConnectionState(bp, &msg->connectionState); break;
      case CLMSG_TYPE_AUTHREQUEST: clmsgWriteAuthRequest(bp, &msg->authRequest); break;
      case CLMSG_TYPE_AUTHRESPONSE: clmsgWriteAuthResponse(bp, &msg->authResponse); break;
      case CLMSG_TYPE_SYNC: clmsgWriteSync(bp, &msg->sync); break;
      case CLMSG_TYPE_ENTITYCREATE: clmsgWriteEntityCreate(bp, &msg->entityCreate); break;
      case CLMSG_TYPE_ENTITYDESTROY: clmsgWriteEntityDestroy(bp, &msg->entityDestroy); break;
      case CLMSG_TYPE_ENTITYUPDATE: clmsgWriteEntityUpdate(bp, &msg->entityUpdate); break;
      case CLMSG_TYPE_ENTITYPLAYERCONTROL: clmsgWriteEntityPlayerControl(bp, &msg->entityPlayerControl); break;
      case CLMSG_TYPE_ENTITYCOMMANDFREE: clmsgWriteEntityCommandFree(bp, &msg->entityCommandFree); break;
      case CLMSG_TYPE_ENTITYCOMMANDSTOP: clmsgWriteEntityCommandStop(bp, &msg->entityCommandStop); break;
      case CLMSG_TYPE_ENTITYCOMMANDVELOCITY: clmsgWriteEntityCommandVelocity(bp, &msg->entityCommandVelocity); break;
      case CLMSG_TYPE_ENTITYCOMMANDPOSITION: clmsgWriteEntityCommandPosition(bp, &msg->entityCommandPosition); break;
      case CLMSG_TYPE_ENTITYPLAYERCOMMANDFREE: clmsgWriteEntityPlayerCommandFree(bp, &msg->entityPlayerCommandFree); break;
      case CLMSG_TYPE_ENTITYPLAYERCOMMANDSTOP: clmsgWriteEntityPlayerCommandStop(bp, &msg->entityPlayerCommandStop); break;
      case CLMSG_TYPE_ENTITYPLAYERCOMMANDVELOCITY: clmsgWriteEntityPlayerCommandVelocity(bp, &msg->entityPlayerCommandVelocity); break;
      case CLMSG_TYPE_ENTITYPLAYERCOMMANDPOSITION: clmsgWriteEntityPlayerCommandPosition(bp, &msg->entityPlayerCommandPosition); break;
      default:
         success = false;
         fprintf(stderr, "%s: %d: error: ClientMessage Type %u is not a valid message type\n", __FILE__, __LINE__, msg->type);
         break;
   }

   if(success && sizeCheck && bypBytesDropped(bp))
   {
      success = false;
      fprintf(stderr, "%s: %d: error: ClientMessage Type %u is larger than provided Buffer: %lu\n", __FILE__, __LINE__, msg->type, bp->size);
   }
   return success;
}

static inline
bool clmsgRead(struct bytepacker * bp, union clientMessage * msg)
{
   bool success = true;
   uint16_t type = bypReadInt16(bp);
   switch(type)
   {
      case CLMSG_TYPE_EXIT: clmsgReadExit(bp, &msg->exit); break;
      case CLMSG_TYPE_HOSTINFO: clmsgReadHostInfo(bp, &msg->hostInfo); break;
      case CLMSG_TYPE_CONNECTIONSTATE: clmsgReadConnectionState(bp, &msg->connectionState); break;
      case CLMSG_TYPE_AUTHREQUEST: clmsgReadAuthRequest(bp, &msg->authRequest); break;
      case CLMSG_TYPE_AUTHRESPONSE: clmsgReadAuthResponse(bp, &msg->authResponse); break;
      case CLMSG_TYPE_SYNC: clmsgReadSync(bp, &msg->sync); break;
      case CLMSG_TYPE_ENTITYCREATE: clmsgReadEntityCreate(bp, &msg->entityCreate); break;
      case CLMSG_TYPE_ENTITYDESTROY: clmsgReadEntityDestroy(bp, &msg->entityDestroy); break;
      case CLMSG_TYPE_ENTITYUPDATE: clmsgReadEntityUpdate(bp, &msg->entityUpdate); break;
      case CLMSG_TYPE_ENTITYPLAYERCONTROL: clmsgReadEntityPlayerControl(bp, &msg->entityPlayerControl); break;
      case CLMSG_TYPE_ENTITYCOMMANDFREE: clmsgReadEntityCommandFree(bp, &msg->entityCommandFree); break;
      case CLMSG_TYPE_ENTITYCOMMANDSTOP: clmsgReadEntityCommandStop(bp, &msg->entityCommandStop); break;
      case CLMSG_TYPE_ENTITYCOMMANDVELOCITY: clmsgReadEntityCommandVelocity(bp, &msg->entityCommandVelocity); break;
      case CLMSG_TYPE_ENTITYCOMMANDPOSITION: clmsgReadEntityCommandPosition(bp, &msg->entityCommandPosition); break;
      case CLMSG_TYPE_ENTITYPLAYERCOMMANDFREE: clmsgReadEntityPlayerCommandFree(bp, &msg->entityPlayerCommandFree); break;
      case CLMSG_TYPE_ENTITYPLAYERCOMMANDSTOP: clmsgReadEntityPlayerCommandStop(bp, &msg->entityPlayerCommandStop); break;
      case CLMSG_TYPE_ENTITYPLAYERCOMMANDVELOCITY: clmsgReadEntityPlayerCommandVelocity(bp, &msg->entityPlayerCommandVelocity); break;
      case CLMSG_TYPE_ENTITYPLAYERCOMMANDPOSITION: clmsgReadEntityPlayerCommandPosition(bp, &msg->entityPlayerCommandPosition); break;
      default:
         success = false;
         fprintf(stderr, "%s: %d: error: ClientMessage Type %u is not a valid message type\n", __FILE__, __LINE__, type);
         break;
   }

   if(success && bypBytesDropped(bp))
   {
      success = false;
      fprintf(stderr, "%s: %d: error: ClientMessage Type %u is larger than provided Buffer: %lu\n", __FILE__, __LINE__, type, bp->size);
   }
   return success;
}

static inline
void clmsgDump(const union clientMessage * msg)
{
   switch(msg->type)
   {
      case CLMSG_TYPE_EXIT: clmsgDumpExit(&msg->exit); break;
      case CLMSG_TYPE_HOSTINFO: clmsgDumpHostInfo(&msg->hostInfo); break;
      case CLMSG_TYPE_CONNECTIONSTATE: clmsgDumpConnectionState(&msg->connectionState); break;
      case CLMSG_TYPE_AUTHREQUEST: clmsgDumpAuthRequest(&msg->authRequest); break;
      case CLMSG_TYPE_AUTHRESPONSE: clmsgDumpAuthResponse(&msg->authResponse); break;
      case CLMSG_TYPE_SYNC: clmsgDumpSync(&msg->sync); break;
      case CLMSG_TYPE_ENTITYCREATE: clmsgDumpEntityCreate(&msg->entityCreate); break;
      case CLMSG_TYPE_ENTITYDESTROY: clmsgDumpEntityDestroy(&msg->entityDestroy); break;
      case CLMSG_TYPE_ENTITYUPDATE: clmsgDumpEntityUpdate(&msg->entityUpdate); break;
      case CLMSG_TYPE_ENTITYPLAYERCONTROL: clmsgDumpEntityPlayerControl(&msg->entityPlayerControl); break;
      case CLMSG_TYPE_ENTITYCOMMANDFREE: clmsgDumpEntityCommandFree(&msg->entityCommandFree); break;
      case CLMSG_TYPE_ENTITYCOMMANDSTOP: clmsgDumpEntityCommandStop(&msg->entityCommandStop); break;
      case CLMSG_TYPE_ENTITYCOMMANDVELOCITY: clmsgDumpEntityCommandVelocity(&msg->entityCommandVelocity); break;
      case CLMSG_TYPE_ENTITYCOMMANDPOSITION: clmsgDumpEntityCommandPosition(&msg->entityCommandPosition); break;
      case CLMSG_TYPE_ENTITYPLAYERCOMMANDFREE: clmsgDumpEntityPlayerCommandFree(&msg->entityPlayerCommandFree); break;
      case CLMSG_TYPE_ENTITYPLAYERCOMMANDSTOP: clmsgDumpEntityPlayerCommandStop(&msg->entityPlayerCommandStop); break;
      case CLMSG_TYPE_ENTITYPLAYERCOMMANDVELOCITY: clmsgDumpEntityPlayerCommandVelocity(&msg->entityPlayerCommandVelocity); break;
      case CLMSG_TYPE_ENTITYPLAYERCOMMANDPOSITION: clmsgDumpEntityPlayerCommandPosition(&msg->entityPlayerCommandPosition); break;
      default:
         fprintf(stderr, "%s: %d: warning: ClientMessage Type %u is not a valid message type\n", __FILE__, __LINE__, msg->type);
         break;
   }
}

#endif // __CLIENTMESSAGE_H__
