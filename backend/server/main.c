// Server Main
#include <stdio.h>
#include <stdlib.h>

#include <common/TCPNetwork.h>
#include <common/MessageLayer.h>
#include <common/SignalInterrupt.h>
#include <common/TimeAndSleep.h>
#include <common/Config.h>
#include <common/List.h>
#include <common/ServerMessage.h>
#include <common/FlightSimulation.h>

// Ship Configurations (This should be a temperary way to set these)
#define SHIP_ALPHA_DYNAMICS_MAXSPEED         10
#define SHIP_ALPHA_DYNAMICS_MAXACCELERATION  1
#define SHIP_ALPHA_DYNAMICS_STOPDISTANCE     60


struct simEntity;
struct simulation;
struct netClient;
struct networking;

LIST_CREATE_INLINE_FUNCTIONS(netClientPtr, struct netClient *)
LIST_CREATE_INLINE_FUNCTIONS(simEntityPtr, struct simEntity *)

struct networking;

struct simulation
{
   struct list entityList;
   uint32_t nextSimId;
   struct networking * networking;
};


struct simEntity
{
   uint32_t simId;
   struct fsimEntity fsim;
   struct fsimDynamics dynamics;
   union fsimCommand activeCommand;
   union fsimCommand requestedCommand;
   bool changeFlagCommand;
   bool changeFlagUpdate;
};

struct simEntity * SimulationCreateNewEntity(struct simulation * sim);

void SimEntitySetPositionF(struct simEntity * ent, float x, float y, float z);
void SimEntitySetVelocityF(struct simEntity * ent, float x, float y, float z);
void SimEntitySetAngularVelocityF(struct simEntity * ent, float x, float y, float z);

static inline 
void SimulationEntityCreateCommandMessage(union serverMessage * sMsg, 
                                          uint32_t simId,
                                          const union fsimCommand * command);
static inline
void SimulationEntityCreateUpdateMessage(union serverMessage * sMsg,
                                         uint32_t simId,
                                         const struct fsimEntity * ent);

// ============================= Networking ================================= //

enum clientState
{
   eCS_New,
   eCS_VerifiedClient,
   eCS_Authenticated
};

struct netClient
{
   struct networking * networking;
   struct tcpNetworkSocket * socket;
   struct messageLayer * msgLayer;
   uint8_t lastClientCmdIdProccessed;
   bool toDelete;
   enum clientState state;
   unsigned int stepsSinceLastMessage;
   struct simEntity * playerControledEntity;
   bool playerControlActive;
};

struct networking
{
   struct tcpNetwork * net;
   struct tcpNetworkServer * server;
   struct list clientList;
   struct bitpackerbuffer sendBuffer;
   struct stringBuffer * stringBuffer;
   struct simulation * simulation;
};

static inline void netClientDestroy(struct netClient * client)
{
   msglDestory(client->msgLayer);
   tcpnSocketDestroy(client->socket);
   free(client);
}

void QueueServerMessageToSend(struct bitpackerbuffer * bpb, struct messageLayer * layer, const union serverMessage * msg)
{
   struct bitpacker bp;
   while(bpbNeedsWrite(bpb, &bp))
   {
      servmsgWrite(&bp, msg, false);
   }
   msglQueueToSend(layer, bp.buffer, bpGetCurrentUsageInBytes(&bp));
   printf("\nSent Message:\n");
   servmsgDump(msg);
}

static inline void NetworkingSendToAllClients(struct networking * networking, const union serverMessage * msg, enum clientState atOrAbove)
{
   size_t i, count;
   struct netClient ** clients;
   clients = listGetArray_netClientPtr(&networking->clientList, &count);
   for(i = 0; i < count; i++)
   {
      if(clients[i]->state >= atOrAbove)
      {
         QueueServerMessageToSend(&networking->sendBuffer, clients[i]->msgLayer, msg);
      }
   }
}

static inline void ClientSendMessage(struct netClient * client, const union serverMessage * msg)
{
   QueueServerMessageToSend(&client->networking->sendBuffer, client->msgLayer, msg);
}

static inline
void NetworkingCreateEntityCreateMessage(union serverMessage * sMsg, uint32_t simId,
                                         const struct fsimDynamics * dynamics)
{
   sMsg->type = SERVMSG_TYPE_ENTITYCREATE;
   sMsg->entityCreate.simId           = simId;
   sMsg->entityCreate.maxSpeed        = dynamics->maxSpeed.value;
   sMsg->entityCreate.maxAcceleration = dynamics->maxAcceleration.value;
   sMsg->entityCreate.stopDistance    = dynamics->stopDistance.value;
}

static inline
void NetworkingNotifyNewEntity(struct networking * net, struct simEntity * ent)
{
   union serverMessage createMsg;
   size_t count, i;
   struct netClient ** clients;

   NetworkingCreateEntityCreateMessage(&createMsg, ent->simId, &ent->dynamics);

   clients = listGetArray(&net->clientList, &count);
   for(i = 0; i < count; i++)
   {
      ClientSendMessage(clients[i], &createMsg);
   }
}

static inline
void CreatePlayerShip(struct netClient * client)
{
   union serverMessage msg;
   struct simulation * sim = client->networking->simulation;
   struct simEntity * ent = SimulationCreateNewEntity(sim);
   i32fpSetDS(&ent->dynamics.maxSpeed,        SHIP_ALPHA_DYNAMICS_MAXSPEED,        CONFIG_SHIFT_VELOCITY);
   i32fpSetDS(&ent->dynamics.maxAcceleration, SHIP_ALPHA_DYNAMICS_MAXACCELERATION, CONFIG_SHIFT_ACCELERATION);
   i32fpSetDS(&ent->dynamics.stopDistance,    SHIP_ALPHA_DYNAMICS_STOPDISTANCE,    CONFIG_SHIFT_POSITION);

   NetworkingNotifyNewEntity(client->networking, ent);

   client->playerControledEntity = ent;
   msg.type = SERVMSG_TYPE_ENTITYPLAYERCONTROL;
   msg.entityPlayerControl.simId  = ent->simId;
   msg.entityPlayerControl.active = true;
   ClientSendMessage(client, &msg);
   
   SimEntitySetPositionF(ent,        0, 0, 0);
   SimEntitySetVelocityF(ent,        0, 0, 0);
   SimEntitySetAngularVelocityF(ent, 0, 0, 0);
   
   // Set Rotation
   i32QuatSetIdentity(&ent->fsim.rotation, CONFIG_SHIFT_ANGLE);
}

static inline
void CreateExistingEntities(struct netClient * client, struct simulation * simulation)
{
   size_t i, count;
   struct simEntity ** entities;

   entities = listGetArray(&simulation->entityList, &count);
   for(i = 0; i < count; i++)
   {
      union serverMessage sMsg;
      struct simEntity * entity = entities[i];
      
      NetworkingCreateEntityCreateMessage(&sMsg, entity->simId, &entity->dynamics);
      ClientSendMessage(client, &sMsg);  

      SimulationEntityCreateUpdateMessage(&sMsg, entity->simId, &entity->fsim);
      ClientSendMessage(client, &sMsg);  

      SimulationEntityCreateCommandMessage(&sMsg, entity->simId, &entity->activeCommand);
      ClientSendMessage(client, &sMsg);  
   }
}

static inline
void HandlePlayerCommandClientMessage(struct netClient * client, union serverMessage * msg)
{
   union serverMessage sMsg;

   if(client->playerControledEntity == NULL)
   {
      printf("Dropping client because they tried to isssue a command with out a controlled entity\n");
      client->toDelete = true;         
   }
   else
   {
      struct simEntity * ent = client->playerControledEntity;
      struct i32Vector3 velocity, position;
      switch(msg->type)
      {
      case SERVMSG_TYPE_ENTITYPLAYERCOMMANDFREE:
         client->lastClientCmdIdProccessed = msg->entityPlayerCommandFree.id;
         fsimCommandFree(&ent->requestedCommand);
         break;
      case SERVMSG_TYPE_ENTITYPLAYERCOMMANDSTOP:
         client->lastClientCmdIdProccessed = msg->entityPlayerCommandStop.id;
         fsimCommandStop(&ent->requestedCommand);
         break;
      case SERVMSG_TYPE_ENTITYPLAYERCOMMANDVELOCITY:
         client->lastClientCmdIdProccessed = msg->entityPlayerCommandVelocity.id;
         i32v3SetI(&velocity, msg->entityPlayerCommandVelocity.targetVelocity.x, 
                              msg->entityPlayerCommandVelocity.targetVelocity.y, 
                              msg->entityPlayerCommandVelocity.targetVelocity.z,
                              CONFIG_SHIFT_VELOCITY);
         fsimCommandVelocity(&ent->requestedCommand, &velocity);
         break;
      case SERVMSG_TYPE_ENTITYPLAYERCOMMANDPOSITION:
         client->lastClientCmdIdProccessed = msg->entityPlayerCommandPosition.id;
         i32v3SetI(&position, msg->entityPlayerCommandPosition.targetPosition.x, 
                              msg->entityPlayerCommandPosition.targetPosition.y, 
                              msg->entityPlayerCommandPosition.targetPosition.z,
                              CONFIG_SHIFT_POSITION);
         fsimCommandPosition(&ent->requestedCommand, &position);
         break;
      default:
         fprintf(stderr, "%s: %d: error: Unhandled Player Command: %u\n", __FILE__, __LINE__, msg->type);         
         break;
      }
   }
}

static inline void HandleClientMessage(struct netClient * client, union serverMessage * msg)
{
   union serverMessage sMsg;
   switch(msg->type)
   {
   case SERVMSG_TYPE_GREETING:
      if(client->state == eCS_New)
      {
         client->state = eCS_VerifiedClient;
         // Send back Greetings
         sMsg.type = SERVMSG_TYPE_GREETING;
         ClientSendMessage(client, &sMsg);
      }
      else
      {
         printf("Dropping client because they greeted more than once\n");
         client->toDelete = true;
      }
      break;
   case SERVMSG_TYPE_AUTHREQUEST:
      sMsg.type = SERVMSG_TYPE_AUTHRESPONSE;
      sMsg.authResponse.requestType = msg->authRequest.requestType;
         
      switch(msg->authRequest.requestType)
      {
      case SERVMSG_AUTHREQUESTTYPE_LOGIN:
         if(client->state == eCS_VerifiedClient)
         {
            printf("Auth Request Attempt: userName: %s, userPassword: %s\n", msg->authRequest.userName, msg->authRequest.userPassword);
            // TODO: Actualy check user name and password :D
            client->state = eCS_Authenticated;
            sMsg.authResponse.success = true;
            ClientSendMessage(client, &sMsg);
            
            CreateExistingEntities(client, client->networking->simulation);
            CreatePlayerShip(client);
         }
         else
         {
            printf("Dropping client due to login attempt when not verified\n");
            client->toDelete = true;
         }
         break;
      case SERVMSG_AUTHREQUESTTYPE_LOGOUT:
         if(client->state == eCS_Authenticated)
         {
            sMsg.authResponse.success = true;
            client->state = eCS_VerifiedClient;
         }
         else
         {
            sMsg.authResponse.success = false;
         }
         ClientSendMessage(client, &sMsg);
         break;
      default:
         printf("Dropping client because of unknown Auth Request Type: %d\n", msg->authRequest.requestType);
         client->toDelete = true;
         break;
      }
      break;
   case SERVMSG_TYPE_ENTITYPLAYERCOMMANDFREE:
   case SERVMSG_TYPE_ENTITYPLAYERCOMMANDSTOP:
   case SERVMSG_TYPE_ENTITYPLAYERCOMMANDVELOCITY:
   case SERVMSG_TYPE_ENTITYPLAYERCOMMANDPOSITION:
      HandlePlayerCommandClientMessage(client, msg);
      break;
   // Messages to ignore
   case SERVMSG_TYPE_KEEPALIVE:
      // Intentionally left blank
      break;
   default:
      fprintf(stderr, "%s: %d: warning: Unhandled Client Message: %d\n", __FILE__, __LINE__, msg->type);
      break;
   }
}

static void ClientMessageReceived(void * userData, 
                                  struct messageLayer * layer, 
                                  struct tcpNetworkSocket * socket, 
                                  void * message, uint16_t messageSize)
{
   struct netClient * client = (struct netClient *)userData;
   struct bitpacker bp;
   union serverMessage serverMessage;
   bpInit(&bp, message, messageSize);
   if(servmsgRead(&bp, client->networking->stringBuffer, &serverMessage))
   {
      client->stepsSinceLastMessage = 0;
      printf("Received Message From Client:\n");
      servmsgDump(&serverMessage);
      HandleClientMessage(client, &serverMessage);
   }
   strbClear(client->networking->stringBuffer);
   
}

void NetworkingSetup(struct networking * networking, const char * service)
{
   networking->net = tcpnCreateNativeNetwork();
   networking->stringBuffer = strbCreate(0);
   bpbInit(&networking->sendBuffer, 0, 0);
   networking->server = tcpnCreateServer(networking->net, NULL, service);
   listInitDefaults_netClientPtr(&networking->clientList);
   
}

void NetworkingTeardown(struct networking * networking)
{
   size_t count, i;
   struct netClient ** clients;
   clients = listGetArray(&networking->clientList, &count);
   for(i = 0; i < count; i++)
   {
      netClientDestroy(clients[i]);
   }
   
   listFree(&networking->clientList);
   tcpnServerDestroy(networking->server);

   strbDestroy(networking->stringBuffer);
   bpbFree(&networking->sendBuffer);
   tcpnDestroy(networking->net);
}

void NetworkingAccept(struct networking * networking)
{

   struct tcpNetworkSocket * socket;

   socket = tcpnServerAccept(networking->server);
   if(socket != NULL)
   {
      printf("Client Connected\n");
      struct netClient * client = malloc(sizeof(struct netClient));

      listAddValue_netClientPtr(&networking->clientList, client, NULL);
      client->socket = socket;
      client->lastClientCmdIdProccessed = 0xFF;
      client->msgLayer = msglCreate(socket, client, ClientMessageReceived);
      client->toDelete = false;
      client->state = eCS_New;
      client->networking = networking;
      client->stepsSinceLastMessage = 0;
      client->playerControledEntity = NULL;
      client->playerControlActive = false;
   }

}

void NetworkReceive(struct networking * networking)
{
   size_t count, i;
   struct netClient ** clients;
   clients = listGetArray(&networking->clientList, &count);
   for(i = 0; i < count; i++)
   {
      clients[i]->stepsSinceLastMessage ++;
      msglReceive(clients[i]->msgLayer);
      
      if(clients[i]->stepsSinceLastMessage > 10)
      {
         printf("Client Timed out, dropping it\n");
         clients[i]->toDelete = true;
      }
   }
}

void NetworkAddSyncMessage(struct networking * networking, uint8_t simulationStep, unsigned int nextTimeStep_ms)
{
   size_t count, i;
   struct netClient ** clients;
   clients = listGetArray(&networking->clientList, &count);
   for(i = 0; i < count; i++)
   {
      union serverMessage msg;

      msg.type = SERVMSG_TYPE_SYNC;
      msg.sync.step = simulationStep;
      msg.sync.lastClientCmdIdProccessed = clients[i]->lastClientCmdIdProccessed;
      msg.sync.nextStepTime_ms = (uint16_t)nextTimeStep_ms;
      ClientSendMessage(clients[i], &msg);
   }
}

void NetworkSend(struct networking * networking)
{
   size_t count, i;
   struct netClient ** clients;
   clients = listGetArray(&networking->clientList, &count);
   for(i = 0; i < count; i++)
   {
      msglSend(clients[i]->msgLayer);
   }
}

void NetworkCheckAndRemoveClients(struct networking * networking)
{
   size_t count, i;
   struct netClient ** clients;
   
   clients = listGetArray(&networking->clientList, &count);

   // Check for closed sockets
   for(i = 0; i < count; i++)
   {
      if(!clients[i]->toDelete && !tcpnSocketIsOpen(clients[i]->socket))
      {
         printf("Client socket closed, marking for removal\n");
         clients[i]->toDelete = true;
      }
   }
   
   // Remove and delete
   for(i = count - 1; i < count; i--)
   {
      if(clients[i]->toDelete)
      {
         netClientDestroy(clients[i]);
         listRemoveFast(&networking->clientList, i);
      }
   }

}

// ============================= Simulation ================================= //


void SimulationSetup(struct simulation * sim)
{
   listInitDefaults_simEntityPtr(&sim->entityList);
   sim->nextSimId = 1;
}


void SimulationTeardown(struct simulation * sim)
{
   size_t i, count;
   struct simEntity ** entities;

   entities = listGetArray(&sim->entityList, &count);
   for(i = 0; i < count; i++)
   {
      struct simEntity * entity = entities[i];
      free(entity);
   }

   listFree(&sim->entityList);
}

struct simEntity * SimulationCreateNewEntity(struct simulation * sim)
{
   struct simEntity * ent;
   ent = malloc(sizeof(struct simEntity));
   listAddValue_simEntityPtr(&sim->entityList, ent, NULL);
   ent->simId = sim->nextSimId;
   fsimCommandFree(&ent->activeCommand);
   fsimCommandFree(&ent->requestedCommand);
   ent->changeFlagCommand = true;
   ent->changeFlagUpdate = true;
   sim->nextSimId ++;
   return ent;
}

void SimEntitySetPositionF(struct simEntity * ent, float x, float y, float z)
{
   ent->changeFlagUpdate = true;
   i32v3SetF(&ent->fsim.position, x, y, z, CONFIG_SHIFT_POSITION);
}

void SimEntitySetVelocityF(struct simEntity * ent, float x, float y, float z)
{
   ent->changeFlagUpdate = true;
   i32v3SetF(&ent->fsim.velocity, x, y, z, CONFIG_SHIFT_VELOCITY);
}

void SimEntitySetAngularVelocityF(struct simEntity * ent, float x, float y, float z)
{
   ent->changeFlagUpdate = true;
   i32v3SetF(&ent->fsim.rotationalVelocity, x, y, z, CONFIG_SHIFT_ANGULAR_VELOCITY);
}

void SimulationUpdate(struct simulation * sim)
{
   size_t i, count;
   struct simEntity ** entities;

   entities = listGetArray(&sim->entityList, &count);
   for(i = 0; i < count; i++)
   {
      struct simEntity * entity = entities[i];
      fsimUpdate(&entity->fsim, 
                 &entity->dynamics,
                 &entity->activeCommand, 
                 CONFIG_SIMULATION_TIME_PER_STEP_MS);
   }
}


static inline 
void SimulationEntityCreateCommandMessage(union serverMessage * sMsg, 
                                          uint32_t simId,
                                          const union fsimCommand * command)
{
   switch(command->type)
   {
   case eFSCT_free:
      sMsg->type = SERVMSG_TYPE_ENTITYCOMMANDFREE;
      sMsg->entityCommandFree.simId = simId;
      break;
   case eFSCT_stop:
      sMsg->type = SERVMSG_TYPE_ENTITYCOMMANDSTOP;
      sMsg->entityCommandStop.simId = simId;
      break;
   case eFSCT_velocity:
      sMsg->type = SERVMSG_TYPE_ENTITYCOMMANDVELOCITY;
      sMsg->entityCommandVelocity.simId = simId;
      sMsg->entityCommandVelocity.targetVelocity.x = command->velocity.velocity.x;
      sMsg->entityCommandVelocity.targetVelocity.y = command->velocity.velocity.y;
      sMsg->entityCommandVelocity.targetVelocity.z = command->velocity.velocity.z;
      break;
   case eFSCT_position:
      sMsg->type = SERVMSG_TYPE_ENTITYCOMMANDPOSITION;
      sMsg->entityCommandPosition.simId = simId;
      sMsg->entityCommandPosition.targetPosition.x = command->position.position.x;
      sMsg->entityCommandPosition.targetPosition.y = command->position.position.y;
      sMsg->entityCommandPosition.targetPosition.z = command->position.position.z;
      break;
   default:
      fprintf(stderr, "%s: %d: error: Unhandled command type: %u\n", __FILE__, __LINE__, (unsigned int)command->type);
      break;
   }
}

static inline
void SimulationEntityCreateUpdateMessage(union serverMessage * sMsg,
                                         uint32_t simId,
                                         const struct fsimEntity * ent)
{
   sMsg->type = SERVMSG_TYPE_ENTITYUPDATE;
   sMsg->entityUpdate.simId                = simId;
   sMsg->entityUpdate.position.x           = ent->position.x;
   sMsg->entityUpdate.position.y           = ent->position.y;
   sMsg->entityUpdate.position.z           = ent->position.z;
   sMsg->entityUpdate.velocity.x           = ent->velocity.x;
   sMsg->entityUpdate.velocity.y           = ent->velocity.y;
   sMsg->entityUpdate.velocity.z           = ent->velocity.z;
   sMsg->entityUpdate.rotation.w           = ent->rotation.w;
   sMsg->entityUpdate.rotation.x           = ent->rotation.x;
   sMsg->entityUpdate.rotation.y           = ent->rotation.y;
   sMsg->entityUpdate.rotation.z           = ent->rotation.z;
   sMsg->entityUpdate.rotationalVelocity.x = ent->rotationalVelocity.x;
   sMsg->entityUpdate.rotationalVelocity.y = ent->rotationalVelocity.y;
   sMsg->entityUpdate.rotationalVelocity.z = ent->rotationalVelocity.z;
}

void SimulationEntityApplyPlayerInputs(struct simulation * simulation, 
                                       struct networking * networking)
{
   size_t i, count;
   struct simEntity ** entities;

   // Check For a diffrence
   entities = listGetArray(&simulation->entityList, &count);
   for(i = 0; i < count; i++)
   {
      struct simEntity * entity = entities[i];
      if(!fsimCommandEqual(&entity->activeCommand, &entity->requestedCommand))
      {
         fsimCommandSet(&entity->activeCommand, &entity->requestedCommand);
         entity->changeFlagCommand = true;
      }
   }

   // Send a message if nessary
   entities = listGetArray(&simulation->entityList, &count);
   for(i = 0; i < count; i++)
   {
      struct simEntity * entity = entities[i];
      if(entity->changeFlagCommand)
      {
         union serverMessage sMsg;
         SimulationEntityCreateCommandMessage(&sMsg, entity->simId, 
                                              &entity->activeCommand);
         NetworkingSendToAllClients(networking, &sMsg, eCS_Authenticated);
         entity->changeFlagCommand = false;
      }
   }
}

void SimulationEntityApplyUpdates(struct simulation * simulation, 
                                  struct networking * networking)
{
   size_t i, count;
   struct simEntity ** entities;

   // Check For a diffrence
   entities = listGetArray(&simulation->entityList, &count);
   for(i = 0; i < count; i++)
   {
      struct simEntity * entity = entities[i];
      if(entity->changeFlagUpdate)
      {
         union serverMessage sMsg;
         SimulationEntityCreateUpdateMessage(&sMsg, entity->simId,
                                             &entity->fsim);
         NetworkingSendToAllClients(networking, &sMsg, eCS_Authenticated);
         entity->changeFlagUpdate = false;
      }
   }
}




// ================================ main ==================================== //

static inline
void CreateOneAIShip(struct simulation * simulation, float x, float y, float z)
{
   struct simEntity * ent;

   ent = SimulationCreateNewEntity(simulation);
   i32fpSetDS(&ent->dynamics.maxSpeed,        SHIP_ALPHA_DYNAMICS_MAXSPEED,        CONFIG_SHIFT_VELOCITY);
   i32fpSetDS(&ent->dynamics.maxAcceleration, SHIP_ALPHA_DYNAMICS_MAXACCELERATION, CONFIG_SHIFT_ACCELERATION);
   i32fpSetDS(&ent->dynamics.stopDistance,    SHIP_ALPHA_DYNAMICS_STOPDISTANCE,    CONFIG_SHIFT_POSITION);

   SimEntitySetPositionF(ent,        x, y, z);
   SimEntitySetVelocityF(ent,        0, 0, 0);
   SimEntitySetAngularVelocityF(ent, 0, 0, 0);
   
   // Set Rotation
   i32QuatSetAxisAngleF(&ent->fsim.rotation, 0, 1, 0, 3.14 / 2, CONFIG_SHIFT_ANGLE);
}

static
void CreateAIShips(struct simulation * simulation)
{
   CreateOneAIShip(simulation,  10,  10,  10);
   CreateOneAIShip(simulation, -10,  10,  10);
   CreateOneAIShip(simulation,  10, -10,  10);
   CreateOneAIShip(simulation, -10, -10,  10);
   CreateOneAIShip(simulation,  10,  10, -10);
   CreateOneAIShip(simulation, -10,  10, -10);
   CreateOneAIShip(simulation,  10, -10, -10);
   CreateOneAIShip(simulation, -10, -10, -10);

}

int main(int argc, char * args[])
{
   unsigned int limiterTimer_ms;
   struct networking networking;
   struct simulation simulation;
   
   uint8_t simulationStep = 0;
   unsigned int nextTimeStep_ms = CONFIG_SIMULATION_TIME_PER_STEP_MS;
   printf("Setting Up Server...\n");
   
   SimulationSetup(&simulation);
   CreateAIShips(&simulation);
   NetworkingSetup(&networking, CONFIG_DEFAULT_PORT);
   simulation.networking = &networking;
   networking.simulation = &simulation;
   
   printf("Server Entering Main Loop\n");
   siSetup(NULL);
   limiterTimer_ms = tasGetMilliseconds();
   while(siNotInterrupted())
   {
      NetworkReceive(&networking);
      SimulationEntityApplyPlayerInputs(&simulation, &networking);
      SimulationEntityApplyUpdates(&simulation, &networking);

      SimulationUpdate(&simulation);
      
      NetworkAddSyncMessage(&networking, simulationStep, nextTimeStep_ms);
      NetworkSend(&networking);
      NetworkCheckAndRemoveClients(&networking);
      NetworkingAccept(&networking);   
      tasSleepUntil(nextTimeStep_ms, &limiterTimer_ms);
      simulationStep ++;
   }
   printf("\nServer Exiting Main Loop\n");
   
   SimulationTeardown(&simulation);
   NetworkingTeardown(&networking);

   printf("End of Server\n");
   return 0;
}

